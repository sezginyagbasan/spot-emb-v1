/** @file link_scheduler.h
 *  @brief Scheduler module.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef LINK_SCHEDULER_H_
#define LINK_SCHEDULER_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "wps_def.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
/** @brief Timeslot instance.
 */
typedef struct timeslot {
    wps_connection_t* connection_main;       /**< Main connection instance. */
    wps_connection_t* connection_auto_reply; /**< Auto-reply connection instance. */
    uint32_t          duration_pll_cycles;   /**< Timeslot duration, in PLL cycles. */
} timeslot_t;

/** @brief Schedule instance.
 */
typedef struct schedule {
    timeslot_t *timeslot;         /**< Array containing every schedule timeslot. */
    uint32_t    size;             /**< Number of timeslot for the schedule. */
    timeslot_t *syncing_timeslot; /**< Timeslot used for fast sync */
} schedule_t;

/** @brief Time slot type enumeration.
 */
typedef struct time_slot_cfg {
    uint32_t   duration_us; /**< Time slot duration in us */
    timeslot_t time_slot;   /**< Time slot */
} time_slot_cfg_t;

typedef struct scheduler {
    schedule_t  *schedule;              /**< The schedule */
    uint16_t     current_time_slot_num; /**< Current time slot number */
    uint16_t     total_time_slot_count; /**< The total amount of time slots configured */
    uint32_t     sleep_cycles;          /**< Sleep time in PLL cycles */
    uint16_t     local_addr;            /**< Local Address. */
    bool         tx_disabled;           /**< TX disabled flag. */
} scheduler_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize scheduler object.
 *
 *  @param[in] scheduler            Scheduler object.
 *  @param[in] schedule             Schedule.
 *  @param[in] local_addr           Local address.
 *  @return None.
 */
void link_scheduler_init(scheduler_t *scheduler,
                         schedule_t  *schedule,
                         uint16_t     local_addr);

/** @brief Reset scheduler object.
 *
 *  @param[in] scheduler  Scheduler object.
 */
void link_scheduler_reset(scheduler_t *scheduler);

/** @brief Add a time slot to schedule.
 *
 *  @param[in] scheduler      Scheduler object.
 *  @param[in] time_slot_cfg  Time slot configuration.
 */
void link_scheduler_add_time_slot(scheduler_t     *scheduler,
                                  time_slot_cfg_t *time_slot_cfg);

/** @brief Add a time slot to schedule.
 *
 *  @param[in] scheduler  Scheduler object.
 *  @return The number of timeslot incremented in the schedule.
 */
uint8_t link_scheduler_increment_time_slot(scheduler_t *scheduler);

/** @brief Set current time slot index.
 *
 *  @param[in] scheduler    Scheduler object.
 *  @param[in] time_slot_i  Time slot index.
 */
void link_scheduler_set_time_slot_i(scheduler_t *scheduler,
                                    uint8_t      time_slot_i);

/** @brief Enable transmissions.
 *
 *  @return The current time slot.
 */
void link_scheduler_enable_tx(scheduler_t *scheduler);

/** @brief Disable transmissions.
 *
 *  @param[in]  scheduler  Scheduler object.
 */
void link_scheduler_disable_tx(scheduler_t *scheduler);

/** @brief Get the current time slot.
 *
 *  @param[in]  scheduler  Scheduler object.
 *  @return The current time slot.
 */
timeslot_t *link_scheduler_get_current_timeslot(scheduler_t *scheduler);

/** @brief Get the total number of time slots.
 *
 *  @param[in]  scheduler  Scheduler object.
 *  @return The total number of time slots.
 */
uint16_t link_scheduler_get_total_timeslot_count(scheduler_t *scheduler);

/** @brief Get the current time slot index.
 *
 *  @param[in]  scheduler  Scheduler object.
 *  @return The current time slot index.
 */
uint16_t link_scheduler_get_next_timeslot_index(scheduler_t *scheduler);

/** @brief Get sleep the amount of time to sleep in PLL cycles.
 *
 *  @param[in]   scheduler  Scheduler object.
 *  @return time to sleep in PLL cycles.
 */
uint32_t link_scheduler_get_sleep_time(scheduler_t *scheduler);

/** @brief Get the the time slot used for syncing.
 *
 *  @param[in]  scheduler  Scheduler object.
 *  @return The time slot used for syncing.
 */
timeslot_t *link_scheduler_get_timeslot_for_syncing(scheduler_t *scheduler);

#ifdef __cplusplus
}
#endif
#endif /* LINK_SCHEDULER_H_ */
