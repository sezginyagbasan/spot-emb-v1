/** @file link_lqi.h
 *  @brief LQI module.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef LINK_LQI_H_
#define LINK_LQI_H_

/* INCLUDES *******************************************************************/
#include <stdint.h>
#include "link_gain_loop.h"
#include "link_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
typedef enum lqi_mode {
    LQI_MODE_0, /**< Consider lost and rejected frames as having weakest RSSI possible and typical RNSI */
    LQI_MODE_1, /**< Don't consider rejected and lost frames in RSSI and RNSI calculation */
} lqi_mode_t;

typedef struct lqi {
    lqi_mode_t mode;                /**< LQI object mode */
    uint64_t   rssi_total_tenth_db; /**< RSSI in tenths of dB */
    uint64_t   rnsi_total_tenth_db; /**< RNSI in tenths of dB */
    uint32_t   sent_count;          /**< Sent frame count */
    uint32_t   ack_count;           /**< ACKed frame count */
    uint32_t   nack_count;          /**< NACKed frame count */
    uint32_t   received_count;      /**< Received frame count */
    uint32_t   rejected_count;      /**< Rejected frame count */
    uint32_t   lost_count;          /**< Lost frame count */
    uint32_t   total_count;         /**< Total frame count */
} lqi_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize LQI object.
 *
 *  @param[in] lqi  LQI object.
 *  @param[in] mode LQI mode.
 *  @return None.
 */
void link_lqi_init(lqi_t *lqi, lqi_mode_t mode);

/** @brief Update LQI.
 *
 *  @param[in] frame_outcome  Outcome of the frame.
 *  @param[in] rssi           Receiver signal strength indicator.
 *  @param[in] rnsi           Receiver noise strength indicator.
 *  @param[in] gain_index     Gain index.
 *  @param[in] lqi            LQI object.
 *  @return None.
 */
void link_lqi_update(lqi_t           *lqi,
                     gain_loop_t     *gain_loop,
                     frame_outcome_t  frame_outcome,
                     uint8_t          rssi,
                     uint8_t          rnsi);

/** @brief Reset LQI object.
 *
 *  @param[in] lqi  LQI object.
 *  @return None.
 */
void link_lqi_reset(lqi_t *lqi);

/** @brief Get sent frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return Sent frame count.
 */
uint32_t link_lqi_get_sent_count(lqi_t *lqi);

/** @brief Get acked frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return ACKed frame count.
 */
uint32_t link_lqi_get_ack_count(lqi_t *lqi);

/** @brief Get nacked frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return NACKed frame count.
 */
uint32_t link_lqi_get_nack_count(lqi_t *lqi);

/** @brief Get received frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return Received frame count.
 */
uint32_t link_lqi_get_received_count(lqi_t *lqi);

/** @brief Get rejected frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return Rejected frame count.
 */
uint32_t link_lqi_get_rejected_count(lqi_t *lqi);

/** @brief Get lost frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return Lost frame count.
 */
uint32_t link_lqi_get_lost_count(lqi_t *lqi);

/** @brief Get total frame count.
 *
 *  @param[in] lqi  LQI object.
 *  @return Total frame count.
 */
uint32_t link_lqi_get_total_count(lqi_t *lqi);

/** @brief Get RSSI average.
 *
 *  @param[in] lqi  LQI object.
 *  @return RSSI average.
 */
uint16_t link_lqi_get_avg_rssi_tenth_db(lqi_t *lqi);

/** @brief Get RNSI average.
 *
 *  @param[in] lqi  LQI object.
 *  @return RNSI average.
 */
uint16_t link_lqi_get_avg_rnsi_tenth_db(lqi_t *lqi);

#ifdef __cplusplus
}
#endif
#endif /* LINK_LQI_H_ */
