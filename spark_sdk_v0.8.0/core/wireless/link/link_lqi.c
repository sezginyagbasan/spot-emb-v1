/** @file link_lqi.c
 *  @brief LQI module.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <string.h>
#include "link_lqi.h"

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static inline void link_lqi_update_mode_0(lqi_t           *lqi,
                                          gain_loop_t     *gain_loop,
                                          frame_outcome_t  frame_outcome,
                                          uint8_t          rssi,
                                          uint8_t          rnsi);
static inline void link_lqi_update_mode_1(lqi_t           *lqi,
                                          gain_loop_t     *gain_loop,
                                          frame_outcome_t  frame_outcome,
                                          uint8_t          rssi,
                                          uint8_t          rnsi);

/* PUBLIC FUNCTIONS ***********************************************************/
void link_lqi_init(lqi_t *lqi, lqi_mode_t mode)
{
    memset(lqi, 0, sizeof(lqi_t));
    lqi->mode = mode;
}

void link_lqi_update(lqi_t *lqi, gain_loop_t *gain_loop, frame_outcome_t frame_outcome, uint8_t rssi, uint8_t rnsi)
{
    switch (lqi->mode) {
    case LQI_MODE_0:
        link_lqi_update_mode_0(lqi, gain_loop, frame_outcome, rssi, rnsi);
        break;
    case LQI_MODE_1:
        link_lqi_update_mode_1(lqi, gain_loop, frame_outcome, rssi, rnsi);
        break;
    default:
        link_lqi_update_mode_0(lqi, gain_loop, frame_outcome, rssi, rnsi);
    }

    /* Total count overflow */
    if (!lqi->total_count) {
        link_lqi_reset(lqi);
    }
}

void link_lqi_reset(lqi_t *lqi)
{
    memset(lqi, 0, sizeof(lqi_t));
}

uint32_t link_lqi_get_sent_count(lqi_t *lqi)
{
    return lqi->sent_count;
}

uint32_t link_lqi_get_ack_count(lqi_t *lqi)
{
    return lqi->ack_count;
}

uint32_t link_lqi_get_nack_count(lqi_t *lqi)
{
    return lqi->nack_count;
}

uint32_t link_lqi_get_received_count(lqi_t *lqi)
{
    return lqi->received_count;
}

uint32_t link_lqi_get_rejected_count(lqi_t *lqi)
{
    return lqi->rejected_count;
}

uint32_t link_lqi_get_lost_count(lqi_t *lqi)
{
    return lqi->lost_count;
}

uint32_t link_lqi_get_total_count(lqi_t *lqi)
{
    return lqi->total_count;
}

uint16_t link_lqi_get_avg_rssi_tenth_db(lqi_t *lqi)
{
    if (!lqi->total_count) {
        return 0;
    }
    switch (lqi->mode) {
            case LQI_MODE_0:
                return (lqi->rssi_total_tenth_db / lqi->total_count);
                break;
            case LQI_MODE_1:
                return (lqi->rssi_total_tenth_db / lqi->received_count);
                break;
            default:
                return (lqi->rssi_total_tenth_db / lqi->total_count);
    }
}

uint16_t link_lqi_get_avg_rnsi_tenth_db(lqi_t *lqi)
{
    if (!lqi->total_count) {
        return 0;
    }
    switch (lqi->mode) {
        case LQI_MODE_0:
            return (lqi->rnsi_total_tenth_db / lqi->total_count);
            break;
        case LQI_MODE_1:
            return (lqi->rnsi_total_tenth_db / lqi->received_count);
            break;
        default:
            return (lqi->rnsi_total_tenth_db / lqi->total_count);
    }
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Update LQI mode 0 (Consider lost and rejected frames as having weakest RSSI possible and typical RNSI).
 *
 *  @param[in] frame_outcome  Outcome of the frame.
 *  @param[in] rssi           Receiver signal strength indicator.
 *  @param[in] rnsi           Receiver noise strength indicator.
 *  @param[in] gain_index     Gain index.
 *  @param[in] lqi            LQI object.
 *  @return None.
 */
static inline void link_lqi_update_mode_0(lqi_t *lqi,
                                          gain_loop_t *gain_loop,
                                          frame_outcome_t frame_outcome,
                                          uint8_t rssi,
                                          uint8_t rnsi)
{
    lqi->total_count++;
    switch (frame_outcome) {
    case FRAME_RECEIVED:
        lqi->received_count++;
        lqi->rssi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rssi);
        lqi->rnsi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rnsi);
        break;
    case FRAME_REJECTED:
        lqi->rejected_count++;
        lqi->rssi_total_tenth_db += link_gain_loop_get_min_tenth_db(gain_loop);
        lqi->rnsi_total_tenth_db += link_gain_loop_get_rnsi_tenth_db(gain_loop);
        break;
    case FRAME_LOST:
        lqi->lost_count++;
        lqi->rssi_total_tenth_db += link_gain_loop_get_min_tenth_db(gain_loop);
        lqi->rnsi_total_tenth_db += link_gain_loop_get_rnsi_tenth_db(gain_loop);
        break;
    case FRAME_SENT_ACK:
        lqi->sent_count++;
        lqi->ack_count++;
        lqi->rssi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rssi);
        lqi->rnsi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rnsi);
        break;
    case FRAME_SENT_ACK_LOST:
    case FRAME_SENT_ACK_REJECTED:
        lqi->sent_count++;
        lqi->nack_count++;
        lqi->rssi_total_tenth_db += link_gain_loop_get_min_tenth_db(gain_loop);
        lqi->rnsi_total_tenth_db += link_gain_loop_get_rnsi_tenth_db(gain_loop);
        break;
    case FRAME_WAIT:
        lqi->sent_count++;
    default:
        break;
    }
}

/** @brief Update LQI mode 1 (Don't consider rejected and lost frames in RSSI and RNSI calculation).
 *
 *  @param[in] frame_outcome  Outcome of the frame.
 *  @param[in] rssi           Receiver signal strength indicator.
 *  @param[in] rnsi           Receiver noise strength indicator.
 *  @param[in] gain_index     Gain index.
 *  @param[in] lqi            LQI object.
 *  @return None.
 */
static inline void link_lqi_update_mode_1(lqi_t *lqi,
                                          gain_loop_t *gain_loop,
                                          frame_outcome_t frame_outcome,
                                          uint8_t rssi,
                                          uint8_t rnsi)
{
    lqi->total_count++;
    switch (frame_outcome) {
    case FRAME_RECEIVED:
        lqi->received_count++;
        lqi->rssi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rssi);
        lqi->rnsi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rnsi);
        break;
    case FRAME_REJECTED:
        lqi->rejected_count++;
        break;
    case FRAME_LOST:
        lqi->lost_count++;
        break;
    case FRAME_SENT_ACK:
        lqi->sent_count++;
        lqi->ack_count++;
        lqi->rssi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rssi);
        lqi->rnsi_total_tenth_db += calculate_normalized_gain(link_gain_loop_get_min_tenth_db(gain_loop), rnsi);
        break;
    case FRAME_SENT_ACK_LOST:
    case FRAME_SENT_ACK_REJECTED:
        lqi->sent_count++;
        lqi->nack_count++;
        break;
    case FRAME_WAIT:
        lqi->sent_count++;
    default:
        break;
    }
}
