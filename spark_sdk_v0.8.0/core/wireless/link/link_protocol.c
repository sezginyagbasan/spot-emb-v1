/** @file link_protocol.c
 *  @brief WPS layer 2 internal connection protocol.
 *
 *  This file is a wrapper use to send/received payload
 *  through the WPS L2 internal connection. Its used to
 *  properly generate a complete packet regrouping one
 *  or multiple information.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
/* INCLUDES *******************************************************************/
#include "link_protocol.h"

/* PUBLIC FUNCTIONS ***********************************************************/
void link_protocol_init(link_protocol_t *link_protocol, link_protocol_init_cfg_t *link_protocol_cfg, uwb_err *err)
{
    *err = LINK_PROTO_NO_ERROR;
    link_protocol->buffer = link_protocol_cfg->buffer;
    link_protocol->current_buffer_pointer = link_protocol_cfg->buffer;
    link_protocol->max_buffer_size = link_protocol_cfg->buffer_size;
    link_protocol->current_number_of_protocol = 0;

    if (link_protocol->buffer == NULL) {
        *err |= LINK_PROTO_NO_BUFFER;
    }
}

void link_protocol_add(link_protocol_t *link_protocol, link_protocol_cfg_t *protocol_cfg, uwb_err *err)
{
    *err = LINK_PROTO_NO_ERROR;
    if (link_protocol->current_number_of_protocol < MAX_NUMBER_OF_PROTOCOL) {
        if (link_protocol->current_buffer_pointer < (link_protocol->buffer + link_protocol->max_buffer_size)) {
            link_protocol->current_number_of_protocol++;
            link_protocol->protocol_info[link_protocol->current_number_of_protocol - 1].buffer   = link_protocol->current_buffer_pointer;
            link_protocol->protocol_info[link_protocol->current_number_of_protocol - 1].instance = protocol_cfg->instance;
            link_protocol->protocol_info[link_protocol->current_number_of_protocol - 1].size     = protocol_cfg->size;
            link_protocol->protocol_info[link_protocol->current_number_of_protocol - 1].send     = protocol_cfg->send;
            link_protocol->protocol_info[link_protocol->current_number_of_protocol - 1].receive  = protocol_cfg->receive;
            link_protocol->current_buffer_pointer += protocol_cfg->size;

        } else {
            *err = LINK_PROTO_NO_MORE_SPACE;
        }
    } else {
        *err = LINK_PROTO_TOO_MANY_PROTO;
    }
}

void link_protocol_send_buffer(void *link_protocol, uint8_t **buffer_to_send, uint32_t *size)
{
    link_protocol_t *link_proto = (link_protocol_t *)link_protocol;
    uint32_t size_to_send       = 0;

    for (uint8_t i = 0; i < link_proto->current_number_of_protocol; i++) {
        link_proto->protocol_info[i].send(link_proto->protocol_info[i].instance,
                                          link_proto->protocol_info[i].buffer);
        size_to_send += link_proto->protocol_info[i].size;
    }

    *buffer_to_send = link_proto->buffer;
    *size           = size_to_send;
}

void link_protocol_receive_buffer(void *link_protocol, uint8_t *received_buffer, uint32_t size)
{
    link_protocol_t *link_proto = (link_protocol_t *)link_protocol;
    uint32_t current_proto_size = 0;

    if (current_proto_size <= size) {
        for (uint8_t i = 0; i < link_proto->current_number_of_protocol; i++) {
            link_proto->protocol_info[i].receive(link_proto->protocol_info[i].instance,
                                                received_buffer + current_proto_size);
            current_proto_size += link_proto->protocol_info[i].size;
        }
    }

}
