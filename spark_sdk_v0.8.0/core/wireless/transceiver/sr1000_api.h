/** @file sr1000_api.h
 *  @brief Radio level application programming interface.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_API_H_
#define SR1000_API_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "sr1000_calib.h"
#include "sr1000_def.h"
#include "sr1000_nvm.h"
#include "sr1000_spectral.h"
#include "sr1000_api_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
static const uint32_t sync_word_table[] = {
    0x08ecad3e, 0x09ae74e5, 0x0a2fb635, 0x0ade3365, 0x0b1ae937, 0x0cbad627,
    0x0ce2a76d, 0x0e6ae45b, 0xe129ab17, 0xe126eac6, 0xe1225779, 0xe620a5db,
    0xe92e8c4e, 0xe5a0af32, 0x0daf91ac, 0x0ca2fb72
};

/* TYPES **********************************************************************/
/** @brief Clear channel assessment information.
 */
typedef struct {
    uint8_t threshold;                           /**< Clear channel threshold */
    uint8_t margin;                              /**< Threshold margin */
    uint8_t retry_time;                          /**< RX pause time register value */
    uint8_t rnsi_offset;                         /**< RNSI input offset */
    int32_t rnsi[CCA_RNSI_FILTER_SIZE];          /**< IIR filter input */
    int32_t threshold_out[CCA_RNSI_FILTER_SIZE]; /**< IIR filter values */
    int32_t filter_coef_b[CCA_RNSI_FILTER_SIZE]; /**< IIR filter coefficients */
    int32_t filter_coef_a[CCA_RNSI_FILTER_SIZE]; /**< IIR filter coefficients */
    bool    enable;                              /**< Enable feature */
} cca_t;

/** @brief Radio instance.
 */
typedef struct {
    radio_hal_t      *radio_hal;                /**< SR1000 radio HAL function pointer structure */
    nvm_t             nvm;                      /**< NVM structure with entry and shadow NVM */
    calib_vars_t      calib_vars;               /**< Calibration variable structure */
    rf_channel_t      channel[NB_CHANNEL];      /**< RF channel structure settings */
    cca_t             cca;                      /**< Clear Channel Assessment structure */
    irq_polarity_t    irq_polarity;             /**< Interrupt polarity */
    address_length_t  addr_len;                 /**< Radio address length, 8 or 16 bits */
} radio_t;

/** @brief Frame quality indicators.
 */
typedef struct {
    uint8_t rssi; /**< Received Signal Strength Indicator */
    uint8_t rnsi; /**< Received Noise Strength Indicator */
} frame_quality_t;

/** @brief Frame configuration.
 */
typedef struct {
    modulation_t modulation;      /**< RF modulation */
    fec_level_t  fec;             /**< Forward Error Correction level */
    uint8_t      preamble_length; /**< Preamble length in bits */
} frame_cfg_t;

/** @brief Synchronization word configuration.
 */
typedef struct {
    uint32_t          syncword;           /**< Synchronization word, 16 or 32 bits.*/
    uint8_t           syncword_bit_cost;  /**< Synchronization word detection bit mismatch's extra cost, 3 bits range value */
    uint8_t           syncword_tolerance; /**< Synchronization word detection tolerance, 5 bits range value */
    syncword_length_t syncword_length;    /**< Synchronization word length, either SYNCWORD_LENGTH_16 or SYNCWORD_LENGTH_32 */
} syncword_cfg_t;

/** @brief Phases value, last received preamble phase correlation data.
 */
typedef struct phase_info {
    int8_t  phase1;     /**< Phase information #1 */
    int8_t  phase2;     /**< Phase information #2 */
    int8_t  phase3;     /**< Phase information #3 */
    int8_t  phase4;     /**< Phase information #4 */
    uint8_t rx_waited1; /**< Receiver time waited (MSB) */
    uint8_t rx_waited0; /**< Receiver time waited (LSB) */
} phase_info_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize the radio.
 *
 *  This function returns a radio "object" with all the information
 *  needed to operate the SR1000 radio.
 *
 *  @param[out] radio    Radio's instance.
 *  @param[in]  hal      Hardware Abstraction Layer structure.
 *  @param[in]  irq_pol  Polarity of the IRQ pin when it is asserted.
 *  @param[out] error    Error message.
 */
void uwb_init(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uwb_error_t *error);

/** @brief Reset the radio.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] delay_ms  Delay in milliseconds before releasing the reset pin.
 *                       Radio is kept IDLE after reset for the same amount of time.
 */
void uwb_reset(radio_t *radio, uint8_t delay_ms);

/** @brief Calibrate radio.
 *
 *  Calibrate the radio RF stage and assign frequency band for each channels.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_calibrate(radio_t *radio);

/** @brief Configure radio frame.
 *
 *  Configure constant parameters related to frame. All radio in the
 *  same network need to have the same values.
 *    - Preamble length
 *    - Modulation
 *    - FEC level
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] cfg    Frame's configuration.
 */
void uwb_config_frame(radio_t *radio, frame_cfg_t cfg);

/** @brief Configure synchronization word.
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] cfg    Synchronization word's configuration.
 */
void uwb_config_syncword(radio_t *radio, syncword_cfg_t cfg);

/** @brief Configure IRQ trigger(s).
 *
 *  Configure the events that will activate the IRQ pin.
 *
 *  @param[in] radio   Radio's instance.
 *  @param[in] events  Events that will trigger an interrupt from the radio.
 */
void uwb_enable_irq(radio_t *radio, radio_events_t events);

/** @brief Set CRC.
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] poly   CRC's generator's polynomial.
 *
 *  @note Concerning the CRC polynomial field :
 *          - Set bit 15 for a 16 bits long CRC.
 *          - Clear bit 15 and set bit 7 for a 8 bits long CRC.
 *          - Clear both bit 15 & 7 to disable CRC.
 */
void uwb_set_crc(radio_t *radio, uint16_t poly);

/** @brief Set radio local address.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] address   Local address.
 *  @param[in] addr_len  Length of the local address.
 */
void uwb_set_local_address(radio_t *radio, uint16_t address, address_length_t addr_len);

/** @brief Disable address filtering.
 *
 *  @note All frames will be accepted.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_disable_address_filter(radio_t *radio);

/** @brief Set radio destination address.
 *
 *  @note Address length is determined by the local address length.
 *
 *  @param[in] radio    Radio's instance.
 *  @param[in] address  Destination address.
 */
void uwb_set_destination_address(radio_t *radio, uint16_t address);

/** @brief Set radio sleep level.
 *
 *  Configure the sleep level and events what will automatically put the radio to sleep.
 *
 *  @param[in] radio   Radio's instance.
 *  @param[in] level   Sleep depth at which the radio will go when asleep.
 *  @param[in] events  Events that will trigger an automatic sleeping when completed.
 */
void uwb_set_sleep_level(radio_t *radio, sleep_lvl_t level, sleep_events_t events);

/** @brief Configure a radio frequency channel before use.
 *
 *  @param[in] radio    Radio's instance.
 *  @param[in] channel  Channel to configure.
 *  @param[in] level    Channel's desired power level.
 */
void uwb_config_channel(radio_t *radio, rf_channel_id_t channel, tx_power_t level);

/** @brief Select the radio frequency channel to use.
 *
 *  @param[in] radio    Radio's instance.
 *  @param[in] channel  Channel to use.
 */
void uwb_select_channel(radio_t *radio, rf_channel_id_t channel);

/** @brief Enable auto reply.
 *
 *  Send content of TX FIFO when a frame is received.
 *  Automatically go in receiver mode after sending a packet to
 *  receive the auto reply.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_enable_auto_reply(radio_t *radio);

/** @brief Disable auto reply.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_disable_auto_reply(radio_t *radio);

/** @brief Fill radio's TX FIFO.
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] data   Data to be sent.
 *  @param[in] size   Size of the data to be sent.
 */
void uwb_fill_tx_fifo(radio_t *radio, uint8_t *data, uint8_t size);

/** @brief Set TX FIFO threshold.
 *
 *  @note When this value is reached, the TX_FIFO_THRESH_EVENT is asserted.
 *
 *  @param[in] radio      Radio's instance.
 *  @param[in] threshold  Threshold in amount of bytes.
 */
void uwb_set_tx_fifo_thresh(radio_t *radio, uint8_t threshold);

/** @brief Set reception buffer threshold.
 *
 *  @note When this value is reached, the RX_FIFO_THRESH_EVENT is asserted.
 *
 *  @param[in] radio      Radio's instance.
 *  @param[in] threshold  Threshold in amount of bytes.
 */
void uwb_set_rx_fifo_thresh(radio_t *radio, uint8_t threshold);

/** @brief Set receiver's constant gain.
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] gain   Receiver's constant gain value.
 */
void uwb_set_receiver_gain(radio_t *radio, uint8_t gain);

/** @brief Get events from the radio.
 *
 *  @param[in] radio  Radio's instance.
 *  @return Internal radio events.
 */
radio_events_t uwb_get_events(radio_t *radio);

/** @brief Get the last frame quality indicators from the radio.
 *
 *  @param[in] radio  Radio's instance.
 *  @return Last frame quality indicators.
 */
frame_quality_t uwb_get_frame_quality(radio_t *radio);

/** @brief Get the phase information from the radio.
 *
 *  @param[in] radio  Radio's instance.
 *  @return Phase information.
 */
phase_info_t uwb_get_phases_info(radio_t *radio);

/** @brief Send a frame over the air.
 *
 *  @param[in] radio      Radio's instance.
 *  @param[in] dest_addr  Frame's destination address.
 *  @param[in] data       Data to send.
 *  @param[in] size       Size of the date to send.
 *  @param[in] delay_us   Delay in microseconds before the frame is sent (Not implemented yet).
 */
void uwb_send(radio_t *radio, uint16_t dest_addr, uint8_t *data, uint8_t size, uint16_t delay_us);

/** @brief Send a frame in broadcast over the air.
 *
 *  @param[in] radio            Radio's instance.
 *  @param[in] network_addr     Frame's destination network.
 *  @param[in] data             Data to send.
 *  @param[in] size             Size of the date to send.
 *  @param[in] delay_us         Delay in microseconds before the frame is sent (Not implemented yet).
 */
void uwb_send_broadcast(radio_t *radio, uint8_t network_addr, uint8_t *data, uint8_t size, uint16_t delay_us);

/** @brief Start transmission.
 *
 *  Send over the air all the data currently in the TX FIFO
 *  to the destination address currently set.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_start_transmission(radio_t *radio);

/** @brief Put the radio in receiver mode.
 *
 *  @param[in] radio       Radio's instance.
 *  @param[in] timeout_us  Timeout in microseconds. Use 0 for no timeout (not implemented yet).
 */
void uwb_receiver_on(radio_t *radio, uint16_t timeout_us);

/** @brief Read receiver buffer.
 *
 *  @param[in]  radio     Radio's instance.
 *  @param[out] data      Data received.
 *  @param[in]  max_size  Size of the receiving buffer.
 *  @return Size of the packet received.
 */
uint8_t uwb_read(radio_t *radio, uint8_t *data, uint8_t max_size);

/** @brief Radio goes to sleep.
 *
 *  @note The sleep level is the one given with set_sleep_level().
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_sleep(radio_t *radio);

/** @brief Radio wakes up.
 *
 *  @note The radio must not be in auto wakeup mode.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_wakeup(radio_t *radio);

/** @brief Flush the radio's internal TX FIFO.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_flush_tx_buffer(radio_t *radio);

/** @brief Flush the radio's internal RX FIFO.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_flush_rx_buffer(radio_t *radio);

/** @brief Get the received packet's payload size.
 *
 *  @param[in] radio  Radio's instance.
 *  @return Size of the RX payload size.
 */
uint8_t uwb_get_rx_payload_size(radio_t *radio);

/** @brief Get the IRQ pin status.
 *
 *  @param[in] radio  Radio's instance.
 *  @retval True   IRQ pin is asserted.
 *  @retval False  IRQ pin is not asserted.
 */
bool uwb_is_irq_assert(radio_t *radio);

/** @brief Write value to register.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] register  Radio's register where the value is written.
 *  @param[in] value     Value to write in register.
 */
void uwb_write_register(radio_t *radio, uint8_t reg, uint8_t value);

/** @brief Write value to register.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] register  Radio's register read.
 *  @return Register's value.
 */
uint8_t uwb_read_register(radio_t *radio, uint8_t reg);

/** @brief Select external PLL clock source.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_select_external_pll_clk_source(radio_t *radio);

/** @brief Select internal PLL clock source.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_select_internal_pll_clk_source(radio_t *radio);

/** @brief Select external XTAL clock source.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_select_external_xtal_clk_source(radio_t *radio);

/** @brief Select internal XTAL clock source.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_select_internal_xtal_clk_source(radio_t *radio);

/** @brief Clear Channel Assessment (CCA) initialization.
 *
 *  @param[in] radio          Radio's instance.
 *  @param[in] retry_time_us  Keep the receiver off for this time before trying again (max 50us).
 */
void uwb_cca_init(radio_t *radio, uint8_t retry_time_us);

/** @brief Update CCA threshold.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_cca_update_threshold(radio_t *radio);

/** @brief Update CCA RNSI offset.
 *  @note  This function must be called each time the REG_CONSTGAINS is changed.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_cca_update_rnsi_offset(radio_t *radio);

/** @brief Update retry time.
 *
 *  @param[in] radio          Radio's instance.
 *  @param[in] retry_time_us  Keep the receiver off for this time before trying again (max 50us).
 */
void uwb_cca_update_retry_time(radio_t *radio, uint8_t time_us);

/** @brief Enable CCA feature.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_cca_enable(radio_t *radio);

/** @brief Disable CCA feature.
 *
 *  @param[in] radio  Radio's instance.
 */
void uwb_cca_disable(radio_t *radio);

/** @brief Set pll wait time.
 *
 *  @param[in] radio      Radio's instance.
 *  @param[in] wait_time  Defines how long to wait for the integrated PLL to lock and settle before powering up any other supply block.
 */
void uwb_set_pll_wait_time(radio_t *radio, uint8_t wait_time);

#ifdef __cplusplus
}
#endif
#endif /* SR1000_API_H_ */
