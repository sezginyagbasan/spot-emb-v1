/** @file sr1000_api.c
 *  @brief Transceiver level application programming interface.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <string.h>
#include "sr1000_access.h"
#include "sr1000_def.h"
#include "sr1000_reg.h"
#include "sr1000_spectral.h"
#include "sr1000_utils.h"
#include "sr1000_api.h"

/* CONSTANTS ******************************************************************/
#define PIN_LOW                     0
#define PIN_HIGH                    1
#define RX_GAIN_VALUE               0x88
#define SYMBOL_RATE_MHZ             20.48
#define GAIN_LOOP_IFOA_GAIN_MAX_VAL 7
#define DEFAULT_PULSE_START_POS     1
#define PULSE_POS_ARRAY_OFFSET      1
#define SYNC_WORD_MSB_DEFAULT       0x5E
#define SYNC_WORD_MSB_ADDR          0x32
#define UNUSED(x)          ((void)x)                 /**< Silence warning for unused value */

static const uint16_t sr1020_channels[] = {
    164, /* 6717 MHz */
    170, /* 6963 MHz */
    176, /* 7209 MHz */
    182, /* 7455 MHz */
    188, /* 7700 MHz */
    194, /* 7946 MHz */
    200, /* 8192 MHz */
    206, /* 8438 MHz */
    212, /* 8684 MHz */
    218  /* 8929 MHz */
};

static const uint16_t sr1010_channels[] = {
    88,  /* 3604 MHz */
    95,  /* 3891 MHz */
    102, /* 4178 MHz */
    109, /* 4465 MHz */
    116, /* 4751 MHz */
    123, /* 5038 MHz */
    130, /* 5325 MHz */
    137, /* 5612 MHz */
    144, /* 5898 MHz */
    151  /* 6185 MHz */
};

static const uint8_t rf_gain_to_rnsi_offset_lookup_table[GAIN_LOOP_IFOA_GAIN_MAX_VAL + 1] = {7, 6, 4, 3, 1, 0, 0, 0};

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static uint8_t retry_time_us_to_reg_val(uint8_t time_us);
static bool assert_hal(radio_hal_t *hal);
static bool is_radio_present(radio_hal_t *hal);

/* PUBLIC FUNCTIONS ***********************************************************/
void uwb_init(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uwb_error_t *error)
{
    radio->radio_hal = hal;
    radio->irq_polarity = irq_pol;

    if (!assert_hal(radio->radio_hal)) {
        *error = UWB_HAL_ERROR;
        return;
    }

    uwb_reset(radio, 10);

    if (is_radio_present(radio->radio_hal)) {
        sr1000_nvm_init(radio->radio_hal, &radio->nvm);
        *error = UWB_NO_ERROR;
    } else {
        *error = UWB_NO_RADIO_PRESENT;
    }
}

void uwb_reset(radio_t *radio, uint8_t delay_ms)
{
    radio->radio_hal->reset_reset_pin();
    radio->radio_hal->delay_ms(delay_ms);
    radio->radio_hal->set_reset_pin();
    radio->radio_hal->delay_ms(delay_ms);
}

void uwb_calibrate(radio_t *radio)
{
    uint8_t reg_resistune;

    /* Get values from NVM */
    radio->calib_vars.chip_id = sr1000_nvm_get_serial_number_chip_id(&radio->nvm);
    radio->calib_vars.phy_model = sr1000_nvm_get_product_id_model(&radio->nvm);
    radio->calib_vars.phy_package = sr1000_nvm_get_product_id_package(&radio->nvm);
    radio->calib_vars.phy_version = sr1000_nvm_get_product_id_version(&radio->nvm);
    radio->calib_vars.binning_setup_code = sr1000_nvm_get_serial_number_binning_setup_code(&radio->nvm);
    radio->calib_vars.resistune = sr1000_nvm_get_calibration(&radio->nvm);
    radio->calib_vars.nvm_vcro_shift = sr1000_nvm_get_vcro_shift(&radio->nvm);
    radio->calib_vars.pulse_width_offset = sr1000_nvm_get_vref_adjust_pulse_width_offset(&radio->nvm);
    radio->calib_vars.vref_tune_offset = sr1000_nvm_get_vref_adjust_vref_tune_offset(&radio->nvm);

    sr1000_calib_init(&radio->calib_vars);

    reg_resistune = sr1000_calib_vref_tune_offset(&radio->calib_vars) | BIT_LNAIMPED;

    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_RESISTUNE, reg_resistune);
    sr1000_access_close(radio->radio_hal);

    sr1000_calib_tune_delay_line(radio->radio_hal);
    sr1000_calib_run_frequency_calibration(radio->radio_hal, &radio->calib_vars);

    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_CONSTGAINS, RX_GAIN_VALUE);
    sr1000_access_write_reg(radio->radio_hal, REG_DEBUGMODEM, MANUPHASE_PHASE_TRACKING_ENABLE);
    sr1000_access_close(radio->radio_hal);
}

void uwb_config_frame(radio_t *radio, frame_cfg_t cfg)
{
    uint8_t modem_reg;
    uint8_t packet_cfg_reg;
    uint8_t preamble_len;

    sr1000_access_open(radio->radio_hal);

    modem_reg = sr1000_access_read_reg(radio->radio_hal, REG_MAINMODEM);
    modem_reg &= 0xF0;
    modem_reg |= cfg.modulation | cfg.fec;

    sr1000_access_write_reg(radio->radio_hal, REG_MAINMODEM, modem_reg);

    preamble_len = (cfg.preamble_length / 2) - 8;
    sr1000_access_write_reg(radio->radio_hal, REG_PREAMBLEN, preamble_len);
    sr1000_access_write_reg(radio->radio_hal, REG_TXPKTSIZE, MAX_FRAMESIZE);
    sr1000_access_write_reg(radio->radio_hal, REG_RXPKTSIZE, MAX_FRAMESIZE);

    packet_cfg_reg = sr1000_access_read_reg(radio->radio_hal, REG_PACKETCFG);
    packet_cfg_reg &= 0xF0;

    /* Variable frame size always enabled */
    packet_cfg_reg |= SIZE_HEADER_ENABLE | SIZE_SRC_TX_PAYLOAD_SIZE | SAVE_SIZE_ENABLE;


    sr1000_access_write_reg(radio->radio_hal, REG_PACKETCFG, packet_cfg_reg);

    sr1000_access_close(radio->radio_hal);
}

void uwb_config_syncword(radio_t *radio, syncword_cfg_t cfg)
{
    uint8_t syncword_serialized[NB_SYNCWORD_REG];

    syncword_serialized[0] = cfg.syncword_length |
                             MOV2MASK(cfg.syncword_bit_cost,  BITS_SWBITCOST) |
                             MOV2MASK(cfg.syncword_tolerance, BITS_SWCORRTOL);

    serialize_uint32_to_uint8_array(cfg.syncword, &(syncword_serialized[1]));

    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_write(radio->radio_hal, REG_SYNCWORDCFG, syncword_serialized, NB_SYNCWORD_REG);
    sr1000_access_close(radio->radio_hal);
}

void uwb_enable_irq(radio_t *radio, radio_events_t events)
{
    uint8_t events_serialized[NB_IRQ_REG];

    serialize_uint16_to_uint8_array(events, events_serialized);

    if (radio->irq_polarity == IRQ_ACTIVE_HIGH) {
        events_serialized[0] |= BIT_IRQPOLAR;
    }

    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_write(radio->radio_hal, REG_IRQMASK1, events_serialized, NB_IRQ_REG);
    sr1000_access_close(radio->radio_hal);
}

void uwb_set_crc(radio_t *radio, uint16_t poly)
{
    uint8_t crc_value_serialized[NB_CRC_REG];

    serialize_uint16_to_uint8_array(poly, crc_value_serialized);

    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_write(radio->radio_hal, REG_CRCPOLYNOM1, crc_value_serialized, NB_CRC_REG);
    sr1000_access_close(radio->radio_hal);
}

void uwb_set_local_address(radio_t *radio, uint16_t address, address_length_t addr_len)
{
    uint8_t addr_serialized[NB_ADDRESS_REG];
    uint8_t packet_cfg;

    sr1000_access_open(radio->radio_hal);
    packet_cfg = sr1000_access_read_reg(radio->radio_hal, REG_PACKETCFG);
    packet_cfg &= 0x0F;
    packet_cfg |= ADDR_FILT_ENABLE  |
                  addr_len          |
                  ADDR_HEADER_ENABLE;

    sr1000_access_write_reg(radio->radio_hal, REG_PACKETCFG, packet_cfg);

    if (addr_len == ADDRESS_LENGTH_8) {
        addr_serialized[0] = address & 0xFF;
        sr1000_access_write_reg(radio->radio_hal, REG_LOCALADDR0, addr_serialized[0]);
    }

    if (addr_len == ADDRESS_LENGTH_16) {
        serialize_uint16_to_uint8_array(address, addr_serialized);
        sr1000_access_burst_write(radio->radio_hal, REG_LOCALADDR1, addr_serialized, 2);
    }

    radio->addr_len = addr_len;

    sr1000_access_close(radio->radio_hal);
}

void uwb_disable_address_filter(radio_t *radio)
{
    uint8_t packet_cfg;

    sr1000_access_open(radio->radio_hal);
    packet_cfg = sr1000_access_read_reg(radio->radio_hal, REG_PACKETCFG);
    packet_cfg &= ~(ADDR_FILT_ENABLE);

    sr1000_access_write_reg(radio->radio_hal, REG_PACKETCFG, packet_cfg);

    sr1000_access_close(radio->radio_hal);
}

void uwb_set_destination_address(radio_t *radio, uint16_t address)
{
    uint8_t dest_addr_ser[NB_ADDRESS_REG];

    sr1000_access_open(radio->radio_hal);

    if (radio->addr_len == ADDRESS_LENGTH_8) {
        sr1000_access_write_reg(radio->radio_hal, REG_REMOTADDR0, EXTRACT_BYTE(address, 0));
    }

    if (radio->addr_len == ADDRESS_LENGTH_16) {
        serialize_uint16_to_uint8_array(address, dest_addr_ser);
        sr1000_access_burst_write(radio->radio_hal, REG_REMOTADDR1, dest_addr_ser, NB_ADDRESS_REG);
    }

    sr1000_access_close(radio->radio_hal);
}

void uwb_set_sleep_level(radio_t *radio, sleep_lvl_t level, sleep_events_t events)
{
    uint8_t reg_data = level | events;
    uint8_t reg_dlltuning;

    sr1000_access_open(radio->radio_hal);
    reg_dlltuning = sr1000_access_read_reg(radio->radio_hal, REG_DLLTUNING);

    if (radio->calib_vars.phy_version == PHY_VERSION_8_3) {
        if (level == SLEEP_IDLE) {
            reg_dlltuning &= ~(1UL << BIT_INTEGLEN);
        } else {
            reg_dlltuning |= BIT_INTEGLEN;
        }

        sr1000_access_write_reg(radio->radio_hal, REG_DLLTUNING, reg_dlltuning);
    }

    sr1000_access_write_reg(radio->radio_hal, REG_SLEEPCONF, reg_data);
    sr1000_access_close(radio->radio_hal);
}

void uwb_config_channel(radio_t *radio, rf_channel_id_t channel, tx_power_t lvl)
{
    uint16_t frequency = 0;

    if (channel < CHANNEL_ALL) {
        if (radio->calib_vars.phy_model == PHY_MODEL_SR1010) {
            frequency = sr1010_channels[channel];
        }

        if (radio->calib_vars.phy_model == PHY_MODEL_SR1020) {
            frequency = sr1020_channels[channel];
        }

        config_spectrum(&radio->calib_vars, frequency, lvl, &(radio->channel[channel]));
    }
}

void uwb_select_channel(radio_t *radio, rf_channel_id_t channel)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_write(radio->radio_hal, REG_RXFILTERS, (uint8_t *)(&radio->channel[channel]), NB_PULSES+2);
    sr1000_access_close(radio->radio_hal);
}

void uwb_enable_auto_reply(radio_t *radio)
{
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_MAINMODEM);
    reg_val |= AUTO_REPLY_ENABLE;
    sr1000_access_write_reg(radio->radio_hal, REG_MAINMODEM, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_disable_auto_reply(radio_t *radio)
{
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_MAINMODEM);
    reg_val &= ~(AUTO_REPLY_ENABLE);
    sr1000_access_write_reg(radio->radio_hal, REG_MAINMODEM, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_fill_tx_fifo(radio_t *radio, uint8_t *data, uint8_t size)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_write(radio->radio_hal, REG_TXFIFO, data, size);
    sr1000_access_close(radio->radio_hal);
}

void uwb_set_tx_fifo_thresh(radio_t *radio, uint8_t threshold)
{
    uint8_t reg_val = BIT_TXIRQEN | threshold;

    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_TXFIFOTHRESH, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_set_rx_fifo_thresh(radio_t *radio, uint8_t threshold)
{
    uint8_t reg_val = BIT_RXIRQEN | threshold;

    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_RXFIFOTHRESH, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_set_receiver_gain(radio_t *radio, uint8_t gain)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_CONSTGAINS, gain);
    sr1000_access_close(radio->radio_hal);
}

radio_events_t uwb_get_events(radio_t *radio)
{
    radio_events_t  events;
    uint8_t  radio_status[NB_STATUS_REG];

    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_read(radio->radio_hal, REG_STATUS1, radio_status, NB_STATUS_REG);
    sr1000_access_close(radio->radio_hal);

    events = (radio_status[0] << 8) | radio_status[1];

    return events;
}

frame_quality_t uwb_get_frame_quality(radio_t *radio)
{
    frame_quality_t frame_quality;

    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_read(radio->radio_hal, REG_RSSI, (uint8_t *)(&frame_quality), NB_QUALITY_REG);
    sr1000_access_close(radio->radio_hal);

    return frame_quality;
}

phase_info_t uwb_get_phases_info(radio_t *radio)
{
    phase_info_t phases_info;

    /* Get last received preamble phases */
    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_read(radio->radio_hal, REG_PHSDATA1, (uint8_t *)&phases_info.phase1, NB_PHASE_REG);
    sr1000_access_close(radio->radio_hal);

    /* Get last receiver timed waited */
    sr1000_access_open(radio->radio_hal);
    sr1000_access_burst_read(radio->radio_hal, REG_RXWAITTIME1, &phases_info.rx_waited1, NB_RX_WAIT_REG);
    sr1000_access_close(radio->radio_hal);

    /* Remove MSB because this bit has a different meaning from rx_waited1 */
    phases_info.rx_waited1 = MASK2VAL(phases_info.rx_waited1, BITS8(6, 0));

    return phases_info;
}

void uwb_send(radio_t *radio, uint16_t dest_addr, uint8_t *data, uint8_t size, uint16_t delay)
{
    if (delay) {
        uwb_write_register(radio, REG_PWRUPDELAY, delay);
    }

    uwb_set_destination_address(radio, dest_addr);
    uwb_fill_tx_fifo(radio, data, size);
    uwb_start_transmission(radio);
}

void uwb_send_broadcast(radio_t *radio, uint8_t network_addr, uint8_t *data, uint8_t size, uint16_t delay_us)
{
    uint16_t dest_addr;

    dest_addr = ((network_addr << 8) | BROADCAST_ADDRESS);
    uwb_send(radio, dest_addr, (uint8_t *)data, size, delay_us);
}

void uwb_start_transmission(radio_t *radio)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, BIT_STARTTX);
    sr1000_access_close(radio->radio_hal);
}

void uwb_receiver_on(radio_t *radio, uint16_t timeout)
{
    UNUSED(timeout);
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, BIT_RXMODE);
    sr1000_access_close(radio->radio_hal);
}

uint8_t uwb_read(radio_t *radio, uint8_t *data, uint8_t max_size)
{
    uint8_t payload_size;

    sr1000_access_open(radio->radio_hal);
    /* Read header */
    payload_size = sr1000_access_read_reg(radio->radio_hal, REG_RXFIFO);

    /* Check if payload is bigger than output buffer size */
    if (payload_size > max_size) {
        sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, BIT_FLUSHRX);
        payload_size = FRAME_SIZE_ERROR;
    } else {
        sr1000_access_burst_read(radio->radio_hal, REG_RXFIFO, data, payload_size);
    }

    sr1000_access_close(radio->radio_hal);

    return payload_size;
}

uint8_t uwb_get_rx_payload_size(radio_t *radio)
{
    uint8_t size;

    sr1000_access_open(radio->radio_hal);
    size = sr1000_access_read_reg(radio->radio_hal, REG_RXFIFOSTAT);
    sr1000_access_close(radio->radio_hal);

    return size;
}

void uwb_sleep(radio_t *radio)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, BIT_GOTOSLP);
    sr1000_access_close(radio->radio_hal);
}

void uwb_wakeup(radio_t *radio)
{
    uint8_t reg_val = 0x00;

    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_flush_tx_buffer(radio_t *radio)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, BIT_FLUSHTX);
    sr1000_access_close(radio->radio_hal);
}

void uwb_flush_rx_buffer(radio_t *radio)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_ACTIONS, BIT_FLUSHRX);
    sr1000_access_close(radio->radio_hal);
}

bool uwb_is_irq_assert(radio_t *radio)
{
    bool pin_logic_level;
    bool assert;

    pin_logic_level = radio->radio_hal->read_irq_pin();

    if (radio->irq_polarity == IRQ_ACTIVE_LOW) {
        assert = (pin_logic_level == PIN_LOW ? true : false);
    } else {
        assert = pin_logic_level;
    }

    return assert;
}

void uwb_write_register(radio_t *radio, uint8_t reg, uint8_t value)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, reg, value);
    sr1000_access_close(radio->radio_hal);
}

uint8_t uwb_read_register(radio_t *radio, uint8_t reg)
{
    uint8_t value;

    sr1000_access_open(radio->radio_hal);
    value = sr1000_access_read_reg(radio->radio_hal, reg);
    sr1000_access_close(radio->radio_hal);

    return value;
}

void uwb_select_external_pll_clk_source(radio_t *radio)
{
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_DISABLES);
    reg_val |= MOV2MASK(1, BIT_SYMBCSRC) | MOV2MASK(1, BIT_PLLDIS);

    sr1000_access_write_reg(radio->radio_hal, REG_DISABLES, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_select_internal_pll_clk_source(radio_t *radio)
{
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_DISABLES);
    reg_val &= ~(MOV2MASK(1, BIT_SYMBCSRC) | MOV2MASK(1, BIT_PLLDIS));

    sr1000_access_write_reg(radio->radio_hal, REG_DISABLES, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_select_external_xtal_clk_source(radio_t *radio)
{
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_DISABLES);
    reg_val |= MOV2MASK(1, BIT_XTALCSRC);

    sr1000_access_write_reg(radio->radio_hal, REG_DISABLES, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_select_internal_xtal_clk_source(radio_t *radio)
{
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_DISABLES);
    reg_val &= ~(MOV2MASK(1, BIT_XTALCSRC));

    sr1000_access_write_reg(radio->radio_hal, REG_DISABLES, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_cca_init(radio_t *radio, uint8_t retry_time_us)
{
    radio->cca.retry_time = retry_time_us_to_reg_val(retry_time_us);

    /* Butterworth IIR filter, 4th order, fc = 0.25, Q7 */
    radio->cca.filter_coef_b[0] =  2;
    radio->cca.filter_coef_b[1] =  10;
    radio->cca.filter_coef_b[2] =  15;
    radio->cca.filter_coef_b[3] =  10;
    radio->cca.filter_coef_b[4] =  2;

    radio->cca.filter_coef_a[0] =  255;
    radio->cca.filter_coef_a[1] = -501;
    radio->cca.filter_coef_a[2] =  442;
    radio->cca.filter_coef_a[3] = -184;
    radio->cca.filter_coef_a[4] =  30;

    radio->cca.threshold_out[0] =  0;
    radio->cca.threshold_out[1] =  0;
    radio->cca.threshold_out[2] =  0;
    radio->cca.threshold_out[3] =  0;
    radio->cca.threshold_out[4] =  0;

    /* Threshold margin */
    radio->cca.margin = CCA_DEFAULT_MARGIN;

    radio->cca.rnsi[0] =  0;
    radio->cca.rnsi[1] =  0;
    radio->cca.rnsi[2] =  0;
    radio->cca.rnsi[3] =  0;
    radio->cca.rnsi[4] =  0;

    /* CCA disabled by default */
    radio->cca.enable = false;
}

void uwb_cca_update_threshold(radio_t *radio)
{
    uint8_t rnsi;
    uint8_t reg_val;

    sr1000_access_open(radio->radio_hal);
    rnsi = sr1000_access_read_reg(radio->radio_hal, REG_RNSI);

    if (rnsi > radio->cca.rnsi_offset) {
        rnsi = rnsi - radio->cca.rnsi_offset;
    } else {
        rnsi = 0;
    }

    emplace_front_int32(rnsi, radio->cca.rnsi, CCA_RNSI_FILTER_SIZE);
    emplace_front_int32(radio->cca.threshold_out[0], radio->cca.threshold_out, CCA_RNSI_FILTER_SIZE);

    /* Filtering
     *
     * Index 0 = k-0
     * Index 1 = k-1
     * ...
     */
    radio->cca.threshold_out[0] = 0;
    for (uint8_t i = 0; i < CCA_RNSI_FILTER_SIZE; ++i) {
        radio->cca.threshold_out[0] += (radio->cca.rnsi[i] * radio->cca.filter_coef_b[i]);
    }

    for (uint8_t i = 1; i < CCA_RNSI_FILTER_SIZE; ++i) {
        radio->cca.threshold_out[0] -= (radio->cca.threshold_out[i] * radio->cca.filter_coef_a[i]);
    }

    radio->cca.threshold_out[0] = radio->cca.threshold_out[0] / radio->cca.filter_coef_a[0];

    /* Check for underflow */
    if (radio->cca.margin < radio->cca.threshold_out[0]) {
        radio->cca.threshold = radio->cca.threshold_out[0] - radio->cca.margin;
    } else {
        /* Underflow */
        radio->cca.threshold = 0;
    }

    if (radio->cca.enable) {
        reg_val = sr1000_access_read_reg(radio->radio_hal, REG_CLEARTOSEND);
        // TODO Create Macro to clear bits
        reg_val &= RX_IDLE_PWR_HIGH | MOV2MASK(0x00, BITS_CSTHRES);
        reg_val |= MOV2MASK(radio->cca.threshold, BITS_CSTHRES);
        sr1000_access_write_reg(radio->radio_hal, REG_CLEARTOSEND, reg_val);

    }
    sr1000_access_close(radio->radio_hal);
}

void uwb_cca_update_rnsi_offset(radio_t *radio)
{
    uint8_t reg_val;
    uint8_t ifoa_gain;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_CONSTGAINS);
    sr1000_access_close(radio->radio_hal);

    ifoa_gain = MASK2VAL(reg_val, BITS_IFOAGAIN);
    radio->cca.rnsi_offset = rf_gain_to_rnsi_offset_lookup_table[ifoa_gain];
}

void uwb_cca_update_retry_time(radio_t *radio, uint8_t retry_time_us)
{
    radio->cca.retry_time = retry_time_us_to_reg_val(retry_time_us);

    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_RXPAUSETIME, radio->cca.retry_time);
    sr1000_access_close(radio->radio_hal);
}

void uwb_cca_enable(radio_t *radio)
{
    uint8_t reg_val;

    radio->cca.enable = true;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_CLEARTOSEND);
    reg_val &= RX_IDLE_PWR_HIGH | MOV2MASK(0x00, BITS_CSTHRES);
    reg_val |= MOV2MASK(radio->cca.threshold, BITS_CSTHRES);
    sr1000_access_write_reg(radio->radio_hal, REG_CLEARTOSEND, reg_val);
    sr1000_access_write_reg(radio->radio_hal, REG_RXPAUSETIME, radio->cca.retry_time);
    sr1000_access_close(radio->radio_hal);
}

void uwb_cca_disable(radio_t *radio)
{
    uint8_t reg_val;

    radio->cca.enable = false;

    sr1000_access_open(radio->radio_hal);
    reg_val = sr1000_access_read_reg(radio->radio_hal, REG_CLEARTOSEND);
    reg_val &= RX_IDLE_PWR_HIGH | MOV2MASK(0x00, BITS_CSTHRES);
    reg_val |= MOV2MASK(CCA_DISABLE, BITS_CSTHRES);
    sr1000_access_write_reg(radio->radio_hal, REG_CLEARTOSEND, reg_val);
    sr1000_access_close(radio->radio_hal);
}

void uwb_set_pll_wait_time(radio_t *radio, uint8_t wait_time)
{
    sr1000_access_open(radio->radio_hal);
    sr1000_access_write_reg(radio->radio_hal, REG_PLLSTARTUP, wait_time);
    sr1000_access_close(radio->radio_hal);
}

/* PRIVATE FUNCTIONS **********************************************************/

/** @brief Convert retry time in us to register value.
 *
 *  @param[in] time_us  Retry time in us.
 *  @return Register's value.
 */
static uint8_t retry_time_us_to_reg_val(uint8_t time_us)
{
    return ((time_us * SYMBOL_RATE_MHZ) / 4) - 1;
}

/** @brief Assert hal
 *
 *  This function check is any parameter is NULL.
 *
 *  @param[in] hal  Hardware Abstraction Layer structure.
 *  @retval True   HAL is OK.
 *  @retval False  HAL is not valid.
 */
static bool assert_hal(radio_hal_t *hal)
{
    if (hal->delay_ms         &&
        hal->read             &&
        hal->write            &&
        hal->read_irq_pin     &&
        hal->reset_cs         &&
        hal->set_cs           &&
        hal->reset_reset_pin  &&
        hal->set_reset_pin    &&
        hal->set_shutdown_pin &&
        hal->reset_shutdown_pin) {

        return true;
    } else {
        return false;
    }
}

/** @brief Check if a radio is present.
 *
 *  This function must be called after a radio reset.
 *  A known value is read. If it doesn't correspond to the datasheet's value,
 *  we know the radio is unreachable.
 *
 *  @param[in] hal  Hardware Abstraction Layer structure.
 *  @retval True   Radio is present.
 *  @retval False  Radio is not present.
 */
static bool is_radio_present(radio_hal_t *hal)
{
    uint8_t sync_msb;

    sr1000_access_open(hal);
    sync_msb = sr1000_access_read_reg(hal, SYNC_WORD_MSB_ADDR);
    sr1000_access_close(hal);

    if (sync_msb != SYNC_WORD_MSB_DEFAULT) {
        return false;
    } else {
        return true;
    }
}
