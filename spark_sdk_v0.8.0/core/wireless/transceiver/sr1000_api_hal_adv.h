/** @file sr1000_api_hal_adv.h
 *  @brief SR1000 hardware abstraction layer advance
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_API_HAL_ADV_H_
#define SR1000_API_HAL_ADV_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "sr1000_api_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
/** @brief SR1000 API Hardware Abstraction Layer advance.
 */
typedef struct radio_hal_adv_struct {

    void (*transfer_full_duplex_blocking)(uint8_t *tx_data, uint8_t *rx_data, uint16_t size);
    void (*transfer_full_duplex_non_blocking)(uint8_t *tx_data, uint8_t *rx_data, uint16_t size);
    bool (*is_spi_busy)(void);
    void (*context_switch)(void);
    void (*disable_radio_irq)(void);
    void (*enable_radio_irq)(void);
    void (*disable_radio_dma_irq)(void);
    void (*enable_radio_dma_irq)(void);

} radio_hal_adv_t;

#ifdef __cplusplus
}
#endif


#endif /* SR1000_API_HAL_ADV_H_ */
