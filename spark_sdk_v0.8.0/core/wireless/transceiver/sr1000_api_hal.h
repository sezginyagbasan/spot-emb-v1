/** @file sr1000_api_hal.h
 *  @brief SR1000 hardware abstraction layer
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_API_HAL_H_
#define SR1000_API_HAL_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
/** @brief SR1000 API Hardware Abstraction Layer.
 *
 *  This structure contains all function pointers used to interact with the
 *  microcontroller's peripherals.
 *
 *  Every functions must be implemented. If a function is not needed, the user
 *  must implement an empty function.
 *
 *  For example, if the shutdown pin is not used, simply create :
 *  void no_shutdown_pin(void) {} and pass it to both
 *  set_shutdown_pin and reset_shutdown_pin.
 */
typedef struct {
    /* GPIO */
    void (*set_shutdown_pin)(void);             /**< Set shutdown pin HIGH */
    void (*reset_shutdown_pin)(void);           /**< Set shutdown pin LOW */
    void (*set_reset_pin)(void);                /**< Set reset pin HIGH */
    void (*reset_reset_pin)(void);              /**< Set reset pin LOW */
    bool (*read_irq_pin)(void);                 /**< Return IRQ pin state. 0 (LOW), 1(HIGH) */
    /* SPI */
    void (*set_cs)(void);                       /**< Set CS pin HIGH */
    void (*reset_cs)(void);                     /**< Set CS pin LOW */
    void (*write)(uint8_t *data, uint8_t size); /**< Send array (*data) over SPI */
    void (*read)(uint8_t *data, uint8_t size);  /**< Read a number of bytes (determined by size)
                                                  * from SPI and populate the data array.
                                                  */
    /* Delay */
    void (*delay_ms)(uint32_t ms);              /**< Blocking delay function in milliseconds */
} radio_hal_t;

#ifdef __cplusplus
}
#endif


#endif /* SR1000_API_HAL_H_ */
