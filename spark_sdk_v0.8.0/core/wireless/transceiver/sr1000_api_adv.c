/** @file sr1000_api_adv.c
 *  @brief Transceiver level advance application programming interface .
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "sr1000_api_adv.h"
#include "sr1000_reg.h"

/* CONSTANTS ******************************************************************/
#define UNUSED(x)            ((void)x) /**< Silence warning for unused value */
#define CLOCK_FACTOR         1000
#define CRYSTAL_CLOCK_PERIOD 32768
#define SYMBOL_CLOCK_PERIOD  20480

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static uint8_t pll_startup_us_to_clock_register(uint32_t pll_startup_us);
static uint16_t rx_timeout_us_to_register(uint32_t rx_period_us);
static uint8_t rx_pwr_up_delay_ns_to_register(uint16_t rx_power_up_delay_ns);
static uint8_t rx_pause_time_pll_cycles_to_register(uint32_t rx_pause_time_pll_cycles);
static uint8_t pwr_up_delay_ns_to_register(uint32_t pwr_up_delay_us);
static uint8_t clamp_8_bit_value(int16_t value);
static uint32_t wake_sleep_us_to_reg_when_idle(uint32_t sleep_time_us);
static uint32_t wake_sleep_us_to_reg_when_shallow(uint32_t sleep_time_us);
static uint32_t wake_sleep_us_to_reg_when_deep(uint32_t sleep_time_us, uint8_t pll_startup_reg_value);

/* PUBLIC FUNCTIONS ***********************************************************/
void uwb_adv_init(radio_adv_t     *radio_adv,
                  radio_hal_adv_t *radio_hal_adv,
                  radio_t         *radio,
                  uint8_t         *app_spi_rx_buffer,
                  uint8_t         *app_spi_tx_buffer,
                  uint8_t          app_spi_buffer_size,
                  uwb_err         *error)
{
    *error = API_ADV_NO_ERROR;

    uwb_err as_error;

    if (!radio->radio_hal) {
        *error = API_ADV_RADIO_NOT_INITIALIZED;
        return;
    }

    sr1000_access_adv_init(&radio_adv->access_sequence,
                           app_spi_rx_buffer,
                           app_spi_tx_buffer,
                           app_spi_buffer_size,
                           &as_error);

    /* Setup advance HAL instance */
    radio_adv->radio_hal_adv = radio_hal_adv;

    /* Setup radio instance */
    radio_adv->radio = radio;

    radio_adv->radio_sleep_time.pwr_up         = 0;
    radio_adv->radio_sleep_time.sleep_cycles   = 0;
    radio_adv->radio_sleep_time.timeout_cycles = 0;

    uwb_adv_set_shadow_reg(radio_adv);
}

void uwb_adv_set_shadow_reg(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    radio_events_t default_events = BIT(15) | TX_END_IT | NEW_PACKET_IT | WAKEUP_IT | CSC_FAIL_IT | TX_UNDERFLOW_IT |
                                    RX_OVRFLOW_IT | TX_OVRFLOW_IT | BUF_LOAD_TH_IT;
    uint8_t *reg_packet_cfg;
    uint8_t *reg_disables;
    uint8_t *reg_dll_tuning;
    uint8_t *reg_pll_startup;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_PACKETCFG, &reg_packet_cfg, &access_sequence_error);
    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_DISABLES, &reg_disables, &access_sequence_error);
    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_DLLTUNING, &reg_dll_tuning, &access_sequence_error);
    sr1000_access_adv_append_read(
        &radio_adv->access_sequence, REG_PLLSTARTUP, &reg_pll_startup, &access_sequence_error);

    uwb_adv_transfer_blocking(radio_adv);

    radio_adv->shadow_reg.irq_mask1       = default_events >> 8;
    radio_adv->shadow_reg.irq_mask2       = default_events;
    radio_adv->shadow_reg.reg_actions     = BIT_RXMODE | BIT_GOTOSLP;
    radio_adv->shadow_reg.reg_modem_gain  = MOV2MASK(0b111111, BITS_MANUGAIN);
    radio_adv->shadow_reg.reg_packet_cfg  = *reg_packet_cfg;
    radio_adv->shadow_reg.reg_disables    = *reg_disables;
    radio_adv->shadow_reg.reg_dll_tuning  = *reg_dll_tuning |= BIT_ECO;
    radio_adv->shadow_reg.reg_pll_startup = *reg_pll_startup;
}

void uwb_adv_set_destination_address(radio_adv_t *radio_adv, uint16_t address, address_length_t address_size)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    if (address_size == ADDRESS_LENGTH_8) {
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_REMOTADDR0, EXTRACT_BYTE(address, 0), &access_sequence_error);
    } else {
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_REMOTADDR0, EXTRACT_BYTE(address, 0), &access_sequence_error);
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_REMOTADDR1, EXTRACT_BYTE(address, 1), &access_sequence_error);
    }
}

void uwb_adv_set_local_address(radio_adv_t *radio_adv, uint16_t address, address_length_t address_size)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    if (address_size == ADDRESS_LENGTH_8) {
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_LOCALADDR0, EXTRACT_BYTE(address, 0), &access_sequence_error);
    } else {
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_LOCALADDR0, EXTRACT_BYTE(address, 0), &access_sequence_error);
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_LOCALADDR1, EXTRACT_BYTE(address, 1), &access_sequence_error);
    }
}

void uwb_adv_enable_modem_wakeup_timer(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXWAITTIME1, BIT_RXWAISRC, &access_sequence_error);
}

void uwb_adv_disable_modem_wakeup_timer(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXWAITTIME1, 0, &access_sequence_error);
}

void uwb_adv_set_rx_pause_time(radio_adv_t *radio_adv, uint16_t rx_pause_time)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    uint8_t reg_value = rx_pause_time_pll_cycles_to_register(rx_pause_time);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXPAUSETIME, reg_value, &access_sequence_error);
}

void uwb_adv_set_timer_config(radio_adv_t *radio_adv, radio_timer_config_t *timer_config)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_timer_config    = timer_config->auto_wake  |
                                    timer_config->wake_once  |
                                    timer_config->syn_at_end |
                                    timer_config->syn_pkt_tx |
                                    timer_config->syn_pkt_rx |
                                    timer_config->syn_match  |
                                    timer_config->syn_brdca  |
                                    timer_config->syn_rx_crc;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCONF, write_timer_config, &access_sequence_error);
}

void uwb_adv_disable_timer_config(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCONF, 0, &access_sequence_error);
}

void uwb_adv_set_irq1_flag(radio_adv_t *radio_adv, radio_events_t *radio_events)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint16_t write_irq            = *radio_events;

    write_irq = write_irq >> 8;

    if (radio_adv->radio->irq_polarity == IRQ_ACTIVE_HIGH) {
        write_irq |= (BIT_IRQPOLAR);
    }

    radio_adv->shadow_reg.irq_mask1 = write_irq;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_IRQMASK1, write_irq, &access_sequence_error);
}

void uwb_adv_set_irq2_flag(radio_adv_t *radio_adv, radio_events_t *radio_events)
{
    uwb_err access_sequence_error       = ACCESS_SEQUENCE_BUSY_ERR;
    irq_polarity_t current_irq_polarity = radio_adv->shadow_reg.irq_mask1 & BIT_IRQPOLAR;
    uint16_t write_irq                  = 0;

    if (radio_adv->radio->irq_polarity != current_irq_polarity) {
        write_irq  = radio_adv->shadow_reg.irq_mask1;
        write_irq ^= BIT_IRQPOLAR;
        sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_IRQMASK1, write_irq, &access_sequence_error);
    }

    write_irq = *radio_events & 0x00FF;
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_IRQMASK2, write_irq, &access_sequence_error);
}

void uwb_adv_disable_irq1(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_IRQMASK1, 0, &access_sequence_error);
}

void uwb_adv_disable_irq2(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_IRQMASK2, 0, &access_sequence_error);
}

void uwb_adv_set_sleep_config(radio_adv_t *radio_adv, sleep_lvl_t *level, sleep_events_t *events)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t reg_data              = *level | *events;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_SLEEPCONF, reg_data, &access_sequence_error);
}

void uwb_adv_set_wake_sleep_period(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint16_t wake_sleep_period    = radio_adv->radio_sleep_time.sleep_cycles;
    uint8_t sleep_period[2]       = {0};

    serialize_uint16_to_uint8_array(wake_sleep_period, sleep_period);

    /* Sending High value - LSB - MSB to ensure no issues when using normal SPI transfer */
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCOUNT1, 0xFF, &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCOUNT0, sleep_period[1], &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCOUNT1, sleep_period[0], &access_sequence_error);
}

void uwb_adv_set_radio_actions(radio_adv_t *radio_adv, radio_actions_t *target_radio_actions)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_actions         = target_radio_actions->cal_dcro   |
                                    target_radio_actions->skip_wake  |
                                    target_radio_actions->rx_mode    |
                                    target_radio_actions->start_tx   |
                                    target_radio_actions->init_timer |
                                    target_radio_actions->go_sleep   |
                                    target_radio_actions->flush_rx   |
                                    target_radio_actions->flush_tx;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_ACTIONS, write_actions, &access_sequence_error);
}

uint8_t *uwb_adv_get_irq1_flag(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t *irq_app1;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_IRQMASK1, &irq_app1, &access_sequence_error);

    return irq_app1;
}

uint8_t *uwb_adv_get_irq2_flag(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t *irq_app2;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_IRQMASK2, &irq_app2, &access_sequence_error);

    return irq_app2;
}

void uwb_adv_set_resistune(radio_adv_t *radio_adv, uint8_t resistune)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RESISTUNE, resistune, &access_sequence_error);
}

void uwb_adv_set_crc(radio_adv_t *radio_adv, uint16_t crc_polynomial)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t crc_high              = EXTRACT_BYTE(crc_polynomial, 1);
    uint8_t crc_low               = EXTRACT_BYTE(crc_polynomial, 0);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_CRCPOLYNOM1, crc_high, &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_CRCPOLYNOM0, crc_low, &access_sequence_error);
}

void uwb_adv_set_main_modem_features(radio_adv_t *radio_adv, main_modem_feat_t *main_modem_feat)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t main_modem_feat_write = 0;

    main_modem_feat_write  = main_modem_feat->modulation |
                             main_modem_feat->fec_lvl    |
                             main_modem_feat->isi_mitig  |
                             main_modem_feat->auto_reply |
                             main_modem_feat->auto_tx;

    radio_adv->shadow_reg.reg_main_modem = main_modem_feat_write;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_MAINMODEM, main_modem_feat_write, &access_sequence_error);
}

void uwb_adv_set_const_gains(radio_adv_t *radio_adv, uint8_t const_gains)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_CONSTGAINS, const_gains, &access_sequence_error);
}

void uwb_adv_enable_phase_tracking(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_value           = MANUPHASE_PHASE_TRACKING_ENABLE;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DEBUGMODEM, write_value, &access_sequence_error);
}

void uwb_adv_disable_phase_tracking(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_value           = 0;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DEBUGMODEM, write_value, &access_sequence_error);
}
void uwb_adv_set_preamble_length(radio_adv_t *radio_adv, uint16_t preamble_size)
{
    uwb_err access_sequence_error  = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t register_preamble_size = (preamble_size / 2) - 8;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PREAMBLEN, register_preamble_size, &access_sequence_error);
}

void uwb_adv_set_syncword_config(radio_adv_t *radio_adv, syncword_cfg_t *cfg)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t syncword[4]           = {0};
    uint8_t write_syncword_cfg    = 0;

    write_syncword_cfg = cfg->syncword_length                              |
                         MOV2MASK(cfg->syncword_bit_cost,  BITS_SWBITCOST) |
                         MOV2MASK(cfg->syncword_tolerance, BITS_SWCORRTOL);

    serialize_uint32_to_uint8_array(cfg->syncword, syncword);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_SYNCWORDCFG, write_syncword_cfg, &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_SYNCWORD0, syncword[3], &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_SYNCWORD1, syncword[2], &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_SYNCWORD2, syncword[1], &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_SYNCWORD3, syncword[0], &access_sequence_error);
}

void uwb_adv_set_tx_packet_size(radio_adv_t *radio_adv, uint8_t packet_size)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TXPKTSIZE, packet_size, &access_sequence_error);
}

void uwb_adv_set_rx_waited_src(radio_adv_t *radio_adv, rx_wait_source_t rx_wait_src)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence,
                                    REG_RXWAITTIME1,
                                    rx_wait_src,
                                    &access_sequence_error);
}

void uwb_adv_set_rx_packet_size(radio_adv_t *radio_adv, uint8_t packet_size)
{
  /** @note  If BIT_SIZEHDRE of register REG_PACKETCFG(0x3E)
   *         is cleared and the user does not send a header
   *         in the payload, it's the only way to tell what
   *         is the payload size.
   */
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXPKTSIZE, packet_size, &access_sequence_error);
}

void uwb_adv_set_pll_startup(radio_adv_t *radio_adv, uint16_t pll_startup_us)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint32_t register_pll_startup = pll_startup_us_to_clock_register(pll_startup_us);

    radio_adv->shadow_reg.reg_pll_startup = register_pll_startup;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PLLSTARTUP, register_pll_startup, &access_sequence_error);
}

void uwb_adv_set_rx_timeout(radio_adv_t *radio_adv, rx_timeout_t *rx_timeout)
{
    uwb_err access_sequence_error           = ACCESS_SEQUENCE_BUSY_ERR;
    uint32_t given_timeout_to_register      = rx_timeout_us_to_register(rx_timeout->rx_period_us);
    uint32_t given_pwr_up_delay_to_register = rx_pwr_up_delay_ns_to_register(rx_timeout->rx_power_up_delay_ns);
    uint8_t rx_period_upper                 = given_timeout_to_register >> 4;
    uint8_t rx_period_lower                 = given_timeout_to_register & 0x0F;

    /* Append rx_power_up_delay to lower register */
    rx_period_lower  = rx_period_lower << 4;
    rx_period_lower |= (given_pwr_up_delay_to_register & 0x0F);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXTIMEOUT0, rx_period_lower, &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXTIMEOUT1, rx_period_upper, &access_sequence_error);
}

void uwb_adv_control_peripherals(radio_adv_t *radio_adv, peripherals_ctrl_t *periph_ctrl)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t periph_ctrl_write     = 0;

    /* Apply only user demands */
    periph_ctrl_write = periph_ctrl->std_spi       |
                        periph_ctrl->flush_dis     |
                        periph_ctrl->vsw_dis       |
                        periph_ctrl->dcdc_dis      |
                        periph_ctrl->pll_dis       |
                        periph_ctrl->symb_clck_src |
                        periph_ctrl->xtal_clck_src |
                        periph_ctrl->outp_xtal;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DISABLES, periph_ctrl_write, &access_sequence_error);
}
void uwb_adv_enable_ext_pll_clk(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t periph_ctrl_write     = radio_adv->shadow_reg.reg_disables;

    periph_ctrl_write &= ~(PLL_DISABLE | SYMBOL_CLOCK_RATE_SOURCE_EXT);
    periph_ctrl_write |= (PLL_DISABLE | SYMBOL_CLOCK_RATE_SOURCE_EXT);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DISABLES, periph_ctrl_write, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_disables = periph_ctrl_write;
}

void uwb_adv_disable_ext_pll_clk(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t periph_ctrl_write     = radio_adv->shadow_reg.reg_disables;

    periph_ctrl_write &= ~(PLL_DISABLE | SYMBOL_CLOCK_RATE_SOURCE_EXT);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DISABLES, periph_ctrl_write, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_disables = periph_ctrl_write;
}

void uwb_adv_enable_ext_xtal_clk(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t periph_ctrl_write     = radio_adv->shadow_reg.reg_disables;

    periph_ctrl_write &= ~XTAL_CLOCK_SOURCE_EXT;
    periph_ctrl_write |= XTAL_CLOCK_SOURCE_EXT;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DISABLES, periph_ctrl_write, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_disables = periph_ctrl_write;
}

void uwb_adv_disable_ext_xtal_clk(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_register_value  = radio_adv->shadow_reg.reg_disables;

    write_register_value &= ~XTAL_CLOCK_SOURCE_EXT;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DISABLES, write_register_value, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_disables = write_register_value;
}

void uwb_adv_enable_integlen(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t dll_tuning_write      = radio_adv->shadow_reg.reg_dll_tuning;

    dll_tuning_write &= ~BIT_INTEGLEN;
    dll_tuning_write |= BIT_INTEGLEN;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DLLTUNING, dll_tuning_write, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_dll_tuning = dll_tuning_write;
}

void uwb_adv_disable_integlen(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t dll_tuning_write      = radio_adv->shadow_reg.reg_dll_tuning;

    dll_tuning_write &= ~BIT_INTEGLEN;
    dll_tuning_write |= 0b000;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_DLLTUNING, dll_tuning_write, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_dll_tuning = dll_tuning_write;
}

void uwb_adv_set_integgain(radio_adv_t *radio_adv, rf_channel_t *channel)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t dll_tuning_write      = radio_adv->shadow_reg.reg_dll_tuning;

    dll_tuning_write &= ~BITS_INTEGGAIN;
    dll_tuning_write |= MOV2MASK(channel->integgain, BITS_INTEGGAIN);

    sr1000_access_adv_append_write(
        &radio_adv->access_sequence, REG_DLLTUNING, dll_tuning_write, &access_sequence_error);

    /* Update shadow_reg to bypass future read */
    radio_adv->shadow_reg.reg_dll_tuning = dll_tuning_write;
}

void uwb_adv_fill_tx_fifo(radio_adv_t *radio_adv, uint8_t *tx_buffer, uint16_t size)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write_burst(&radio_adv->access_sequence, REG_TXFIFO, tx_buffer, size, &access_sequence_error);
}

uint8_t *uwb_adv_read(radio_adv_t *radio_adv, uint8_t size)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t *rx_buffer;

    sr1000_access_adv_append_read_burst(&radio_adv->access_sequence, REG_RXFIFO, &rx_buffer, size, &access_sequence_error);

    return rx_buffer;
}

void uwb_adv_set_tx_buffer_load_irq_threshold(radio_adv_t *radio_adv, uint8_t threshold)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_value           = 0;

    write_value = BIT_TXIRQEN | (threshold & 0x7F);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TXFIFOSTAT, write_value, &access_sequence_error);
}

void uwb_adv_disable_tx_buffer_load_irq(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TXFIFOSTAT, 0, &access_sequence_error);
}

void uwb_adv_set_rx_buffer_load_irq_threshold(radio_adv_t *radio_adv, uint8_t threshold)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t write_value           = 0;


    write_value = BIT_RXIRQEN | (threshold & 0x7F);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXFIFOSTAT, write_value, &access_sequence_error);
}

void uwb_adv_disable_rx_buffer_load_irq(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXFIFOSTAT, 0, &access_sequence_error);
}

void uwb_adv_set_pwr_up_delay(radio_adv_t *radio_adv, uint32_t power_up_delay_ns)
{
    uwb_err access_sequence_error     = ACCESS_SEQUENCE_BUSY_ERR;
    uint32_t power_up_delay_in_cycles = 0;

    power_up_delay_in_cycles = pwr_up_delay_ns_to_register(power_up_delay_ns);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PWRUPDELAY, power_up_delay_in_cycles, &access_sequence_error);
}

void uwb_adv_set_rx_filters(radio_adv_t *radio_adv, uint8_t lna_peak, uint8_t rx_filters)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t rx_filters_value      = MOV2MASK(lna_peak, BITS_LNAPEAK) |
                                    MOV2MASK(rx_filters, BITS_RFFILFREQ);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXFILTERS, rx_filters_value, &access_sequence_error);
}

void uwb_adv_set_cac(radio_adv_t *radio_adv, rx_idle_pwr_t rx_idle_pwr, uint8_t cac_thresh)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t cac_reg_value         = rx_idle_pwr |
                                    MOV2MASK(cac_thresh, BITS_CSTHRES);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_CLEARTOSEND, cac_reg_value, &access_sequence_error);
}

void uwb_adv_set_tx_params(radio_adv_t *radio_adv, tx_params_t *tx_params)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t tx_params_value       = tx_params->hold_tx_on |
                                    tx_params->iff_en     |
                                    tx_params->rnd_phase  |
                                    tx_params->tx_power;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TXPARAMS, tx_params_value, &access_sequence_error);
}

void uwb_adv_set_packet_config(radio_adv_t *radio_adv, packet_cfg_t *packet_cfg)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t packet_cfg_value      = packet_cfg->addr_filt   |
                                    packet_cfg->addr_len    |
                                    packet_cfg->addr_header |
                                    packet_cfg->size_header |
                                    packet_cfg->size_src    |
                                    packet_cfg->save_addr   |
                                    packet_cfg->save_size;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PACKETCFG, packet_cfg_value, &access_sequence_error);
}

void uwb_adv_set_pulse_pattern(radio_adv_t *radio_adv, uint8_t pulse_num, uint8_t pattern)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    /* Linear formula based on number of pulse and register value */
    uint8_t pulse_register = REG_TXPULSE1 + 1 - pulse_num;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, pulse_register, pattern, &access_sequence_error);
}

void uwb_adv_select_channel(radio_adv_t *radio_adv, rf_channel_t *channel)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write_burst(&radio_adv->access_sequence,
                                         REG_RXFILTERS,
                                         (uint8_t *)&channel->channel,
                                         NB_PULSES + 2,
                                         &access_sequence_error);
}

uint8_t *uwb_adv_get_phase_info(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t *phase_info;

    sr1000_access_adv_append_read_burst(&radio_adv->access_sequence,
                                         REG_PHSDATA1,
                                         &phase_info,
                                         REG_PHSDATA4 - REG_PHSDATA1,
                                         &access_sequence_error);

    return phase_info;
}

void uwb_adv_set_noise_floor(radio_adv_t *radio_adv, uint8_t max_adc_signal, uint8_t min_adc_signal)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t noise_floor_value     = MOV2MASK(max_adc_signal, BITS_MAXADCSIG) |
                                    MOV2MASK(min_adc_signal, BITS_MINADCSIG);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PHSDATA1, noise_floor_value, &access_sequence_error);
}

void uwb_adv_tune_preamble(radio_adv_t *radio_adv, preamble_tuning_t *preamble_cfg)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t preamble_tune_value   = MOV2MASK(preamble_cfg->delay_reply, BIT_DLAYRPLY)                  |
                                    MOV2MASK(preamble_cfg->number_preamble_detections, BITS_NUMPREDET) |
                                    MOV2MASK(preamble_cfg->preamble_detect_thresh, BITS_PREATHRES);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PHSDATA2, preamble_tune_value, &access_sequence_error);
}

void uwb_adv_override_gain_loop(radio_adv_t *radio_adv, uint8_t manual_gain)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t gain_loop_value       = MOV2MASK(manual_gain, BITS_MANUGAIN);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_MODEMGAIN, gain_loop_value, &access_sequence_error);
}

void uwb_adv_enable_random_data_source(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t data_src              = BITS_DATASRC | radio_adv->shadow_reg.reg_modem_gain;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_MODEMGAIN, data_src, &access_sequence_error);
}

void uwb_adv_disable_random_data_source(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t data_src              = ~(BITS_DATASRC) | radio_adv->shadow_reg.reg_modem_gain;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_MODEMGAIN, data_src, &access_sequence_error);
}

void uwb_adv_set_modem_debug_feat(radio_adv_t *radio_adv, modem_debug_feat_t *debug_feat)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t debug_feat_value      = debug_feat->override       |
                                    debug_feat->man_phase_sel  |
                                    debug_feat->man_bit_thresh |
                                    debug_feat->bit_thresh_adj;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PHSDATA4, debug_feat_value, &access_sequence_error);
}

rx_wait_time_t uwb_adv_get_rx_wait_time(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    rx_wait_time_t rx_wait_time;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_RXWAITTIME1, &rx_wait_time.rx_wait_time1, &access_sequence_error);
    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_RXWAITTIME0, &rx_wait_time.rx_wait_time0, &access_sequence_error);

    return rx_wait_time;
}

uint8_t *uwb_adv_get_rssi(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t *rssi;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_RSSI, &rssi, &access_sequence_error);

    return rssi;
}

uint8_t *uwb_adv_get_rnsi(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t *rnsi;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, REG_RNSI, &rnsi, &access_sequence_error);

    return rnsi;
}

void uwb_adv_fill_header_non_blocking(radio_adv_t *radio_adv, uint8_t *hdr_buffer, uint8_t hdr_size)
{
    uwb_adv_fill_tx_fifo(radio_adv, hdr_buffer, hdr_size);
    uwb_adv_transfer_non_blocking(radio_adv);
}

void uwb_adv_fill_header_blocking(radio_adv_t *radio_adv, uint8_t *hdr_buffer, uint8_t hdr_size)
{
    uwb_adv_fill_tx_fifo(radio_adv, hdr_buffer, hdr_size);
    uwb_adv_transfer_blocking(radio_adv);
}

void uwb_adv_fill_data_non_blocking(radio_adv_t *radio_adv, uint8_t *tx_buffer, uint16_t size)
{
    sr1000_access_adv_spi_transfer_non_blocking(radio_adv->radio_hal_adv, tx_buffer,
                                                radio_adv->access_sequence.rx_buffer, size);
}

void uwb_adv_fill_data_blocking(radio_adv_t *radio_adv, uint8_t *tx_buffer, uint16_t size)
{
    sr1000_access_adv_spi_transfer_blocking(radio_adv->radio_hal_adv, tx_buffer, radio_adv->access_sequence.rx_buffer,
                                            size);
}

void uwb_adv_fill_cut_through_data(radio_adv_t *radio_adv, uint8_t *buffer, uint8_t size)
{
    uwb_adv_fill_tx_fifo(radio_adv, buffer, size);
}

void uwb_adv_transfer_non_blocking(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_transfer_non_blocking(radio_adv->radio_hal_adv, &radio_adv->access_sequence,
                                            &access_sequence_error);
}

void uwb_adv_transfer_blocking(radio_adv_t *radio_adv)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_transfer_blocking(radio_adv->radio_hal_adv, &radio_adv->access_sequence, &access_sequence_error);
}

void uwb_adv_update_wake_sleep_period(radio_adv_t *radio_adv, sleep_lvl_t *sleep_lvl, uint32_t sleep_time_us)
{
    uint32_t register_sleep_period = 0;

    switch (*sleep_lvl) {
    case SLEEP_IDLE:
        register_sleep_period = wake_sleep_us_to_reg_when_idle(sleep_time_us);
        break;
    case SLEEP_SHALLOW:
        register_sleep_period = wake_sleep_us_to_reg_when_shallow(sleep_time_us);
        break;
    case SLEEP_DEEP:
        register_sleep_period = wake_sleep_us_to_reg_when_deep(sleep_time_us, radio_adv->shadow_reg.reg_pll_startup);
        break;
    default:
        register_sleep_period = wake_sleep_us_to_reg_when_idle(sleep_time_us);
        break;
    }

     radio_adv->radio_sleep_time.sleep_cycles = register_sleep_period;
}

void uwb_adv_set_wake_sleep_raw(radio_adv_t *radio_adv, uint16_t sleep_time)
{
    uwb_err access_sequence_error    = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t wake_sleep_period_raw[2] = {0};

    serialize_uint16_to_uint8_array(sleep_time, wake_sleep_period_raw);

    /* Sending High value - LSB - MSB to ensure no issues when using normal SPI transfer */
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCOUNT1, 0xFF, &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCOUNT0, wake_sleep_period_raw[1], &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_TIMERCOUNT1, wake_sleep_period_raw[0], &access_sequence_error);
}

void uwb_adv_set_pwr_up_delay_raw(radio_adv_t *radio_adv, int16_t pwr_up)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t pwr_up_reg_value      = pwr_up / 4;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_PWRUPDELAY, pwr_up_reg_value, &access_sequence_error);
}

void uwb_adv_set_rx_timeout_raw(radio_adv_t *radio_adv, uint32_t rx_timeout, uint8_t rx_pwr_up_delay)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;
    uint8_t rx_timeout_lsb        = (((EXTRACT_BYTE((rx_timeout / 8), 0) & 0x0F) << 4) | (rx_pwr_up_delay & 0x0F));
    uint8_t rx_timeout_msb        = (EXTRACT_BYTE((rx_timeout / 8), 1) << 4) |
                                    (EXTRACT_BYTE((rx_timeout / 8), 0) >> 4);

    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXTIMEOUT1, rx_timeout_msb, &access_sequence_error);
    sr1000_access_adv_append_write(&radio_adv->access_sequence, REG_RXTIMEOUT0, rx_timeout_lsb, &access_sequence_error);
}

last_addr_t uwb_adv_get_last_rcv_frame_addr(radio_adv_t *radio_adv)
{
    last_addr_t last_rcv_address;
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_read(&radio_adv->access_sequence,
                                  REG_FRAMEADDR1,
                                  &last_rcv_address.msb,
                                  &access_sequence_error);
    sr1000_access_adv_append_read(&radio_adv->access_sequence,
                                  REG_FRAMEADDR0,
                                  &last_rcv_address.lsb,
                                  &access_sequence_error);

    return last_rcv_address;
}

bool uwb_adv_is_spi_busy(radio_adv_t *radio_adv)
{
    return sr1000_access_adv_is_spi_busy(radio_adv->radio_hal_adv);
}

void uwb_adv_write_register(radio_adv_t *radio_adv, uint8_t target_reg, uint8_t value)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write(&radio_adv->access_sequence, target_reg, value, &access_sequence_error);
}

void uwb_adv_burst_write(radio_adv_t *radio_adv, uint8_t starting_reg, uint8_t *buffer, uint8_t buffer_size)
{
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_write_burst(&radio_adv->access_sequence, starting_reg, buffer, buffer_size, &access_sequence_error);
}

uint8_t *uwb_adv_read_register(radio_adv_t *radio_adv, uint8_t target_reg)
{
    uint8_t *read_value_ptr;
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_read(&radio_adv->access_sequence, target_reg, &read_value_ptr, &access_sequence_error);

    return read_value_ptr;
}

uint8_t *uwb_adv_burst_read(radio_adv_t *radio_adv, uint8_t starting_reg, uint8_t read_size)
{
    uint8_t *read_value_ptr;
    uwb_err access_sequence_error = ACCESS_SEQUENCE_BUSY_ERR;

    sr1000_access_adv_append_read_burst(&radio_adv->access_sequence,
                                        starting_reg,
                                        &read_value_ptr,
                                        read_size,
                                        &access_sequence_error);

    return read_value_ptr;
}
/* PRIVATE FUNCTION PROTOTYPES ************************************************/

/** @brief Convert given PLL startup time (us)
 *         to the equivalent register value.
 *
 *  @param[in] uint32_t  PLL startup time, in us.
 *  @return Equivalent 8-bit register value.
 */
static uint8_t pll_startup_us_to_clock_register(uint32_t pll_startup_us)
{
    return (pll_startup_us * CRYSTAL_CLOCK_PERIOD) / (CLOCK_FACTOR * CLOCK_FACTOR);
}

/** @brief Convert given RX timeout (us)
 *         to the equivalent register value.
 *
 *  @param[in] rx_period_us  RX timeout period, in us.
 *  @return Equivalent 16-bit register value.
 */
static uint16_t rx_timeout_us_to_register(uint32_t rx_period_us)
{
    return ((rx_period_us * SYMBOL_CLOCK_PERIOD) / (CLOCK_FACTOR) - 1) / 8;
}

/** @brief Convert given RX power-up delay (ns)
 *         to the equivalent register value.
 *
 *  @param[in] rx_power_up_delay_ns  RX power-up delay, in ns.
 *  @return Equivalent 4-bit register value.
 */
static uint8_t rx_pwr_up_delay_ns_to_register(uint16_t rx_power_up_delay_ns)
{
    return ((rx_power_up_delay_ns * SYMBOL_CLOCK_PERIOD) / (CLOCK_FACTOR * CLOCK_FACTOR)) - 1;
}

/** @brief Convert given RX pause time (us)
 *         to the equivalent register value.
 *
 *  @param[in] rx_pause_time_us  Receiver pause time, in us.
 *  @return Equivalent 8-bit register value.
 */
static uint8_t rx_pause_time_pll_cycles_to_register(uint32_t rx_pause_time_pll_cycles)
{
    int16_t raw_value      = (rx_pause_time_pll_cycles / 4) - 1;
    uint8_t register_value = clamp_8_bit_value(raw_value);

    return register_value;
}

/** @brief Convert given power-up delay (ns)
 *         to the equivalent register value.
 *
 *  @param[in] pwr_up_delay_ns  Power-up delay, in ns.
 *  @return Equivalent 8-bit register value.
 */
static uint8_t pwr_up_delay_ns_to_register(uint32_t pwr_up_delay_ns)
{
    return (pwr_up_delay_ns * SYMBOL_CLOCK_PERIOD) / (4 * CLOCK_FACTOR * CLOCK_FACTOR);
}

/** @brief Clamp 8-bit max and min to the given value.
 *
 *  @param[in] value  Value to be clamped.
 *  @return Clamped value, if necessary.
 */
static uint8_t clamp_8_bit_value(int16_t value)
{
    if (value > 255) {
        return 255;
    } else if (value < 0) {
        return 0;
    }
    return (uint8_t)value;
}

/** @brief Convert given sleep time when sleep level is set to idle.
 *
 *  @param[in] sleep_time_us  Desired sleep time, in us.
 *  @return Register value.
 */
static uint32_t wake_sleep_us_to_reg_when_idle(uint32_t sleep_time_us)
{
    uint8_t sleep_offset = 1; /* Datasheet offset */

    uint32_t register_sleep_period = sleep_time_us * SYMBOL_CLOCK_PERIOD / (CLOCK_FACTOR);

    if (register_sleep_period <= sleep_offset) {
        register_sleep_period = sleep_offset;
    }
    /* Apply offset to computed sleep period*/
    register_sleep_period -= sleep_offset;

    /* Clamp 16-bit value */
    if (register_sleep_period > 65535) {
        register_sleep_period = 65535;
    }

    return register_sleep_period;
}

/** @brief Convert given sleep time when sleep level is set to shallow.
 *
 *  @param[in] sleep_time_us  Desired sleep time, in us.
 *  @return Register value.
 */
static uint32_t wake_sleep_us_to_reg_when_shallow(uint32_t sleep_time_us)
{
    uint8_t sleep_offset = 1; /* Datasheet offset */
    uint32_t register_sleep_period = ((uint64_t)sleep_time_us * (uint64_t)CRYSTAL_CLOCK_PERIOD /
                                     ((uint64_t)CLOCK_FACTOR * (uint64_t)CLOCK_FACTOR));

    if (register_sleep_period <= sleep_offset) {
        register_sleep_period = sleep_offset;
    }

    /* Apply offset to computed value */
    register_sleep_period -= sleep_offset;

    /* Clamp 16-bit value */
    if (register_sleep_period > 65535) {
        register_sleep_period = 65535;
    }

    return register_sleep_period;
}

/** @brief Convert given sleep time when sleep level is set to deep.
 *
 *  @param[in] sleep_time_us  Desired sleep time, in us.
 *  @return Register value.
 */
static uint32_t wake_sleep_us_to_reg_when_deep(uint32_t sleep_time_us, uint8_t pll_startup_reg_value)
{
    uint8_t sleep_offset = pll_startup_reg_value + 2;  /* Datasheet offset */
    uint32_t register_sleep_period = (uint64_t)sleep_time_us * (uint64_t)CRYSTAL_CLOCK_PERIOD /
                                     ((uint64_t)CLOCK_FACTOR * (uint64_t)CLOCK_FACTOR);

    if (register_sleep_period <= sleep_offset) {
        register_sleep_period = sleep_offset;
    }

    /* Apply offset to computed value */
    register_sleep_period -= sleep_offset;

    /* Clamp 16-bit value */
    if (register_sleep_period > 65535) {
        register_sleep_period = 65535;
    }

    return register_sleep_period;
}
