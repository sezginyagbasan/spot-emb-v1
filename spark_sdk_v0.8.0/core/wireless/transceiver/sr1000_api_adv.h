/** @file sr1000_api_adv.h
 *  @brief Radio level advanced application programming interface.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_API_ADV_H_
#define SR1000_API_ADV_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "sr1000_access_adv.h"
#include "sr1000_def.h"
#include "sr1000_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/* MACRO **********************************************************************/
#ifndef UWB_ERR_TYPE
#define UWB_ERR_TYPE
typedef uint32_t uwb_err;
#endif

#ifndef ERR_BASE_API_ADV
#error Set the base error index in your project symbols.
#endif

/* ERROR **********************************************************************/
#define API_ADV_NO_ERROR                      ((ERR_BASE_API_ADV << ERR_BASE_POS) + 0x0000) /**< No error */
#define API_ADV_RADIO_NOT_INITIALIZED         ((ERR_BASE_API_ADV << ERR_BASE_POS) + 0x0001) /**< API radio's instance has not been
                                                                                              * initialized prior to the API
                                                                                              * advance initialization
                                                                                              */
#define API_ADV_BUFFER_LOAD_THRESHOLD_TOO_BIG ((ERR_BASE_API_ADV << ERR_BASE_POS) + 0x0002) /**< Buffer load threshold is higher
                                                                                              * than what the register can store
                                                                                              */
#define API_ADV_ERROR_DIVISION_BY_ZERO        ((ERR_BASE_API_ADV << ERR_BASE_POS) + 0x0003) /**< Division by zero */

/* TYPES **********************************************************************/
/** @brief API advance shadow register.
 *
 *  @note This is use by a couple of api_adv function
 *        when a register has been changed but not written
 *        to yet.
 */
typedef struct shadow_register {
    uint8_t irq_mask1;       /**< Current value in REG_IRQMASK1 */
    uint8_t irq_mask2;       /**< Current value in REG_IRQMASK2 */
    uint8_t reg_packet_cfg;  /**< Current value in REG_PACKETCFG */
    uint8_t reg_actions;     /**< Current value in REG_ACTIONS */
    uint8_t reg_disables;    /**< Current value in REG_DISABLES */
    uint8_t reg_dll_tuning;  /**< Current value in REG_DLLTUNING */
    uint8_t reg_pll_startup; /**< Current value in REG_PLLSTARTUP */
    uint8_t reg_modem_gain;  /**< Current value in REG_MODEMGAIN */
    uint8_t reg_main_modem;  /**< Current value in REG_MAINMODEM */
} radio_shadow_reg_t;

/** @brief Internal sleep time struct for related register.
 */
typedef struct sleep_time {
    uint32_t sleep_cycles;    /**< Wake sleep period in clock cycle depending on the sleep level */
    uint16_t timeout_cycles;  /**< Timeout cycles for the REG_RXTIMEOUT1 */
    int16_t  pwr_up;          /**< Receiver power up delay */
} sleep_time_t;

/** @brief Radio's advance instance.
 */
typedef struct api_adv_struct {
    access_sequence_instance_t  access_sequence;  /**< Sequence to be written/read from the communication bus */
    radio_shadow_reg_t          shadow_reg;       /**< Shadow register instance */
    radio_hal_adv_t            *radio_hal_adv;    /**< Radio HAL advance instance */
    radio_t                    *radio;            /**< Radio's instance */
    sleep_time_t                radio_sleep_time; /**< Sleep/time related instance */
} radio_adv_t;

/** @brief Radio timer configuration instance.
 */
typedef struct api_adv_radio_timer_config {
    auto_wake_t      auto_wake;  /**< Timer configuration AUTOWAKE field */
    wake_up_once_t   wake_once;  /**< Timer configuration WAKEONCE field */
    sync_at_end_t    syn_at_end; /**< Timer configuration SYNATEND field */
    sync_packet_tx_t syn_pkt_tx; /**< Timer configuration SYNPKTTX field */
    sync_packet_rx_t syn_pkt_rx; /**< Timer configuration SYNPKTRX field */
    sync_match_t     syn_match;  /**< Timer configuration SYNMATCH field */
    sync_brdca_t     syn_brdca;  /**< Timer configuration SYNBRDCA field */
    sync_rx_crc_t    syn_rx_crc; /**< Timer configuration SYNRXCRC field */
} radio_timer_config_t;

/** @brief Radio actions instance.
 */
typedef struct api_adv_actions {
    cal_dcro_t   cal_dcro;   /**< Power status and commands register field CAL_DCRO */
    skip_wake_t  skip_wake;  /**< Power status and commands register field SKIPWAKE */
    rx_mode_t    rx_mode;    /**< Power status and commands register field RX_MODE */
    start_tx_t   start_tx;   /**< Power status and commands register field START_TX */
    init_timer_t init_timer; /**< Power status and commands register field INITTIMER */
    go_sleep_t   go_sleep;   /**< Power status and commands register field GO_SLEEP */
    flush_rx_t   flush_rx;   /**< Power status and commands register field FLUSH_RX */
    flush_tx_t   flush_tx;   /**< Power status and commands register field FLUSH_TX */
} radio_actions_t;

/** @brief Radio main features instance.
 */
typedef struct main_modem_feat {
    modulation_t modulation; /**< Modulation type */
    fec_level_t  fec_lvl;    /**< FEC level settings */
    isi_mitig_t  isi_mitig;  /**< Inter-symbol interference mitigation settings */
    auto_reply_t auto_reply; /**< Enable/Disable auto-reply */
    auto_tx_t    auto_tx;    /**< Enable/Disable automatic transmission of predetermined pattern */
} main_modem_feat_t;

/** @brief Receiver timeout and power-up duration instance.
 */
typedef struct rx_timeout {
    uint32_t rx_period_us;         /**< Receiver timeout, in us */
    uint16_t rx_power_up_delay_ns; /**< Receiver power-up delay, in ns */
} rx_timeout_t;

/** @brief Peripherals control instance.
 */
typedef struct periph_ctrl {
    std_spi_t                std_spi;       /**< Enable/Disable standard SPI operation */
    flush_dis_t              flush_dis;     /**< Enable/Disable automatic RX buffer flush on rejected packet */
    one_vsw_dis_t            vsw_dis;       /**< Enable/Disable 1 volt capacitor switch */
    dcdc_conv_dis_t          dcdc_dis;      /**< Enable/Disable DC/DC converter */
    pll_dis_t                pll_dis;       /**< Enable/Disable PLL clock source */
    symbol_rate_clk_source_t symb_clck_src; /**< Symbol rate clock source */
    xtal_osc_clk_source_t    xtal_clck_src; /**< Crystal rate clock source */
    output_xtal_t            outp_xtal;     /**< Enable/Disable output of crystal clock source */
} peripherals_ctrl_t;

/** @brief Share pulse parameter instance.
 */
typedef struct shared_pulse_param {
    hold_tx_on_t      hold_tx_on;     /**< Enable/Disable hold of transmitter data signal */
    rnd_phase_t       rnd_phase;      /**< Enable/Disable randomization of pulse phase */
    if_filt_en_t      iff_en;         /**< Enable/Disable the intermediate-frequency filter */
    tx_params_power_t tx_power;       /**< Transmission power settings */
    uint8_t           vdd_level_read; /**< Read only VDD level */
} tx_params_t;

/** @brief Packet (MAC layer) configuration instance.
 */
typedef struct mac_layer_config {
    addr_filt_t      addr_filt;   /**< Address filtering configuration */
    address_length_t addr_len;    /**< Local address length (8-bit or 16-bit) */
    addr_hdre_t      addr_header; /**< Enable/Disable address field in packet header */
    size_hdre_t      size_header; /**< Enable/Disable payload size field in packet header */
    size_src_t       size_src;    /**< Transmitted frames payload size source */
    save_addr_t      save_addr;   /**< Enable/Disable save of frame address field in reception buffer */
    save_size_t      save_size;   /**< Enable/Disable save of payload size field in reception buffer */
} packet_cfg_t;

/** @brief Preamble tuning instance
 */
typedef struct preamble_tuning {
    bool    delay_reply;                /**< Enable/Disable delay of auto-reply frame */
    uint8_t number_preamble_detections; /**< Number of consecutive preamble detections */
    uint8_t preamble_detect_thresh;     /**< Preamble detection threshold */
} preamble_tuning_t;

/** @brief Modem debug features instance.
 */
typedef struct modem_debug {
    modem_debug_override_t override;       /**< Enable/Disable override modem */
    manuphase_t            man_phase_sel;  /**< Manual receiver phase selection / phase tracking enable */
    man_bit_thresh_t       man_bit_thresh; /**< Manual bit decision threshold */
    uint8_t                bit_thresh_adj; /**< Bit decision threshold adjustment */
} modem_debug_feat_t;

/** @brief Receiver wait time reception buffer instance.
 */
typedef struct rx_wait_time {
    uint8_t *rx_wait_time0; /**< 8-bit LSB value of the 16-bit register. */
    uint8_t *rx_wait_time1; /**< 8-bit MSB value of the 16-bit register. */
} rx_wait_time_t;

/** @brief Last received address instance.
 */
typedef struct last_addr {
    uint8_t *msb; /**< 8-bit LSB value of the 16-bit register. */
    uint8_t *lsb; /**< 8-bit MSB value of the 16-bit register. */
} last_addr_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize the api advance.
 *
 *  @param[out] radio_adv            Radio's advance instance.
 *  @param[in]  radio_hal_adv        Radio HAL advance instance.
 *  @param[in]  radio                Radio's instance.
 *  @param[in]  app_spi_rx_buffer    Application buffer for SPI reception.
 *  @param[in]  app_spi_tx_buffer    Application buffer for SPI transmission.
 *  @param[in]  app_spi_buffer_size  Application buffer size.
 *  @param[out] error                Api advance error instance.
 */
void uwb_adv_init(radio_adv_t     *radio_adv,
                  radio_hal_adv_t *radio_hal_adv,
                  radio_t         *radio,
                  uint8_t         *app_spi_rx_buffer,
                  uint8_t         *app_spi_tx_buffer,
                  uint8_t          app_spi_buffer_size,
                  uwb_err         *error);

/** @brief Reset the shadow register values to the internal radio values.
 *
 *  @param[in] radio_adv Radio's advance instance.
 */
void uwb_adv_set_shadow_reg(radio_adv_t *radio_adv);

/** @brief Set the radio destination address.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] address       Destination address.
 *  @param[in] address_size  false for 8-bit address, true for 16-bit address.
 */
void uwb_adv_set_destination_address(radio_adv_t *radio_adv, uint16_t address, address_length_t address_size);

/** @brief Set the radio local address.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] address       Local address.
 *  @param[in] address_size  false for 8-bit address, true for 16-bit address.
 */
void uwb_adv_set_local_address(radio_adv_t *radio_adv, uint16_t address, address_length_t address_size);

/** @brief Enable the radio sleep timer.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_enable_modem_wakeup_timer(radio_adv_t *radio_adv);

/** @brief Disable the radio sleep timer.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_modem_wakeup_timer(radio_adv_t *radio_adv);

/** @brief Set the radio off time interval.
 *
 *  @param[in] radio_adv      Radio's advance instance.
 *  @param[in] rx_pause_time  Receiver off time interval, in pll cycles.
 */
void uwb_adv_set_rx_pause_time(radio_adv_t *radio_adv, uint16_t rx_pause_time);

/** @brief Configure the radio timer.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] timer_config  Timer configuration instance.
 */
void uwb_adv_set_timer_config(radio_adv_t *radio_adv, radio_timer_config_t *timer_config);

/** @brief Disable the radio timer.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 */
void uwb_adv_disable_timer_config(radio_adv_t *radio_adv);

/** @brief Set the main IRQ register flag.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] radio_events  Radio's event instance.
 */
void uwb_adv_set_irq1_flag(radio_adv_t *radio_adv, radio_events_t *radio_events);

/** @brief Set the auxiliary IRQ register flag.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] radio_events  Radio's event instance.
 */
void uwb_adv_set_irq2_flag(radio_adv_t *radio_adv, radio_events_t *radio_events);

/** @brief Disable all the main IRQ register flag.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 */
void uwb_adv_disable_irq1(radio_adv_t *radio_adv);

/** @brief Disable all the auxiliary IRQ register flag.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 */
void uwb_adv_disable_irq2(radio_adv_t *radio_adv);

/** @brief Setup the radio sleep level and
 *         the sleep trigger event.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] level      Sleep level instance.
 *  @param[in] events     Sleep event instance.
 */
void uwb_adv_set_sleep_config(radio_adv_t *radio_adv, sleep_lvl_t *level, sleep_events_t *events);

/** @brief Setup the device sleep period.
 *
 *  @note This need to be call after the
 *        uwb_adv_update_wake_sleep_period in
 *        order to update the register value.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_set_wake_sleep_period(radio_adv_t *radio_adv);

/** @brief Sent specific commands to the radio.
 *
 *  @param[in] radio_adv             Radio's advance instance.
 *  @param[in] target_radio_actions  Radio actions instance.
 */
void uwb_adv_set_radio_actions(radio_adv_t *radio_adv, radio_actions_t *target_radio_actions);

/** @brief Get main IRQ status flag.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @return Pointer containing the main IRQ flag register.
 */
uint8_t *uwb_adv_get_irq1_flag(radio_adv_t *radio_adv);

/** @brief Get auxiliary IRQ status flag.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @return Pointer containing the auxiliary IRQ flag register.
 */
uint8_t *uwb_adv_get_irq2_flag(radio_adv_t *radio_adv);

/** @brief Configure the calibration resistors and impedance tuning.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] resistune  Resitune value.
 */
void uwb_adv_set_resistune(radio_adv_t *radio_adv, uint8_t resistune);

/** @brief Set the payload CRC polynomial.
 *
 *  @param[in] radio_adv       Radio's advance instance.
 *  @param[in] crc_polynomial  Target CRC polynomial.
 */
void uwb_adv_set_crc(radio_adv_t *radio_adv, uint16_t crc_polynomial);

/** @brief Set the radio main features.
 *
 *  @param[in] radio_adv        Radio's advance instance.
 *  @param[in] main_modem_feat  Radio main features instance.
 */
void uwb_adv_set_main_modem_features(radio_adv_t *radio_adv, main_modem_feat_t *main_modem_feat);

/** @brief Set the radio constant gains.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] const_gains  Constant gains value.
 */
void uwb_adv_set_const_gains(radio_adv_t *radio_adv, uint8_t const_gains);

/** @brief Enable the radio phase tracking.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_enable_phase_tracking(radio_adv_t *radio_adv);

/** @brief Disable the radio phase tracking.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_phase_tracking(radio_adv_t *radio_adv);

/** @brief Configure the frame preamble length.
 *
 *  @param[in] radio_adv      Radio's advance instance.
 *  @param[in] preamble_size  Preamble size.
 */
void uwb_adv_set_preamble_length(radio_adv_t *radio_adv, uint16_t preamble_size);

/** @brief Configure the frame syncword.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] cfg        Syncword configuration instance.
 */
void uwb_adv_set_syncword_config(radio_adv_t *radio_adv, syncword_cfg_t *cfg);

/** @brief Configure the TX payload size.
 *
 *  @note Useless if BIT_SIZESRC of register REG_PACKETCFG(0x3E)
 *        is set.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] packet_size  TX payload size.
 */
void uwb_adv_set_tx_packet_size(radio_adv_t *radio_adv, uint8_t packet_size);

/** @brief Configure the RX waited source.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] rx_wait_src  RX waited source.
 */
void uwb_adv_set_rx_waited_src(radio_adv_t *radio_adv, rx_wait_source_t rx_wait_src);

/** @brief Configure the RX payload size.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] packet_size  RX payload size.
 */
void uwb_adv_set_rx_packet_size(radio_adv_t *radio_adv, uint8_t packet_size);

/** @brief Configure the phase-locked loop power-up waiting time.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] pll_startup  PLL power-up waiting time, in us.
 *
 *  @note The PLL startup time have a resolution of 30.51us
 */
void uwb_adv_set_pll_startup(radio_adv_t *radio_adv, uint16_t pll_startup);

/** @brief Configure the receiver timeout and the power-up duration.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] rx_timeout  RX timeout and power-up duration instance.
 *
 *  @note RX timeout is extended to 12-bit and the power-up duration
 *        is 4-bit long.
 *        The timeout have a resolution of 390.625025 ns per bit.
 *        Receiver power up delay have a resolution of 48.82815 ns per bit
 */
void uwb_adv_set_rx_timeout(radio_adv_t *radio_adv, rx_timeout_t *rx_timeout);

/** @brief Control the radio peripherals.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] periph_ctrl  Peripherals control instance.
 */
void uwb_adv_control_peripherals(radio_adv_t *radio_adv, peripherals_ctrl_t *periph_ctrl);

/** @brief Enable the external PLL clock.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_enable_ext_pll_clk(radio_adv_t *radio_adv);

/** @brief Disable the external PLL clock.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_ext_pll_clk(radio_adv_t *radio_adv);

/** @brief Enable the external crystal source clock.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_enable_ext_xtal_clk(radio_adv_t *radio_adv);

/** @brief Disable the external crystal source clock.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_ext_xtal_clk(radio_adv_t *radio_adv);

/** @brief Enable the receiver integration window length.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_enable_integlen(radio_adv_t *radio_adv);

/** @brief Disable the receiver integration window length.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_integlen(radio_adv_t *radio_adv);

/** @brief Set the receiver integrators gain.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] channel    Channel to use.
 */
void uwb_adv_set_integgain(radio_adv_t *radio_adv, rf_channel_t *channel);

/** @brief Fill the radio TX FIFO in BURST mode.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] tx_buffer  Buffer to transmit OTA.
 *  @param[in] size       Size of the transmission.
 */
void uwb_adv_fill_tx_fifo(radio_adv_t *radio_adv, uint8_t *tx_buffer, uint16_t size);

/** @brief Read an packet from the radio's RX FIFO in BURST mode.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] size       Reception size.
 *  @return RX data pointer
 */
uint8_t *uwb_adv_read(radio_adv_t *radio_adv, uint8_t size);

/** @brief Configure the TX buffer load IRQ threshold.
 *
 *  @param[in]  radio_adv  Radio's advance instance.
 *  @param[in]  threshold  Buffer load threshold.
 */
void uwb_adv_set_tx_buffer_load_irq_threshold(radio_adv_t *radio_adv, uint8_t threshold);

/** @brief Disable the TX buffer load IRQ.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_tx_buffer_load_irq(radio_adv_t *radio_adv);

/** @brief Configure the RX buffer load IRQ threshold.
 *
 *  @param[in]  radio_adv  Radio's advance instance.
 *  @param[in]  threshold  Buffer load threshold.
 */
void uwb_adv_set_rx_buffer_load_irq_threshold(radio_adv_t *radio_adv, uint8_t threshold);

/** @brief Disable the RX buffer load IRQ.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_rx_buffer_load_irq(radio_adv_t *radio_adv);

/** @brief Set the radio power up delay.
 *
 *  @param[in] radio_adv       Radio's advance instance.
 *  @param[in] power_up_delay  Power up delay, in ns.
 *
 *  @note The register have a resolution of approximately
 *        195 ns per register increment.
 */
void uwb_adv_set_pwr_up_delay(radio_adv_t *radio_adv, uint32_t power_up_delay_us);

/** @brief Set the register REG_RXFILTERS.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] lna_peak    Low noise amplifier peak value. BIT(7..5).
 *  @param[in] rx_filters  RX filters frequency. BIT(4..0).
 */
void uwb_adv_set_rx_filters(radio_adv_t *radio_adv, uint8_t lna_peak, uint8_t rx_filters);

/** @brief Set collision avoidance parameters.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] rx_idle_pwr  Receiver's idle power consumption. BIT(7..6).
 *  @param[in] cac_thresh   Collision-avoidance check threshold. BIT(5..0).
 */
void uwb_adv_set_cac(radio_adv_t *radio_adv, rx_idle_pwr_t rx_idle_pwr, uint8_t cac_thresh);

/** @brief Set the radio shared pulse parameters.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] tx_params  Shared pulse parameters structure.
 */
void uwb_adv_set_tx_params(radio_adv_t *radio_adv, tx_params_t *tx_params);

/** @brief Setup the packet configuration(MAC layer).
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] packet_cfg  Packet configuration struct.
 */
void uwb_adv_set_packet_config(radio_adv_t *radio_adv, packet_cfg_t *packet_cfg);

/** @brief Setup one pulse configuration.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] pulse_num   Pulse number.
 *  @param[in] pattern     Pulse width(7..5) and frequency(4..0).
 */
void uwb_adv_set_pulse_pattern(radio_adv_t *radio_adv, uint8_t pulse_num, uint8_t pattern);

/** @brief Select the radio frequency channel to use in BURST mode.
 *
 *  @note This function is very similar to the API one.
 *        It only allow the user the append this burst write
 *        to a sequence.
 *        Also, this method should be only call after the API
 *        call uwb_config_channel().
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] channel    Channel to use.
 */
void uwb_adv_select_channel(radio_adv_t *radio_adv, rf_channel_t *channel);

/** @brief Get radio phase information (4 phases) in BURST mode.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @return Pointer that will contains the received phase info.
 */
uint8_t *uwb_adv_get_phase_info(radio_adv_t *radio_adv);

/** @brief Set the target noise floor.
 *
 *  @param[in] radio_adv       Radio's advance instance.
 *  @param[in] max_adc_signal  Maximum average ADC output signal level.
 *  @param[in] min_adc_signal  Minimum average ADC output signal level.
 */
void uwb_adv_set_noise_floor(radio_adv_t *radio_adv, uint8_t max_adc_signal, uint8_t min_adc_signal);

/** @brief Tune the message preamble.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] preamble_cfg  Preamble's tuning instance.
 */
void uwb_adv_tune_preamble(radio_adv_t *radio_adv, preamble_tuning_t *preamble_cfg);

/** @brief Override the automatic gain control feedback loop.
 *
 *  @param[in] radio_adv    Radio's advance instance.
 *  @param[in] manual_gain  Manual gain control.
 */
void uwb_adv_override_gain_loop(radio_adv_t *radio_adv, uint8_t manual_gain);

/** @brief Enable data source to be the random predetermined one.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_enable_random_data_source(radio_adv_t *radio_adv);

/** @brief Disable random data source and use TX FIFO source instead.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_disable_random_data_source(radio_adv_t *radio_adv);

/** @brief Configure the modem debug features.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] debug_feat  Radio's debug feature instance.
 */
void uwb_adv_set_modem_debug_feat(radio_adv_t *radio_adv, modem_debug_feat_t *debug_feat);

/** @brief Get the content of the REG_RXWAITTIME1..0.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @return RX wait time structure
 */
rx_wait_time_t uwb_adv_get_rx_wait_time(radio_adv_t *radio_adv);

/** @brief Get the last received signal strength indicator.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @return Last received frame RSSI.
 */
uint8_t *uwb_adv_get_rssi(radio_adv_t *radio_adv);

/** @brief Get the last received noise strength indicator.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @return Last received frame RNSI.
 */
uint8_t *uwb_adv_get_rnsi(radio_adv_t *radio_adv);

/** @brief Fill the header field in the radio TX_FIFO.
 *
 *  This function do not work with any of the other append function.
 *  It should be call right before calling the fill_data function.
 *  It provided an optimized way to fill the TX FIFO using a
 *  header and a payload.
 *
 *  @note The header field can be null. This function is required
 *        in conjunction with the fill_data function in order to
 *        achieve optimal filling of the radio TX FIFO.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] hdr_buffer  Header to transmit OTA.
 *  @param[in] hdr_size    Size of the header.
 */
void uwb_adv_fill_header_non_blocking(radio_adv_t *radio_adv, uint8_t *hdr_buffer, uint8_t hdr_size);

/** @brief Fill the header field in the radio TX_FIFO in blocking mode.
 *
 *  This function do not work with any of the other append function.
 *  It should be call right before calling the fill_data function.
 *  It provided an optimized way to fill the TX FIFO using a
 *  header and a payload.
 *
 *  @note The header field can be null. This function is required
 *        in conjunction with the fill_data function in order to
 *        achieve optimal filling of the radio TX FIFO.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] hdr_buffer  Header to transmit OTA.
 *  @param[in] hdr_size    Size of the header.
 */
void uwb_adv_fill_header_blocking(radio_adv_t *radio_adv, uint8_t *hdr_buffer, uint8_t hdr_size);

/** @brief Fill the radio TX FIFO in non-blocking mode.
 *
 *  This function do not work with any of the other append function.
 *  It should be call right after calling the fill_header function.
 *  It provided an optimized way to fill the TX FIFO using a
 *  header and a payload.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] tx_buffer  Buffer to transmit OTA.
 *  @param[in] size       Size of the transmission.
 */
void uwb_adv_fill_data_non_blocking(radio_adv_t *radio_adv, uint8_t *tx_buffer, uint16_t size);

/** @brief Fill the radio TX FIFO in blocking mode.
 *
 *  This function do not work with any of the other append function.
 *  It should be call right after calling the fill_header function.
 *  It provided an optimized way to fill the TX FIFO using a
 *  header and a payload.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] tx_buffer  Buffer to transmit OTA.
 *  @param[in] size       Size of the transmission.
 */
void uwb_adv_fill_data_blocking(radio_adv_t *radio_adv, uint8_t *tx_buffer, uint16_t size);

/** @brief Fill the radio TX FIFO for the specific cut through mode case.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] tx_buffer  Buffer to transmit OTA.
 *  @param[in] size       Size of the transmission.
 */
void uwb_adv_fill_cut_through_data(radio_adv_t *radio_adv, uint8_t *buffer, uint8_t size);

/** @brief Initiate a transfer in non blocking mode
 *         using all the appended sequence.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_transfer_non_blocking(radio_adv_t *radio_adv);

/** @brief Initiate a transfer in blocking mode
 *         using all the appended sequence.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 */
void uwb_adv_transfer_blocking(radio_adv_t *radio_adv);

/** @brief Update the radio wake sleep period
 *         depending on the sleep mode.
 *
 *  @param[in] radio_adv          Radio's advance instance.
 *  @param[in] sleep_lvl          Radio's sleep depths.
 *  @param[in] wake_sleep_period  Desired wake sleep period, in us.
 */
void uwb_adv_update_wake_sleep_period(radio_adv_t *radio_adv, sleep_lvl_t *sleep_lvl, uint32_t sleep_time_us);

/** @brief Send raw wake-sleep period to radio.
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] sleep_time  Desired wake sleep period, in clock cycles.
 */
void uwb_adv_set_wake_sleep_raw(radio_adv_t *radio_adv, uint16_t sleep_time);

/** @brief Send raw power-up delay to radio.
 *
 *  @note  The raw value will be divided by 4
 *         as per the register conversion.
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @param[in] pwr_up     Desired pwr-up delay, in clock cycles.
 */
void uwb_adv_set_pwr_up_delay_raw(radio_adv_t *radio_adv, int16_t pwr_up);

/** @brief Send raw RX timeout to the radio.
 *
 *  @note  The raw RX timeout will be divided by 8
 *         as per the register conversion. The Receiver
 *         power-up delay will be appended raw.
 *
 *  @param[in] radio_adv        Radio's advance instance.
 *  @param[in] rx_timeout       Desired timeout, in clock cycles.
 *  @param[in] rx_pwr_up_delay  Receiver power-up delay, in clock cycles (4-bit).
 */
void uwb_adv_set_rx_timeout_raw(radio_adv_t *radio_adv, uint32_t rx_timeout, uint8_t rx_pwr_up_delay);

/** @brief Check last received frame address
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @return  Last address instance.
 */
last_addr_t uwb_adv_get_last_rcv_frame_addr(radio_adv_t *radio_adv);

/** @brief Check last received frame address
 *
 *  @param[in] radio_adv  Radio's advance instance.
 *  @retval True   SPI is busy.
 *  @retval False  SPI is not busy.
 */
bool uwb_adv_is_spi_busy(radio_adv_t *radio_adv);

/** @brief Append a write command over the SPI
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] target_reg  Target radio register.
 *  @param[in] value       Value to write in register.
 */
void uwb_adv_write_register(radio_adv_t *radio_adv, uint8_t target_reg, uint8_t value);

/** @brief Append a burst write command over the SPI
 *
 *  This should be the last command before using the
 *  uwb_transfer function.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] starting_reg  Starting radio register.
 *  @param[in] buffer        Buffer to write in register.
 *  @param[in] buffer_size   Buffer size to write in burst.
 */
void uwb_adv_burst_write(radio_adv_t *radio_adv, uint8_t starting_reg, uint8_t *buffer, uint8_t buffer_size);

/** @brief Append a read command over the SPI
 *
 *  @param[in] radio_adv   Radio's advance instance.
 *  @param[in] target_reg  Target radio register.
 *  @return Pointer to the read value.
 */
uint8_t *uwb_adv_read_register(radio_adv_t *radio_adv, uint8_t target_reg);

/** @brief Append a burst read command over the SPI
 *
 *  This should be the last command before using the
 *  uwb_transfer function.
 *
 *  @param[in] radio_adv     Radio's advance instance.
 *  @param[in] starting_reg  Starting radio register.
 *  @param[in] read_size     Burst read size.
 *  @return Pointer to the read buffer.
 */
uint8_t *uwb_adv_burst_read(radio_adv_t *radio_adv, uint8_t starting_reg, uint8_t read_size);

#ifdef __cplusplus
}
#endif
#endif /* SR1000_API_ADV_H_ */
