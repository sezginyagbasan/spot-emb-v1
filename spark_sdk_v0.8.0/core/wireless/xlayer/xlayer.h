/** @file xlayer.h
 *  @brief SPARK cross layer queue
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

#ifndef XLAYER_H
#define XLAYER_H

/* INCLUDES *******************************************************************/
#include "sr1000_def.h"
#include "sr1000_spectral.h"
#include "wps_def.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/

/** @brief Cross layer configuration
 */
typedef struct xlayer_config {

    /* Layer 2 */
    uint16_t source_address;          /**< Source address */
    uint16_t destination_address;     /**< Destination address */
    bool     expect_ack;              /**< Expect ACK? */

    /* Layer 1 */
    modulation_t      modulation;        /**< modulation */
    fec_level_t       fec;               /**< FEC level */
    rf_channel_t      channel;           /**< Current channel information */
    packet_cfg_t      packet_cfg;        /**< Packet configuration */
    uint16_t          power_up_delay;    /**< Power up delay */
    uint16_t          rx_timeout;        /**< RX timeout */
    uint16_t          sleep_time;        /**< Sleep time in PLL cycles */
    uint16_t          rx_wait_time;      /**< RX wait time */
    uint8_t           rx_constgain;      /**< Receiver constant gain */
    uint8_t           cca_threshold;     /**< Clear Channel Assessment threshold */
    uint16_t          cca_retry_time;    /**< CCA retry time */
    uint8_t           cca_max_try_count; /**< CCA max try count */
    cca_fail_action_t cca_fail_action;   /**< CCA fail action */
    uint32_t          rnsi_raw;          /**< RNSI in 1/10 dB */
    uint32_t          rssi_raw;          /**< RSSI in 1/10 dB */
    sleep_lvl_t       sleep_level;       /**< Sleep Level */

    /* Callback */
    void (*callback)(wps_connection_t* connection); /**< Function called when the packet is fully processed */
} xlayer_cfg_t;

/** @brief Cross layer frame
 */
typedef struct xlayer_frame {
    /* Frame */
    uint8_t* header_memory;        /**< Header's buffer memory, point to index 0 */
    uint8_t  header_memory_size;   /**< Header's buffer size */
    uint8_t* header_begin_it;      /**< Header's begin iterator */
    uint8_t* header_end_it;        /**< Header's end iterator */
    uint64_t time_stamp;           /**< Frame's timestamps */
    uint16_t retry_count;          /**< Frame's retry count */

    uint8_t* payload_memory;       /**< Payload's buffer memory, point to index 0 */
    uint8_t  payload_memory_size;  /**< Payload's buffer size */
    uint8_t* payload_begin_it;     /**< Payload's begin iterator */
    uint8_t* payload_end_it;       /**< Payload's end iterator */

    frame_outcome_t frame_outcome; /**< Frame outcome */
} xlayer_frame_t;

/** @brief Cross layer
 */
typedef struct xlayer {
    xlayer_cfg_t   config; /**< Configuration */
    xlayer_frame_t frame;  /**< Frame */
} xlayer_t;

#ifdef __cplusplus
}
#endif
#endif //XLAYER_H
