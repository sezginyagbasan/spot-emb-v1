/** @file sr1000_def.h
 *  @brief SR1000 API definitions
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef API_SR1000_API_DEF_H_
#define API_SR1000_API_DEF_H_

/* INCLUDES *******************************************************************/
#include "../phy/sr1000_reg.h"

/* IRQ EVENTS *****************************************************************/
/** @brief Radio's interrupt events enumeration.
 */
typedef enum radio_events {
    PACKET_BEGIN_IT = BIT(14), /**< Packet beginning interrupt.
                                 *  Set when radio transmit or detect a sync word in reception.
                                 */
    RX_TIMEOUT_IT   = BIT(13), /**< Receiver timeout interrupt.
                                 *  Set when radio is idle and in RX mode and the selected timeout
                                 *  counter reaches or exceed the value specified in the RX_PERIOD
                                 *  register.
                                 */
    TX_END_IT       = BIT(12), /**< Packet transmission end interrupt.
                                 *  Set when the radio complete a transmission.
                                 */
    NEW_PACKET_IT   = BIT(11), /**< New packet reception interrupt.
                                 *  Set when radio completes the reception of a frame into the
                                 *  RX FIFO.
                                 */
    ADDR_MATCH_IT   = BIT(10), /**< New packet address field match interrupt.
                                 *  Set when the radio completes the reception of a frame with
                                 *  its address field matching the address field defined in register
                                 *  LOCAL_ADDR.
                                 */
    BROADCAST_IT    = BIT(9),  /**< New broadcast packet reception end interrupt.
                                 *  Set when radio completes the reception of a frame with address
                                 *  field's lower byte set to 0xFF.
                                 */
    CRC_PASS_IT     = BIT(8),  /**< New packet CRC pass interrupt.
                                 *  Set when radio complete the reception of a frame with a CRC that
                                 *  matches the computed one during reception.
                                 */
    XO_TIMEOUT_IT   = BIT(7),  /**< Crystal oscillator timer interrupt.
                                 *  Set when the crystal-clocked wake up timer's count reaches the value
                                 *  in register field SLPPERIOD while the device is asleep at a sleep level
                                 *  for which the DC/DC converter is off.
                                 */
    WAKEUP_IT       = BIT(6),  /**< Wake-up interrupt.
                                 *  Set when radio finishes waking up from any sleep level.
                                 */
    CSC_FAIL_IT     = BIT(5),  /**< Carrier sensing check failure interrupt.
                                 *  Set when the carrier sensing function detects too much energy in
                                 *  the wireless channel before transmission.
                                 */
    TX_UNDERFLOW_IT = BIT(4),  /**< TX FIFO underflow interrupt.
                                 *  Set when the TX FIFO is not filled on time with the data required
                                 *  for transmitting the current frame. This flag only signal that the
                                 *  last frame transmission was aborted and the frame to transmit came
                                 *  out of the chip incomplete.
                                 */
    RX_OVRFLOW_IT   = BIT(3),  /**< Reception buffer overflow interrupt.
                                 *  Set when the RX FIFO is already full at the moment the radio intends
                                 *  to push into the FIFO a byte from a frame being received.
                                 */
    TX_OVRFLOW_IT   = BIT(2),  /**< TX FIFO overflow interrupt.
                                 *  Set when the SPI master writes another byte into the TX FIFO when
                                 *  the latter is already full.
                                 */
    BUF_LOAD_TH_IT  = BIT(1),  /**< Data buffer load threshold interrupt.
                                 *  Reflects whether the load of the current direction's buffer is passed
                                 *  the defined interrupt threshold. This function's main intended purpose
                                 *  is to warn the on-board controller that either a TX FIFO underflow
                                 *  or a RX FIFO overflow is imminent if it doesn't intervene.
                                 */
    BUF_LOAD_STP_IT = BIT(0),  /**< Data buffer stop interrupt
                                 *  Work in a similar way to BUF_LOAD_TH_IT and can serves to prevents its
                                 *  opposite problem.
                                 */
    NO_IRQ          = 0
} radio_events_t;

/* IRQ ************************************************************************/

/** @brief Interrupt polarity pin enumeration.
 */
typedef enum irq_polarity {
    IRQ_ACTIVE_LOW  = MOV2MASK(0, BIT_IRQPOLAR), /**< Interrupt pin active in LOW state */
    IRQ_ACTIVE_HIGH = MOV2MASK(1, BIT_IRQPOLAR)  /**< Interrupt pin active in HIGH state */
} irq_polarity_t;

/* SYNCWORD *******************************************************************/

/** @brief Radio's syncword length enumeration.
 */
typedef enum syncword_length {
    SYNCWORD_LENGTH_16 = MOV2MASK(0, BIT_SWLENGTH), /**< Syncword length on 16-bit */
    SYNCWORD_LENGTH_32 = MOV2MASK(1, BIT_SWLENGTH)  /**< Syncword length on 32-bit */
} syncword_length_t;

/* FRAME CONFIG ***************************************************************/
/** @brief Radio's modulation type enumeration.
 */
typedef enum modulation {
    MODULATION_OOK     = MOV2MASK(0b00, BITS_MODULATION), /**< Frame modulation OOK (On-off keying) */
    MODULATION_IOOK    = MOV2MASK(0b01, BITS_MODULATION), /**< Frame modulation OOK (On-off keying) */
    MODULATION_PPM     = MOV2MASK(0b10, BITS_MODULATION), /**< Frame modulation PPM (Pulse-position modulation) */
    MODULATION_2BITPPM = MOV2MASK(0b11, BITS_MODULATION), /**< Frame modulation 2BITPPM(2-bit Pulse-position modulation) */
} modulation_t;

/** @brief Radio's Forward error correction enumeration.
 */
typedef enum fec_level {
    FEC_LVL_0 = MOV2MASK(0b00, BITS_FECLEVEL), /**< Forward error correction level 0 */
    FEC_LVL_1 = MOV2MASK(0b01, BITS_FECLEVEL), /**< Forward error correction level 1 */
    FEC_LVL_2 = MOV2MASK(0b10, BITS_FECLEVEL), /**< Forward error correction level 2 */
    FEC_LVL_3 = MOV2MASK(0b11, BITS_FECLEVEL)  /**< Forward error correction level 3 */
} fec_level_t;

/** @brief Inter-symbol interference mitigation enum
 */
typedef enum isi_mitig {
    ISI_MITIG_0 = MOV2MASK(0b00, BITS_ISIMITIG), /**< Inter-symbol interference mitigation level 0 */
    ISI_MITIG_1 = MOV2MASK(0b01, BITS_ISIMITIG), /**< Inter-symbol interference mitigation level 1 */
    ISI_MITIG_2 = MOV2MASK(0b10, BITS_ISIMITIG), /**< Inter-symbol interference mitigation level 2 */
    ISI_MITIG_3 = MOV2MASK(0b11, BITS_ISIMITIG)  /**< Inter-symbol interference mitigation level 3 */
} isi_mitig_t;

/** @brief Automatic transmission enumeration
 */
typedef enum auto_tx {
    AUTO_TX_DISABLE = MOV2MASK(0, BIT_AUTOTX), /**< Disable automatic transmission and follow the one
                                                 *  based on the START_TX of register 0x1F
                                                 */
    AUTO_TX_ENABLE  = MOV2MASK(1, BIT_AUTOTX)  /**< Enable automatic transmission on wake-up event of predetermined pattern */
} auto_tx_t;

/** @brief Enable/Disable Automatic frame reply enumeration
 */
typedef enum auto_reply {
    AUTO_REPLY_DISABLE = MOV2MASK(0, BIT_AUTORPLY), /**< Disable automatic reply on received frame */
    AUTO_REPLY_ENABLE  = MOV2MASK(1, BIT_AUTORPLY)  /**< Enable automatic reply on received frame depending on certain condition */
} auto_reply_t;

/* TRANSMIT / RECEIVE *********************************************************/
#define MAX_FRAMESIZE     128
#define FRAME_SIZE_ERROR  0
#define BROADCAST_ADDRESS 0xFF

#define NO_ADDR    0
#define NO_DELAY   0
#define NO_TIMEOUT 0
#define NB_CHANNEL 10
#define NB_PULSES  12

/** @brief Radio's RF channel ID enumeration.
 */
typedef enum rf_channel_id {
    CHANNEL_0 = 0, /**< RF Channel 0 */
    CHANNEL_1,     /**< RF Channel 1 */
    CHANNEL_2,     /**< RF Channel 2 */
    CHANNEL_3,     /**< RF Channel 3 */
    CHANNEL_4,     /**< RF Channel 4 */
    CHANNEL_5,     /**< RF Channel 5 */
    CHANNEL_6,     /**< RF Channel 6 */
    CHANNEL_7,     /**< RF Channel 7 */
    CHANNEL_8,     /**< RF Channel 8 */
    CHANNEL_9,     /**< RF Channel 9 */
    CHANNEL_ALL    /**< Reference to all RF channel */
} rf_channel_id_t;

/** @brief Radio's TX power level enumeration in dBFs.
 */
typedef enum tx_power {
    MINUS_9_9_DBFS = 0,     /**< TX power -9.9 dBFs             */
    MINUS_9_0_DBFS,         /**< TX power -9.0 dBFs             */
    MINUS_8_1_DBFS,         /**< TX power -8.1 dBFs             */
    MINUS_7_2_DBFS,         /**< TX power -7.2 dBFs             */
    MINUS_6_3_DBFS,         /**< TX power -6.3 dBFs             */
    MINUS_5_4_DBFS,         /**< TX power -5.4 dBFs             */
    MINUS_4_5_DBFS,         /**< TX power -4.5 dBFs             */
    MINUS_3_6_DBFS,         /**< TX power -3.6 dBFs             */
    MINUS_2_7_DBFS,         /**< TX power -2.7 dBFs             */
    MINUS_1_8_DBFS,         /**< TX power -1.8 dBFs             */
    MINUS_0_9_DBFS,         /**< TX power -0.9 dBFs             */
    MINUS_0_0_DBFS,         /**< TX power -0.0 dBFs             */
    MINUS_9_9_DBFS_RANGING, /**< TX power -9.9 dBFs for ranging */
    MINUS_9_0_DBFS_RANGING, /**< TX power -9.0 dBFs for ranging */
    MINUS_8_1_DBFS_RANGING, /**< TX power -8.1 dBFs for ranging */
    MINUS_7_2_DBFS_RANGING, /**< TX power -7.2 dBFs for ranging */
    MINUS_6_3_DBFS_RANGING, /**< TX power -6.3 dBFs for ranging */
    MINUS_5_4_DBFS_RANGING, /**< TX power -5.4 dBFs for ranging */
    MINUS_4_5_DBFS_RANGING, /**< TX power -4.5 dBFs for ranging */
    MINUS_3_6_DBFS_RANGING, /**< TX power -3.6 dBFs for ranging */
    MINUS_2_7_DBFS_RANGING, /**< TX power -2.7 dBFs for ranging */
    MINUS_1_8_DBFS_RANGING, /**< TX power -1.8 dBFs for ranging */
    MINUS_0_9_DBFS_RANGING, /**< TX power -0.9 dBFs for ranging */
    MINUS_0_0_DBFS_RANGING, /**< TX power -0.0 dBFs for ranging */
    LOW_OUTPUT_POWER_RANGING, /**< Recommended short-range power setting */
    HIGH_OUTPUT_POWER_RANGING, /**< Recommended long-range power setting */
} tx_power_t;

/* SLEEP **********************************************************************/
/** @brief Radio's sleep level.
 *
 *  Sleep depth at which the radio will go when asleep.
 */
typedef enum sleep_lvl {
    SLEEP_IDLE    = MOV2MASK(0b00, BITS_SLPDEPTH), /**< Radio sleep level IDLE */
    SLEEP_SHALLOW = MOV2MASK(0b01, BITS_SLPDEPTH), /**< Radio sleep level SHALLOW */
    SLEEP_DEEP    = MOV2MASK(0b11, BITS_SLPDEPTH)  /**< Radio sleep level DEEP */
} sleep_lvl_t;


/** @brief Radio's sleep event enumeration.
 *
 *  Events that will trigger an automatic sleeping when completed.
 */
typedef enum sleep_events {
    SLEEP_RX_TIMEOUT = MOV2MASK(1, BIT_SLPRXTO),  /**< Sleep event RX timeout */
    SLEEP_TX_END     = MOV2MASK(1, BIT_SLPTXEND), /**< Sleep event TX end */
    SLEEP_RX_END     = MOV2MASK(1, BIT_SLPRXEND), /**< Sleep event RX end */
    SLEEP_ADDR_MATCH = MOV2MASK(1, BIT_SLPMATCH), /**< Sleep event address match */
    SLEEP_BROADCAST  = MOV2MASK(1, BIT_SLPBRDCA), /**< Sleep event broadcast */
    SLEEP_CSC_FAIL   = MOV2MASK(1, BIT_SLPNOISY), /**< Sleep event CSC fail */
    SLEEP_NO_EVENT   = 0                          /**< No sleep event */
} sleep_events_t;

/** @brief Radio's API errors.
 */
typedef enum uwb_error {
    UWB_NO_ERROR,
    UWB_HAL_ERROR,
    UWB_NO_RADIO_PRESENT
} uwb_error_t;

/* API Advance */

/* Timer configuration register */

/** @brief Timer configuration AUTOWAKE field enum.
 */
typedef enum auto_wake {
    AUTOWAKE_UP_DISABLE = MOV2MASK(0, BIT_AUTOWAKE), /**< Automatically wake-up disable */
    AUTOWAKE_UP_ENABLE  = MOV2MASK(1, BIT_AUTOWAKE)  /**< Automatically wake-up enable using the wake-up timer */
} auto_wake_t;

/** @brief Timer configuration WAKEONCE field enum.
 */
typedef enum wake_up_once {
    WAKE_UP_ONCE_DISABLE = MOV2MASK(0, BIT_WAKEONCE), /**< Disable wake-up once feature*/
    WAKE_UP_ONCE_ENABLE  = MOV2MASK(1, BIT_WAKEONCE)  /**< Enable wake-up once until user manually clear GO_SLEEP(0x1F) or AUTOWAKE(0x05) */
} wake_up_once_t;

/** @brief Timer configuration SYNATEND field enum.
 */
typedef enum sync_at_end {
    SYNC_AT_END_OF_FRAME_DISABLE = MOV2MASK(0, BIT_SYNATEND), /**< Disable wake up timer synchronization at end of frames */
    SYNC_AT_END_OF_FRAME_ENABLE  = MOV2MASK(1, BIT_SYNATEND), /**< Enable wake up timer synchronization at end of frames */
} sync_at_end_t;

/** @brief Timer configuration SYNPKTTX field enum.
 */
typedef enum sync_packet_tx {
    SYNC_PACKET_TX_DISABLE = MOV2MASK(0, BIT_SYNTXSTA), /**< Disable wake up timer synchronization on frame transmission event */
    SYNC_PACKET_TX_ENABLE  = MOV2MASK(1, BIT_SYNTXSTA)  /**< Enable wake up timer synchronization on frame transmission event */
} sync_packet_tx_t;

/** @brief Timer configuration SYNPKTRX field enum.
 */
typedef enum sync_packet_rx {
    SYNC_PACKET_RX_DISABLE = MOV2MASK(0, BIT_SYNRXSTA), /**< Disable wake up timer synchronization on frame reception event */
    SYNC_PACKET_RX_ENABLE  = MOV2MASK(1, BIT_SYNRXSTA)  /**< Enable wake up timer synchronization on frame reception event */
} sync_packet_rx_t;

/** @brief Timer configuration SYNMATCH field enum.
 */
typedef enum sync_match {
    SYNC_MATCH_DISABLE = MOV2MASK(0, BIT_SYNMATCH), /**< Disable wake up timer synchronization on frame address match event */
    SYNC_MATCH_ENABLE  = MOV2MASK(1, BIT_SYNMATCH)  /**< Enable wake up timer synchronization on frame address match event */
} sync_match_t;

/** @brief Timer configuration SYNBRDCA field enum.
 */
typedef enum sync_brdca {
    SYNC_BRDCA_DISABLE = MOV2MASK(0, BIT_SYNBRDCA), /**< Disable wake up timer synchronization on broadcast frame reception event */
    SYNC_BRDCA_ENABLE  = MOV2MASK(1, BIT_SYNBRDCA)  /**< Enable wake up timer synchronization on broadcast frame reception event */
} sync_brdca_t;

/** @brief Timer configuration SYNRXCRC field enum.
 */
typedef enum sync_rx_crc {
    SYNC_RX_CRC_DISABLE = MOV2MASK(0, BIT_SYNRXCRC), /**< Disable wake up timer synchronization on CRC pass event */
    SYNC_RX_CRC_ENABLE  = MOV2MASK(1, BIT_SYNRXCRC)  /**< Enable wake up timer synchronization on CRC pass event */
} sync_rx_crc_t;

/* Power status and commands register */

/** @brief Power status and commands field CAL_DCRO.
 */
typedef enum cal_dcro {
    CALIBRATION_DCRO_DISABLE = MOV2MASK(0, BIT_CALIBRAT), /**< Disable the digitally-controlled ring oscillator calibration */
    CALIBRATION_DCRO_ENABLE  = MOV2MASK(1, BIT_CALIBRAT)  /**< Trigger the calibration block counting process of
                                                            *  the digitally-controlled ring oscillator
                                                            */
} cal_dcro_t;

/** @brief Power status and commands field SKIP_WAKE.
 */
typedef enum skip_wake {
    SKIP_WAKE_DISABLE = MOV2MASK(0, BIT_SKIPWAKE), /**< Don't skip the next wake up event */
    SKIP_WAKE_ENABLE  = MOV2MASK(1, BIT_SKIPWAKE)  /**< Skip the automated wake-up process and remain asleep */
} skip_wake_t;

/** @brief Power status and commands field RX_MODE.
 */
typedef enum rx_mode {
    RX_MODE_ENABLE_TRANSMISSION = MOV2MASK(0, BIT_RXMODE), /**< Configure the device for frame transmission */
    RX_MODE_ENABLE_RECEPTION    = MOV2MASK(1, BIT_RXMODE)  /**< Configure the device for frame reception */
} rx_mode_t;

/** @brief Power status and commands field START_TX.
 */
typedef enum start_tx {
    START_TX_NO_EFFECT = MOV2MASK(0, BIT_STARTTX), /**< Has no effect on the register */
    START_TX           = MOV2MASK(1, BIT_STARTTX)  /**< Schedule a frame transmission the next time the radio is awake */
} start_tx_t;

/** @brief Power status and commands field INITIMER.
 */
typedef enum init_timer {
    INIT_TIMER_NO_EFFECT                = MOV2MASK(0, BIT_INITTIME), /**< Has no effect on the register */
    INIT_TIMER_RESET_BOTH_WAKE_UP_TIMER = MOV2MASK(1, BIT_INITTIME), /**< Reset both wake-up timers to 0x0000 */
} init_timer_t;

/** @brief Power status and commands field GO_SLEEP.
 */
typedef enum go_sleep {
    GO_TO_SLEEP_WAKE_UP = MOV2MASK(0, BIT_GOTOSLP), /**< Wake up the radio only if bit AUTOWAKE of register 0x05 is cleared */
    GO_TO_SLEEP         = MOV2MASK(1, BIT_GOTOSLP)  /**< Put the radio to sleep if it is awake and idle */
} go_sleep_t;

/** @brief Power status and commands field FLUSH_RX.
 */
typedef enum flush_rx {
    FLUSH_RX_NO_EFFECT       = MOV2MASK(0, BIT_FLUSHRX), /**< Has no effect on register */
    FLUSH_RX_RESET_RX_BUFFER = MOV2MASK(1, BIT_FLUSHRX)  /**< Flush and reset the reception buffer */
} flush_rx_t;

/** @brief Power status and commands field FLUSH_TX.
 */
typedef enum flush_tx {
    FLUSH_TX_NO_EFFECT       = MOV2MASK(0, BIT_FLUSHTX), /**< Has no effect on register */
    FLUSH_TX_RESET_TX_BUFFER = MOV2MASK(1, BIT_FLUSHTX)  /**< Flush and reset the transmission buffer */
} flush_tx_t;

/* Packet configuration register (MAC layer) */

/** @brief Address-based frame filtering field.
 */
typedef enum addr_filt {
    ADDR_FILT_DISABLE               = MOV2MASK(0b00, BITS_ADDRFILT), /**< Disable address filtering for both receive packet and auto-reply */
    ADDR_FILT_ON_RX_FRAME_ENABLE    = MOV2MASK(0b01, BITS_ADDRFILT), /**< Enable address filtering on receive packet only */
    ADDR_FILT_FOR_AUTO_REPLY_ENABLE = MOV2MASK(0b10, BITS_ADDRFILT), /**< Enable auto-reply only when receive packet match the local address
                                                                       *  and is not the multicast/broadcast one
                                                                       */
    ADDR_FILT_ENABLE                = MOV2MASK(0b11, BITS_ADDRFILT), /**< Enable address filtering for auto-reply and received packet */
} addr_filt_t;

/** @brief Radio's address length enumeration.
 */
typedef enum address_length {
    ADDRESS_LENGTH_8  = MOV2MASK(0, BIT_ADDRLEN), /**< Radio address on 8-bit */
    ADDRESS_LENGTH_16 = MOV2MASK(1, BIT_ADDRLEN), /**< Radio address on 16-bit */
} address_length_t;

/** @brief Frame address field header.
 */
typedef enum addr_hdre {
    ADDR_HEADER_DISABLE = MOV2MASK(0, BIT_ADDRHDRE), /**< Disable frame address field header */
    ADDR_HEADER_ENABLE  = MOV2MASK(1, BIT_ADDRHDRE), /**< Enable frame address field header */
} addr_hdre_t;

/** @brief Payload size field header.
 */
typedef enum size_hdre {
    SIZE_HEADER_DISABLE = MOV2MASK(0, BIT_SIZEHDRE), /**< Disable payload size field header */
    SIZE_HEADER_ENABLE  = MOV2MASK(1, BIT_SIZEHDRE)  /**< Insert a payload size field before frame payload */
} size_hdre_t;

/** @brief Transmitted frame payload size source.
 */
typedef enum size_src {
    SIZE_SRC_REG_TXPKTSIZE   = MOV2MASK(0, BIT_SIZESRC), /**< Use value of TXPKTSIZE register for the frame payload size*/
    SIZE_SRC_TX_PAYLOAD_SIZE = MOV2MASK(1, BIT_SIZESRC), /**< Use value in packet header for frame payload size */
} size_src_t;

/** @brief Save frame address field in reception buffer.
 */
typedef enum save_addr {
    SAVE_ADDR_DISABLE = MOV2MASK(0, BIT_SAVEADDR), /**< Disable save of address field */
    SAVE_ADDR_ENABLE  = MOV2MASK(1, BIT_SAVEADDR)  /**< Enable save of address field of each received frame into the
                                                     *  reception buffer with the frame payload
                                                     */
} save_addr_t;

/** @brief Save frame payload size field in reception buffer.
 */
typedef enum save_size {
    SAVE_SIZE_DISABLE = MOV2MASK(0, BIT_SAVESIZE), /**< Disable saving of payload size */
    SAVE_SIZE_ENABLE  = MOV2MASK(1, BIT_SAVESIZE)  /**< Enable saving of payload size of each received frame into the
                                                     *  reception buffer with the frame payload
                                                     */
} save_size_t;

/* Debug modem feature */

/** @brief Override modem register field.
 */
typedef enum modem_debug_override {
    OVERRIDE_DISABLE = MOV2MASK(0, BIT_OVERRIDE), /**< Do not override modem */
    OVERRIDE_ENABLE  = MOV2MASK(1, BIT_OVERRIDE)  /**< Override modem */
} modem_debug_override_t;

/** @brief Manual receiver phase selection / Phase tracking register field.
 */
typedef enum manuphase {
    MANUPHASE_INTEG_WINDOW_1             = MOV2MASK(0b00, BITS_MANUPHASE), /**< Use integration window 1 for decoding of the received packet
                                                                             *  and propagate to the STRM_OUT pins
                                                                             */
    MANUPHASE_INTEG_WINDOW_2             = MOV2MASK(0b01, BITS_MANUPHASE), /**< Use integration window 2 for decoding of the received packet
                                                                             *  and propagate to the STRM_OUT pins
                                                                             */
    MANUPHASE_INTEG_WINDOW_3             = MOV2MASK(0b10, BITS_MANUPHASE), /**< Use integration window 3 for decoding of the received packet
                                                                             *  and propagate to the STRM_OUT pins
                                                                             */
    MANUPHASE_INTEG_WINDOW_4             = MOV2MASK(0b11, BITS_MANUPHASE), /**< Use integration window 4 for decoding of the received packet
                                                                             *  and propagate to the STRM_OUT pins
                                                                             */
    MANUPHASE_PHASE_TRACKING_DISABLE     = MOV2MASK(0b00, BITS_MANUPHASE), /**< Disable phase tracking */
    MANUPHASE_PHASE_TRACKING_ENABLE      = MOV2MASK(0b11, BITS_MANUPHASE), /**< Enable phase tracking */
} manuphase_t;

/** @brief Manual bit decision threshold enum.
 */
typedef enum man_bit_thresh {
    MANUAL_BIT_THRESHOLD_DISABLE = MOV2MASK(0, BIT_MANBITHR), /* Radio will select the bit decision threshold */
    MANUAL_BIT_THRESHOLD_ENABLE  = MOV2MASK(1, BIT_MANBITHR)  /* Use the bit decision threshold in the BITTHRADJ field of this register */
} man_bit_thresh_t;

/* Collision avoidance */

/** @brief Receiver's idle power consumption enum.
 */
typedef enum rx_idle_pwr {
    RX_IDLE_PWR_MIN  = MOV2MASK(0b00, BITS_IDLERXPWR), /**< The receiver will remain on for only 8 symbol clock cycle
                                                         *  after the RXPUDELAY before turning off again
                                                         */
    RX_IDLE_PWR_MED  = MOV2MASK(0b01, BITS_IDLERXPWR), /**< The receiver will remain on for only 16 symbol clock cycle
                                                         *  after the RXPUDELAY before turning off again
                                                         */
    RX_IDLE_PWR_HIGH = MOV2MASK(0b11, BITS_IDLERXPWR)  /**< The receiver will always remain on when waiting for preamble */
} rx_idle_pwr_t;

/* Shared pulse parameters */

/** @brief Hold transmitter's data signal on.
 */
typedef enum hold_tx_on {
    HOLD_TX_ON_DISABLE = MOV2MASK(0, BIT_HOLDTXON), /**< Disable pattern transmission on every symbol clock cycle */
    HOLD_TX_ON_ENABLE  = MOV2MASK(1, BIT_HOLDTXON)  /**< Force pulse pattern transmission at every symbol clock cycle */
} hold_tx_on_t;

/** @brief Randomize transmitted pulse phase.
 */
typedef enum rnd_phase {
    RND_PHASE_DISABLE = MOV2MASK(0, BIT_RNDPHASE), /**< All pulses oscillations will have the same phase. */
    RND_PHASE_ENABLE  = MOV2MASK(1, BIT_RNDPHASE)  /**< Randomly flip oscillation phase of transmitted pulse */
} rnd_phase_t;

/** @brief Transmission power field in shared pulse parameters register.
 */
typedef enum tx_params_power {
    TX_PARAM_PWR_0_DB         = MOV2MASK(0b00, BITS_TXPOWER), /**< Radio transmitter power level  0dB */
    TX_PARAM_PWR_MINUS_0_6_DB = MOV2MASK(0b01, BITS_TXPOWER), /**< Radio transmitter power level -0.6dB */
    TX_PARAM_PWR_MINUS_1_2_DB = MOV2MASK(0b10, BITS_TXPOWER), /**< Radio transmitter power level -1.2dB */
    TX_PARAM_PWR_MINUS_1_8_DB = MOV2MASK(0b11, BITS_TXPOWER)  /**< Radio transmitter power level -1.8dB */
} tx_params_power_t;

/** @brief Intermediate-frequency filter enable.
 */
typedef enum if_filt_en {
    IF_FILT_DISABLE = MOV2MASK(0, BIT_IFFILTEN), /**< Disable the intermediate-frequency filter */
    IF_FILT_ENABLE  = MOV2MASK(1, BIT_IFFILTEN)  /**< Enable the intermediate-frequence filter */
} if_filt_en_t;

/* Peripherals controls */

/** @brief Enable/Disable standard SPI operation.
 */
typedef enum std_spi {
    STD_SPI_DISABLE = MOV2MASK(0, BIT_STDSPI), /**< Enable fast SPI mode, that doesn't comply with the
                                                 *  de facto industry SPI communication
                                                 */
    STD_SPI_ENABLE  = MOV2MASK(1, BIT_STDSPI)  /**< Enable standard SPI mode */
} std_spi_t;

/** @brief Enable/Disable automatic reception buffer flush.
 */
typedef enum flush_dis {
    FLUSH_ENABLE  = MOV2MASK(0, BIT_AUTOFLUSHDIS), /**< Radio will use the automatic reception buffer flushing when a frame is rejected */
    FLUSH_DISABLE = MOV2MASK(1, BIT_AUTOFLUSHDIS)  /**< Disable automatic flushing of reception buffer */
} flush_dis_t;

/** @brief Enable/Disable storage capacitor switch.
 */
typedef enum one_vsw_dis {
    ONE_VSW_ENABLE  = MOV2MASK(0, BIT_1VSWDIS), /**< Enable the 1 volt storage capacitor switch */
    ONE_VSW_DISABLE = MOV2MASK(1, BIT_1VSWDIS)  /**< Disable the 1 volt storage capacitor switch*/
} one_vsw_dis_t;

/** @brief Enable/Disable DC/DC converter.
 */
typedef enum dcdc_conv_dis {
    DCDC_CONVERTER_ENABLE  = MOV2MASK(0, BIT_DCDCDIS), /**< Enable the DC/DC converter */
    DCDC_CONVERTER_DISABLE = MOV2MASK(1, BIT_DCDCDIS)  /**< Disable the DC/DC converter */
} dcdc_conv_dis_t;

/** @brief Enable/Disable Phase-locked loop (PLL).
 */
typedef enum pll_dis {
    PLL_ENABLE  = MOV2MASK(0, BIT_PLLDIS), /**< Enable the PLL */
    PLL_DISABLE = MOV2MASK(1, BIT_PLLDIS)  /**< Disable the PLL */
} pll_dis_t;

/** @brief Symbol rate clock source.
 */
typedef enum symbol_rate_clk_source {
    SYMBOL_CLOCK_RATE_SOURCE_INT = MOV2MASK(0, BIT_SYMBCSRC), /**< Use PLL output clock for the symbol rate clock */
    SYMBOL_CLOCK_RATE_SOURCE_EXT = MOV2MASK(1, BIT_SYMBCSRC)  /**< Use an external PLL clock source and replace the internal one */
} symbol_rate_clk_source_t;

/** @brief Crystal oscillator clock source.
 */
typedef enum xtal_osc_clk_source {
    XTAL_CLOCK_SOURCE_INT = MOV2MASK(0, BIT_XTALCSRC), /**< Use the integrated crystal oscillator for the sleep control state machine */
    XTAL_CLOCK_SOURCE_EXT = MOV2MASK(1, BIT_XTALCSRC)  /**< Use an external crystal oscillator for the low frequency reference clock signal*/
} xtal_osc_clk_source_t;

/** @brief Enable/Disable output crystal oscillator clock signal.
 */
typedef enum output_xtal {
    OUTPUT_XTAL_SIGNAL_DISABLE = MOV2MASK(0, BIT_OUTPXTAL), /**< Disable the output of the internal crystal oscillator  */
    OUTPUT_XTAL_SIGNAL_ENABLE  = MOV2MASK(1, BIT_OUTPXTAL), /**< Enable the output of the internal
                                                              *  crystal oscillator only when XTALCSRC = 1
                                                              */
} output_xtal_t;

/**
 * @brief Last received frame wait source instance.
 */
typedef enum rx_wait_source {
    RX_WAIT_SOURCE_DEFAULT  = MOV2MASK(0, BIT_RXWAISRC), /**< Default receiver idle time counter reusing the output shift register of the modem is used */
    RX_WAIT_SOURCE_REGISTER = MOV2MASK(1, BIT_RXWAISRC), /**< Modem’s wake-up timer value is the one that gets stored in the RX WAITED register. */
} rx_wait_source_t;

/** @brief Frame outcome enumeration.
 */
typedef enum frame_outcome {
    FRAME_RECEIVED,          /**< Frame received */
    FRAME_LOST,              /**< Frame lost */
    FRAME_REJECTED,          /**< Frame rejected */
    FRAME_SENT_ACK,          /**< Frame sent and acknowledged */
    FRAME_SENT_ACK_LOST,     /**< Frame sent and ack is lost*/
    FRAME_SENT_ACK_REJECTED, /**< Frame sent and ack is rejected */
    FRAME_WAIT               /**< No frame sent or received */
} frame_outcome_t;

/* REGISTERS ******************************************************************/
#define NB_STATUS_REG         2
#define NB_IRQ_REG            2
#define NB_QUALITY_REG        2
#define NB_PHASE_REG          4
#define NB_RX_WAIT_REG        2
#define NB_SYNCWORD_REG       5
#define NB_CRC_REG            2
#define NB_ADDRESS_REG        2
#define NB_SLEEP_REG          1
#define TX_PATTERN_BYTE_COUNT 12
#define PLL_RATIO             625
#define PLL_FREQ_HZ           20480000
#define PLL_FREQ_KHZ          20480

/* CCA ************************************************************************/
#define CCA_RNSI_FILTER_SIZE  5
#define CCA_DISABLE           0xFF
#define CCA_DEFAULT_MARGIN    12
#define CCA_MAX_RETRY_TIME_US 50


#endif /* API_SR1000_API_DEF_H_ */
