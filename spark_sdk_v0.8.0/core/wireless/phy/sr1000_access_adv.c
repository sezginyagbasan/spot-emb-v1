/** @file sr1000_access_adv.c
 *  @brief SR1000 Protocol external access layer file.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <string.h>
#include "sr1000_access.h"
#include "sr1000_access_adv.h"
#include "sr1000_reg.h"

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
static bool is_sequence_at_max_capacity(access_sequence_instance_t *access_sequence, size_t size);
static bool sequence_is_empty(access_sequence_instance_t *access_sequence);
static bool buffer_is_empty(uint8_t *buffer);
static bool burst_mode_is_on(access_sequence_instance_t *access_sequence);
#endif

/* PUBLIC FUNCTIONS ***********************************************************/
void sr1000_access_adv_init(access_sequence_instance_t *access_sequence,
                            uint8_t *app_rx_buffer,
                            uint8_t *app_tx_buffer,
                            uint8_t size,
                            uwb_err *error)
{
    *error = ACCESS_SEQUENCE_NO_ERR;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    if (buffer_is_empty(app_rx_buffer)) {
        *error = ACCESS_SEQUENCE_INVALID_RX_BUFFER_ERR;
        return;
    } else {
        *error = ACCESS_SEQUENCE_NO_ERR;
    }

    if (buffer_is_empty(app_tx_buffer)) {
        *error = ACCESS_SEQUENCE_INVALID_TX_BUFFER_ERR;
        return;
    } else {
        *error = ACCESS_SEQUENCE_NO_ERR;
    }

    if (*error == ACCESS_SEQUENCE_NO_ERR) {
        access_sequence->tx_buffer = app_tx_buffer;
        access_sequence->rx_buffer = app_rx_buffer;
    }
#else
    access_sequence->tx_buffer = app_tx_buffer;
    access_sequence->rx_buffer = app_rx_buffer;
#endif

    access_sequence->index         = 0;
    access_sequence->capacity      = size;
    access_sequence->burst_mode    = false;
}

void sr1000_access_adv_append_read(access_sequence_instance_t *access_sequence,
                                   uint8_t target_reg,
                                   uint8_t **buffer,
                                   uwb_err *error)
{

    uint32_t index = access_sequence->index;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    const uint8_t append_number = 2;

    *error = ACCESS_SEQUENCE_BUSY_ERR;

    if (burst_mode_is_on(access_sequence)) {
        *error = ACCESS_SEQUENCE_CANT_APPEND_IN_BURST_MODE_ERR;
        return;
    }

    if (is_sequence_at_max_capacity(access_sequence, append_number)) {
        *error = ACCESS_SEQUENCE_TOO_MUCH_QUEUED_TRANSACTION_ERR;
        return;
    }
#else
    (void)error;
#endif

    access_sequence->tx_buffer[index++] = target_reg;

    /* Change application pointer to the appropriate RX buffer address for real time update */
    *buffer = &access_sequence->rx_buffer[index++];

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_NO_ERR;
#endif
    access_sequence->index = index;
}

void sr1000_access_adv_transfer_blocking(radio_hal_adv_t *radio_hal_adv,
                                         access_sequence_instance_t *access_sequence,
                                         uwb_err *error)
{
#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_BUSY_ERR;

    if (sequence_is_empty(access_sequence)) {
        *error = ACCESS_SEQUENCE_EMPTY_SEQUENCE_ERR;
        return;
    }
#else
    (void)(error);
#endif

    radio_hal_adv->transfer_full_duplex_blocking(access_sequence->tx_buffer,
                                                 access_sequence->rx_buffer,
                                                 access_sequence->index);

    access_sequence->index = 0;
    access_sequence->burst_mode = false;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_NO_ERR;
#endif
}

void sr1000_access_adv_transfer_non_blocking(radio_hal_adv_t *radio_hal_adv,
                                             access_sequence_instance_t *access_sequence,
                                             uwb_err *error)
{

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_BUSY_ERR;

    if (sequence_is_empty(access_sequence)) {
        *error = ACCESS_SEQUENCE_EMPTY_SEQUENCE_ERR;
        return;
    }
#else
    (void)(error);
#endif

    radio_hal_adv->transfer_full_duplex_non_blocking(access_sequence->tx_buffer,
                                                     access_sequence->rx_buffer,
                                                     access_sequence->index);
    access_sequence->index = 0;
    access_sequence->burst_mode = false;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_NO_ERR;
#endif

}

void sr1000_access_adv_append_write(access_sequence_instance_t *access_sequence,
                                    uint8_t target_reg,
                                    uint8_t value,
                                    uwb_err *error)
{
    uint32_t index = access_sequence->index;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    const uint8_t append_number = 2;

    *error = ACCESS_SEQUENCE_BUSY_ERR;

    if (burst_mode_is_on(access_sequence)) {
        *error = ACCESS_SEQUENCE_CANT_APPEND_IN_BURST_MODE_ERR;
        return;
    }

    if (is_sequence_at_max_capacity(access_sequence, append_number)) {
        *error = ACCESS_SEQUENCE_TOO_MUCH_QUEUED_TRANSACTION_ERR;
        return;
    }
#else
    (void)error;
#endif

    access_sequence->tx_buffer[index++] = target_reg | REG_WRITE;
    access_sequence->tx_buffer[index++] = value;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_NO_ERR;
#endif
    access_sequence->index = index;
}

bool sr1000_access_adv_is_spi_busy(radio_hal_adv_t *radio_adv)
{
    return radio_adv->is_spi_busy();
}

void sr1000_access_reset_index(access_sequence_instance_t *access_sequence)
{
    access_sequence->index = 0;
}

void sr1000_access_adv_append_write_burst(access_sequence_instance_t *access_sequence,
                                          uint8_t starting_reg,
                                          uint8_t *tx_buffer,
                                          uint16_t buffer_size,
                                          uwb_err *error)
{
    uint32_t index = access_sequence->index;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_BUSY_ERR;

    if (burst_mode_is_on(access_sequence)) {
        *error = ACCESS_SEQUENCE_CANT_APPEND_IN_BURST_MODE_ERR;
        return;
    }

    if (is_sequence_at_max_capacity(access_sequence, buffer_size)) {
        *error = ACCESS_SEQUENCE_TOO_MUCH_QUEUED_TRANSACTION_ERR;
        return;
    }
#else
    (void)error;
#endif

    access_sequence->tx_buffer[index++] = starting_reg | REG_WRITE_BURST;

    memcpy(&access_sequence->tx_buffer[index], tx_buffer, buffer_size); /* Flawfinder: ignore */

    index                       += buffer_size;
    access_sequence->burst_mode =  true;
    access_sequence->index      =  index;
#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_NO_ERR;
#endif
}

void sr1000_access_adv_append_read_burst(access_sequence_instance_t *access_sequence,
                                         uint8_t starting_reg,
                                         uint8_t **buffer,
                                         uint16_t rx_size,
                                         uwb_err *error)
{
    uint32_t index = access_sequence->index;
#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_BUSY_ERR;

    if (burst_mode_is_on(access_sequence)) {
        *error = ACCESS_SEQUENCE_CANT_APPEND_IN_BURST_MODE_ERR;
        return;
    }

    if (is_sequence_at_max_capacity(access_sequence, rx_size)) {
        *error = ACCESS_SEQUENCE_TOO_MUCH_QUEUED_TRANSACTION_ERR;
        return;
    }
#else
    (void)error;
#endif

    access_sequence->tx_buffer[index++] = REG_READ_BURST | starting_reg;
    /* Change application pointer to the appropriate RX buffer address for real time update */
    *buffer  = &access_sequence->rx_buffer[index];
    index   += rx_size;

    access_sequence->burst_mode = true;

#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
    *error = ACCESS_SEQUENCE_NO_ERR;
#endif
    access_sequence->index = index;
}

void sr1000_access_adv_context_switch(radio_hal_adv_t *radio_adv)
{
    radio_adv->context_switch();
}

void sr1000_access_adv_enable_dma_irq(radio_hal_adv_t *radio_adv)
{
    radio_adv->enable_radio_dma_irq();
}

void sr1000_access_adv_disable_dma_irq(radio_hal_adv_t *radio_adv)
{
    radio_adv->disable_radio_dma_irq();
}

void sr1000_access_adv_enable_radio_irq(radio_hal_adv_t *radio_adv)
{
    radio_adv->enable_radio_irq();
}

void sr1000_access_adv_disable_radio_irq(radio_hal_adv_t *radio_adv)
{
    radio_adv->disable_radio_irq();
}

void sr1000_access_adv_spi_transfer_non_blocking(radio_hal_adv_t *radio_adv,
                                                 uint8_t *tx_buffer,
                                                 uint8_t *rx_buffer,
                                                 uint16_t size)
{
    radio_adv->transfer_full_duplex_non_blocking(tx_buffer, rx_buffer, size);
}

void sr1000_access_adv_spi_transfer_blocking(radio_hal_adv_t *radio_adv,
                                             uint8_t *tx_buffer,
                                             uint8_t *rx_buffer,
                                             uint16_t size)
{
    radio_adv->transfer_full_duplex_blocking(tx_buffer, rx_buffer, size);
}

/* PRIVATE FUNCTIONS **********************************************************/
#if (ACCESS_ADV_ERR_CHECK_EN > 0U)
/** @brief Check if sequence is at max capacity.
 *
 * This method is use to check max sequence capacity right before
 * incrementing the sequence index.
 *
 *  @param[in] access_sequence  Access sequence instance.
 *  @retval True   Sequence is at max capacity.
 *  @retval False  Sequence can still be populated.
 */
static bool is_sequence_at_max_capacity(access_sequence_instance_t *access_sequence, size_t size)
{
    if ((access_sequence->index + size) >= access_sequence->capacity) {
        return true;
    } else {
        return false;
    }
}

/** @brief Check if provided sequence is empty
 *
 *  @param[in] access_sequence  Access sequence instance.
 *  @retval True   Sequence is empty.
 *  @retval False  Sequence is not empty.
 */
static bool sequence_is_empty(access_sequence_instance_t *access_sequence)
{
    if (access_sequence->index == 0) {
        return true;
    } else {
        return false;
    }
}

/** @brief Check if provided buffer is empty
 *
 *  @param[in] buffer  Buffer pointer.
 *  @retval True   Buffer is empty.
 *  @retval False  Buffer is not empty.
 */
static bool buffer_is_empty(uint8_t *buffer)
{
    if (buffer != 0) {
        return false;
    } else {
        return true;
    }
}

/** @brief Check if burst write/read have been initiated
 *
 *  @param[in] access_sequence  Access sequence instance.
 *  @retval True   Burst mode is on.
 *  @retval False  Burst mode is off.
 *
 *  @note When burst mode is activated, user can no longer
 *        append stuff to the current sequence because
 *        the SPI CS pin need to be lowered in order to
 *        continue normal SPI operation.
 */
static bool burst_mode_is_on(access_sequence_instance_t *access_sequence)
{
    if (access_sequence->burst_mode) {
        return true;
    } else {
        return false;
    }
}
#endif
