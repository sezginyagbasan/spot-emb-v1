/** @file sr1000_nvm_private.h
 *  @brief SR1000 non-volatile memory private module.
 *
 *  Functions related to writing the NVM and to its protocol.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_NVM_PRIVATE_H_
#define SR1000_NVM_PRIVATE_H_

/* INCLUDES *******************************************************************/
#include <stdint.h>
#include "sr1000_nvm.h"

/* TYPES **********************************************************************/
/** @brief NVM structure.
 *
 */
typedef struct {
    void (*enable_vdd)(void);  /**< Enable NVM VDD power supply  */
    void (*disable_vdd)(void); /**< Disable NVM VDD power supply */
} nvm_vdd_hal_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
void sr1000_nvm_write(radio_hal_t *radio, nvm_vdd_hal_t *vdd, uint8_t *buf, uint8_t addr_start, uint8_t addr_end);



#endif /* SR1000_NVM_PRIVATE_H_ */
