/** @file sr1000_access_adv.h
 *  @brief SR1000 Protocol external access layer file.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_ACCESS_ADV_H_
#define SR1000_ACCESS_ADV_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "sr1000_access.h"
#include "sr1000_api.h"
#include "sr1000_api_hal_adv.h"
#include "uwb_error.h"

#ifdef __cplusplus
extern "C" {
#endif

/* MACRO **********************************************************************/
#ifndef UWB_ERR_TYPE
#define UWB_ERR_TYPE
typedef uint32_t uwb_err;
#endif
#ifndef ERR_BASE_ACCESS_ADV
#error Set the base error index in your project symbols.
#endif

/* ERROR **********************************************************************/
#ifndef ACCESS_ADV_ERR_CHECK_EN
#define ACCESS_ADV_ERR_CHECK_EN 0
#endif

#define ACCESS_SEQUENCE_NO_ERR                          ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0000) /**< No error */
#define ACCESS_SEQUENCE_INVALID_RX_BUFFER_ERR           ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0001) /**< RX buffer null error */
#define ACCESS_SEQUENCE_INVALID_TX_BUFFER_ERR           ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0002) /**< TX buffer null error */
#define ACCESS_SEQUENCE_TOO_MUCH_QUEUED_TRANSACTION_ERR ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0003) /**< Sequence overflow error */
#define ACCESS_SEQUENCE_CANT_APPEND_IN_BURST_MODE_ERR   ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0004) /**< Append after burst mode err */
#define ACCESS_SEQUENCE_EMPTY_SEQUENCE_ERR              ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0005) /**< Empty sequence error */
#define ACCESS_SEQUENCE_BUSY_ERR                        ((ERR_BASE_ACCESS_ADV << ERR_BASE_POS) + 0x0006) /**< Busy error */

/* PUBLIC FUNCTIONS ***********************************************************/
/** @brief Access sequence instance.
 */
typedef struct access_sequence {
    uint8_t *tx_buffer;             /**< TX buffer available for queuing register value to write */
    uint8_t *rx_buffer;             /**< RX buffer available for receiving value from register */
    uint8_t  index;                 /**< Sequence index */
    uint8_t  capacity;              /**< Max sequence capacity (Only support same RX/TX size)*/
    bool     burst_mode;
} access_sequence_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize an Access sequence structure
 *
 *  @param[out] access_sequence  Access sequence to initialize.
 *  @param[in]  app_rx_buffer    Application buffer for SPI RX allocation.
 *  @param[in]  app_tx_buffer    Application buffer for SPI TX allocation.
 *  @param[in]  size             Size of the application buffer (Only support RX/TX of same size).
 *  @param[out] error            Buffer error.
 */
void sr1000_access_adv_init(access_sequence_instance_t *access_sequence,
                            uint8_t *app_rx_buffer,
                            uint8_t *app_tx_buffer,
                            uint8_t size,
                            uwb_err *error);

/** @brief Append a read register command to the Access sequence
 *
 *  @param[in]  access_sequence  Access sequence instance.
 *  @param[in]  target_reg       Target register to read from.
 *  @param[in]  buffer           Application accessible read buffer.
 *  @param[out] error            Access sequence error.
 *
 *  @note The double pointer design choice here is to
 *        be able to change the location of the application buffer
 *        in run-time so that the value inside is updated right when
 *        the after the transfer is done.
 */
void sr1000_access_adv_append_read(access_sequence_instance_t *access_sequence,
                                   uint8_t target_reg,
                                   uint8_t **buffer,
                                   uwb_err *error);

/** @brief Append a write register command to the Access sequence
 *
 *  @param[in]  access_sequence  Access sequence instance.
 *  @param[in]  target_reg       Target register to write to.
 *  @param[in]  value            Value to write in register.
 *  @param[out] error            Access sequence error.
 */
void sr1000_access_adv_append_write(access_sequence_instance_t *access_sequence,
                                    uint8_t target_reg,
                                    uint8_t value,
                                    uwb_err *error);

/** @brief Initiate a SPI full duplex transfer in blocking mode
 *
 *  @param[in]  radio_hal_adv    Full duplex transfer implementation.
 *  @param[in]  access_sequence  Access sequence instance.
 *  @param[out] error            Access sequence error.
 */
void sr1000_access_adv_transfer_blocking(radio_hal_adv_t *radio_hal_adv,
                                         access_sequence_instance_t *access_sequence,
                                         uwb_err *error);

/** @brief Initiate a SPI full duplex transfer in non blocking mode
 *
 *  @param[in]  radio_hal_adv    Full duplex transfer implementation.
 *  @param[in]  access_sequence  Access sequence instance.
 *  @param[out] error            Access sequence error.
 */
void sr1000_access_adv_transfer_non_blocking(radio_hal_adv_t *radio_hal_adv,
                                             access_sequence_instance_t *access_sequence,
                                             uwb_err *error);

/** @brief Check if SPI is busy
 *
 *  @param[in] radio_hal_adv  Full duplex transfer implementation.
 */
bool sr1000_access_adv_is_spi_busy(radio_hal_adv_t *radio_adv);

/** @brief Reset access advance buffer index
 *
 *  @param[in] radio_hal_adv  Full duplex transfer implementation.
 */
void sr1000_access_reset_index(access_sequence_instance_t *access_sequence);

/** @brief Append a write burst command to the sequence
 *
 *  @note One should not append any of the other command after
 *        a burst command because the CS pin should be lowered
 *        at the end of a burst transaction in order to access
 *        other register that are not following.
 *
 *  @param[in]  access_sequence  Access sequence instance.
 *  @param[in]  starting_reg     Starting register to write to. Subsequent value will be written to following register.
 *  @param[in]  tx_buffer        Data to transfer in burst mode.
 *  @param[in]  buffer_size      Number of byte to transfer.
 *  @param[out] error            Output error.
 */
void sr1000_access_adv_append_write_burst(access_sequence_instance_t *access_sequence,
                                          uint8_t starting_reg,
                                          uint8_t *tx_buffer,
                                          uint16_t buffer_size,
                                          uwb_err *error);

/** @brief Append a read burst command to the sequence
 *
 *  @note One should not append any of the other command after
 *        a burst command because the CS pin should be lowered
 *        at the end of a burst transaction in order to access
 *        other register that are not following.
 *
 *  @param[in]  access_sequence  Access sequence instance.
 *  @param[in]  starting_reg     Starting register to read from.
 *  @param[in]  buffer           Buffer that will contain received data.
 *  @param[in]  buffer_size      Number of byte to receive.
 *  @param[out] error            Output error.
 *
 *  @note The double pointer design choice here is to
 *        be able to change the location of the application buffer
 *        in run-time so that the value inside is updated right when
 *        the after the transfer is done.
 */
void sr1000_access_adv_append_read_burst(access_sequence_instance_t *access_sequence,
                                         uint8_t starting_reg,
                                         uint8_t **buffer,
                                         uint16_t rx_size,
                                         uwb_err *error);

/** @brief Trigger a context switch to the Radio IRQ context.
 *
 *  @param[in] radio_adv  Radio HAL ADV instance.
 */
void sr1000_access_adv_context_switch(radio_hal_adv_t *radio_adv);

/** @brief Enable the Radio DMA interrupt.
 *
 *  @param[in] radio_adv  Radio HAL ADV instance.
 */
void sr1000_access_adv_enable_dma_irq(radio_hal_adv_t *radio_adv);

/** @brief Disable the Radio DMA interrupt.
 *
 *  @param[in] radio_adv  Radio HAL ADV instance.
 */
void sr1000_access_adv_disable_dma_irq(radio_hal_adv_t *radio_adv);

/** @brief Enable the Radio external interrupt.
 *
 *  @param[in] radio_adv  Radio HAL ADV instance.
 */
void sr1000_access_adv_enable_radio_irq(radio_hal_adv_t *radio_adv);

/** @brief Disable the Radio external interrupt.
 *
 *  @param[in] radio_adv  Radio HAL ADV instance.
 */
void sr1000_access_adv_disable_radio_irq(radio_hal_adv_t *radio_adv);

/** @brief Initiate an SPI transfer in non blocking mode
 *
 *  @param[in]  radio_adv  Radio HAL ADV instance.
 *  @param[in]  tx_buffer  Pointer to the buffer to send to the radio.
 *  @param[out] rx_buffer  Buffer containing the radio response.
 *  @param[in]  size       Size of the transfer.
 */
void sr1000_access_adv_spi_transfer_non_blocking(radio_hal_adv_t *radio_adv,
                                                 uint8_t         *tx_buffer,
                                                 uint8_t         *rx_buffer,
                                                 uint16_t         size);

/** @brief Initiate an SPI transfer in blocking mode
 *
 *  @param[in]  radio_adv  Radio HAL ADV instance.
 *  @param[in]  tx_buffer  Pointer to the buffer to send to the radio.
 *  @param[out] rx_buffer  Buffer containing the radio response.
 *  @param[in]  size       Size of the transfer.
 */
void sr1000_access_adv_spi_transfer_blocking(radio_hal_adv_t *radio_adv,
                                             uint8_t         *tx_buffer,
                                             uint8_t         *rx_buffer,
                                             uint16_t         size);
#ifdef __cplusplus
}
#endif
#endif /* SR1000_ACCESS_ADV_H_ */
