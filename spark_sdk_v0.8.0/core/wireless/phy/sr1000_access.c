/** @file sr1000_access.c
 *  @brief SR1000 Protocol access layer file.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "sr1000_access.h"
#include "sr1000_reg.h"

/* PUBLIC FUNCTIONS ***********************************************************/
void sr1000_access_open(radio_hal_t *radio)
{
    radio->reset_cs();
}

void sr1000_access_close(radio_hal_t *radio)
{
    radio->set_cs();
}

void sr1000_access_burst_end(radio_hal_t *radio)
{
    radio->set_cs();
    radio->reset_cs();
}

void sr1000_access_burst_read(radio_hal_t *radio, uint8_t reg_addr, uint8_t *data, uint8_t size)
{
    uint8_t burst_reg = REG_READ_BURST | reg_addr;

    radio->write(&burst_reg, 1);
    /* Check buffer boundaries if used in a loop including recursive loops (CWE-120, CWE-20) */
    /* Flawfinder: Ignore */
    radio->read(data, size);
}

void sr1000_access_burst_write(radio_hal_t *radio, uint8_t reg_addr, uint8_t *data, uint8_t size)
{
    uint8_t burst_reg = REG_WRITE_BURST | reg_addr;

    radio->write(&burst_reg, 1);
    radio->write(data, size);
}

void sr1000_access_write_reg(radio_hal_t *radio, uint8_t reg_addr, uint8_t data)
{
    uint8_t write_reg = REG_WRITE | reg_addr;

    radio->write(&write_reg, 1);
    radio->write(&data, 1);
}

uint8_t sr1000_access_read_reg(radio_hal_t *radio, uint8_t reg_addr)
{
    uint8_t data;

    radio->write(&reg_addr, 1);
    /* Check buffer boundaries if used in a loop including recursive loops (CWE-120, CWE-20) */
    /* Flawfinder: Ignore */
    radio->read(&data, 1);

    return data;
}

void sr1000_access_wait_for_bit_set(radio_hal_t *radio, uint8_t reg, uint8_t bit)
{
    uint8_t val = 0x00;

    do {
        val = sr1000_access_read_reg(radio, reg);
    } while (!(val & bit));
}

void sr1000_access_wait_for_bit_clear(radio_hal_t *radio, uint8_t reg, uint8_t bit)
{
    uint8_t val = 0xff;

    do {
        val = sr1000_access_read_reg(radio, reg);
    } while (val & bit);
}
