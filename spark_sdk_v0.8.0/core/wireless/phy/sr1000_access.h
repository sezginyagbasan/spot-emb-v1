/** @file sr1000_access.h
 *  @brief SR1000 hardware abstraction layer.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_ACCESS_H_
#define SR1000_ACCESS_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "sr1000_api_hal.h"

/* PUBLIC FUNCTIONS ***********************************************************/
/** @brief Open the communication with the radio.
 *
 *  @param[in] radio  Radio's instance.
 */
void sr1000_access_open(radio_hal_t *radio);

/** @brief Close the communication with the radio.
 *
 *  @param[in] radio  Radio's instance.
 */
void sr1000_access_close(radio_hal_t *radio);

/** @brief End a burst sequence on the radio communication.
 *
 *  @param[in] radio  Radio's instance.
 */
void sr1000_access_burst_end(radio_hal_t *radio);

/** @brief Start a burst read sequence with the radio.
 *
 *  @note This will read one or multiple bytes on the bus.
 *
 *  @param[in]  radio     Radio's instance.
 *  @param[in]  reg_addr  Address of the register in radio to read value.
 *  @param[out] data      Data received.
 *  @param[in]  size      Size to read.
 */
void sr1000_access_burst_read(radio_hal_t *radio, uint8_t reg_addr, uint8_t *data, uint8_t size);

/** @brief Start a burst write sequence with the radio.
 *
 *  @note This will write one or multiple bytes on the bus.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] reg_addr  Address of the register in radio to burst write.
 *  @param[in] data      Data to write.
 *  @param[in] size      Size to write.
 */
void sr1000_access_burst_write(radio_hal_t *radio, uint8_t reg_addr, uint8_t *data, uint8_t size);

/** @brief Write data in target radio's register.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] reg_addr  Address of the register in radio to write value.
 *  @param[in] data      Data to write.
 */
void sr1000_access_write_reg(radio_hal_t *radio, uint8_t reg_addr, uint8_t data);

/** @brief Read data in target radio's register.
 *
 *  @param[in] radio     Radio's instance.
 *  @param[in] reg_addr  Address of the register in radio to read from.
 *
 *  @return Value of the register.
 */
uint8_t sr1000_access_read_reg(radio_hal_t *radio, uint8_t reg_addr);

/** @brief Wait for a register bit to be set.
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] reg    Register of the bit to be set.
 *  @param[in] bit    Bit to wait for.
 */
void sr1000_access_wait_for_bit_set(radio_hal_t *radio, uint8_t reg, uint8_t bit);

/** @brief Wait for a register bit to be clear.
 *
 *  @param[in] radio  Radio's instance.
 *  @param[in] reg    Register of the bit to be clear.
 *  @param[in] bit    Bit to wait for.
 */
void sr1000_access_wait_for_bit_clear(radio_hal_t *radio, uint8_t reg, uint8_t bit);


#endif /* SR1000_ACCESS_H_ */
