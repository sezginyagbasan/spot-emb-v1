/** @file sr1000_reg.h
 *  @brief SR1010/SR1020 registers map main include.
 *
 *  @copyright Copyright (C) 2018 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef SR1000_REG_H_
#define SR1000_REG_H_

/* MACROS *********************************************************************/
#define BIT(n)              (1 << n)                                          /**< Register field single bit mask */
#define BITS2SHIFT(mask)    (mask & -mask)                                    /**< Calculates leftshift for given mask
                                                                                *  (e.g BITS2SHIFT(0x30) = 16, where
                                                                                *  log2(16) = 4, to shift 4).
                                                                                */
#define BITS8(b, a)         ((0xff >> (7 - (b))) & ~((1U << (a)) - 1))        /**< Register field multi bit mask, 8-bit. */
#define BITS16(b, a)        ((0xffff >> (15 - (b))) & ~((1U << (a)) - 1))     /**< Register field multi bit mask, 16-bit */
#define BITS24(b, a)        ((0xffffff >> (23 - (b))) & ~((1U << (a)) - 1))   /**< Register field multi bit mask, 24-bit */
#define BITS32(b, a)        ((0xffffffff >> (31 - (b))) & ~((1U << (a)) - 1)) /**< Register field multi bit mask, 32-bit */
#define MASK2VAL(val, mask) ((val & mask) / BITS2SHIFT(mask))                 /**< Returns values written in mask        */
#define MOV2MASK(val, mask) ((val * BITS2SHIFT(mask)) & mask)                 /**< Returns a value within the given mask */

/* CONSTANTS ******************************************************************/
#define REG_READ_BURST   BIT(7)
#define REG_WRITE        BIT(6)
#define REG_WRITE_BURST (BIT(7) | REG_WRITE)

/* INCLUDES *******************************************************************/
#include "sr1000_reg_v8_2.h"


#endif /* SR1000_REG_H_ */
