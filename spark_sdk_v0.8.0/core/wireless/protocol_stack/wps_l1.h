/** @file  wps_l1.h
 *  @brief Wireless protocol stack Layer 1: PHY control.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef WPS_L1_H
#define WPS_L1_H

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "circular_queue.h"
#include "sr1000_api.h"
#include "sr1000_api_adv.h"
#include "xlayer.h"

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#define STATE_Q_SIZE 10

/* TYPES **********************************************************************/
/** @brief Wireless protocol stack Layer 1 input signal.
 */
typedef enum l1_input_signal {
    L1_SIGNAL_RADIO_IRQ = 0,
    L1_SIGNAL_DMA_CMPLT,
    L1_SIGNAL_PREPARE_RADIO,
    L1_SIGNAL_SYNCING
} l1_input_signal_t;

/** @brief Wireless protocol stack Layer 1 output signal.
 */
typedef enum l1_output_signal {
    L1_SIGNAL_NONE = 0,
    L1_SIGNAL_PROCESSING,
    L1_SIGNAL_FRAME_SENT_ACK,
    L1_SIGNAL_FRAME_SENT_NACK,
    L1_SIGNAL_FRAME_RECEIVED,
    L1_SIGNAL_FRAME_MISSED,
    L1_SIGNAL_PREPARE_DONE,
    L1_SIGNAL_YIELD,
    L1_SIGNAL_ERROR
} l1_output_signal_t;

/** @brief Layer one state machine function pointer type.
 */
typedef void (*wps_l1_state_t)(void *signal_data);

/** @brief TX configuration settings for the layer 1
 */
typedef struct l1_tx_frame {
    modulation_t       *modulation;        /**< Modulation */
    fec_level_t        *fec;               /**< FEC level */
    uint8_t            *cca_threshold;     /**< Clear Channel Assessment threshold */
    xlayer_frame_t     *frame;             /**< Cross layer frame */
    l1_output_signal_t *signal;            /**< Wireless protocol stack Layer 1 output signal */
    uint8_t             payload_size;      /**< TX payload size (used for cut-through mode) */
    uint16_t            cca_retry_time;    /**< CCA retry time */
    uint8_t             cca_max_try_count; /**< CCA max try count */
    uint8_t             cca_try_count;     /**< CCA try count */
    cca_fail_action_t   cca_fail_action;   /**< CCA fail action */
} l1_tx_frame_t;

/** @brief RX configuration settings for the layer 1
 */
typedef struct l1_rx_frame {
    uint8_t            *rx_constgain; /**< Receiver constant gain */
    uint32_t           *rnsi_raw;     /**< RNSI in 1/10 dB */
    uint32_t           *rssi_raw;     /**< RSSI in 1/10 dB */
    xlayer_frame_t     *frame;        /**< Cross layer frame */
    l1_output_signal_t *signal;       /**< Wireless protocol stack Layer 1 output signal */
    uint8_t             payload_size; /**< RX payload size (used for cut-through mode) */
} l1_rx_frame_t;

/** @brief General configuration settings for the layer 1
 */
typedef struct l1_frame_cfg {
    uint16_t     *destination_address; /**< Main frame destination address */
    uint16_t     *source_address;      /**< Main frame source address */
    rf_channel_t *channel;             /**< RF channel settings */
    uint16_t     *power_up_delay;      /**< Power up delay */
    uint16_t     *rx_timeout;          /**< RX timeout */
    uint16_t     *sleep_time;          /**< Sleep time in PLL cycles */
    uint16_t     *rx_wait_time;        /**< RX wait time */
    packet_cfg_t  packet_cfg;          /**< Packet configuration */
    sleep_lvl_t   sleep_level;         /**< Sleep Level */
} l1_frame_cfg_t;

typedef struct wps_l1 {
    l1_input_signal_t input_signal;  /**< Wireless protocol stack Layer 1 input signal*/

    l1_output_signal_t signal_main;  /**< Wireless protocol stack Layer 1 output signal main*/
    l1_output_signal_t signal_auto;  /**< Wireless protocol stack Layer 1 output signal auto*/

    radio_adv_t *radio_adv;          /**< Already initialized radio ADV. */

    uint16_t     source_address;     /**< Node source address*/
    auto_reply_t auto_reply_mode;    /**< Auto reply mode*/
    xlayer_t    *xlayer_main;        /**< Main cross layer*/
    xlayer_t    *xlayer_auto;        /**< Auto cross layer*/

    /* internal variables */
    wps_l1_state_t  *current_state;                 /**< Current state machine state*/
    circular_queue_t next_states;                   /**< Next state machine state queue*/
    wps_l1_state_t  *next_state_pool[STATE_Q_SIZE]; /**< Next state machine state pool*/
    uint8_t          state_step;                    /**< State index*/

    l1_frame_cfg_t cfg;             /**< General configuration settings for the layer 1*/
    l1_tx_frame_t  tx;              /**< TX configuration settings for the layer 1*/
    l1_rx_frame_t  rx;              /**< RX configuration settings for the layer 1*/

    rx_wait_time_t rx_wait;         /**< RX wait time */
    uint8_t       *rssi;            /**< RSSI in 1/10 dB */
    uint8_t       *rnsi;            /**< RNSI in 1/10 dB */
    uint8_t       *irq_status_1;    /**< radio_events byte 1 */
    uint8_t       *irq_status_2;    /**< radio_events byte 2*/
    uint8_t       *rx_frame_size;   /**< RX frame size*/
    uint8_t       *pwr_status_cmd;  /**< Pwr status and command register value */

    uint8_t        partial_frame_count; /**< The number of parts the frame will be divided in for cut-through mode */
    uint8_t        partial_frame_index; /**< Current partial frame for cut through mode */

    uint16_t       syncing_period_pll_cycles; /**<  Syncing period in PLL cycles */
} wps_l1_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize The layer 1 of the wireless protocol stack.
 *
 *  @param[in]  wps_l1     WPS L1 instance.
 *  @param[in]  radio_adv  Radio's advance instance.
 */
void wps_l1_init(wps_l1_t *wps_l1, radio_adv_t *radio_adv);

/** @brief Reset the Layer 1 object.
 *
 *  @param wps_l1 WPS L1 instance.
 */
void wps_l1_reset(wps_l1_t *wps_l1);

/** @brief Process the Layer 1 state machine of the wireless protocol stack.
 *
 *  This function should be called by the WPS inside the dma or the radio interrupt.
 *
 *  @param[in]  wps_l1  WPS L1 instance.
 */
void wps_l1_process(wps_l1_t *wps_l1);

/** @brief Set syncing RX period duration.
 *
 *   @param[in] wps_l1        WPS L1 instance.
 *   @param[in] sleep_period  Time in PLL cycle the radio is sleeping between RX period.
 */
void wps_l1_set_syncing_duration(wps_l1_t *wps_l1, uint16_t syncing_period_pll_cycles);

#ifdef __cplusplus
}
#endif

#endif /* WPS_L1_H */
