/** @file  wps_def.h
 *  @brief Wireless Protocol Stack definitions used by multiple modules.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef WPS_DEFS_H
#define WPS_DEFS_H

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include "circular_queue.h"
#include "link_cca.h"
#include "link_gain_loop.h"
#include "link_lqi.h"
#include "link_protocol.h"
#include "link_random_datarate_offset.h"
#include "link_saw_arq.h"
#include "sr1000_api.h"
#include "sr1000_api_adv.h"
#include "sr1000_def.h"
#include "wps_error.h"

/* CONSTANTS ******************************************************************/
#define WPS_RADIO_FIFO_SIZE   128
#define WPS_RADIO_HEADER_SIZE sizeof(wps_l2_frame_header_t)
#define WPS_MAX_PAYLOAD_SIZE  WPS_RADIO_FIFO_SIZE - WPS_RADIO_HEADER_SIZE - 1

#ifndef WPS_NB_RF_CHANNEL
#define WPS_NB_RF_CHANNEL 5
#endif

/* TYPES **********************************************************************/
typedef struct wps_connection wps_connection_t;

typedef struct wps_stats {
    uint32_t tx_sent;          /* Number of payload sent */
    uint32_t tx_byte_sent;     /* Number of byte sent */
    uint32_t tx_dropped;       /* Number of payload dropped */
    uint32_t rx_received;      /* Number of payload received */
    uint32_t rx_byte_received; /* Number of byte received */
    uint32_t rx_overrun;       /* Number of payload dropped because of an RX buffer overrun */
} wps_stats_t;

/** @brief WPS Connection
 */
struct wps_connection {
    uint16_t source_address;                   /**< Source address */
    uint16_t destination_address;              /**< Destination address */

    bool fixed_payload_size_enable;            /**< Fixed payload size enable flag (necessary for Frames lager than RX FIFO) */
    uint8_t fixed_payload_size;                /**< Frame size (only used if fixed frame size mode is enabled) */
    wps_error_t wps_error;
    /* Layer 2 */
    bool ack_enable;                           /**< Ack received frame or expect ack when sending frame */
    bool auto_sync_enable;                     /**< Auto sync mode enable flag*/
    bool internal_connection;                  /**< Enable/disable L2 internal connection. */
    link_protocol_t *link_protocol;            /**< Internal connection protocol */
    link_rdo_t      *link_rdo;                 /**< Internal connection Random Datarate Offset (RDO) instances. */
    saw_arq_t stop_and_wait_arq;               /**< Stop and Wait (SaW) and Automatic Repeat Query (ARQ) */
    link_cca_t cca;                            /**< Clear Channel Assessment */
    lqi_t lqi;                                 /**< Link quality indicator */
    lqi_t used_frame_lqi;                      /**< WPS frames Link quality indicator (Excludes unused or sync timeslots)*/
    lqi_t channel_lqi[WPS_NB_RF_CHANNEL];      /**< Channel frames Link quality indicator */
    wps_stats_t wps_stats;                     /**< Wireless protocol stack statistics */

    /* Gain loop */
    gain_loop_t gain_loop[WPS_NB_RF_CHANNEL];  /**< Gain loop */

    /* Queue */
    circular_queue_t xlayer_queue;             /**< Cross layer queue */

    /* Layer 1 */
    frame_cfg_t  frame_cfg;                    /**< Connection frame config */
    sleep_lvl_t  sleep_lvl;                    /**< Connection sleep level */
    rf_channel_t channel[WPS_NB_RF_CHANNEL];   /**< RF Channel information */
    packet_cfg_t packet_cfg;                   /**< Packet configuration */

    /* Callback */
    void (*send_ack_callback_t)(wps_connection_t *connection);  /**< Function called by the wps to indicate the frame has been acknowledged */
    void (*send_nack_callback_t)(wps_connection_t *connection); /**< Function called by the wps to indicate the frame has NOT been acknowledged */
    void (*receive_callback_t)(wps_connection_t *connection);   /**< Function called by the wps to indicate the frame has been received */
    void (*error_callback_t)(wps_connection_t *connection);     /**< Function called by the wps to indicate that an error has occurs */
    uint64_t (*get_tick_quarter_ms)(void);                      /**< Get free running timer tick in quarter ms */
};

/** @brief WPS role.
 */
typedef enum wps_role {
    NETWORK_COORDINATOR = 0, /**< Coordinator dictate the time to the whole network */
    NETWORK_NODE             /**< Node re-adjust its timer to stay in sync with coordinator */
} wps_role_t;

typedef struct node {
    radio_adv_t    *radio;
    wps_role_t      role;
    uint16_t        local_address;
    syncword_cfg_t  syncword_cfg;
    uint32_t        preamble_len;
    sleep_lvl_t     sleep_lvl;
    uint16_t        crc_polynomial;
} wps_node_t;

/** @brief WPS specific PHY parameters structure.
 */
typedef struct wps_phy {
    bool        lna_imped; /**< Enable/Disable LNA impedance */
    manuphase_t manuphase; /**< Enable/Disable phase tracking */
} wps_phy_t;

/** @brief Received frame.
 */
typedef struct rx_frame {
    uint8_t *payload;  /**< Pointer to payload */
    uint8_t  size;     /**< Size of payload */
} wps_rx_frame;

/** @brief L2 internal connection config structure.
 *
 *  @note As of right now, only one protocol (RDO) is activated
 *        in the internal connection. The buffer size should
 *        be set to at least 2.
 */
typedef struct wps_internal_connection_cfg {
    uint8_t    *buffer;              /**< Internal connection RX/TX buffer */
    uint8_t     buffer_size;         /**< Internal connection buffer size */
    link_rdo_t *rdo_inst;            /**< Internal connection link rdo instance */
    bool       *rdo_enable;          /**< Enable random datarate offset */
    uint16_t   *rdo_rollover_value;  /**< Random datarate offset rollover value, u16 */
    uint8_t     rdo_num;
} wps_internal_connection_cfg_t;

typedef void (*wps_callback_t)(wps_connection_t *connection);

#ifdef __cplusplus
}
#endif
#endif /* WPS_DEFS_H */
