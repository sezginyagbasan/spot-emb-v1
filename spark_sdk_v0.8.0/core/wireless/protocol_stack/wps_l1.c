/** @file  wps_l1.c
 *  @brief Wireless protocol stack Layer 1: PHY control.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "wps_l1.h"
#include "wps.h"

/* CONSTANTS ******************************************************************/
#define MAX_CUT_THROUGH_FRAME_SIZE 255
#define EMPTY_BYTE                 1
#define PARTIAL_FRAME_COUNT        3
#define PARTIAL_FRAME_BASE_INDEX   0
#define SIZE_HDR_SIZE              1
#define DEFAULT_RX_IDLE_PWR        3
#define DISABLE_CCA_THRSH_VALUE    0xff

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void prepare_l1(void *signal_data);
static void prepare_radio(void *signal_data);
static void set_config(void *signal_data);
static void enable_radio_irq(void *signal_data);
static void check_radio_irq(void *signal_data);
static void set_header(void *signal_data);
static void set_payload(void *signal_data);
static void set_payload_cut_through(void *signal_data);
static void read_events(void *signal_data);
static void read_events_syncing(void *signal_data);
static void process_event(void *signal_data);
static void handle_good_frame(wps_l1_t *wps_l1);
static void get_header(void *signal_data);
static void get_payload(void *signal_data);
static void setup_cut_through(void *signal_data);
static void get_payload_cut_through(void *signal_data);
static void handle_missed_frame(wps_l1_t *wps_l1);
static void handle_cca_fail(wps_l1_t *wps_l1);
static void end_flush(void *signal_data);
static void end_tx_cut_through(void *signal_data);
static void close_spi(void *signal_data);
static void signal_yield(void *signal_data);
static void end(void *signal_data);
static void clear_err(wps_l1_t *wps_l1);
static void prepare_syncing(void *signal_data);

static bool           main_is_tx(wps_l1_t *l1);
static bool           auto_is_tx(wps_l1_t *l1);
static bool           tx_complete(radio_events_t radio_events);
static bool           rx_good(radio_events_t radio_events);
static bool           rx_rejected(radio_events_t radio_events);
static bool           rx_lost(radio_events_t radio_events);
static bool           frame_fits_in_radio_fifo(uint8_t payload_size);
static uint8_t        get_bufload_thresh_tx(fec_level_t fec_level, uint8_t partial_frame_size);
static uint8_t        get_bufload_thresh_rx(fec_level_t fec_level, uint8_t partial_frame_size);
static bool           is_fixed_payload_size_disabled(save_size_t save_size);
static radio_events_t set_events_for_tx_with_ack(void);
static radio_events_t set_events_for_tx_without_ack(void);
static radio_events_t set_events_for_rx_with_ack(void);
static radio_events_t set_events_for_rx_without_ack(void);
static radio_events_t set_events_for_rx_with_auto_payload(void);

/* TYPES **********************************************************************/
static wps_l1_state_t prepare_l1_states[]                = {prepare_l1, end};
static wps_l1_state_t set_config_states[]                = {set_config, end};
static wps_l1_state_t prepare_radio_cut_through_states[] = {set_header, set_payload_cut_through, prepare_radio, signal_yield,
                                                            close_spi, set_config, close_spi, enable_radio_irq, end};
static wps_l1_state_t set_header_states[]                = {close_spi, set_header, end};
static wps_l1_state_t set_payload_states[]               = {set_payload, end};
static wps_l1_state_t set_payload_cut_through_states[]   = {set_payload_cut_through, close_spi, enable_radio_irq, end};
static wps_l1_state_t wait_radio_states[]                = {close_spi, enable_radio_irq, read_events, close_spi, process_event, end};
static wps_l1_state_t get_event_states[]                 = {read_events, close_spi, process_event, end};
static wps_l1_state_t get_header_states[]                = {close_spi, get_header, end};
static wps_l1_state_t get_payload_states[]               = {get_payload, end};
static wps_l1_state_t get_payload_cut_through_states[]   = {close_spi, setup_cut_through, get_payload_cut_through, end};
static wps_l1_state_t new_frame_states[]                 = {close_spi, end};
static wps_l1_state_t flush_fifo_states[]                = {close_spi, end_flush, end};
static wps_l1_state_t end_tx_cut_through_states[]        = {close_spi, end_tx_cut_through, end};
static wps_l1_state_t syncing_states[]                   = {close_spi, enable_radio_irq, read_events_syncing, close_spi, process_event, end};
static wps_l1_state_t wait_to_send_auto_reply[]          = {check_radio_irq, end};

/* PRIVATE GLOBALS ************************************************************/
static l1_output_signal_t unused_signal;
static xlayer_frame_t     unused_frame;

/* PUBLIC FUNCTIONS ***********************************************************/
void wps_l1_init(wps_l1_t *wps_l1, radio_adv_t *radio_adv)
{
    wps_l1->state_step    = 0;
    wps_l1->radio_adv     = radio_adv;
    circular_queue_init(&wps_l1->next_states, wps_l1->next_state_pool, STATE_Q_SIZE, sizeof(wps_l1_state_t **));
    wps_l1->current_state = prepare_l1_states;
}

void wps_l1_reset(wps_l1_t *wps_l1)
{
    wps_l1->state_step    = 0;
    wps_l1->current_state = prepare_l1_states;
    circular_queue_init(&wps_l1->next_states, wps_l1->next_state_pool, STATE_Q_SIZE, sizeof(wps_l1_state_t **));
}

void wps_l1_process(wps_l1_t *wps_l1)
{
    wps_l1->signal_main = L1_SIGNAL_PROCESSING;

    while (wps_l1->signal_main == L1_SIGNAL_PROCESSING) {
        wps_l1->current_state[wps_l1->state_step++](wps_l1);
    }
    if (wps_l1->current_state[wps_l1->state_step] == end) {
        wps_l1->current_state[wps_l1->state_step](wps_l1);
    }
}

void wps_l1_set_syncing_duration(wps_l1_t *wps_l1, uint16_t syncing_period_pll_cycles)
{
    wps_l1->syncing_period_pll_cycles = syncing_period_pll_cycles;
}

/* PRIVATE FUNCTION ***********************************************************/
/** @brief Enqueue a new state to the state machine.
 *
 *   @param[in] wps_l1  L1 instance struct.
 *   @param[in] state   New state to enqueue.
 */
static void enqueue_states(wps_l1_t *wps_l1, wps_l1_state_t *state)
{
    wps_l1_state_t **enqueue_states;

    enqueue_states  = (wps_l1_state_t **)circular_queue_get_free_slot_raw(&wps_l1->next_states);
    *enqueue_states = state;
    circular_queue_enqueue_raw(&wps_l1->next_states);
}

/** @brief Setup the state machine to send the frame payload to the radio.
 *
 *  @param[in] wps_l1        L1 instance struct.
 *  @param[in] payload_size  Frame payload size.
 */
static void enqueue_tx_prepare_frame_states(wps_l1_t *wps_l1, uint8_t header_size, uint8_t payload_size)
{
    if (header_size != 0) {
        enqueue_states(wps_l1, set_header_states);
    }

    if (payload_size != 0) {
        enqueue_states(wps_l1, set_payload_states);
    }
    enqueue_states(wps_l1, wait_radio_states);
}

/** @brief Setup the state machine to receive payload from the radio.
 *
 *  @param[in] wps_l1  L1 instance struct.
 */
static void enqueue_rx_prepare_frame_states(wps_l1_t *wps_l1)
{
    enqueue_states(wps_l1, wait_radio_states);
}

/** @brief Setup the L1 state machine.
 *
 *  @param[in] wps_l1        L1 instance struct.
 *  @param[in] payload_size  Frame payload size.
 */
static void prepare_l1(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal == L1_SIGNAL_PREPARE_RADIO) {
        l1->partial_frame_index = PARTIAL_FRAME_BASE_INDEX;
        l1->partial_frame_count = PARTIAL_FRAME_COUNT;

        if (main_is_tx(l1)) {
            l1->tx.frame        = &l1->xlayer_main->frame;
            l1->tx.payload_size = l1->tx.frame->payload_end_it - l1->tx.frame->payload_begin_it;
            if (!frame_fits_in_radio_fifo(l1->tx.payload_size)) {
                enqueue_states(l1, prepare_radio_cut_through_states);

                wps_l1_state_t **next_state = (wps_l1_state_t **)circular_queue_front_raw(&l1->next_states);

                if (*next_state == new_frame_states) {
                    l1->signal_main = L1_SIGNAL_YIELD;
                }
            } else {
                enqueue_states(l1, set_config_states);
                prepare_radio(l1);
            }
        } else {
            enqueue_states(l1, set_config_states);
            prepare_radio(l1);
        }

    } else if (l1->input_signal == L1_SIGNAL_SYNCING) {
        enqueue_states(l1, syncing_states);
        prepare_syncing(l1);
    } else {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }
}

/** @brief Sub function to prepare a transmit frame.
 *
 *  @param[in] signal_data   Data required to process the state. The type shall be wps_l1_t.
 *  @param[in] radio_events  Radio's interrupt events enumeration.
 *  @param[in] reg_actions   Radio actions instance.
 */
static void prepare_radio_tx(wps_l1_t *l1, radio_events_t *radio_events, radio_actions_t *reg_actions)
{
    uint8_t header_size;

    l1->cfg.destination_address = &l1->xlayer_main->config.destination_address;
    l1->cfg.source_address      = &l1->xlayer_main->config.source_address;
    l1->cfg.power_up_delay      = &l1->xlayer_main->config.power_up_delay;
    l1->cfg.rx_timeout          = &l1->xlayer_main->config.rx_timeout;
    l1->cfg.sleep_time          = &l1->xlayer_main->config.sleep_time;
    l1->cfg.rx_wait_time        = &l1->xlayer_main->config.rx_wait_time;
    l1->cfg.packet_cfg          =  l1->xlayer_main->config.packet_cfg;
    l1->cfg.sleep_level         = l1->xlayer_main->config.sleep_level;
    l1->tx.modulation           = &l1->xlayer_main->config.modulation;
    l1->tx.fec                  = &l1->xlayer_main->config.fec;
    l1->tx.cca_threshold        = &l1->xlayer_main->config.cca_threshold;
    l1->tx.cca_retry_time       =  l1->xlayer_main->config.cca_retry_time;
    l1->tx.cca_max_try_count    =  l1->xlayer_main->config.cca_max_try_count;
    l1->tx.cca_fail_action      =  l1->xlayer_main->config.cca_fail_action;
    l1->tx.cca_try_count        =  0;
    l1->tx.frame                = &l1->xlayer_main->frame;
    l1->tx.signal               = &l1->signal_main;

    header_size = l1->tx.frame->header_end_it - l1->tx.frame->header_begin_it;

    /* Auto-reply mode */
    if (l1->xlayer_auto != NULL) {
        l1->auto_reply_mode        = AUTO_REPLY_ENABLE;
        l1->cfg.channel            = &l1->xlayer_auto->config.channel;
        l1->rx.rx_constgain        = &l1->xlayer_auto->config.rx_constgain;
        l1->rx.rssi_raw            = &l1->xlayer_auto->config.rssi_raw;
        l1->rx.rnsi_raw            = &l1->xlayer_auto->config.rnsi_raw;
        l1->rx.frame               = &l1->xlayer_auto->frame;
        l1->rx.signal              = &l1->signal_auto;
        *radio_events              = set_events_for_tx_with_ack();
        /* Ack mode */
    } else if (l1->xlayer_main->config.expect_ack) {
        l1->auto_reply_mode        = AUTO_REPLY_ENABLE;
        l1->cfg.channel            = &l1->xlayer_main->config.channel;
        l1->rx.rx_constgain        = &l1->xlayer_main->config.rx_constgain;
        l1->rx.rssi_raw            = &l1->xlayer_main->config.rssi_raw;
        l1->rx.rnsi_raw            = &l1->xlayer_main->config.rnsi_raw;
        l1->rx.frame               = &unused_frame;
        l1->rx.signal              = &unused_signal;
        l1->rx.payload_size        = 0;
        *radio_events              = set_events_for_tx_with_ack();
        uwb_adv_set_rx_packet_size(l1->radio_adv, 1);
        /* Nack mode */
    } else {
        l1->auto_reply_mode        = AUTO_REPLY_DISABLE;
        l1->cfg.channel            = &l1->xlayer_main->config.channel;
        l1->rx.rx_constgain        = &l1->xlayer_main->config.rx_constgain;
        l1->rx.rssi_raw            = &l1->xlayer_main->config.rssi_raw;
        l1->rx.rnsi_raw            = &l1->xlayer_main->config.rnsi_raw;
        l1->rx.frame               = &unused_frame;
        l1->rx.signal              = &unused_signal;
        *radio_events              = set_events_for_tx_without_ack();
    }

    uwb_adv_set_const_gains(l1->radio_adv, *l1->rx.rx_constgain);

    reg_actions->rx_mode  = RX_MODE_ENABLE_TRANSMISSION;
    reg_actions->go_sleep = GO_TO_SLEEP;
    if (header_size == 0) {
        *radio_events = (radio_events_t)(WAKEUP_IT);
    } else {
        reg_actions->start_tx = START_TX;
    }

    uwb_adv_set_packet_config(l1->radio_adv, &l1->cfg.packet_cfg);
    uwb_adv_set_tx_packet_size(l1->radio_adv, (header_size + l1->tx.payload_size));
    uwb_adv_set_cac(l1->radio_adv, 3, *l1->tx.cca_threshold);
    uwb_adv_set_rx_pause_time(l1->radio_adv, l1->tx.cca_retry_time);
    uwb_adv_disable_rx_buffer_load_irq(l1->radio_adv);

    if (frame_fits_in_radio_fifo(l1->tx.payload_size)) {
        uwb_adv_disable_tx_buffer_load_irq(l1->radio_adv);
        enqueue_tx_prepare_frame_states(l1, header_size, l1->tx.payload_size);
    } else {
        uint8_t bufload_thresh = get_bufload_thresh_tx(l1->xlayer_main->config.fec, l1->tx.payload_size / l1->partial_frame_count);

        uwb_adv_set_tx_buffer_load_irq_threshold(l1->radio_adv, bufload_thresh);

        for (int i = 0; i < (l1->partial_frame_count - 1); i++) {
            enqueue_states(l1, set_payload_cut_through_states);
        }
        enqueue_states(l1, get_event_states);
    }
}

/** @brief Sub function to prepare a received frame.
 *
 *  @param[in] signal_data   Data required to process the state. The type shall be wps_l1_t.
 *  @param[in] radio_events  Radio's interrupt events enumeration.
 *  @param[in] reg_actions   Radio actions instance.
 */
static void prepare_radio_rx(wps_l1_t *l1, radio_events_t *radio_events, radio_actions_t *reg_actions)
{
    uint8_t payload_size;
    uint8_t header_size;

    l1->cfg.channel            = &l1->xlayer_main->config.channel;
    l1->cfg.power_up_delay     = &l1->xlayer_main->config.power_up_delay;
    l1->cfg.rx_timeout         = &l1->xlayer_main->config.rx_timeout;
    l1->cfg.sleep_time         = &l1->xlayer_main->config.sleep_time;
    l1->cfg.rx_wait_time       = &l1->xlayer_main->config.rx_wait_time;
    l1->cfg.packet_cfg         =  l1->xlayer_main->config.packet_cfg;
    l1->cfg.sleep_level        = l1->xlayer_main->config.sleep_level;
    l1->rx.rx_constgain        = &l1->xlayer_main->config.rx_constgain;
    l1->rx.rssi_raw            = &l1->xlayer_main->config.rssi_raw;
    l1->rx.rnsi_raw            = &l1->xlayer_main->config.rnsi_raw;
    l1->rx.frame               = &l1->xlayer_main->frame;
    l1->rx.signal              = &l1->signal_main;
    l1->rx.payload_size        = l1->xlayer_main->frame.payload_memory_size - l1->xlayer_main->frame.header_memory_size - SIZE_HDR_SIZE;
    l1->tx.modulation          = &l1->xlayer_main->config.modulation;
    l1->tx.fec                 = &l1->xlayer_main->config.fec;

    /* Autoreply mode */
    if (l1->xlayer_auto != NULL) {

        l1->auto_reply_mode         = AUTO_REPLY_ENABLE;
        *radio_events               = set_events_for_rx_with_ack();
        l1->cfg.destination_address = &l1->xlayer_auto->config.destination_address;
        l1->cfg.source_address      = &l1->xlayer_auto->config.source_address;
        l1->cfg.channel             = &l1->xlayer_auto->config.channel;
        l1->tx.cca_threshold        = &l1->xlayer_auto->config.cca_threshold;
        l1->tx.frame                = &l1->xlayer_auto->frame;
        l1->tx.signal               = &l1->signal_auto;

        payload_size = l1->tx.frame->payload_end_it - l1->tx.frame->payload_begin_it;
        header_size  = l1->tx.frame->header_end_it - l1->tx.frame->header_begin_it;
        uwb_adv_set_tx_packet_size(l1->radio_adv, (header_size + payload_size));
        enqueue_tx_prepare_frame_states(l1, header_size, payload_size);
        /* Ack mode */
    } else if (l1->xlayer_main->config.expect_ack) {
        l1->auto_reply_mode         = AUTO_REPLY_ENABLE;
        *radio_events               = set_events_for_rx_with_ack();
        l1->cfg.destination_address = &l1->xlayer_main->config.source_address;
        l1->cfg.source_address      = &l1->xlayer_main->config.destination_address;
        l1->cfg.channel             = &l1->xlayer_main->config.channel;
        l1->tx.cca_threshold        = &l1->xlayer_main->config.cca_threshold;
        l1->tx.frame                = &unused_frame;
        l1->tx.signal               = &unused_signal;
        enqueue_rx_prepare_frame_states(l1);

        payload_size = l1->tx.frame->payload_end_it - l1->tx.frame->payload_begin_it;
        header_size  = l1->tx.frame->header_end_it - l1->tx.frame->header_begin_it;
        uwb_adv_set_tx_packet_size(l1->radio_adv, (header_size + payload_size));
        /* Nack mode */
    } else {
        l1->auto_reply_mode         = AUTO_REPLY_DISABLE;
        *radio_events               = set_events_for_rx_without_ack();
        l1->cfg.destination_address = &l1->xlayer_main->config.source_address;
        l1->cfg.source_address      = &l1->xlayer_main->config.destination_address;
        l1->cfg.channel             = &l1->xlayer_main->config.channel;
        l1->tx.cca_threshold        = &l1->xlayer_main->config.cca_threshold;
        l1->tx.frame                = &unused_frame;
        l1->tx.signal               = &unused_signal;
        enqueue_rx_prepare_frame_states(l1);
    }

    uwb_adv_disable_tx_buffer_load_irq(l1->radio_adv);

    if (!frame_fits_in_radio_fifo(l1->rx.payload_size)) {
        uint8_t bufload_thresh = get_bufload_thresh_rx(l1->xlayer_main->config.fec, l1->rx.payload_size / l1->partial_frame_count);
        uwb_adv_set_rx_buffer_load_irq_threshold(l1->radio_adv, bufload_thresh);
    } else {
        uwb_adv_disable_rx_buffer_load_irq(l1->radio_adv);
    }

    uwb_adv_set_rx_packet_size(l1->radio_adv, l1->rx.payload_size + sizeof(wps_l2_frame_header_t));

    uwb_adv_set_packet_config(l1->radio_adv, &l1->cfg.packet_cfg);
    uwb_adv_set_const_gains(l1->radio_adv, *l1->rx.rx_constgain);
    /* Disable CCA */
    uwb_adv_set_cac(l1->radio_adv, DEFAULT_RX_IDLE_PWR, DISABLE_CCA_THRSH_VALUE);
    uwb_adv_set_rx_pause_time(l1->radio_adv, 0);
    reg_actions->rx_mode  = RX_MODE_ENABLE_RECEPTION;
    reg_actions->go_sleep = GO_TO_SLEEP;
}

/** @brief State : prepare the radio to send or receive a frame.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void prepare_radio(void *signal_data)
{
    wps_l1_t         *l1           = (wps_l1_t *)signal_data;
    radio_events_t    radio_events = NO_IRQ;
    radio_actions_t   reg_actions  = {0};
    main_modem_feat_t main_modem_feat;

    l1->signal_auto = L1_SIGNAL_NONE;

    if (main_is_tx(l1)) {
        prepare_radio_tx(l1, &radio_events, &reg_actions);
    } else {
        prepare_radio_rx(l1, &radio_events, &reg_actions);
    }

    /* Clear radio flags */
    uwb_adv_get_irq1_flag(l1->radio_adv);
    uwb_adv_get_irq2_flag(l1->radio_adv);

    uwb_adv_set_destination_address(l1->radio_adv, *l1->cfg.destination_address, ADDRESS_LENGTH_16);
    /* Setup auto-reply in radio */
    main_modem_feat.auto_reply = l1->auto_reply_mode;
    main_modem_feat.modulation = *l1->tx.modulation;
    main_modem_feat.fec_lvl    = *l1->tx.fec;
    main_modem_feat.auto_tx    = AUTO_TX_DISABLE;
    main_modem_feat.isi_mitig  = ISI_MITIG_0;
    uwb_adv_set_main_modem_features(l1->radio_adv, &main_modem_feat);

    uwb_adv_set_irq1_flag(l1->radio_adv, &radio_events);
    uwb_adv_set_irq2_flag(l1->radio_adv, &radio_events);

    uwb_adv_set_rx_timeout_raw(l1->radio_adv, *l1->cfg.rx_timeout, 3);
    uwb_adv_set_pwr_up_delay_raw(l1->radio_adv, *l1->cfg.power_up_delay);
    uwb_adv_set_wake_sleep_raw(l1->radio_adv, *l1->cfg.sleep_time);

    uwb_adv_set_radio_actions(l1->radio_adv, &reg_actions);

    uwb_adv_set_integgain(l1->radio_adv, &l1->xlayer_main->config.channel);

    uwb_adv_select_channel(l1->radio_adv, &l1->xlayer_main->config.channel);

    wps_l1_state_t **next_state = (wps_l1_state_t **)circular_queue_front_raw(&l1->next_states);

    if (*next_state == new_frame_states) {
        l1->signal_main = L1_SIGNAL_YIELD;
    }
}

/** @brief State : Send the Radio config through the SPI.
 *
 *  @param[in] signal_data Data required to process the state. The type shall be wps_l1_t.
 */
static void set_config(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    l1->signal_main = L1_SIGNAL_PREPARE_DONE;
    uwb_adv_transfer_non_blocking(l1->radio_adv);
}

/** @brief State : Fill the header of the frame in the radio tx fifo.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void set_header(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    xlayer_frame_t *frame = l1->tx.frame;

    sr1000_access_adv_disable_radio_irq(l1->radio_adv->radio_hal_adv);

    l1->signal_main = L1_SIGNAL_YIELD;

    uwb_adv_fill_header_non_blocking(l1->radio_adv, frame->header_begin_it, (frame->header_end_it - frame->header_begin_it));
}

/** @brief State : Fill the payload of the frame in the radio tx fifo.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void set_payload(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    xlayer_frame_t *frame = l1->tx.frame;

    l1->signal_main = L1_SIGNAL_YIELD;

    uwb_adv_fill_data_non_blocking(l1->radio_adv, frame->payload_begin_it, (frame->payload_end_it - frame->payload_begin_it));
}

/** @brief State : Fill the payload of a partial frame in the radio tx fifo for cut through mode.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void set_payload_cut_through(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;
    uint8_t partial_frame_len;

    sr1000_access_adv_disable_radio_irq(l1->radio_adv->radio_hal_adv);
    if (l1->partial_frame_index == 0) {
        l1->tx.frame->payload_end_it = l1->tx.frame->payload_begin_it;

        partial_frame_len = l1->tx.payload_size / l1->partial_frame_count + l1->tx.payload_size % l1->partial_frame_count;
        uwb_adv_fill_data_non_blocking(l1->radio_adv, l1->tx.frame->payload_end_it, partial_frame_len);
        l1->tx.frame->payload_end_it += partial_frame_len;
    } else {
        if (!(l1->partial_frame_index + 1 < l1->partial_frame_count)) {
            uwb_adv_disable_tx_buffer_load_irq(l1->radio_adv);
        }
        partial_frame_len = l1->tx.payload_size / l1->partial_frame_count;
        uwb_adv_fill_cut_through_data(l1->radio_adv, l1->tx.frame->payload_end_it, partial_frame_len);
        uwb_adv_transfer_non_blocking(l1->radio_adv);
        l1->tx.frame->payload_end_it += partial_frame_len;
        l1->signal_main = L1_SIGNAL_YIELD;
    }
    l1->partial_frame_index++;
}

/** @brief State : Re-enable the radio when the data have been written on the radio.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void enable_radio_irq(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    l1->signal_main = L1_SIGNAL_YIELD;
    sr1000_access_adv_enable_radio_irq(l1->radio_adv->radio_hal_adv);

    if (l1->radio_adv->radio->radio_hal->read_irq_pin()) {
        l1->radio_adv->radio_hal_adv->context_switch();
    }
}

/** @brief State : Re-enable the radio when the data have been written on the radio.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void check_radio_irq(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    /* irq pin is low, auto-reply is not sent */
    if (!l1->radio_adv->radio->radio_hal->read_irq_pin()) {
        l1->signal_main = L1_SIGNAL_YIELD;
    }
}

/** @brief State : Ask the radio for the irq flags after a radio interrupt.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void read_events(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_RADIO_IRQ) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    l1->irq_status_1   = uwb_adv_get_irq1_flag(l1->radio_adv);
    l1->irq_status_2   = uwb_adv_get_irq2_flag(l1->radio_adv);
    l1->pwr_status_cmd = uwb_adv_read_register(l1->radio_adv, REG_PWRSTATUS);

    l1->signal_main = L1_SIGNAL_YIELD;
    uwb_adv_transfer_non_blocking(l1->radio_adv);
}

/** @brief State : Ask the radio for the IRQ flags after a radio interrupt when syncing.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void read_events_syncing(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_RADIO_IRQ) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    radio_timer_config_t timer_config  = {0};

    memset(&timer_config, 0, sizeof(radio_timer_config_t));
    timer_config.auto_wake = AUTOWAKE_UP_ENABLE;
    timer_config.wake_once = WAKE_UP_ONCE_ENABLE;
    uwb_adv_set_timer_config(l1->radio_adv, &timer_config);

    l1->irq_status_1   = uwb_adv_get_irq1_flag(l1->radio_adv);
    l1->irq_status_2   = uwb_adv_get_irq2_flag(l1->radio_adv);
    l1->pwr_status_cmd = uwb_adv_read_register(l1->radio_adv, REG_PWRSTATUS);

    l1->signal_main = L1_SIGNAL_YIELD;
    uwb_adv_transfer_non_blocking(l1->radio_adv);
}

/** @brief State : Read the IRQ flags and take action regarding of the outcome.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void process_event(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    radio_events_t radio_events;

    radio_events = (*l1->irq_status_1 << 8) | (*l1->irq_status_2);

    /* Handle CCA fail */
    if (radio_events & CSC_FAIL_IT) {
        handle_cca_fail(l1);
        /* Handle RX or auto-reply */
    } else if (rx_good(radio_events)) {
        l1->tx.frame->frame_outcome = FRAME_SENT_ACK;
        l1->rx.frame->frame_outcome = FRAME_RECEIVED;
        handle_good_frame(l1);
    } else if (rx_lost(radio_events)) {
        /* Fix fake timeout bug - ASIC BUG */
        if (BIT_RXEN & *l1->pwr_status_cmd) {
            l1->signal_main = L1_SIGNAL_YIELD;
            enqueue_states(l1, get_event_states);
        } else {
            l1->tx.frame->frame_outcome = FRAME_SENT_ACK_LOST;
            l1->rx.frame->frame_outcome = FRAME_LOST;
            handle_missed_frame(l1);
            enqueue_states(l1, prepare_l1_states);
        }
    } else if (rx_rejected(radio_events)) {
        l1->tx.frame->frame_outcome = FRAME_SENT_ACK_REJECTED;
        l1->rx.frame->frame_outcome = FRAME_REJECTED;
        handle_missed_frame(l1);
        enqueue_states(l1, prepare_l1_states);
    } else if (radio_events & BUF_LOAD_TH_IT) {
        enqueue_states(l1, get_payload_cut_through_states);
        /* Handle TX */
    } else if (tx_complete(radio_events)) {
        *l1->tx.signal = L1_SIGNAL_FRAME_SENT_NACK;
        l1->tx.frame->frame_outcome = FRAME_SENT_ACK_LOST;
        enqueue_states(l1, prepare_l1_states);
    } else if (radio_events & WAKEUP_IT) {
        *l1->tx.signal              = L1_SIGNAL_FRAME_SENT_NACK;
        l1->tx.frame->frame_outcome = FRAME_WAIT;
        enqueue_states(l1, prepare_l1_states);
    }
}

/** @brief Handle a good frame received by the radio.
 *
 *  Ask the radio for RX wait time, RSSI, RNSI and payload size.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void handle_good_frame(wps_l1_t *l1)
{
    radio_events_t radio_events;
    l1->signal_main = L1_SIGNAL_YIELD;

    l1->rx_wait = uwb_adv_get_rx_wait_time(l1->radio_adv);
    l1->rssi    = uwb_adv_get_rssi(l1->radio_adv);
    l1->rnsi    = uwb_adv_get_rnsi(l1->radio_adv);

    if (is_fixed_payload_size_disabled(l1->cfg.packet_cfg.save_size)) {
        if ((l1->xlayer_auto != NULL) && (auto_is_tx(l1))) {
            /*TX end is enable to wait the transmission of the auto-reply.*/
            radio_events = set_events_for_rx_with_auto_payload();
            uwb_adv_set_irq1_flag(l1->radio_adv, &radio_events);
            uwb_adv_set_irq2_flag(l1->radio_adv, &radio_events);
            l1->signal_auto = L1_SIGNAL_FRAME_SENT_NACK;
        }
        l1->rx_frame_size = uwb_adv_read(l1->radio_adv, 1);
        uwb_adv_transfer_non_blocking(l1->radio_adv);
        enqueue_states(l1, get_header_states);
    } else {
        if (main_is_tx(l1)) {
            radio_actions_t radio_actions = {0};
            radio_actions.flush_rx = FLUSH_RX_RESET_RX_BUFFER;
            uwb_adv_set_radio_actions(l1->radio_adv, &radio_actions);
            *l1->rx_frame_size = 0;
            uwb_adv_transfer_non_blocking(l1->radio_adv);
            enqueue_states(l1, end_tx_cut_through_states);
            enqueue_states(l1, prepare_l1_states);
        } else {
            uwb_adv_transfer_non_blocking(l1->radio_adv);
            enqueue_states(l1, get_payload_cut_through_states);
        }
    }
}

/** @brief Handle a missed frame.
 *
 *  Set the signals to notify the user and flush the TX fifo in RX mode.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void handle_missed_frame(wps_l1_t *l1)
{
    radio_actions_t radio_actions = {0};

    l1->signal_main = L1_SIGNAL_YIELD;

    if (main_is_tx(l1)) {
        *l1->tx.signal = L1_SIGNAL_FRAME_SENT_NACK;
        *l1->rx.signal = L1_SIGNAL_FRAME_MISSED;
    } else {
        radio_actions.flush_tx = FLUSH_TX_RESET_TX_BUFFER;
        uwb_adv_set_radio_actions(l1->radio_adv, &radio_actions);
        radio_actions.flush_rx = FLUSH_RX_RESET_RX_BUFFER;
        uwb_adv_set_radio_actions(l1->radio_adv, &radio_actions);
        uwb_adv_transfer_non_blocking(l1->radio_adv);
        enqueue_states(l1, flush_fifo_states);
    }
}

/** @brief Handle a Clear Channel Assessment (CCA) fail.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void handle_cca_fail(wps_l1_t *l1)
{
    l1->signal_main = L1_SIGNAL_YIELD;

    l1->tx.cca_try_count++;
    if (l1->tx.cca_try_count >= l1->tx.cca_max_try_count) {
        if (l1->tx.cca_fail_action == CCA_FAIL_ACTION_ABORT_TX) {
            radio_actions_t   reg_actions  = {0};

            reg_actions.flush_tx = FLUSH_TX_RESET_TX_BUFFER;
            uwb_adv_set_radio_actions(l1->radio_adv, &reg_actions);
        }
        /* Disable CCA */
        uwb_adv_set_cac(l1->radio_adv, DEFAULT_RX_IDLE_PWR, DISABLE_CCA_THRSH_VALUE);
        uwb_adv_set_rx_pause_time(l1->radio_adv, 0);
        uwb_adv_transfer_blocking(l1->radio_adv);
    }

    enqueue_states(l1, get_event_states);
}

/** @brief State : Ask the radio to get the header.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void get_header(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    *l1->cfg.rx_wait_time = (MOV2MASK(*l1->rx_wait.rx_wait_time1, BITS_RXWAITED8) << 8) | (*l1->rx_wait.rx_wait_time0);
    *l1->rx.rssi_raw = *l1->rssi;
    *l1->rx.rnsi_raw = *l1->rnsi;

    if (*l1->rx_frame_size == 0) {
        l1->rx.frame->payload_end_it = l1->rx.frame->header_begin_it;
        *l1->tx.signal               = main_is_tx(l1) ? L1_SIGNAL_FRAME_SENT_ACK : L1_SIGNAL_FRAME_SENT_NACK;
        *l1->rx.signal               = L1_SIGNAL_FRAME_MISSED;
        enqueue_states(l1, prepare_l1_states);
    } else {
        l1->signal_main = L1_SIGNAL_YIELD;

        l1->rx.frame->header_begin_it  = l1->rx.frame->header_memory;
        l1->rx.frame->payload_begin_it = l1->rx.frame->header_memory;
        l1->rx.frame->payload_end_it   = l1->rx.frame->header_memory + l1->rx.frame->header_memory_size + EMPTY_BYTE;

        memset(l1->radio_adv->access_sequence.tx_buffer, 0, l1->rx.frame->header_memory_size + EMPTY_BYTE);
        l1->radio_adv->access_sequence.tx_buffer[0] = REG_READ_BURST | REG_RXFIFO;
        sr1000_access_adv_spi_transfer_non_blocking(l1->radio_adv->radio_hal_adv,
                                                    l1->radio_adv->access_sequence.tx_buffer,
                                                    l1->rx.frame->header_memory,
                                                    l1->rx.frame->header_memory_size + EMPTY_BYTE);
        enqueue_states(l1, get_payload_states);
        enqueue_states(l1, prepare_l1_states);
    }
}

/** @brief State : Ask the radio to get the payload.
 *
 *  If the payload is empty, the user is notified.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void get_payload(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    uint8_t payload_size = *l1->rx_frame_size - l1->rx.frame->header_memory_size;

    *l1->rx.signal  = L1_SIGNAL_FRAME_RECEIVED;

    if (payload_size == 0) {
        sr1000_access_close(l1->radio_adv->radio->radio_hal);
    } else {
        memset(l1->radio_adv->access_sequence.tx_buffer, 0, payload_size);
        sr1000_access_adv_spi_transfer_non_blocking(l1->radio_adv->radio_hal_adv,
                                                    l1->radio_adv->access_sequence.tx_buffer,
                                                    l1->rx.frame->payload_end_it,
                                                    payload_size);
        l1->rx.frame->payload_end_it += payload_size;
        enqueue_states(l1, new_frame_states);
    }
    if ((l1->xlayer_auto  != NULL)) {
        if (auto_is_tx(l1)) {
            enqueue_states(l1, wait_to_send_auto_reply);
        } else {
            *l1->tx.signal = L1_SIGNAL_FRAME_SENT_ACK;
        }
    }
}

/** @brief State : Setup get payload in cut-through mode.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void setup_cut_through(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    if (l1->partial_frame_index >= (l1->partial_frame_count - 1)) {
        *l1->cfg.rx_wait_time = (MOV2MASK(*l1->rx_wait.rx_wait_time1, BITS_RXWAITED8) << 8) | (*l1->rx_wait.rx_wait_time0);
        *l1->rx.rssi_raw = *l1->rssi;
        *l1->rx.rnsi_raw = *l1->rnsi;
    }

    if(l1->partial_frame_index >= (l1->partial_frame_count - 2)) {
        uwb_adv_disable_rx_buffer_load_irq(l1->radio_adv);
    }

    l1->signal_main = L1_SIGNAL_YIELD;

    l1->radio_adv->access_sequence.tx_buffer[l1->radio_adv->access_sequence.index++] = REG_READ_BURST | REG_RXFIFO;
    uwb_adv_transfer_non_blocking(l1->radio_adv);
}

/** @brief State : Ask the radio to get the payload of a partial frame in cut-through mode.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void get_payload_cut_through(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;
    uint8_t partial_frame_len;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    l1->signal_main = L1_SIGNAL_YIELD;

    /* First partial frame */
    if (l1->partial_frame_index == 0) {
        partial_frame_len = l1->rx.payload_size / l1->partial_frame_count +
                            l1->rx.payload_size % l1->partial_frame_count +
                            sizeof(wps_l2_frame_header_t);

        l1->rx.frame->header_begin_it  = l1->rx.frame->header_memory;
        l1->rx.frame->payload_begin_it = l1->rx.frame->header_memory;
        l1->rx.frame->payload_end_it   = l1->rx.frame->header_memory + EMPTY_BYTE;
        /* Subsequent partial frames */
    } else if (l1->partial_frame_index < (l1->partial_frame_count - 1)) {
        partial_frame_len = l1->rx.payload_size / l1->partial_frame_count;
        /* Last partial frame */
    } else {
        partial_frame_len = l1->rx.payload_size / l1->partial_frame_count;

        *l1->tx.signal = main_is_tx(l1) ? L1_SIGNAL_FRAME_SENT_ACK : L1_SIGNAL_FRAME_SENT_NACK;
        *l1->rx.signal = L1_SIGNAL_FRAME_RECEIVED;
    }

    sr1000_access_adv_spi_transfer_non_blocking(l1->radio_adv->radio_hal_adv,
                                                l1->radio_adv->access_sequence.tx_buffer,
                                                l1->rx.frame->payload_end_it,
                                                partial_frame_len);

    l1->rx.frame->payload_end_it += partial_frame_len;
    l1->partial_frame_index++;

    if (l1->partial_frame_index < l1->partial_frame_count) {
        sr1000_access_adv_disable_radio_irq(l1->radio_adv->radio_hal_adv);
        enqueue_states(l1, wait_radio_states);
    } else {
        enqueue_states(l1, prepare_l1_states);
        enqueue_states(l1, new_frame_states);
    }
}

/** @brief State : handle the end of the tx fifo flush. Notify the user for a frame lost.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void end_tx_cut_through(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    *l1->rx.rssi_raw = *l1->rssi;
    *l1->rx.rnsi_raw = *l1->rnsi;

    *l1->tx.signal = L1_SIGNAL_FRAME_SENT_ACK;
}

/** @brief State : handle the end of the tx fifo flush. Notify the user for a frame lost.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void end_flush(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        clear_err(l1);
        l1->signal_main = L1_SIGNAL_ERROR;
        return;
    }

    *l1->tx.signal = L1_SIGNAL_FRAME_SENT_NACK;
    *l1->rx.signal = L1_SIGNAL_FRAME_MISSED;
}

/** @brief State : Close the spi after transmission or reception.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void close_spi(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    if (l1->input_signal != L1_SIGNAL_DMA_CMPLT) {
        l1->state_step--;
        l1->signal_main = L1_SIGNAL_YIELD;
        return;
    }

    sr1000_access_close(l1->radio_adv->radio->radio_hal);
}

/** @brief Release the CPU.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void signal_yield(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    l1->signal_main = L1_SIGNAL_YIELD;
}

/** @brief State : End of a state machine sequence.
 *
 *  @param[in] signal_data  Data required to process the state. The type shall be wps_l1_t.
 */
static void end(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;
    wps_l1_state_t **dequeue_state;

    l1->state_step    = 0;
    dequeue_state     = (wps_l1_state_t **)circular_queue_front_raw(&l1->next_states);
    l1->current_state = *dequeue_state;
    circular_queue_dequeue_raw(&l1->next_states);
}

/** @brief Clear radio irq pin on error.
 *
 *  @param wps_l1 WPS L1 instance.
 */
static void clear_err(wps_l1_t *wps_l1)
{
    radio_timer_config_t timer_config  = {0};
    radio_actions_t      radio_actions = {0};

    /* Disable radio interrupts */
    uwb_enable_irq(wps_l1->radio_adv->radio, NO_IRQ);

    /* Clear Status */
    uwb_get_events(wps_l1->radio_adv->radio);

    memset(&timer_config, 0, sizeof(radio_timer_config_t));
    timer_config.auto_wake = AUTOWAKE_UP_ENABLE;
    timer_config.wake_once = WAKE_UP_ONCE_ENABLE;

    uwb_adv_set_timer_config(wps_l1->radio_adv, &timer_config);
    uwb_adv_set_rx_timeout_raw(wps_l1->radio_adv, 0xFFFF, 0xFF);
    uwb_adv_write_register(wps_l1->radio_adv, REG_ACTIONS, BIT_INITTIME | BIT_FLUSHRX | BIT_FLUSHTX | BIT_GOTOSLP);

    radio_actions.flush_rx = FLUSH_RX_RESET_RX_BUFFER;
    radio_actions.flush_tx = FLUSH_TX_RESET_TX_BUFFER;
    radio_actions.go_sleep = GO_TO_SLEEP;
    uwb_adv_set_radio_actions(wps_l1->radio_adv, &radio_actions);

    sr1000_access_adv_enable_radio_irq(wps_l1->radio_adv->radio_hal_adv);
    sr1000_access_open(wps_l1->radio_adv->radio->radio_hal);
    uwb_adv_transfer_blocking(wps_l1->radio_adv);
    sr1000_access_close(wps_l1->radio_adv->radio->radio_hal);
}

static void prepare_syncing(void *signal_data)
{
    wps_l1_t *l1 = (wps_l1_t *)signal_data;

    main_modem_feat_t main_modem_feat;
    radio_timer_config_t timer_config  = {0};
    radio_actions_t radio_actions = {0};
    radio_events_t radio_events;

    l1->signal_main = L1_SIGNAL_YIELD;

    l1->cfg.channel            = &l1->xlayer_main->config.channel;
    l1->cfg.power_up_delay     = &l1->xlayer_main->config.power_up_delay;
    l1->cfg.rx_timeout         = &l1->xlayer_main->config.rx_timeout;
    l1->cfg.sleep_time         = &l1->xlayer_main->config.sleep_time;
    l1->cfg.rx_wait_time       = &l1->xlayer_main->config.rx_wait_time;
    l1->cfg.packet_cfg         =  l1->xlayer_main->config.packet_cfg;
    l1->rx.rx_constgain        = &l1->xlayer_main->config.rx_constgain;
    l1->rx.rssi_raw            = &l1->xlayer_main->config.rssi_raw;
    l1->rx.rnsi_raw            = &l1->xlayer_main->config.rnsi_raw;
    l1->rx.frame               = &l1->xlayer_main->frame;
    l1->rx.signal              = &l1->signal_main;
    l1->rx.payload_size        = l1->xlayer_main->frame.payload_memory_size - l1->xlayer_main->frame.header_memory_size - SIZE_HDR_SIZE;
    l1->tx.modulation          = &l1->xlayer_main->config.modulation;
    l1->tx.fec                 = &l1->xlayer_main->config.fec;

    if (l1->xlayer_main->config.expect_ack) {
        uwb_adv_set_tx_packet_size(l1->radio_adv, 0);
    }
    uwb_adv_set_rx_packet_size(l1->radio_adv, MAX_FRAMESIZE);

    uwb_adv_set_destination_address(l1->radio_adv, *l1->cfg.destination_address, ADDRESS_LENGTH_16);
    if (l1->xlayer_main->config.expect_ack) {
        main_modem_feat.auto_reply = AUTO_REPLY_ENABLE;
    } else {
        main_modem_feat.auto_reply = AUTO_REPLY_DISABLE;
    }
    main_modem_feat.modulation = *l1->tx.modulation;
    main_modem_feat.fec_lvl    = *l1->tx.fec;
    main_modem_feat.auto_tx    = AUTO_TX_DISABLE;
    main_modem_feat.isi_mitig  = ISI_MITIG_0;
    uwb_adv_set_main_modem_features(l1->radio_adv, &main_modem_feat);

    uwb_adv_set_integgain(l1->radio_adv, l1->cfg.channel);

    uwb_adv_set_packet_config(l1->radio_adv, &l1->cfg.packet_cfg);
    uwb_adv_set_const_gains(l1->radio_adv, *l1->rx.rx_constgain);

    radio_events = NEW_PACKET_IT;
    uwb_adv_set_irq1_flag(l1->radio_adv, &radio_events);
    uwb_adv_set_irq2_flag(l1->radio_adv, &radio_events);

    memset(&timer_config, 0, sizeof(radio_timer_config_t));
    timer_config.auto_wake = AUTOWAKE_UP_ENABLE;

    if (l1->cfg.sleep_level == SLEEP_IDLE) {
        timer_config.wake_once = WAKE_UP_ONCE_DISABLE;
        uwb_adv_set_wake_sleep_raw(l1->radio_adv, l1->syncing_period_pll_cycles);
    } else {
        /* Should not care about WAKEONCE in shallow, but we need to enable it because of ASIC bug*/
        timer_config.wake_once = WAKE_UP_ONCE_ENABLE;
        uwb_adv_set_wake_sleep_raw(l1->radio_adv, l1->syncing_period_pll_cycles / PLL_RATIO);
    }

    uwb_adv_set_timer_config(l1->radio_adv, &timer_config);
    uwb_adv_set_rx_timeout_raw(l1->radio_adv, *l1->cfg.rx_timeout, 3);
    uwb_adv_set_pwr_up_delay_raw(l1->radio_adv, *l1->cfg.power_up_delay);

    radio_actions.flush_rx = FLUSH_RX_RESET_RX_BUFFER;
    radio_actions.flush_tx = FLUSH_TX_RESET_TX_BUFFER;
    radio_actions.go_sleep = GO_TO_SLEEP;
    radio_actions.rx_mode = RX_MODE_ENABLE_RECEPTION;
    uwb_adv_set_radio_actions(l1->radio_adv, &radio_actions);

    uwb_adv_select_channel(l1->radio_adv, l1->cfg.channel);

    sr1000_access_adv_disable_radio_irq(l1->radio_adv->radio_hal_adv);

    uwb_adv_transfer_non_blocking(l1->radio_adv);
}

/** @brief Get if the main frame is in transmit mode.
 *
 *  @param[in] l1  Layer one instance.
 *  @retval true
 *  @retval false
 */
static bool main_is_tx(wps_l1_t *l1)
{
    return (l1->xlayer_main->config.destination_address != l1->source_address);
}

/** @brief Get if the main frame is in transmit mode.
 *
 *  @param[in] l1  Layer one instance.
 *  @retval true
 *  @retval false
 */
static bool auto_is_tx(wps_l1_t *l1)
{
    return (l1->xlayer_auto->config.destination_address != l1->source_address);
}

/** @brief Get the TX complete status.
 *
 *  @param[in] radio_events
 *  @retval true
 *  @retval false
 */
static bool tx_complete(radio_events_t radio_events)
{
    return (((radio_events & TX_END_IT) &&
            !(radio_events & NEW_PACKET_IT) &&
            !(radio_events & RX_TIMEOUT_IT))) || radio_events & TX_UNDERFLOW_IT;
}

/** @brief Get the RX good status.
 *
 *  @param[in] radio_events
 *  @retval true
 *  @retval false
 */
static bool rx_good(radio_events_t radio_events)
{
    return ((radio_events & NEW_PACKET_IT) &&
            (radio_events & CRC_PASS_IT) &&
            ((radio_events & ADDR_MATCH_IT) || (radio_events & BROADCAST_IT)));
}

/** @brief Get the RX rejected status.
 *
 *  @param[in] radio_events
 *  @retval true
 *  @retval false
 */
static bool rx_rejected(radio_events_t radio_events)
{
    return ((radio_events & NEW_PACKET_IT) &&
            (!(radio_events & CRC_PASS_IT) || !((radio_events & ADDR_MATCH_IT) || (radio_events & BROADCAST_IT))));
}

/** @brief Get the RX lost status.
 *
 *  @param[in] radio_events
 *  @retval true
 *  @retval false
 */
static bool rx_lost(radio_events_t radio_events)
{
    return (radio_events & RX_TIMEOUT_IT);
}

bool frame_fits_in_radio_fifo(uint8_t payload_size)
{
    return ((payload_size + sizeof(wps_l2_frame_header_t)) <= MAX_FRAMESIZE);
}

/** @brief Get buffer load threshold for TX.
 *
 *  @param[in] fec_level
 *  @param[in] partial_frame_size
 *  @return buff_load_thresh
 */
uint8_t get_bufload_thresh_tx(fec_level_t fec_level, uint8_t partial_frame_size)
{
    uint8_t bufload_thresh = 0;

    switch (fec_level) {
        case FEC_LVL_0:
        case FEC_LVL_1:
            bufload_thresh = partial_frame_size - 1;
            break;
        case FEC_LVL_2:
            bufload_thresh = partial_frame_size - partial_frame_size / 4;
            break;
        case FEC_LVL_3:
            bufload_thresh = partial_frame_size - partial_frame_size / 2;
            break;
    }
    return bufload_thresh;
}

/** @brief Get buffer load threshold for RX.
 *
 *  @param[in] fec_level
 *  @param[in] partial_frame_size
 *  @return buff_load_thresh
 */
uint8_t get_bufload_thresh_rx(fec_level_t fec_level, uint8_t partial_frame_size)
{
    uint8_t bufload_thresh = 0;

    switch (fec_level) {
        case FEC_LVL_0:
        case FEC_LVL_1:
            bufload_thresh = 1 + partial_frame_size / 2;
            break;
        case FEC_LVL_2:
            bufload_thresh = partial_frame_size / 2 + partial_frame_size / 4;
            break;
        case FEC_LVL_3:
            bufload_thresh = partial_frame_size;
            break;
    }
    return bufload_thresh;
}

/** @brief Get the fixed payload size flag.
 *
 *  @return is_fixed_payload_size
 */
static bool is_fixed_payload_size_disabled(save_size_t save_size)
{
    return (save_size == SAVE_SIZE_ENABLE);
}

/** @brief Set the events for tx with ack.
 *
 *  @return radio_events_t
 */
static radio_events_t set_events_for_tx_with_ack(void)
{
    return (radio_events_t)(NEW_PACKET_IT | RX_TIMEOUT_IT | BUF_LOAD_TH_IT | CSC_FAIL_IT | TX_UNDERFLOW_IT);
}

/** @brief Set the events for tx without ack.
 *
 *  @return radio_events_t
 */
static radio_events_t set_events_for_tx_without_ack(void)
{
    return (radio_events_t)(TX_END_IT | BUF_LOAD_TH_IT | CSC_FAIL_IT | TX_UNDERFLOW_IT);
}

/** @brief Set the events for rx with ack.
 *
 *  @return radio_events_t
 */
static radio_events_t set_events_for_rx_with_ack(void)
{
    return (radio_events_t)(NEW_PACKET_IT | RX_TIMEOUT_IT | BUF_LOAD_TH_IT);
}

/** @brief Set the events for rx without ack.
 *
 *  @return radio_events_t
 */
static radio_events_t set_events_for_rx_without_ack(void)
{
    return (radio_events_t)(NEW_PACKET_IT | RX_TIMEOUT_IT | BUF_LOAD_TH_IT);
}

/** @brief Set the events for rx with payload.
 *
 *  @return radio_events_t
 */
static radio_events_t set_events_for_rx_with_auto_payload(void)
{
    return (radio_events_t)(TX_END_IT | RX_TIMEOUT_IT);
}
