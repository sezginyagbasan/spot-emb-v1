/** @file  wps.c
 *  @brief SPARK Wireless Protocol Stack.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "wps.h"
#include "sr1000_spectral.h"

/* CONSTANTS ******************************************************************/
#define US_TO_PLL_FACTOR          1000
#define TIMESLOT_VALUE_MASK       0x7F
#define EXTRACT_NETWORK_ID(addr, msbits_count) (addr >> (16 - msbits_count))

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
/** @brief Convert time in us to PLL cycles.
 *
 *  @return Number of PLL cycles.
 */
static uint32_t us_to_pll_cycle(uint32_t time_us)
{
    return  (time_us * PLL_FREQ_KHZ / US_TO_PLL_FACTOR) - 1;
}

/** @brief Check if ID is main or auto timeslot
 *
 * @retval false  Connection is on auto-reply.
 * @retval true   Connection is on main.
 */
static bool is_main_timeslot(int8_t id)
{
    if (id & (BIT_AUTO_REPLY_TIMESLOT)) {
        return false;
    } else {
        return true;
    }
}

/** @brief Convert auto reply ID to timeslot ID.
 *
 * @return Timeslot ID.
 */
static uint32_t auto_reply_id_to_id(int8_t id)
{
    return (id & TIMESLOT_VALUE_MASK);
}

/** @brief Convert auto-reply ID to timeslot ID.
 *
 * @param[in] buffer        Frame's header buffer, must be 2D array [queue size][max header size].
 * @param[in] queue         Cross layer queue.
 * @param[in] queue_size    Cross layer queue size.
 * @param[in] hdr_len       Length of the WPS header
 * @param[in] max_frame_len Total frame buffer length (Header + Payload).
 */
static void set_tx_conn_queue_memory(uint8_t *buffer,
                                     circular_queue_t *queue,
                                     uint32_t queue_size,
                                     uint32_t hdr_len,
                                     uint32_t max_frame_len)
{
    xlayer_t *buffer_begin = (xlayer_t *)queue->buffer_begin;

    if (max_frame_len > hdr_len) {
        for (uint32_t i = 0; i < queue_size; ++i) {
            buffer_begin[i].frame.payload_memory      = (buffer + i * max_frame_len);
            buffer_begin[i].frame.payload_memory_size = max_frame_len - hdr_len;
            buffer_begin[i].frame.payload_begin_it    = buffer_begin[i].frame.payload_memory + hdr_len;
        }
    }

    for (uint32_t i = 0; i < queue_size; ++i) {
        buffer_begin[i].frame.header_memory      = (buffer + i * max_frame_len);
        buffer_begin[i].frame.header_memory_size = hdr_len;
        buffer_begin[i].frame.header_end_it      = (buffer + i * max_frame_len + hdr_len);
        buffer_begin[i].frame.header_begin_it    = buffer_begin[i].frame.header_end_it;
    }
}

/** @brief Convert auto-reply ID to timeslot ID.
 *
 * @param[in] buffer         Frame's header + payload buffer, must be 2D array [queue size][max header + payload size].
 * @param[in] queue          Cross layer queue.
 * @param[in] queue_size     Cross layer queue size.
 * @param[in] max_frame_len  Total frame buffer length (Header + Payload).
 */
static void set_rx_conn_queue_memory(uint8_t *buffer,
                                     circular_queue_t *queue,
                                     uint32_t queue_size,
                                     uint32_t hdr_len,
                                     uint32_t max_frame_len)
{
    xlayer_t *buffer_begin = (xlayer_t *)queue->buffer_begin;

    for (uint32_t i = 0; i < queue_size; ++i) {
        buffer_begin[i].frame.payload_memory      = (buffer + i * max_frame_len);
        buffer_begin[i].frame.payload_memory_size = max_frame_len;
        buffer_begin[i].frame.payload_begin_it    = buffer_begin[i].frame.payload_memory;

        buffer_begin[i].frame.header_memory      = buffer_begin[i].frame.payload_memory;
        buffer_begin[i].frame.header_memory_size = hdr_len;

        buffer_begin[i].frame.header_begin_it = buffer_begin[i].frame.payload_memory;
        buffer_begin[i].frame.header_end_it   = buffer_begin[i].frame.payload_memory;
    }
}

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
void wps_init_critical_section(wps_critical_cfg_t* critical_cfg)
{
    circular_queue_critical_section_init((circular_queue_critical_cfg_t *)critical_cfg);
}

void wps_radio_init(wps_radio_t *radio, wps_error_t *error)
{
    uwb_error_t uwb_error;
    uwb_err     uwb_err;

    uwb_init(&radio->radio, &radio->radio_hal, radio->irq_polarity, &uwb_error);

    uwb_adv_init(&radio->radio_adv,
                 &radio->radio_hal_adv,
                 &radio->radio,
                 radio->spi_rx_buffer,
                 radio->spi_tx_buffer,
                 radio->spi_buffer_size,
                 &uwb_err);

    *error = WPS_NO_ERROR;
}

void wps_radio_calibrate(wps_radio_t *radio, wps_error_t *error)
{
    uwb_calibrate(&radio->radio);
    *error = WPS_NO_ERROR;
}

void wps_init_callback_queue(wps_t *wps,
                             wps_callback_inst_t *callback_buffer,
                             size_t size,
                             void (*context_switch)(void))
{
    wps->callback_context_switch = context_switch;
    circular_queue_init(&wps->l7.callback_queue, callback_buffer, size, sizeof(wps_callback_inst_t));
}

void wps_init(wps_t *wps, wps_node_t *node)
{
    wps_l2_sync_cfg_t l2_sync_cfg = {0};

    wps->node   = node;
    wps->status = WPS_IDLE;
    wps->signal = WPS_NONE;

    wps->l1.source_address = node->local_address;

    uwb_adv_set_shadow_reg(wps->node->radio);

    wps_l1_init(&wps->l1, wps->node->radio);

    l2_sync_cfg.preamble_len = wps->node->preamble_len;
    l2_sync_cfg.sleep_level  = wps->node->sleep_lvl;
    l2_sync_cfg.syncword_len = (wps->node->syncword_cfg.syncword_length == SYNCWORD_LENGTH_32) ? 32 : 16;

    wps_l2_init(&wps->l2,
                &wps->l7.callback_queue,
                &wps->schedule,
                &wps->channel_sequence,
                &l2_sync_cfg,
                wps->node->local_address,
                wps->node->role,
                wps->concurrency_enabled,
                wps->fast_sync_enabled,
                wps->network_id);

    if (wps->concurrency_enabled) {
        node->syncword_cfg.syncword_length    = SYNCWORD_LENGTH_32;
        node->syncword_cfg.syncword_bit_cost  = 2;
        node->syncword_cfg.syncword_tolerance = 0xC;
        node->syncword_cfg.syncword           = sync_word_table[wps->network_id];

        uwb_adv_set_syncword_config(node->radio, &node->syncword_cfg);
        uwb_adv_transfer_blocking(node->radio);
    }
}

void wps_set_coordinator_address(wps_t *wps, uint16_t address)
{
    wps->l2.coordinator_address = address;
}

void wps_phy_init(wps_radio_t *radio, wps_phy_t *wps_phy)
{
    uint8_t resistune_reg = uwb_read_register(&radio->radio, REG_RESISTUNE);

    resistune_reg |= MOV2MASK(wps_phy->lna_imped, BIT_LNAIMPED);

    uwb_write_register(&radio->radio, REG_RESISTUNE, resistune_reg);
    uwb_write_register(&radio->radio, REG_DEBUGMODEM, wps_phy->manuphase);
}

void wps_config_node(wps_node_t *node, wps_radio_t *radio, wps_node_cfg_t *cfg)
{
    node->radio          = &radio->radio_adv;
    node->role           = cfg->role;
    node->local_address  = cfg->local_address;
    node->crc_polynomial = cfg->crc_polynomial;
    node->preamble_len   = cfg->preamble_len;
    node->sleep_lvl      = cfg->sleep_lvl;

    /* Disable IRQ during INIT */
    uwb_adv_disable_irq1(node->radio);
    uwb_adv_disable_irq2(node->radio);

    /* Syncword */
    node->syncword_cfg.syncword_length    = SYNCWORD_LENGTH_32;
    node->syncword_cfg.syncword_bit_cost  = 2;
    node->syncword_cfg.syncword_tolerance = 0xC;
    node->syncword_cfg.syncword           = cfg->syncword;

    uwb_adv_set_syncword_config(node->radio, &node->syncword_cfg);

    /* Configure CRC */
    uwb_adv_set_crc(node->radio, node->crc_polynomial);

    /* Preamble */
    uwb_adv_set_preamble_length(node->radio, node->preamble_len);

    /* Set TX/RX len */
    uwb_adv_set_rx_packet_size(node->radio, MAX_FRAMESIZE);
    uwb_adv_set_tx_packet_size(node->radio, MAX_FRAMESIZE);

    /* Config local address */
    uwb_adv_set_local_address(node->radio, node->local_address, ADDRESS_LENGTH_16);

    /* Set sleep level */
    sleep_events_t sleep_events = (sleep_events_t)(SLEEP_RX_TIMEOUT | SLEEP_TX_END | SLEEP_RX_END);

    uwb_adv_set_sleep_config(node->radio, &node->sleep_lvl, &sleep_events);

    /* Flush RX/TX FIFO */
    radio_actions_t radio_actions = {0};

    radio_actions.flush_rx = FLUSH_RX_RESET_RX_BUFFER;
    radio_actions.flush_tx = FLUSH_TX_RESET_TX_BUFFER;

    uwb_adv_set_radio_actions(node->radio, &radio_actions);

    /* Enable IRQ on NEW_PACKET_IT */
    radio_events_t radio_events = (NEW_PACKET_IT | RX_TIMEOUT_IT | TX_END_IT);

    uwb_adv_set_irq1_flag(node->radio, &radio_events);

    /* Set RX waited source to use with address filtering */
    uwb_adv_set_rx_waited_src(node->radio, RX_WAIT_SOURCE_REGISTER);

    if ((cfg->sleep_lvl != SLEEP_IDLE) && (node->radio->radio->calib_vars.phy_version == PHY_VERSION_8_3)) {
        uwb_adv_enable_integlen(node->radio);
    }

    /* Transfer */
    uwb_adv_transfer_blocking(node->radio);

    /* Clear status */
    uwb_get_events(node->radio->radio);
}

void wps_config_network_schedule(wps_t *wps, uint32_t *timeslot_duration_us, timeslot_t *timeslot, uint32_t schedule_size)
{
    wps->schedule.size = schedule_size;
    wps->schedule.timeslot = timeslot;

    for (uint32_t i = 0; i < schedule_size; ++i) {
        timeslot[i].duration_pll_cycles = us_to_pll_cycle(timeslot_duration_us[i]);
    }
}

void wps_reset_schedule(wps_t *wps)
{
    link_scheduler_reset(&wps->l2.scheduler);
}

void wps_config_network_channel_sequence(wps_t *wps, uint32_t *channel_sequence, uint32_t sequence_size)
{
    wps->channel_sequence.channel = channel_sequence;
    wps->channel_sequence.sequence_size = sequence_size;
}

void wps_enable_concurrency(wps_t *wps, uint16_t coordinator_address, uint8_t network_msbits_count)
{
    wps->concurrency_enabled = true;
    wps->network_id = EXTRACT_NETWORK_ID(coordinator_address, network_msbits_count);
}

void wps_disable_concurrency(wps_t *wps)
{
    wps->concurrency_enabled = false;
}

void wps_enable_fast_sync(wps_t *wps, uint16_t syncing_period_us, timeslot_t *syncing_time_slot)
{
    wps->fast_sync_enabled = true;
    wps_l1_set_syncing_duration(&wps->l1, us_to_pll_cycle(syncing_period_us));
    wps->schedule.syncing_timeslot = syncing_time_slot;
}

void wps_disable_fast_sync(wps_t *wps)
{
    wps->fast_sync_enabled = false;
}

void wps_enable_internal_connection(wps_connection_t *connection, wps_internal_connection_cfg_t *internal_conn_cfg)
{
    connection->internal_connection = true;

    link_protocol_init_cfg_t proto_init_cfg;
    uwb_err err;

    proto_init_cfg.buffer      = internal_conn_cfg->buffer;
    proto_init_cfg.buffer_size = internal_conn_cfg->buffer_size;

    link_protocol_init(connection->link_protocol, &proto_init_cfg, &err);

    /* First protocol - Random datarate offset */
    for (uint8_t i = 0; i < internal_conn_cfg->rdo_num; i++) {

        link_rdo_init(&internal_conn_cfg->rdo_inst[i], internal_conn_cfg->rdo_rollover_value[i], internal_conn_cfg->rdo_enable[i]);

        link_protocol_cfg_t link_proto_cfg;

        link_proto_cfg.instance = &internal_conn_cfg->rdo_inst[i];
        link_proto_cfg.send     = link_rdo_send_offset;
        link_proto_cfg.receive  = link_rdo_set_offset;
        link_proto_cfg.size     = sizeof(internal_conn_cfg->rdo_inst[i].offset);

        link_protocol_add(connection->link_protocol, &link_proto_cfg, &err);
    }
}

void wps_disable_internal_connection(wps_connection_t *connection)
{
    connection->internal_connection = false;
    connection->link_rdo            = NULL;
    connection->link_protocol       = NULL;
}

void wps_create_connection(wps_connection_t *connection, wps_node_t *node, wps_connection_cfg_t *config)
{
    connection->source_address      = config->source_address;
    connection->destination_address = config->destination_address;
    connection->auto_sync_enable    = true;

    circular_queue_init(&connection->xlayer_queue, config->fifo_buffer, config->fifo_buffer_size, sizeof(xlayer_t));

    if (node->local_address == connection->source_address) {
        /* TX */
        set_tx_conn_queue_memory(config->frame_buffer,
                                 &connection->xlayer_queue,
                                 config->fifo_buffer_size,
                                 config->header_length,
                                 config->frame_length);
    } else {
        /* RX */
        set_rx_conn_queue_memory(config->frame_buffer,
                                 &connection->xlayer_queue,
                                 config->fifo_buffer_size,
                                 config->header_length,
                                 config->frame_length);
    }

    connection->send_ack_callback_t  = config->send_ack_callback_t;
    connection->send_nack_callback_t = config->send_nack_callback_t;
    connection->receive_callback_t   = config->receive_callback_t;
    connection->error_callback_t     = config->error_callback_t;
    connection->get_tick_quarter_ms  = config->get_tick_quarter_ms;

    connection->packet_cfg.addr_filt   = ADDR_FILT_ENABLE;
    connection->packet_cfg.addr_len    = ADDRESS_LENGTH_16;
    connection->packet_cfg.addr_header = ADDR_HEADER_ENABLE;
    connection->packet_cfg.size_header = SIZE_HEADER_ENABLE;
    connection->packet_cfg.size_src    = SIZE_SRC_REG_TXPKTSIZE;
    connection->packet_cfg.save_addr   = SAVE_ADDR_DISABLE;
    connection->packet_cfg.save_size   = SAVE_SIZE_ENABLE;
}

void wps_connection_set_timeslot(wps_connection_t *connection, wps_t *network, int32_t *timeslot_id, uint32_t nb_timeslots)
{
    uint32_t id;

    for (uint32_t i = 0; i < nb_timeslots; ++i) {
        id = timeslot_id[i];

        if (is_main_timeslot(id)) {
            network->schedule.timeslot[id].connection_main = connection;
        } else {
            id = auto_reply_id_to_id(id);
            network->schedule.timeslot[id].connection_auto_reply = connection;
        }
    }
}

void wps_connection_config_channel(wps_connection_t *connection, wps_node_t *node, uint8_t channel_x, channel_cfg_t *config)
{
    radio_t *radio = node->radio->radio;
    config_spectrum_advance(&radio->calib_vars, config, &connection->channel[channel_x]);
}

void wps_connection_config_frame(wps_connection_t *connection, modulation_t modulation, fec_level_t fec)
{
    connection->frame_cfg.modulation = modulation;
    connection->frame_cfg.fec = fec;
}

void wps_connection_enable_ack(wps_connection_t *connection)
{
    connection->ack_enable = true;
}

void wps_connection_disable_ack(wps_connection_t *connection)
{
    connection->ack_enable = false;
}

void wps_connection_enable_stop_and_wait_arq(wps_connection_t *connection, uint16_t local_address, uint32_t retry, uint32_t deadline_quarter_ms)
{
    bool board_seq;

    if (local_address == connection->destination_address) {
        board_seq = true;
    } else {
        board_seq = false;
    }

    link_saw_arq_init(&connection->stop_and_wait_arq, deadline_quarter_ms, retry, board_seq, true);
}

void wps_connection_disable_stop_and_wait_arq(wps_connection_t *connection)
{
    link_saw_arq_init(&connection->stop_and_wait_arq, 0, 0, false, false);
}

void wps_connection_enable_auto_sync(wps_connection_t *connection)
{
    connection->auto_sync_enable = true;
}

void wps_connection_disable_auto_sync(wps_connection_t *connection)
{
    connection->auto_sync_enable = false;
}

void wps_connection_enable_fixed_payload_size(wps_connection_t *connection, uint8_t fixed_payload_size)
{
    connection->fixed_payload_size_enable = true;
    connection->fixed_payload_size = fixed_payload_size;
    connection->packet_cfg.size_header = SIZE_HEADER_DISABLE;
    connection->packet_cfg.save_size   = SAVE_SIZE_DISABLE;
}

void wps_connection_enable_cca(wps_connection_t *connection,
                               uint8_t           threshold,
                               uint8_t           retry_time_us,
                               uint8_t           try_count,
                               cca_fail_action_t fail_action)
{
    link_cca_init(&connection->cca, threshold, us_to_pll_cycle(retry_time_us), try_count, fail_action, true);
}

void wps_connection_disable_cca(wps_connection_t *connection)
{
    link_cca_init(&connection->cca, 0xff, 0, 0, CCA_FAIL_ACTION_TX, false);
}

void wps_connect(wps_t *wps)
{
    uint8_t *status1;
    uint8_t *status2;
    radio_timer_config_t timer_config  = {0};
    radio_actions_t radio_actions = {0};

    memset(&timer_config, 0, sizeof(radio_timer_config_t));
    timer_config.auto_wake = AUTOWAKE_UP_ENABLE;
    timer_config.wake_once = WAKE_UP_ONCE_DISABLE;

    uwb_adv_set_timer_config(wps->node->radio, &timer_config);
    uwb_adv_set_rx_timeout_raw(wps->node->radio, 0xFFFF, 0xFF);
    uwb_adv_write_register(wps->node->radio, REG_ACTIONS, BIT_INITTIME | BIT_FLUSHRX | BIT_FLUSHTX);

    /* Clear Status */
    status1 = uwb_adv_get_irq1_flag(wps->node->radio);
    status2 = uwb_adv_get_irq2_flag(wps->node->radio);
    (void)status1;
    (void)status2;

    sr1000_access_adv_enable_radio_irq(wps->node->radio->radio_hal_adv);
    radio_actions.flush_rx = FLUSH_RX_RESET_RX_BUFFER;
    radio_actions.flush_tx = FLUSH_TX_RESET_TX_BUFFER;
    radio_actions.go_sleep = GO_TO_SLEEP;

    uwb_adv_set_radio_actions(wps->node->radio, &radio_actions);
    sr1000_access_open(wps->node->radio->radio->radio_hal);
    uwb_adv_transfer_blocking(wps->node->radio);
    sr1000_access_close(wps->node->radio->radio->radio_hal);

    wps->signal = WPS_CONNECT;

    wps_l1_reset(&wps->l1);
    wps_l2_reset(&wps->l2);

    wps->node->radio->radio_hal_adv->enable_radio_dma_irq();
    wps->node->radio->radio_hal_adv->enable_radio_irq();

    wps->node->radio->radio_hal_adv->context_switch();
}

void wps_disconnect(wps_t *wps)
{
    uint8_t *status1;
    uint8_t *status2;
    radio_timer_config_t timer_config;

    while (wps->node->radio->radio_hal_adv->is_spi_busy()) {
        /* wait for any SPI transfer to complete */
    }
    /* Disable peripherals interrupts */
    wps->node->radio->radio_hal_adv->disable_radio_dma_irq();
    wps->node->radio->radio_hal_adv->disable_radio_irq();

    /* Disable radio interrupts */
    uwb_enable_irq(wps->node->radio->radio, NO_IRQ);

    /* Reset timer configuration for proper sleep */
    memset(&timer_config, 0, sizeof(radio_timer_config_t));
    timer_config.auto_wake = AUTOWAKE_UP_DISABLE;
    timer_config.wake_once = WAKE_UP_ONCE_DISABLE;
    uwb_adv_set_timer_config(wps->node->radio, &timer_config);

    /* Go to sleep */
    uwb_adv_write_register(wps->node->radio, REG_ACTIONS, BIT_GOTOSLP);

    /* Clear Status */
    status1 = uwb_adv_get_irq1_flag(wps->node->radio);
    status2 = uwb_adv_get_irq2_flag(wps->node->radio);
    (void)status1;
    (void)status2;

    /* Clear Status */
    uwb_get_events(wps->node->radio->radio);
    /* SPI transfer */
    sr1000_access_open(wps->node->radio->radio->radio_hal);
    uwb_adv_transfer_blocking(wps->node->radio);
    sr1000_access_close(wps->node->radio->radio->radio_hal);

    wps->signal = WPS_DISCONNECT;
}

void wps_reset(wps_t *wps)
{
    wps_disconnect(wps);
    wps_connect(wps);
}

void wps_halt(wps_t *wps)
{
    (void)wps;
}

void wps_resume(wps_t *wps)
{
    (void)wps;
}

bool wps_get_free_slot(wps_connection_t *connection, uint8_t **payload)
{
    bool success = true;

    xlayer_t *frame = circular_queue_get_free_slot(&connection->xlayer_queue);
    if (frame) {
        *payload = frame->frame.payload_begin_it;
    } else {
        success = false;
    }

    return success;
}

bool wps_send(wps_connection_t *connection, uint8_t *payload, uint8_t size)
{
    bool success = true;

    xlayer_t *frame = circular_queue_get_free_slot(&connection->xlayer_queue);

    if (connection->fixed_payload_size_enable && (connection->fixed_payload_size != size)) {
        success = false;
        return success;
    }

    if (frame) {
        frame->frame.retry_count         = 0;
        frame->frame.time_stamp          = connection->get_tick_quarter_ms();
        frame->frame.payload_memory      = payload;
        frame->frame.payload_memory_size = size;
        frame->frame.payload_begin_it    = payload;
        frame->frame.payload_end_it      = payload + size;

        circular_queue_enqueue(&connection->xlayer_queue);
    } else {
        success = false;
    }

    return success;
}

wps_rx_frame wps_read(wps_connection_t *connection)
{
    wps_rx_frame frame_out;

    if (circular_queue_is_empty(&connection->xlayer_queue)) {
        frame_out.payload = NULL;
        frame_out.size = 0;
        return frame_out;
    }

    xlayer_t *frame = circular_queue_front(&connection->xlayer_queue);

    frame_out.payload = (frame->frame.payload_begin_it);
    frame_out.size = frame->frame.payload_end_it - frame->frame.payload_begin_it;

    return frame_out;
}

bool wps_read_done(wps_connection_t *connection)
{
    return circular_queue_dequeue(&connection->xlayer_queue);
}

uint32_t wps_get_fifo_size(wps_connection_t *connection)
{
    return circular_queue_size(&connection->xlayer_queue);
}

uint32_t wps_get_fifo_free_space(wps_connection_t *connection)
{
    return circular_queue_free_space(&connection->xlayer_queue);
}

wps_error_t wps_get_error(wps_connection_t *connection)
{
    return connection->wps_error;
}
