/** @file  wps_l2.c
 *  @brief Wireless Protocol Stack Layer 2.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "wps_l2.h"
#include "wps_callback.h"

/* CONSTANTS ******************************************************************/
#define RADIO_MAX_PACKET_SIZE               255
#define SYNC_PLL_STARTUP_CYCLES   (uint32_t)0x60
#define SYNC_RX_SETUP_PLL_CYCLES  (uint32_t)147
#define SYNC_FRAME_LOST_MAX_COUNT (uint32_t)100
#define HEADER_BYTE0_SEQ_NUM_MASK           BIT(7)
#define HEADER_BYTE0_TIME_SLOT_ID_MASK      BITS8(6, 0)

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void state_link_quality(void *signal_data);
static void state_sync(void *signal_data);
static void state_post_rx(void *signal_data);
static void state_post_tx(void *signal_data);
static void state_stop_wait_arq(void *signal_data);
static void state_l2_prepare_frame(void *signal_data);
static void state_prepare_internal_frame(void *signal_data);
static void state_setup_primary_link(void *signal_data);
static void state_setup_ack_link(void *signal_data);
static void state_setup_prime_link(void *signal_data);
static void state_scheduler(void *signal_data);
static void end(void *signal_data);

static bool outcome_is_frame_received(wps_l2_t *wps_l2);
static bool outcome_is_tx_sent_ack(wps_l2_t *wps_l2);
static bool is_network_node(wps_l2_t *wps_l2);
static bool is_current_timeslot_tx(wps_l2_t *wps_l2);
static bool is_current_timeslot_rx(wps_l2_t *wps_l2);
static bool is_current_prime_timeslot_tx(wps_l2_t *wps_l2);
static bool is_current_prime_timeslot_rx(wps_l2_t *wps_l2);
static bool is_current_timeslot_prime(wps_l2_t *wps_l2);
static bool is_saw_arq_enable(wps_connection_t *connection);
static bool is_current_connection_internal(wps_connection_t *connection);
static bool header_space_available(xlayer_t *current_queue);
static bool no_payload_received(xlayer_t *current_queue);
static bool scheduling_needed(wps_l2_t *wps_l2);
static bool no_space_available_for_rx(wps_l2_t *wps_l2);
static void update_xlayer_sync(wps_l2_t *wps_l2, xlayer_t *current_xlayer);
static void update_main_xlayer_gain_loop(wps_l2_t *wps_l2, xlayer_t *xlayer);
static void update_prime_xlayer_gain_loop(wps_l2_t *wps_l2, xlayer_t *xlayer);
static void update_main_xlayer_link_parameter(wps_l2_t *wps_l2, xlayer_t *xlayer);
static void update_prime_xlayer_link_parameter(wps_l2_t *wps_l2, xlayer_t *xlayer);
static void update_xlayer_modem_feat(wps_l2_t *wps_l2, xlayer_t *current_xlayer);
static bool extract_header(wps_l2_t *wps_l2, xlayer_t *current_queue);
static void fill_header(wps_l2_t *wps_l2, xlayer_t *current_queue);
static void setup_prime_timeslot_status(wps_l2_t *wps_l2);
static void process_scheduler(wps_l2_t *wps_l2);
static void process_rx_tx_outcome(wps_l2_t *wps_l2);
static void handle_saw_arq_rx(wps_l2_t *wps_l2);
static void flush_timeout_frames_before_sending(wps_l2_t *wps_l2, wps_connection_t *connection);
static bool send_l2_internal_payload(wps_l2_t *wps_l2, wps_connection_t *connection);
static xlayer_t *get_xlayer_for_tx(wps_l2_t *wps_l2, wps_connection_t *connection);
static xlayer_t *get_xlayer_for_rx(wps_l2_t *wps_l2, wps_connection_t *connection);
static void update_wps_stats(wps_l2_t *l2);
static bool send_done(wps_connection_t *connection);
static void handle_internal_frame_reception(wps_l2_t *wps_l2);
static void handle_internal_frame_transmission_ack(wps_l2_t *wps_l2);
static void handle_internal_frame_transmission_failed(wps_l2_t *wps_l2);
static xlayer_t *overwrite_first_frame_in_queue(wps_l2_t *wps_l2, wps_connection_t *connection);

/* TYPES **********************************************************************/
wps_l2_state rx_frame_sm[]               = {state_link_quality, state_post_rx, state_sync, end};
wps_l2_state rx_frame_miss_sm[]          = {state_link_quality, state_sync, state_post_rx, end};
wps_l2_state tx_sm[]                     = {state_stop_wait_arq, state_post_tx, end};
wps_l2_state prepare_internal_frame_sm[] = {state_prepare_internal_frame, end};
wps_l2_state prepare_frame_sm[]          = {state_l2_prepare_frame, end};
wps_l2_state setup_link_sm[]             = {state_setup_primary_link, state_setup_ack_link, state_setup_prime_link, end};
wps_l2_state schedule_sm[]               = {state_scheduler, end};
wps_l2_state empty_sm[]                  = {end};

/* PRIVATE GLOBALS ************************************************************/
uint8_t overrun_buffer[RADIO_MAX_PACKET_SIZE];
link_rdo_t empty_rdo;

/* PUBLIC FUNCTIONS ***********************************************************/
void wps_l2_init(wps_l2_t *wps_l2,
                 circular_queue_t *callback_queue,
                 schedule_t *schedule,
                 channel_sequence_t *channel_sequence,
                 wps_l2_sync_cfg_t *sync_cfg,
                 uint16_t local_address,
                 wps_role_t node_role,
                 bool concurrency_enabled,
                 bool fast_sync_enabled,
                 uint8_t network_id)
{
    wps_l2->state_machine[L2_SIGNAL_RX_FRAME]               = rx_frame_sm;
    wps_l2->state_machine[L2_SIGNAL_RX_FRAME_MISS]          = rx_frame_miss_sm;
    wps_l2->state_machine[L2_SIGNAL_TX_SENT_ACK]            = tx_sm;
    wps_l2->state_machine[L2_SIGNAL_TX_SENT_NACK]           = tx_sm;
    wps_l2->state_machine[L2_SIGNAL_TX]                     = tx_sm;
    wps_l2->state_machine[L2_SIGNAL_PREPARE_FRAME]          = prepare_frame_sm;
    wps_l2->state_machine[L2_SIGNAL_SETUP_LINK]             = setup_link_sm;
    wps_l2->state_machine[L2_SIGNAL_SCHEDULE]               = schedule_sm;
    wps_l2->state_machine[L2_SIGNAL_EMPTY]                  = empty_sm;
    wps_l2->state_machine[L2_SIGNAL_PREPARE_INTERNAL_FRAME] = prepare_internal_frame_sm;

    wps_l2->state_process_idx                = 0;
    wps_l2->current_ts_prime                 = false;
    wps_l2->current_ts_prime_tx              = false;
    wps_l2->local_address                    = local_address;
    wps_l2->node_role                        = node_role;
    wps_l2->callback_queue                   = callback_queue;

    wps_l2->concurrency_enabled = concurrency_enabled;
    wps_l2->network_id          = network_id;
    wps_l2->fast_sync_enabled   = fast_sync_enabled;

    /* Scheduler init */
    link_scheduler_init(&wps_l2->scheduler, schedule, wps_l2->local_address);
    wps_l2->scheduler.total_time_slot_count = wps_l2->scheduler.schedule->size;
    link_scheduler_enable_tx(&wps_l2->scheduler);

    link_channel_hopping_init(&wps_l2->channel_hopping, channel_sequence, wps_l2->concurrency_enabled, wps_l2->network_id);

    /* Sync module init */
    link_tdma_sync_init(&wps_l2->tdma_sync,
                        sync_cfg->sleep_level,
                        SYNC_RX_SETUP_PLL_CYCLES,
                        SYNC_FRAME_LOST_MAX_COUNT,
                        sync_cfg->syncword_len,
                        sync_cfg->preamble_len,
                        SYNC_PLL_STARTUP_CYCLES);

    /* Initialize empty RDO for when no internal connection is available */
    link_rdo_init(&empty_rdo, 0, false);
    wps_l2->current_rdo = &empty_rdo;
}

void wps_l2_reset(wps_l2_t *wps_l2)
{
    /* Sync module reset */
    wps_l2->tdma_sync.frame_lost_count  = 0;
    wps_l2->tdma_sync.sync_slave_offset = 0;
    wps_l2->tdma_sync.slave_sync_state  = STATE_SYNCING;

    /* Internal state machine reset */
    wps_l2->current_input  = L2_SIGNAL_EMPTY;
    wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
}

void wps_l2_process(wps_l2_t *wps_l2)
{
    wps_l2->state_process_idx = 0;
    wps_l2->current_input     = wps_l2->input_signal.main_signal;
    wps_l2->current_output    = L2_SIGNAL_WPS_EMPTY;

    if (scheduling_needed(wps_l2)) {
        process_scheduler(wps_l2);
    } else {
        process_rx_tx_outcome(wps_l2);
    }
    update_wps_stats(wps_l2);
}

/* PRIVATE STATE FUNCTIONS ****************************************************/
/** @brief Link Quality Indicator (LQI) state.
 *
 *  This state handles LQI and gain loop module update.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_link_quality(void *signal_data)
{
    wps_l2_t    *wps_l2             = (wps_l2_t *)signal_data;
    gain_loop_t *current_gain_loop  = NULL;
    lqi_t       *current_lqi        = NULL;
    lqi_t *current_channel_lqi      = NULL;

    if (is_current_prime_timeslot_rx(wps_l2)) {
        /* RX prime frame, update LQI and gain loop */
        current_lqi       = &wps_l2->current_timeslot->connection_auto_reply->lqi;
        current_channel_lqi = &wps_l2->current_timeslot->connection_auto_reply->channel_lqi[wps_l2->current_channel_index];
        current_gain_loop = &wps_l2->current_timeslot->connection_auto_reply->gain_loop[wps_l2->current_channel_index];
    } else {
        /* Timeslot is not prime OR is prime but TX */
        current_lqi       = &wps_l2->current_timeslot->connection_main->lqi;
        current_channel_lqi = &wps_l2->current_timeslot->connection_main->channel_lqi[wps_l2->current_channel_index];
        current_gain_loop = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];
    }

    /* Update LQI */
    link_lqi_update(current_lqi,
                    current_gain_loop,
                    wps_l2->current_xlayer->frame.frame_outcome,
                    wps_l2->current_xlayer->config.rssi_raw,
                    wps_l2->current_xlayer->config.rnsi_raw);
    link_lqi_update(current_channel_lqi,
                    current_gain_loop,
                    wps_l2->current_xlayer->frame.frame_outcome,
                    wps_l2->current_xlayer->config.rssi_raw,
                    wps_l2->current_xlayer->config.rnsi_raw);

    /* Update gain loop */
    link_gain_loop_update(wps_l2->current_xlayer->frame.frame_outcome,
                          wps_l2->current_xlayer->config.rssi_raw,
                          current_gain_loop);
}

/** @brief Synchronization state.
 *
 *  This state handles sync module update when node role
 *  is set to NETWORK_NODE.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_sync(void *signal_data)
{
    wps_l2_t *wps_l2 = (wps_l2_t *)signal_data;

    if (is_network_node(wps_l2)) {
        if ((!wps_l2->current_ts_prime) || (wps_l2->current_ts_prime_tx)) {
            if (!link_tdma_sync_is_slave_synced(&wps_l2->tdma_sync)) {
                link_tdma_sync_slave_find(&wps_l2->tdma_sync,
                                          wps_l2->current_xlayer->frame.frame_outcome,
                                          wps_l2->current_xlayer->config.rx_wait_time,
                                          wps_l2->current_timeslot->connection_main->cca.enable,
                                          wps_l2->current_timeslot->connection_main->cca.max_try_count,
                                          wps_l2->current_timeslot->connection_main->cca.retry_time_pll_cycles);
            } else if (wps_l2->current_timeslot->connection_main->source_address == wps_l2->coordinator_address) {
                link_tdma_sync_slave_adjust(&wps_l2->tdma_sync,
                                            wps_l2->current_xlayer->frame.frame_outcome,
                                            wps_l2->current_xlayer->config.rx_wait_time,
                                            wps_l2->current_timeslot->connection_main->cca.enable,
                                            wps_l2->current_timeslot->connection_main->cca.max_try_count,
                                            wps_l2->current_timeslot->connection_main->cca.retry_time_pll_cycles);
            }
        }
    }
}

/** @brief Post RX state.
 *
 *  This state handle header extraction and operation after
 *  the reception of valid frame.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_post_rx(void *signal_data)
{
    wps_l2_t *wps_l2 = (wps_l2_t *)signal_data;
    gain_loop_t *current_gain_loop = NULL;
    lqi_t *current_wps_lqi         = NULL;
    bool timeslot_mismatch = false;

    if (outcome_is_frame_received(wps_l2)) {
        if (no_payload_received(wps_l2->current_xlayer)) {
            wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
        } else {
            /* Frame Received */
            if (extract_header(wps_l2, wps_l2->current_xlayer)) {
                timeslot_mismatch = true;
            }

            /* After header extraction, if no payload received, then do not elevate payload to WPS */
            if (no_payload_received(wps_l2->current_xlayer)) {
                /* Frame received is internal to L2 */
                wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
            } else if (is_current_timeslot_rx(wps_l2)) {
                handle_saw_arq_rx(wps_l2);
            } else {
                /* Good frame received */
                wps_l2->current_output = L2_SIGNAL_WPS_FRAME_RX;
            }

            /* Update timeslot ID only if current node is not Coordinator */
            if (is_network_node(wps_l2)) {
                link_scheduler_set_time_slot_i(&wps_l2->scheduler, wps_l2->current_timeslot_id);
            }

            /* Assign proper callback */
            if (is_current_prime_timeslot_rx(wps_l2)) {
                /* RX prime frame, update LQI and gain loop */
                current_wps_lqi   = &wps_l2->current_timeslot->connection_auto_reply->used_frame_lqi;
                current_gain_loop = &wps_l2->current_timeslot->connection_auto_reply->gain_loop[wps_l2->current_channel_index];
                wps_l2->current_xlayer->config.callback = wps_l2->current_timeslot->connection_auto_reply->receive_callback_t;
                if (!no_payload_received(wps_l2->current_xlayer)) {
                    wps_l2->output_signal.auto_signal = L2_SIGNAL_WPS_FRAME_RX;
                }
            } else {
                /* Timeslot is not prime OR is prime but TX */
                current_wps_lqi                         = &wps_l2->current_timeslot->connection_main->used_frame_lqi;
                current_gain_loop                       = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];
                wps_l2->current_xlayer->config.callback = wps_l2->current_timeslot->connection_main->receive_callback_t;
            }
            if (!no_payload_received(wps_l2->current_xlayer)) {
                /* Update WPS LQI */
                link_lqi_update(current_wps_lqi,
                                current_gain_loop,
                                wps_l2->current_xlayer->frame.frame_outcome,
                                wps_l2->current_xlayer->config.rssi_raw,
                                wps_l2->current_xlayer->config.rnsi_raw);
            }
        }
        if (no_space_available_for_rx(wps_l2)) {
            wps_l2->current_xlayer->config.callback = wps_l2->current_timeslot->connection_main->error_callback_t;
            wps_l2->current_output                  = L2_SIGNAL_WPS_FRAME_OVERRUN;
        }
    } else {
        wps_l2->current_output = L2_SIGNAL_WPS_FRAME_MISS;
    }

    if (is_current_connection_internal(wps_l2->current_timeslot->connection_main)) {
        handle_internal_frame_reception(wps_l2);
    }

    if (timeslot_mismatch) {
        wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
    }
}

/** @brief Post TX state.
 *
 *  This state handle operation after the transmission
 *  of a frame with or without acknowledgement.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_post_tx(void *signal_data)
{
    wps_l2_t        *wps_l2             = (wps_l2_t *)signal_data;
    frame_outcome_t  ack_frame_outcome  = FRAME_RECEIVED;
    frame_outcome_t  xlayer_outcome     = wps_l2->current_xlayer->frame.frame_outcome;
    uint32_t         ack_rssi           = wps_l2->current_xlayer->config.rssi_raw;
    uint32_t         ack_rnsi           = wps_l2->current_xlayer->config.rnsi_raw;
    gain_loop_t     *current_gain_loop;
    lqi_t           *current_lqi;
    lqi_t *current_channel_lqi;
    wps_connection_t *current_connection;

    if (wps_l2->current_xlayer != &wps_l2->empty_frame_tx) {
        if (outcome_is_tx_sent_ack(wps_l2)) {
            current_connection                      = wps_l2->current_timeslot->connection_main;
            wps_l2->current_output                  = L2_SIGNAL_WPS_TX_SENT_ACK;
            wps_l2->current_xlayer->config.callback = wps_l2->current_timeslot->connection_main->send_ack_callback_t;
            ack_frame_outcome                       = FRAME_RECEIVED;
            current_gain_loop                       = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];
            current_lqi                             = &wps_l2->current_timeslot->connection_main->lqi;
            current_channel_lqi                     = &wps_l2->current_timeslot->connection_main->channel_lqi[wps_l2->current_channel_index];
            if (is_current_timeslot_tx(wps_l2)) {
                link_saw_arq_inc_seq_num(&wps_l2->current_timeslot->connection_main->stop_and_wait_arq);
            }
        } else {
            if (is_current_timeslot_tx(wps_l2)) {
                if(!wps_l2->current_timeslot->connection_main->ack_enable){
                    wps_l2->current_output = L2_SIGNAL_WPS_TX_SENT_NACK;
                }
                current_connection  = wps_l2->current_timeslot->connection_main;
                ack_frame_outcome   = (wps_l2->current_xlayer->frame.frame_outcome == FRAME_SENT_ACK_LOST) ? FRAME_LOST : FRAME_REJECTED;
                current_gain_loop   = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];
                current_lqi         = &wps_l2->current_timeslot->connection_main->lqi;
                current_channel_lqi = &wps_l2->current_timeslot->connection_main->channel_lqi[wps_l2->current_channel_index];
            } else {
                /* SaW only for main connection */
                current_connection                        = wps_l2->current_timeslot->connection_auto_reply;
                wps_l2->current_output                    = L2_SIGNAL_WPS_TX_SENT_NACK;
                wps_l2->current_xlayer->config.callback   = wps_l2->current_timeslot->connection_auto_reply->send_nack_callback_t;
                ack_frame_outcome                         = (xlayer_outcome == FRAME_SENT_ACK_LOST) ? FRAME_LOST : FRAME_REJECTED;
                current_gain_loop                         = &wps_l2->current_timeslot->connection_auto_reply->gain_loop[wps_l2->current_channel_index];
                current_lqi                               = &wps_l2->current_timeslot->connection_auto_reply->lqi;
                current_channel_lqi = &wps_l2->current_timeslot->connection_auto_reply->channel_lqi[wps_l2->current_channel_index];
            }
        }
        link_lqi_update(&current_connection->used_frame_lqi, current_gain_loop, xlayer_outcome, ack_rssi, ack_rnsi);
    } else {
        wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
        if (wps_l2->current_ts_prime_tx) {
            current_lqi            = &wps_l2->current_timeslot->connection_auto_reply->lqi;
            current_channel_lqi    = &wps_l2->current_timeslot->connection_auto_reply->channel_lqi[wps_l2->current_channel_index];
            ack_frame_outcome = (xlayer_outcome == FRAME_SENT_ACK_LOST) ? FRAME_LOST : FRAME_REJECTED;
            current_gain_loop = &wps_l2->current_timeslot->connection_auto_reply->gain_loop[wps_l2->current_channel_index];
        } else {
            current_lqi            = &wps_l2->current_timeslot->connection_main->lqi;
            current_channel_lqi    = &wps_l2->current_timeslot->connection_main->channel_lqi[wps_l2->current_channel_index];
            if (outcome_is_tx_sent_ack(wps_l2)) {
                ack_frame_outcome = FRAME_RECEIVED;
                current_gain_loop = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];
            } else {
                ack_frame_outcome = (xlayer_outcome == FRAME_SENT_ACK_LOST) ? FRAME_LOST : FRAME_REJECTED;
                current_gain_loop = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];
            }
        }
    }
    link_lqi_update(current_lqi, current_gain_loop, xlayer_outcome, ack_rssi, ack_rnsi);
    link_lqi_update(current_channel_lqi, current_gain_loop, xlayer_outcome, ack_rssi, ack_rnsi);
    link_gain_loop_update(ack_frame_outcome, ack_rssi, current_gain_loop);

    if (is_current_connection_internal(wps_l2->current_timeslot->connection_main)) {
        wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
        if (outcome_is_tx_sent_ack(wps_l2)) {
            handle_internal_frame_transmission_ack(wps_l2);
        } else {
            handle_internal_frame_transmission_failed(wps_l2);
        }
    }
}

/** @brief Stop and Wait (SaW) and Automatic Repeat Request (ARQ) state.
 *
 *  This state handles ACK algorithm and status.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_stop_wait_arq(void *signal_data)
{
    (void)signal_data;
}

/** @brief Prepare frame state.
 *
 *  This state handles preparation of layer 2 header
 *  for frame transmission.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_l2_prepare_frame(void *signal_data)
{
    wps_l2_t *wps_l2 = (wps_l2_t *)signal_data;

    /* TODO Add packet fragmentation module */
    if (is_current_timeslot_tx(wps_l2) || wps_l2->current_ts_prime) {

        /* TX timeslot / TX timeslot prime */
        if (header_space_available(wps_l2->current_xlayer)) {

            fill_header(wps_l2, wps_l2->current_xlayer);
        }
    }
}

/** @brief Handle an internal L2 frame.
 *
 *  @note This state is used to prepare a frame
 *        that will be sent to another L2 using
 *        any useful information (Ex: CCA information).
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_prepare_internal_frame(void *signal_data)
{
    wps_l2_t *wps_l2 = (wps_l2_t *)signal_data;

    send_l2_internal_payload(wps_l2, wps_l2->current_timeslot->connection_main);

    wps_l2->current_input     = L2_SIGNAL_SETUP_LINK;
    wps_l2->state_process_idx = 0;
}

/** @brief Prepare frame state.
 *
 *  This state handles preparation of layer 2 header
 *  for frame transmission.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_setup_primary_link(void *signal_data)
{
    wps_l2_t *wps_l2      = (wps_l2_t *)signal_data;
    uint32_t next_channel = link_channel_hopping_get_channel(&wps_l2->channel_hopping);
    uint16_t rdo_value    = link_rdo_get_offset(wps_l2->current_rdo);

    if (is_current_timeslot_tx(wps_l2)) {
        link_tdma_sync_update_tx(&wps_l2->tdma_sync,
                                 link_scheduler_get_sleep_time(&wps_l2->scheduler) + rdo_value,
                                 wps_l2->current_timeslot->connection_main->cca.enable,
                                 wps_l2->current_timeslot->connection_main->cca.max_try_count,
                                 wps_l2->current_timeslot->connection_main->cca.retry_time_pll_cycles);

        wps_l2->output_signal.main_xlayer = get_xlayer_for_tx(wps_l2, wps_l2->current_timeslot->connection_main);
        wps_l2->output_signal.auto_xlayer = NULL;

        /* Set current xlayer to prepare frame */
        wps_l2->current_xlayer = wps_l2->output_signal.main_xlayer;

        wps_l2->output_signal.main_signal = L2_SIGNAL_WPS_SEND_FRAME;
        wps_l2->output_signal.auto_signal = L2_SIGNAL_WPS_EMPTY;

    } else {
        link_tdma_sync_update_rx(&wps_l2->tdma_sync,
                                 link_scheduler_get_sleep_time(&wps_l2->scheduler) + rdo_value,
                                 wps_l2->current_timeslot->connection_main->cca.enable,
                                 wps_l2->current_timeslot->connection_main->cca.max_try_count,
                                 wps_l2->current_timeslot->connection_main->cca.retry_time_pll_cycles);

        wps_l2->output_signal.main_signal = L2_SIGNAL_WPS_RECEIVE_FRAME;
        wps_l2->output_signal.main_xlayer = get_xlayer_for_rx(wps_l2, wps_l2->current_timeslot->connection_main);
        wps_l2->output_signal.auto_signal = L2_SIGNAL_WPS_EMPTY;
        wps_l2->output_signal.auto_xlayer = NULL;

        if ((!link_tdma_sync_is_slave_synced(&wps_l2->tdma_sync)) &&
            (wps_l2->node_role == NETWORK_NODE) &&
             wps_l2->fast_sync_enabled) {
            wps_l2->current_timeslot = link_scheduler_get_timeslot_for_syncing(&wps_l2->scheduler);
            link_gain_loop_reset_gain_index(&wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index]);
            wps_l2->output_signal.main_signal = L2_SIGNAL_SYNCING;
        }
    }

    wps_l2->output_signal.main_xlayer->config.channel = wps_l2->current_timeslot->connection_main->channel[next_channel];
    wps_l2->output_signal.main_xlayer->config.packet_cfg = wps_l2->current_timeslot->connection_main->packet_cfg;
    wps_l2->output_signal.main_xlayer->config.cca_threshold   = wps_l2->current_timeslot->connection_main->cca.threshold;
    wps_l2->output_signal.main_xlayer->config.cca_retry_time  = wps_l2->current_timeslot->connection_main->cca.retry_time_pll_cycles;
    wps_l2->output_signal.main_xlayer->config.cca_max_try_count = wps_l2->current_timeslot->connection_main->cca.max_try_count;
    wps_l2->output_signal.main_xlayer->config.cca_fail_action = wps_l2->current_timeslot->connection_main->cca.fail_action;
    wps_l2->output_signal.main_xlayer->config.sleep_level = wps_l2->tdma_sync.sleep_mode;

    update_main_xlayer_gain_loop(wps_l2, wps_l2->output_signal.main_xlayer);
    update_main_xlayer_link_parameter(wps_l2, wps_l2->output_signal.main_xlayer);
    update_xlayer_sync(wps_l2, wps_l2->output_signal.main_xlayer);
    update_xlayer_modem_feat(wps_l2, wps_l2->output_signal.main_xlayer);
}

/** @brief Setup ACK link state.
 *
 *  This state handle preparation of xlayer for proper
 *  ACK reception/transmission.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_setup_ack_link(void *signal_data)
{
    (void)signal_data;
}

/** @brief Setup prime link state.
 *
 *  This state handle preparation of xlayer for proper
 *  prime timeslot frame reception/transmission.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_setup_prime_link(void *signal_data)
{
    wps_l2_t *wps_l2 = (wps_l2_t *)signal_data;

    if (wps_l2->current_ts_prime) {
        /* Prime status */
        if (wps_l2->current_ts_prime_tx) {
            /* TX Prime */
            /* TODO Add tx pulse/Channel cfg */
            wps_l2->output_signal.auto_signal = L2_SIGNAL_WPS_SEND_FRAME;
            wps_l2->output_signal.auto_xlayer = get_xlayer_for_tx(wps_l2, wps_l2->current_timeslot->connection_auto_reply);

            /* Set current xlayer for Prepare frame */
            wps_l2->current_xlayer = wps_l2->output_signal.auto_xlayer;
        } else {
            /* RX Prime*/
            wps_l2->output_signal.auto_signal = L2_SIGNAL_WPS_RECEIVE_FRAME;
            wps_l2->output_signal.auto_xlayer = get_xlayer_for_rx(wps_l2, wps_l2->current_timeslot->connection_auto_reply);
            update_prime_xlayer_gain_loop(wps_l2, wps_l2->output_signal.auto_xlayer);
        }
        update_prime_xlayer_link_parameter(wps_l2, wps_l2->output_signal.auto_xlayer);
    }
    if( wps_l2->output_signal.auto_xlayer != NULL){
        wps_l2->output_signal.auto_xlayer->config.channel = wps_l2->output_signal.main_xlayer->config.channel;
        wps_l2->output_signal.auto_xlayer->config.packet_cfg = wps_l2->output_signal.main_xlayer->config.packet_cfg;
    }
    /* Reassign input for loop back */
    wps_l2->current_input = L2_SIGNAL_PREPARE_FRAME;
    wps_l2->state_process_idx   = 0;
}

/** @brief Scheduler state.
 *
 *  This state gets the next timeslot to handle.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void state_scheduler(void *signal_data)
{
    wps_l2_t *wps_l2 = (wps_l2_t *)signal_data;
    uint8_t inc_count;

    inc_count = link_scheduler_increment_time_slot(&wps_l2->scheduler);
    link_channel_hopping_increment_sequence(&wps_l2->channel_hopping, inc_count);

    wps_l2->current_channel_index = link_channel_hopping_get_channel(&wps_l2->channel_hopping);
    wps_l2->current_timeslot      = link_scheduler_get_current_timeslot(&wps_l2->scheduler);
    wps_l2->current_timeslot_id   = link_scheduler_get_next_timeslot_index(&wps_l2->scheduler);

    setup_prime_timeslot_status(wps_l2);

    /* Internal signal change with process index reset */
    if (is_current_connection_internal(wps_l2->current_timeslot->connection_main) && is_current_timeslot_tx(wps_l2)) {
        wps_l2->current_input     = L2_SIGNAL_PREPARE_INTERNAL_FRAME;
        wps_l2->current_rdo       = (&wps_l2->current_timeslot->connection_main->link_rdo[0] == NULL) ? &empty_rdo : &wps_l2->current_timeslot->connection_main->link_rdo[0];
        wps_l2->state_process_idx = 0;
    } else {
        wps_l2->current_input     = L2_SIGNAL_SETUP_LINK;
        wps_l2->state_process_idx = 0;
        if (is_current_connection_internal(wps_l2->current_timeslot->connection_main)) {
            wps_l2->current_rdo = (&wps_l2->current_timeslot->connection_main->link_rdo[0] == NULL) ?
            &empty_rdo :
            &wps_l2->current_timeslot->connection_main->link_rdo[0];
        }
    }
}

/** @brief End state.
 *
 *  This state is used in the jump tables to
 *  know when jump table are ending.
 *
 *  @param[in] signal_data  Layer 2 structure.
 */
static void end(void *signal_data)
{
    (void)signal_data;
}

/** @brief Output if input signal is RX_FRAME.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   Input signal is RX_FRAME.
 *  @retval False  Input signal is not RX_FRAME.
 */
static bool outcome_is_frame_received(wps_l2_t *wps_l2)
{
    return (wps_l2->current_input == L2_SIGNAL_RX_FRAME);
}

/** @brief Output if sent frame has been acknowledged.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   Frame sent has been acknowledged.
 *  @retval False  Frame sent has not been acknowledged.
 */
static bool outcome_is_tx_sent_ack(wps_l2_t *wps_l2)
{
    return (wps_l2->current_input == L2_SIGNAL_TX_SENT_ACK);
}

/** @brief Output if node role is NETWORK_NODE.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   Node role is NETWORK_NODE.
 *  @retval False  Node role is not NETWORK_NODE.
 */
static bool is_network_node(wps_l2_t *wps_l2)
{
    return (wps_l2->node_role == NETWORK_NODE);
}

/** @brief Output if current main connection timeslot is TX.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   Main connection timeslot is TX.
 *  @retval False  Main connection timeslot is RX.
 */
static bool is_current_timeslot_tx(wps_l2_t *wps_l2)
{
    return (wps_l2->current_timeslot->connection_main->source_address == wps_l2->local_address);
}

/** @brief Output if current main connection timeslot is RX.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   Main connection timeslot is RX.
 *  @retval False  Main connection timeslot is TX.
 */
static bool is_current_timeslot_rx(wps_l2_t *wps_l2)
{
    return (wps_l2->current_timeslot->connection_main->source_address != wps_l2->local_address);
}

/** @brief Output if auto-reply connection timeslot is TX.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   Auto-reply connection timeslot is TX.
 *  @retval False  Auto-reply connection timeslot is RX.
 */
static bool is_current_prime_timeslot_tx(wps_l2_t *wps_l2)
{
    return (wps_l2->current_timeslot->connection_auto_reply->source_address == wps_l2->local_address);
}

/** @brief Output if current timeslot is prime RX.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval true   Current timeslot is not prime RX.
 *  @retval false  Current timeslot is prime RX.
 */
static bool is_current_prime_timeslot_rx(wps_l2_t *wps_l2)
{
    return (wps_l2->current_ts_prime && !wps_l2->current_ts_prime_tx);
}

/** @brief Output if auto-reply connection exist in timeslot.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @retval True   There is an auto-reply connection.
 *  @retval False  There is not an auto-reply connection.
 */
static bool is_current_timeslot_prime(wps_l2_t *wps_l2)
{
    return (wps_l2->current_timeslot->connection_auto_reply != NULL);
}
/** @brief Return if stop and wait is enable or not.
 *
 *  @param wps_l2  Layer 2 structure.
 *  @retval True   Stop and wait ARQ is enable.
 *  @retval False  Stop and wait ARQ is disable.
 */
static bool is_saw_arq_enable(wps_connection_t *connection)
{
    return connection->stop_and_wait_arq.enable;
}

/** @brief Return if current connection is the internal one.
 *
 *  @param[in] connection  L2 current connection.
 *  @retval True   Current connection is the L2 internal connection.
 *  @retval False  Current connection is not the L2 internal connection.
 */
static bool is_current_connection_internal(wps_connection_t *connection)
{
    return connection->internal_connection;
}

/** @brief Update the xlayer sync module value for L1 .
 *
 *  @param[in] wps_l2  Layer 2 structure.
 */
static void update_xlayer_sync(wps_l2_t *wps_l2, xlayer_t *current_xlayer)
{
    current_xlayer->config.power_up_delay = link_tdma_sync_get_pwr_up(&wps_l2->tdma_sync);
    current_xlayer->config.rx_timeout     = link_tdma_sync_get_timeout(&wps_l2->tdma_sync);
    current_xlayer->config.sleep_time     = link_tdma_sync_get_sleep_cycles(&wps_l2->tdma_sync);
}

/** @brief Update the main connection xlayer gain loop value for L1.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @param[in] xlayer  xlayer node to update.
 */
static void update_main_xlayer_gain_loop(wps_l2_t *wps_l2, xlayer_t *xlayer)
{
    gain_loop_t *current_gl = &wps_l2->current_timeslot->connection_main->gain_loop[wps_l2->current_channel_index];

    xlayer->config.rx_constgain = link_gain_loop_get_gain_value(current_gl);
}

/** @brief Update the auto-reply connection xlayer gain loop value for L1.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @param[in] xlayer  xlayer node to update.
 */
static void update_prime_xlayer_gain_loop(wps_l2_t *wps_l2, xlayer_t *xlayer)
{
    gain_loop_t *current_gl = &wps_l2->current_timeslot->connection_auto_reply->gain_loop[wps_l2->current_channel_index];

    xlayer->config.rx_constgain = link_gain_loop_get_gain_value(current_gl);
}

/** @brief Update the main connection xlayer gain loop value for L1.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @param[in] xlayer  xlayer node to update.
 */
static void update_main_xlayer_link_parameter(wps_l2_t *wps_l2, xlayer_t *xlayer)
{
    xlayer->config.destination_address = wps_l2->current_timeslot->connection_main->destination_address;
    xlayer->config.source_address      = wps_l2->current_timeslot->connection_main->source_address;
    xlayer->config.expect_ack          = wps_l2->current_timeslot->connection_main->ack_enable;
}

/** @brief Update the main connection xlayer gain loop value for L1.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @param[in] xlayer  xlayer node to update.
 */
static void update_prime_xlayer_link_parameter(wps_l2_t *wps_l2, xlayer_t *xlayer)
{
    xlayer->config.destination_address = wps_l2->current_timeslot->connection_auto_reply->destination_address;
    xlayer->config.source_address      = wps_l2->current_timeslot->connection_auto_reply->source_address;
}

/** @brief Update the main connection xlayer modem feat value for L1.
 *
 *  @param[in] wps_l2  Layer 2 structure.
 *  @param[in] xlayer  xlayer node to update.
 */
static void update_xlayer_modem_feat(wps_l2_t *wps_l2, xlayer_t *current_xlayer)
{
    current_xlayer->config.fec        = wps_l2->current_timeslot->connection_main->frame_cfg.fec;
    current_xlayer->config.modulation = wps_l2->current_timeslot->connection_main->frame_cfg.modulation;
}

/** @brief Return the corresponding queue for TX depending on the input connection.
 *
 *  For TX timeslot, applications should have enqueued a node
 *  inside the queue, so the L2 only needs to get the front of
 *  the queue in order to get the good node for the process.
 *
 *  @param[in] connection  Queue node connection.
 *  @return Current pending node in queue.
 */
static xlayer_t *get_xlayer_for_tx(wps_l2_t *wps_l2, wps_connection_t *connection)
{
    xlayer_t *free_xlayer;
    bool no_sync = ((wps_l2->tdma_sync.slave_sync_state == STATE_SYNCING) && (wps_l2->node_role == NETWORK_NODE));

    if (is_saw_arq_enable(connection)) {
        flush_timeout_frames_before_sending(wps_l2, connection);
    }

    free_xlayer = circular_queue_front(&connection->xlayer_queue);

    if (free_xlayer == NULL || no_sync) {
        if (connection->auto_sync_enable && !no_sync) {
            wps_l2->empty_frame_tx.frame.header_memory = overrun_buffer;
            wps_l2->empty_frame_tx.frame.header_end_it = overrun_buffer + sizeof(wps_l2_frame_header_t);
        } else {
            wps_l2->empty_frame_tx.frame.header_memory    = NULL;
            wps_l2->empty_frame_tx.frame.header_end_it    = NULL;
        }
        wps_l2->empty_frame_tx.frame.header_begin_it      = wps_l2->empty_frame_tx.frame.header_end_it;
        wps_l2->empty_frame_tx.frame.payload_end_it       = wps_l2->empty_frame_tx.frame.header_end_it;
        wps_l2->empty_frame_tx.frame.payload_begin_it     = wps_l2->empty_frame_tx.frame.header_end_it;
        free_xlayer                                       = &wps_l2->empty_frame_tx;
        wps_l2->empty_frame_tx.frame.time_stamp           = connection->get_tick_quarter_ms();

    } else {
        free_xlayer->frame.header_begin_it = free_xlayer->frame.header_end_it;
    }

    return free_xlayer;
}

/** @brief Return the corresponding queue for RX depending on the input connection.
 *
 *  For RX timeslot, L2 should get the first free slot, WPS
 *  should enqueue for application.
 *
 *  @param[in] connection  Queue node connection.
 *  @return Current pending node in queue.
 */
static xlayer_t *get_xlayer_for_rx(wps_l2_t *wps_l2, wps_connection_t *connection)
{
    xlayer_t *free_xlayer;

    free_xlayer = circular_queue_get_free_slot(&connection->xlayer_queue);
    if (free_xlayer == NULL) {
        free_xlayer = &wps_l2->empty_frame_rx;

        wps_l2->empty_frame_rx.frame.payload_memory      = overrun_buffer;
        wps_l2->empty_frame_rx.frame.payload_memory_size = connection->fixed_payload_size_enable
                                                         ? (connection->fixed_payload_size + sizeof(wps_l2_frame_header_t) + 1)
                                                         : RADIO_MAX_PACKET_SIZE;
        wps_l2->empty_frame_rx.frame.payload_begin_it    = overrun_buffer;
        wps_l2->empty_frame_rx.frame.payload_end_it      = connection->fixed_payload_size_enable
                                                         ? overrun_buffer + (connection->fixed_payload_size + sizeof(wps_l2_frame_header_t) + 1)
                                                         : overrun_buffer + RADIO_MAX_PACKET_SIZE;
        wps_l2->empty_frame_rx.frame.header_memory       = overrun_buffer;
        wps_l2->empty_frame_rx.frame.header_memory_size  = sizeof(wps_l2_frame_header_t);
        wps_l2->empty_frame_rx.frame.header_begin_it     = overrun_buffer;
        wps_l2->empty_frame_rx.frame.header_end_it       = overrun_buffer;
    }

    return free_xlayer;
}

/** @brief Output if there is enough space for L2 header in xlayer.
 *
 *  @param[in] current_queue  Current header xlayer.
 *  @retval True   Header have enough space for L2 header.
 *  @retval False  Header have not enough space for L2 header.
 */
static bool header_space_available(xlayer_t *current_queue)
{
    int8_t header_space_available = current_queue->frame.header_begin_it - current_queue->frame.header_memory;

    /* Ensure begin_it is available AND that enough space is available to write header */
    return (current_queue->frame.header_begin_it != NULL && (header_space_available >= (int8_t)sizeof(wps_l2_frame_header_t)));
}

/** @brief Extract the header fields from a received node queue.
 *
 *  @param[out] wps_l2         Frame header layer 2 instance.
 *  @param[in]  current_queue  xlayer header.
 *  @retval True   The received timeslot ID was different from the expected one.
 *  @retval False  The received timeslot ID was identical to the expected one.
 */
static bool extract_header(wps_l2_t *wps_l2, xlayer_t *current_queue)
{
    uint8_t time_slot_id;
    bool timeslot_mismatch = false;

    /* Layer 2 should always be the first to extract */
    current_queue->frame.header_begin_it = current_queue->frame.header_memory;
    if (current_queue->frame.header_begin_it != NULL) {
        /* First byte should always be the radio automatic response */
        current_queue->frame.header_begin_it++;

        wps_l2->frame_header.byte0 = *current_queue->frame.header_begin_it++;
        wps_l2->frame_header.byte1 = *current_queue->frame.header_begin_it++;

        /* Assign payload pointer as if there is no other layer on top. */
        current_queue->frame.payload_begin_it = current_queue->frame.header_begin_it;

        if (wps_l2->node_role == NETWORK_NODE) {
            time_slot_id = MASK2VAL(wps_l2->frame_header.byte0, HEADER_BYTE0_TIME_SLOT_ID_MASK);
            if (time_slot_id < wps_l2->scheduler.schedule->size) {
                if (wps_l2->current_timeslot_id != time_slot_id) {
                    timeslot_mismatch = true;
                }
                wps_l2->current_timeslot_id = time_slot_id;
            }
            link_channel_hopping_set_seq_index(&wps_l2->channel_hopping, wps_l2->frame_header.byte1);
        }
    }
    return timeslot_mismatch;
}

/** @brief Fill the header fields for a TX node queue.
 *
 *  @param[in] wps_l2         Frame header layer 2 instance.
 *  @param[in] current_queue  Header xlayer.
 */
static void fill_header(wps_l2_t *wps_l2, xlayer_t *current_queue)
{
    /* Prepare Header */
    wps_l2->frame_header.byte1                = link_channel_hopping_get_seq_index(&wps_l2->channel_hopping);
    *(--current_queue->frame.header_begin_it) = wps_l2->frame_header.byte1;

    wps_l2->frame_header.byte0  = MASK2VAL(wps_l2->current_timeslot_id, HEADER_BYTE0_TIME_SLOT_ID_MASK) |
                                  MOV2MASK(link_saw_arq_get_seq_num(&wps_l2->current_timeslot->connection_main->stop_and_wait_arq),
                                           HEADER_BYTE0_SEQ_NUM_MASK);
    *(--current_queue->frame.header_begin_it) = wps_l2->frame_header.byte0;
}

/** @brief Fill the header fields for a TX node queue.
 *
 *  @param[in] current_queue  Current xlayer.
 *  @retval True   No payload have been received.
 *  @retval False  Payload have been received.
 */
static bool no_payload_received(xlayer_t *current_queue)
{
    return (current_queue->frame.header_begin_it == current_queue->frame.payload_end_it);
}

/** @brief Setup L2 instance flag for timeslot prime status.
 *
 *  @param[in] wps_l2  Layer 2 instance.
 */
static void setup_prime_timeslot_status(wps_l2_t *wps_l2)
{
    if (is_current_timeslot_prime(wps_l2)) {
        wps_l2->current_ts_prime    = true;
        if (is_current_prime_timeslot_tx(wps_l2)) {
            wps_l2->current_ts_prime_tx = true;
        } else {
            wps_l2->current_ts_prime_tx = false;
        }
    } else {
        wps_l2->current_ts_prime    = false;
        wps_l2->current_ts_prime_tx = false;
    }
}

/** @brief Output if current input signal is L2_SIGNAL_SCHEDULE.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 *  @retval true   Scheduling needed.
 *  @retval false  No scheduling needed.
 */
static bool scheduling_needed(wps_l2_t *wps_l2)
{
    return (wps_l2->current_input == L2_SIGNAL_SCHEDULE);
}

/** @brief Process the L2_SIGNAL_SCHEDULE input.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 */
static void process_scheduler(wps_l2_t *wps_l2)
{
    do {
        wps_l2->state_machine[wps_l2->current_input][wps_l2->state_process_idx++](wps_l2);
    } while (wps_l2->state_machine[wps_l2->current_input][wps_l2->state_process_idx] != end);
}

/** @brief Process frame outcome coming from WPS layer 1.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 */
static void process_rx_tx_outcome(wps_l2_t *wps_l2)
{
    wps_l2->current_xlayer = wps_l2->input_signal.main_xlayer;
    /* Handle main xlayer processing */
    do {
        wps_l2->state_machine[wps_l2->current_input][wps_l2->state_process_idx++](wps_l2);
    } while (wps_l2->state_machine[wps_l2->current_input][wps_l2->state_process_idx] != end);

    /* Assign main output */
    wps_l2->output_signal.main_xlayer  = wps_l2->current_xlayer;
    wps_l2->output_signal.main_signal  = wps_l2->current_output;

    /* Reset state process for state machine */
    wps_l2->state_process_idx = 0;

    wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;

    if (wps_l2->input_signal.auto_xlayer != NULL) {
        /* Get WPS input xlayer and reset output */
        wps_l2->current_input  = wps_l2->input_signal.auto_signal;
        wps_l2->current_xlayer = wps_l2->input_signal.auto_xlayer;
        /* Handle auto xlayer processing*/
        while (wps_l2->state_machine[wps_l2->current_input][wps_l2->state_process_idx] != end) {
            wps_l2->state_machine[wps_l2->current_input][wps_l2->state_process_idx++](wps_l2);
        }
        /* Assign auto output */
        wps_l2->output_signal.auto_xlayer = wps_l2->current_xlayer;
    }
    /* Assign auto output */
    wps_l2->output_signal.auto_signal = wps_l2->current_output;
}

/** @brief  Check if current used queue is the internal one.
 *
 *  This is used to know if there is still space available
 *  in queue, since the internal one is used, it means there
 *  is no more space available.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 *  @return true   No space is available in queue for reception.
 *  @return false  Space is available in queue for reception.
 */
static bool no_space_available_for_rx(wps_l2_t *wps_l2)
{
    return (wps_l2->current_xlayer == &wps_l2->empty_frame_rx);
}

/** @brief Handle Stop and Wait (SaW) and Automatic Repeat Request (ARQ) during post RX.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 */
static void handle_saw_arq_rx(wps_l2_t *wps_l2)
{
    link_saw_arq_update_rx_seq_num(&wps_l2->current_timeslot->connection_main->stop_and_wait_arq,
                                    MASK2VAL(wps_l2->frame_header.byte0, HEADER_BYTE0_SEQ_NUM_MASK));
    if (link_saw_arq_is_rx_frame_duplicate(&wps_l2->current_timeslot->connection_main->stop_and_wait_arq)) {
        /* Frame is duplicate */
        wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
    } else {
        /* Good frame received */
        wps_l2->current_output = L2_SIGNAL_WPS_FRAME_RX;
    }
}

/** @brief Finish a transmission.
 *
 *  @param[in] connection Connection instance.
 *  @retval true   On success.
 *  @retval false  On error.
 */
static bool send_done(wps_connection_t *connection)
{
    return circular_queue_dequeue(&connection->xlayer_queue);
}

/** @brief Check and flush timeout frame before sending to L1.
 *
 *  @param wps_l2  WPS layer 2 instance.
 */
static void flush_timeout_frames_before_sending(wps_l2_t *wps_l2, wps_connection_t *connection)
{
    bool timeout = false;
    xlayer_t *xlayer;

    do {
        xlayer = circular_queue_front(&connection->xlayer_queue);
        if (xlayer != NULL) {
            timeout = link_saw_arq_is_frame_timeout(&connection->stop_and_wait_arq,
                                                    xlayer->frame.time_stamp,
                                                    xlayer->frame.retry_count++,
                                                    connection->get_tick_quarter_ms());
            if (timeout) {
                xlayer->config.callback = connection->send_nack_callback_t;
                if (!is_current_connection_internal(connection)) {
                    wps_callback_enqueue(wps_l2->callback_queue, xlayer, connection);
                }
                wps_l2->output_signal.main_signal = L2_SIGNAL_WPS_TX_SENT_NACK;
                update_wps_stats(wps_l2);
                send_done(connection);
            }
        } else {
            timeout = false;
        }
    } while (timeout);
}

/** @brief Send a frame on the L2 internal connection.
 *
 *  @note This method is a direct implementation of the
 *        wps_send method. It should behave exactly the
 *        same since the L2 sends a payload the same way
 *        an application send one.
 *
 *  @param[in] wps_l2      WPS L2 instance.
 *  @param[in] connection  Current L2 internal connection.
 *  @return true   Payload is successfully enqueue.
 *  @return false  Jumbo frame cfg error OR TX queue full
 */
static bool send_l2_internal_payload(wps_l2_t *wps_l2, wps_connection_t *connection)
{
    bool success = true;
    xlayer_t *frame = circular_queue_get_free_slot(&connection->xlayer_queue);
    uint8_t *payload = NULL;
    uint32_t size = 0;

    link_protocol_send_buffer(connection->link_protocol, &payload, &size);

    if (connection->fixed_payload_size_enable && (connection->fixed_payload_size != size)) {
        success = false;
        return success;
    }

    if (!frame) {
        /* Overrun occurs */
        frame = overwrite_first_frame_in_queue(wps_l2, connection);
    }

    frame->frame.retry_count         = 0;
    frame->frame.time_stamp          = connection->get_tick_quarter_ms();
    frame->frame.payload_memory      = payload;
    frame->frame.payload_memory_size = size;
    frame->frame.payload_begin_it    = payload;
    frame->frame.payload_end_it      = payload + size;

    circular_queue_enqueue(&connection->xlayer_queue);

    return success;
}
/** @brief Update WPS statistics.
 *
 *  @param[in] l2  WPS layer 2 instance.
 */
void update_wps_stats(wps_l2_t *l2)
{
    xlayer_t *current_xlayer;

    current_xlayer = l2->output_signal.main_xlayer;
    switch (l2->output_signal.main_signal) {
    case L2_SIGNAL_WPS_FRAME_RX:
        l2->current_timeslot->connection_main->wps_stats.rx_received++;
        l2->current_timeslot->connection_main->wps_stats.rx_byte_received +=
            (current_xlayer->frame.payload_end_it - current_xlayer->frame.payload_begin_it);
        break;
    case L2_SIGNAL_WPS_FRAME_OVERRUN:
        l2->current_timeslot->connection_main->wps_stats.rx_overrun++;
        break;
    case L2_SIGNAL_WPS_TX_SENT_ACK:
        l2->current_timeslot->connection_main->wps_stats.tx_sent++;
        l2->current_timeslot->connection_main->wps_stats.tx_byte_sent +=
            (current_xlayer->frame.payload_end_it - current_xlayer->frame.payload_begin_it);
        break;
    case L2_SIGNAL_WPS_TX_SENT_NACK:
        if(l2->current_timeslot->connection_main->ack_enable){
            l2->current_timeslot->connection_main->wps_stats.tx_dropped++;
        } else {
            l2->current_timeslot->connection_main->wps_stats.tx_byte_sent +=
                (current_xlayer->frame.payload_end_it - current_xlayer->frame.payload_begin_it);
        }
        break;
    default:
        break;
    }
    current_xlayer = l2->output_signal.auto_xlayer;
    switch (l2->output_signal.auto_signal) {
    case L2_SIGNAL_WPS_FRAME_RX:
        l2->current_timeslot->connection_auto_reply->wps_stats.rx_received++;
        l2->current_timeslot->connection_auto_reply->wps_stats.rx_byte_received +=
            (current_xlayer->frame.payload_end_it - current_xlayer->frame.payload_begin_it);
        break;
    case L2_SIGNAL_WPS_FRAME_OVERRUN:
        l2->current_timeslot->connection_auto_reply->wps_stats.rx_overrun++;
        break;
    case L2_SIGNAL_WPS_TX_SENT_ACK:
        l2->current_timeslot->connection_auto_reply->wps_stats.tx_sent++;
        l2->current_timeslot->connection_main->wps_stats.tx_byte_sent +=
            (current_xlayer->frame.payload_end_it - current_xlayer->frame.payload_begin_it);
        break;
    case L2_SIGNAL_WPS_TX_SENT_NACK:
            l2->current_timeslot->connection_auto_reply->wps_stats.tx_sent++;
            l2->current_timeslot->connection_auto_reply->wps_stats.tx_byte_sent +=
                (current_xlayer->frame.payload_end_it - current_xlayer->frame.payload_begin_it);
        break;
    default:
        break;
    }
}

/** @brief Handle the reception of an internal frame.
 *
 *  @note  This method takes the current L2 output and
 *         processes it as if it was the application
 *         getting the signal. The output signal
 *         should always be reset to EMPTY since
 *         the application should not be aware of
 *         the output of this connection.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 */
static void handle_internal_frame_reception(wps_l2_t *wps_l2)
{
    uint8_t rx_size = 0;

    switch (wps_l2->current_output) {
    case L2_SIGNAL_WPS_FRAME_RX:
        /* Payload successfully received on the internal connection */
        rx_size = wps_l2->current_xlayer->frame.payload_end_it - wps_l2->current_xlayer->frame.payload_begin_it;

        link_protocol_receive_buffer(wps_l2->current_timeslot->connection_main->link_protocol,
                                     wps_l2->current_xlayer->frame.payload_begin_it,
                                     rx_size);
        circular_queue_dequeue(&wps_l2->current_timeslot->connection_main->xlayer_queue);
        break;
    case L2_SIGNAL_WPS_EMPTY:
    case L2_SIGNAL_WPS_FRAME_OVERRUN:
    case L2_SIGNAL_WPS_FRAME_MISS:
    default:
        /* Let module handle when no frame is received */
        link_protocol_receive_buffer(wps_l2->current_timeslot->connection_main->link_protocol, NULL, 0);
        break;

    }
    /* Assign main signal for stats update */
    wps_l2->output_signal.main_signal = wps_l2->current_output;
    update_wps_stats(wps_l2);

    /* Reset output to remove application notification */
    wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
}

/** @brief Handle internal frame successful transmission.
 *
 *  @note This updates the stats and dequeue the current node.
 *        Output is set to EMPTY in order to omit notification
 *        to the application.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 */
static void handle_internal_frame_transmission_ack(wps_l2_t *wps_l2)
{
    wps_l2->output_signal.main_signal = L2_SIGNAL_WPS_TX_SENT_ACK;
    update_wps_stats(wps_l2);
    circular_queue_dequeue(&wps_l2->current_timeslot->connection_main->xlayer_queue);
    wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
}

/** @brief Handle internal frame failed transmission.
 *
 *  @note This updates the stats and dequeue the current node when
 *        ACK is disabled. When ACK is enabled, the NACK is handled by
 *        the stop and wait.
 *
 *  @param[in] wps_l2  WPS layer 2 instance.
 */
static void handle_internal_frame_transmission_failed(wps_l2_t *wps_l2)
{
    /* Connection with ACK should be handled by stop and wait. */
    if (!wps_l2->current_timeslot->connection_main->ack_enable) {
        wps_l2->output_signal.main_signal = L2_SIGNAL_WPS_TX_SENT_NACK;
        update_wps_stats(wps_l2);
        circular_queue_dequeue(&wps_l2->current_timeslot->connection_main->xlayer_queue);
        wps_l2->current_output = L2_SIGNAL_WPS_EMPTY;
    }
}

/** @brief Overwrite oldest node in XLayer.
 *
 *  @param[in] wps_l2      WPS layer 2 instance.
 *  @param[in] connection  Current L2 internal connection.
 *  @return Newly free xlayer node.
 */
static xlayer_t *overwrite_first_frame_in_queue(wps_l2_t *wps_l2, wps_connection_t *connection)
{
    wps_l2->output_signal.main_signal = L2_SIGNAL_WPS_TX_SENT_NACK;
    update_wps_stats(wps_l2);
    send_done(connection);

    return circular_queue_get_free_slot(&connection->xlayer_queue);
}
