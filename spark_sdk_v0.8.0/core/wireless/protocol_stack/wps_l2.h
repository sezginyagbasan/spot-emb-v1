/** @file  wps_l2.h
 *  @brief Wireless protocol stack layer 2 : Link Layer.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef LINK_L2_H_
#define LINK_L2_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "link_channel_hopping.h"
#include "link_gain_loop.h"
#include "link_lqi.h"
#include "link_protocol.h"
#include "link_random_datarate_offset.h"
#include "link_scheduler.h"
#include "link_tdma_sync.h"
#include "wps_def.h"
#include "xlayer.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
/** @brief Wireless protocol stack Layer 2 input signal.
  */
typedef enum wps_l2_input_signal {

    L2_SIGNAL_RX_FRAME = 0,           /**< Layer 2 frame receive notify signal */
    L2_SIGNAL_RX_FRAME_MISS,          /**< Layer 2 frame miss notify signal */
    L2_SIGNAL_TX_SENT_ACK,            /**< Layer 2 ack receive notify signal */
    L2_SIGNAL_TX_SENT_NACK,           /**< Layer 2 ack miss notify signal */
    L2_SIGNAL_TX,                     /**< Layer 2 TX end notify signal */
    L2_SIGNAL_PREPARE_FRAME,          /**< Layer 2 prepare frame notify signal */
    L2_SIGNAL_SETUP_LINK,             /**< Layer 2 setup link parameter notify signal */
    L2_SIGNAL_SCHEDULE,               /**< Layer 2 scheduler notify signal */
    L2_SIGNAL_EMPTY,                  /**< Layer 2 empty signal */
    L2_SIGNAL_PREPARE_INTERNAL_FRAME, /**< Layer 2 prepare internal frame */

    L2_SIGNAL_NUM

} wps_l2_input_signal_t;

/** @brief Wireless protocol stack Layer 2 output signal.
  */
typedef enum wps_l2_output_signal {

    L2_SIGNAL_WPS_EMPTY = 0,     /**< Layer 2 empty output signal */
    L2_SIGNAL_WPS_FRAME_RX,      /**< Layer 2 frame receive output signal */
    L2_SIGNAL_WPS_FRAME_MISS,    /**< Layer 2 frame miss output signal */
    L2_SIGNAL_WPS_TX_SENT_ACK,   /**< Layer 2 ack receive output signal */
    L2_SIGNAL_WPS_TX_SENT_NACK,  /**< Layer 2 ack miss output signal */
    L2_SIGNAL_WPS_SEND_ACK,      /**< Layer 2 send ack output signal */
    L2_SIGNAL_WPS_SEND_FRAME,    /**< Layer 2 send frame output signal */
    L2_SIGNAL_WPS_RECEIVE_FRAME, /**< Layer 2 receive frame output signal */
    L2_SIGNAL_WPS_RECEIVE_ACK,   /**< Layer 2 receive ack output signal */
    L2_SIGNAL_WPS_FRAME_OVERRUN, /**< Layer 2 No more space available in RX queue */
    L2_SIGNAL_SYNCING,           /**< Layer 2 Enter syncing state output signal */

} wps_l2_output_signal_t;

/** @brief Wireless protocol stack Layer 2 state function pointer.
  */
typedef void (*wps_l2_state)(void *signal_data);

/** @brief Wireless protocol stack Layer 2 output signal parameters.
  */
typedef struct wps_l2_output_signal_info {

    wps_l2_output_signal_t  main_signal; /**< Main output signal */
    wps_l2_output_signal_t  auto_signal; /**< Pending output signal */
    xlayer_t               *main_xlayer; /**< Main output Xlayer node */
    xlayer_t               *auto_xlayer; /**< Pending output Xlayer node */

} wps_l2_output_signal_info_t;

/** @brief Wireless protocol stack Layer 2 input signal parameters.
  */
typedef struct wps_l2_input_signal_info {

    wps_l2_input_signal_t   main_signal; /**< Layer 2 input signal */
    wps_l2_input_signal_t   auto_signal; /**< Layer 2 input signal */
    xlayer_t               *main_xlayer; /**< Layer 2 input Xlayer node */
    xlayer_t               *auto_xlayer; /**< Layer 2 input Xlayer node */

} wps_l2_input_signal_info_t;

/** @brief Wireless protocol stack Layer 2 frame header field.
  */
typedef struct wps_l2_frame_header {

    uint8_t byte0; /**< Byte 0: bit 7 SAW sequence number, bit 6 down to 0 -> timeslot ID */
    uint8_t byte1; /**< Byte 1: bit 7 down to 0 -> Channel hopping sequence */

} wps_l2_frame_header_t;

/** @brief Wireless protocol stack Layer 2 sync module initialization field.
  */
typedef struct wps_l2_sync_cfg {

    sleep_lvl_t sleep_level;  /**< Desired sleep level for Sync */
    uint32_t    preamble_len; /**< Frame preamble length */
    uint32_t    syncword_len; /**< Frame syncword length */

} wps_l2_sync_cfg_t;

/** @brief Wireless protocol stack Layer 2 main structure.
 */
typedef struct wps_l2_struct {

    wps_l2_input_signal_info_t   input_signal;                 /**< Input signal instance */
    wps_l2_output_signal_info_t  output_signal;                /**< Output signal instance */

    wps_l2_frame_header_t        frame_header;                 /**< Frame header */

    wps_l2_state                *state_machine[L2_SIGNAL_NUM]; /**< State machine jump tables */
    uint8_t                      state_process_idx;            /**< State machine process index for jump table */

    timeslot_t                  *current_timeslot;             /**< Current scheduler timeslot */
    uint16_t                     current_timeslot_id;          /**< Current scheduler timeslot ID */
    bool                         current_ts_prime;             /**< True if current timeslot is prime */
    bool                         current_ts_prime_tx;          /**< True if current timeslot is prime and TX */
    scheduler_t                  scheduler;                    /**< Schedule instance */
    channel_hopping_t            channel_hopping;              /**< Channel hopping instance */
    uint8_t                      current_channel_index;        /**< Current channel hopping index */
    bool                         concurrency_enabled;          /**< Concurrency enable flag */
    uint8_t                      network_id;                   /**< Concurrent network ID */
    bool                         fast_sync_enabled;            /**< Fast sync enable flag */

    uint16_t                     local_address;                /**< Node address to handle RX/TX timeslot */
    uint16_t                     coordinator_address;          /**< Coordinator address */

    tdma_sync_t                  tdma_sync;                    /**< Synchronization module instance */

    wps_role_t                   node_role;                    /**< Current node role (Coordinator/Node) */

    xlayer_t                     empty_frame_tx;               /**< Xlayer instance when application TX queue is empty */
    xlayer_t                     empty_frame_rx;               /**< Xlayer instance when application RX queue is empty */

    wps_l2_input_signal_t        current_input;                /**< Currently processed input signal */
    wps_l2_output_signal_t       current_output;
    xlayer_t                    *current_xlayer;               /**< Currently processed Xlayer */

    circular_queue_t            *callback_queue;               /**< callback_queue> */
    link_rdo_t                  *current_rdo;                  /**< Current connection Random Datarate Offset (RDO) algorithm */
} wps_l2_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Layer 2 initialization function.
 *
 *  @param[out] wps_l2               Layer 2 instance.
 *  @param[in]  callback_queue       Pointer to the callback queue instance.
 *  @param[in]  schedule             Link schedule.
 *  @param[in]  sync_cfg             Synchronization module configuration.
 *  @param[in]  local_address        Node local address.
 *  @param[in]  node_role            Node role.
 *  @param[in]  concurrency_enabled  Concurrency enabled.
 *  @param[in]  fast_sync_enabled    Fast sync enabled.
 *  @param[in]  network_id           Network ID.
 */
void wps_l2_init(wps_l2_t *wps_l2,
                 circular_queue_t *callback_queue,
                 schedule_t *schedule,
                 channel_sequence_t *channel_sequence,
                 wps_l2_sync_cfg_t *sync_cfg,
                 uint16_t local_address,
                 wps_role_t node_role,
                 bool concurrency_enabled,
                 bool fast_sync_enabled,
                 uint8_t network_id);

/** @brief Reset the Layer 2 object.
 *
 *  @param wps_l2  Layer 2 instance.
 */
void wps_l2_reset(wps_l2_t *wps_l2);

/** @brief Layer 2 state machine process.
 *
 *  WPS should initialize the input signal inside the layer 2 instance
 *  in order to provide an input signal to the L2 process. Output will be
 *  available through the output_signal member inside the wps_l2 object.
 *
 *  @param[in]  wps_l2  Already initialized Layer 2 instance.
 */
void wps_l2_process(wps_l2_t *wps_l2);

#ifdef __cplusplus
}
#endif

#endif /* LINK_L2_H_ */
