/** @file  wps.h
 *  @brief SPARK Wireless Protocol Stack.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef WPS_H
#define WPS_H

/* INCLUDES *******************************************************************/
#include "sr1000_api_adv.h"
#include "wps_callback.h"
#include "wps_def.h"
#include "wps_error.h"
#include "wps_l1.h"
#include "wps_l2.h"

#ifdef __cplusplus
extern "C" {
#endif

/* MACROS *********************************************************************/
#define BIT_AUTO_REPLY_TIMESLOT (1 << 7)
#define MAIN_TIMESLOT(x) ((x) & 0x7F)
#define AUTO_TIMESLOT(x) ((x) | BIT_AUTO_REPLY_TIMESLOT)

/* TYPES **********************************************************************/
/** @brief Wireless Protocol Stack input signals.
 */
typedef enum wps_input_signal {
    WPS_NONE,
    WPS_RADIO_IRQ,
    WPS_TRANSFER_COMPLETE,
    WPS_CONNECT,
    WPS_DISCONNECT,
    WPS_HALT,
    WPS_RESUME,
    WPS_YIELD,
    WPS_PROCESS_L1,
    WPS_PROCESS_L2,
    WPS_PROCESS_L7,
} wps_input_signal_t;

/** @brief Wireless Protocol Stack status.
 */
typedef enum wps_state {
    WPS_IDLE,
    WPS_PROCESSING,
} wps_status_t;

/** @brief Wireless Protocol Stack connection configuration.
 */
typedef struct wps_connection_config {
    /* Address */
    uint16_t source_address;      /**< Current connection source address (Transmitting node address) */
    uint16_t destination_address; /**< Current connection destination address (Receiving node address) */

    /* Buffer */
    xlayer_t  *fifo_buffer;       /**< Queue buffer */
    uint32_t   fifo_buffer_size;  /**< Queue size */
    uint8_t   *frame_buffer;      /**< Frame buffer use for send/receive payload, depending on the nature of the connection */
    uint16_t   header_length;     /**< Length of the WPS header in the current configuration */
    uint32_t   frame_length;      /**< Frame length to send/receive. Set to header + max payload size.*/

    /* Callback */
    void (*send_ack_callback_t)(wps_connection_t *conn);  /**< Function called by the wps to indicate the frame has been acknowledged */
    void (*send_nack_callback_t)(wps_connection_t *conn); /**< Function called by the wps to indicate the frame has NOT been acknowledged */
    void (*receive_callback_t)(wps_connection_t *conn);   /**< Function called by the wps to indicate the frame has been received */
    void (*error_callback_t)(wps_connection_t *conn);     /**< Function called by the wps to indicate that an error has occurs */

    uint64_t (*get_tick_quarter_ms)(void);  /**< Get free running timer tick in quarter ms */
} wps_connection_cfg_t;

/** @brief Wireless Protocol Stack node configuration.
 */
typedef struct node_config {
    wps_role_t  role;           /**< Current node role : Coordinator or node */
    uint32_t    preamble_len;   /**< Length of the preamble, in bits */
    sleep_lvl_t sleep_lvl;      /**< Radio sleep level */
    uint16_t    crc_polynomial; /**< Radio CRC polynomial */
    uint16_t    local_address;  /**< Node current address */
    uint32_t    syncword;       /**< Radio syncword */
} wps_node_cfg_t;

/** @brief Wireless Protocol Stack radio.
 */
typedef struct wps_radio {
    radio_t          radio;           /**< Radio instance */
    radio_hal_t      radio_hal;       /**< Radio HAL instance */
    radio_adv_t      radio_adv;       /**< Radio advance instance */
    radio_hal_adv_t  radio_hal_adv;   /**< Radio HAL advance instance */
    uint8_t         *spi_rx_buffer;   /**< SPI TX transfer buffer */
    uint8_t         *spi_tx_buffer;   /**< SPI TX transfer buffer */
    uint8_t          spi_buffer_size; /**< Radio TX/RX SPI buffer size, for SPI transfer queue */
    irq_polarity_t   irq_polarity;    /**< Radio IRQ polarity */
} wps_radio_t;

/** @brief Wireless Protocol Stack layer 7.
 */
typedef struct wps_l7 {
    circular_queue_t callback_queue; /**< Circular queue instance to save the callbacks */
} wps_l7_t;

/** @brief Wireless Protocol Stack structure.
 */
typedef struct wps {
    wps_node_t *node;                    /**< WPS node instance */
    schedule_t schedule;                 /**< WPS link schedule (TDMA) */
    channel_sequence_t channel_sequence; /**< WPS channel sequence for channel hopping */
    bool concurrency_enabled;            /**< WPS concurrency enable flag */
    uint8_t network_id;                  /**< WPS concurrent network ID */
    bool fast_sync_enabled;              /**< WPS fast sync enable flag */

    wps_l7_t l7; /**< WPS Layer 7 instance */
    wps_l2_t l2; /**< WPS Layer 2 instance */
    wps_l1_t l1; /**< WPS Layer 1 instance */

    wps_input_signal_t signal;             /**< WPS current signal */
    wps_status_t status;                   /**< WPS status : Idle or processing */
    void (*callback_context_switch)(void); /**< function pointer to trig a context switch to the callback process */
} wps_t;

/** @brief Critical section callback.
 */
typedef struct wps_critical {
    void (*enter_critical)(void); /**< Critical enter callback */
    void (*exit_critical)(void);  /**< Critical exit callback */
} wps_critical_cfg_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Wireless Protocol Stack critical section initialization.
 *
 *  Assign function to enter and exit critical section which disables the interrupts.
 *
 *  @param[in] critical_cfg  User function to enter & exit critical section.
 */
void wps_init_critical_section(wps_critical_cfg_t* critical_cfg);

/** @brief Wireless Protocol Stack radio initialization.
 *
 *  Read and save NVM.
 *  radio_hal_t, radio_hal_adv_t and spi buffer must be already be assigned in wps_radio_t.
 *
 *  @param[in]  radio  Radio instance.
 *  @param[out] error  Error code.
 */
void wps_radio_init(wps_radio_t *radio, wps_error_t *error);

/** @brief Calibrate radio.
 *
 *  Calibrate the radio RF stage and assign frequency band for each channel.
 *
 *  @param[in]  radio  Radio instance.
 *  @param[out] error  Error code.
 */
void wps_radio_calibrate(wps_radio_t *radio, wps_error_t *error);

/** @brief Initialize the callback queue.
 *
 *  The callback queue is used to store event actions waiting to
 *  be executed by the application.
 *
 *  The size of the callback buffer should be equal to the size
 *  of the biggest Xlayer queue.
 *
 *  @param[in] wps              Wireless Protocol Stack instance.
 *  @param[in] callback_buffer  Array of callback function pointer.
 *  @param[in] size             Array size.
 *  @param[in] context_switch   Function that trigger the context switch to the callback process
 */
void wps_init_callback_queue(wps_t *wps,
                             wps_callback_inst_t *callback_buffer,
                             size_t size,
                             void (*context_switch)(void));

/** @brief Wireless Protocol Stack initialization.
 *
 *  Initialize the WPS and all the layers inside the WPS.
 *
 *  @param[in] wps   Wireless Protocol Stack instance.
 *  @param[in] node  Node instance.
 */
void wps_init(wps_t *wps, wps_node_t *node);

/** @brief Set network coordinator address.
 *
 *  @param[in] wps      Wireless Protocol Stack instance.
 *  @param[in] address  Coordinator address.
 */
void wps_set_coordinator_address(wps_t *wps, uint16_t address);

/** @brief Initialize specific PHY layer parameters.
 *
 *  Currently, only the LNA impedance and the phase tracking
 *  are supported.
 *
 *  @param radio    Radio instance.
 *  @param wps_phy  WPS PHY structure.
 */
void wps_phy_init(wps_radio_t *radio, wps_phy_t *wps_phy);

/** @brief Node configuration.
 *
 *  Configure the SPARK radio for proper communication. This goes
 *  through all of the packet configurations, the interrupt event,
 *  the sleep level and the internal radio timer source.
 *
 *  @param[in] node   Node instance.
 *  @param[in] radio  Radio instance.
 *  @param[in] cfg    Node configuration.
 */
void wps_config_node(wps_node_t *node, wps_radio_t *radio, wps_node_cfg_t *cfg);

/** @brief Configure network schedule.
 *
 *  Initialize the schedule object and convert the given duration
 *  to the time base of the SPARK radio.
 *
 *  @param[in] wps                   Wireless Protocol Stack instance.
 *  @param[in] timeslot_duration_us  Timeslot duration array in us.
 *  @param[in] timeslot              Timeslot array used by schedule.
 *  @param[in] schedule_size         Schedule size. Timeslot's array size must match.
 */
void wps_config_network_schedule(wps_t *wps, uint32_t *timeslot_duration_us, timeslot_t *timeslot, uint32_t schedule_size);

/** @brief Reset schedule.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_reset_schedule(wps_t *wps);

/** @brief Configure network channel sequence.
 *
 *  Initialize the channel sequence for the Channel Hopping
 *  module inside the Layer 2 of the WPS.
 *
 *  @param[in] wps               Wireless Protocol Stack instance.
 *  @param[in] channel_sequence  Channel sequence.
 *  @param[in] sequence_size     Channel sequence size.
 */
void wps_config_network_channel_sequence(wps_t *wps, uint32_t *channel_sequence, uint32_t sequence_size);

/** @brief Enable concurrency.
 *
 *  When enabling concurrency, the number of most significant bits reserved
 *  to indicate the network ID out of the 16 address bits needs to be provided.
 *  This value needs to be consistent across all nodes in all concurrent networks.
 *
 *  There is a possibility of 2^network_msbits_count concurrent networks with (16-network_msbits_count)^2 nodes within each of them.
 *
 *  For example, if network_msbits_count == 5:
 *
 *  This means there is a possibility of 32 concurrent networks with 2048 nodes within each of them.
 *  Any node address 0b 0000 0xxx xxxx xxxx is in the network 0.
 *  Any node address 0b 0000 1xxx xxxx xxxx is in the network 1.
 *  ...
 *  Any node address 0b 1111 1xxx xxxx xxxx is in the network 31.
 *
 *  Each of these concurrent networks will have 1 chosen coordinator address within that address space
 *  as well as 2047 possible node addresses.
 *
 *  @param[in] wps                   Wireless Protocol Stack instance.
 *  @param[in] coordinator_address   Coordinator address.
 *  @param[in] network_msbits_count  Number of significant bits used for the network ID.
 */
void wps_enable_concurrency(wps_t *wps, uint16_t coordinator_address, uint8_t network_msbits_count);

/** @brief Disable concurrency.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_disable_concurrency(wps_t *wps);

/** @brief Enable fast sync.
 *
 *  Enable the WPS to synchronize the link using a specific timeslot.
 *  This allows the link to get synchronized faster when connections are not set to auto_sync.
 *
 *  The radio listens for 15.92 us + preamble_len (set in wps_node_cfg_t) * 48.828125 ns every syncing_period_us.
 *
 *  @param[in] wps                Wireless Protocol Stack instance.
 *  @param[in] syncing_period_us  Syncing period in us.
 *  @param[in] syncing_time_slot  Syncing time slot.
 */
void wps_enable_fast_sync(wps_t *wps, uint16_t syncing_period_us, timeslot_t *syncing_time_slot);

/** @brief Disable fast sync.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_disable_fast_sync(wps_t *wps);

/** @brief  Enable the WPS internal connection.
 *
 *  @note   Here are the available protocol as of right
 *          now in the connection :
 *          - Random datarate offset (2 bytes)
 *
 *  @param[in] connection         Connection instance.
 *  @param[in] internal_conn_cfg  Internal connection configuration.
 */
void wps_enable_internal_connection(wps_connection_t *connection, wps_internal_connection_cfg_t *internal_conn_cfg);

/** @brief Disable the WPS internal connection.
 *
 *  @note This disable every protocol inside the
 *        internal connection.
 *
 *  @param[in] connection Connection instance.
 */
void wps_disable_internal_connection(wps_connection_t *connection);

/** @brief Create a connection between two nodes.
 *
 *  A connection is a unidirectional link between two nodes.
 *  The direction of the data will be determined by the relation
 *  between the source address of the connection and the current
 *  node address. For example, if source address is equal to
 *  this node address, the node will transmit during this
 *  connection.
 *
 *  @param[in] connection  Connection instance.
 *  @param[in] node        Node instance.
 *  @param[in] config      Connection configuration.
 */
void wps_create_connection(wps_connection_t *connection, wps_node_t *node, wps_connection_cfg_t *config);

/** @brief Set connection's timeslot.
 *
 *  A connection may send its payload via the MAIN_TIMESLOT or AUTO_TIMESLOT. It can't use both on the same timeslot.
 *  Feature like retransmission are not available when using AUTO_TIMESLOT.
 *
 *  @param[in] connection    Connection instance.
 *  @param[in] wps           Wireless Protocol Stack instance.
 *  @param[in] timeslot_id   Timeslot id used by the connection.
 *  @param[in] nb_timeslots  Number of timeslot used by the connection. Must match timeslot_id array size.
 */
void wps_connection_set_timeslot(wps_connection_t *connection, wps_t *wps, int32_t *timeslot_id, uint32_t nb_timeslots);

/** @brief Configure connection's RF channel.
 *
 *  Configure the receiver's filter, transmission power and power amplifier.
 *
 *  @param[in] connection  Connection instance.
 *  @param[in] node        Node instance.
 *  @param[in] channel_x   Channel number.
 *  @param[in] config      Channel configuration.
 */
void wps_connection_config_channel(wps_connection_t *connection,wps_node_t *node, uint8_t channel_x, channel_cfg_t *config);

/** @brief Configure connection's frame modulation and FEC level.
 *
 *  @param[in] connection  Connection instance.
 *  @param[in] modulation  Modulation.
 *  @param[in] fec         FEC level.
 */
void wps_connection_config_frame(wps_connection_t *connection, modulation_t modulation, fec_level_t fec);

/** @brief Enable acknowledgment for connection's packet.
 *
 *  @param[in] connection  Connection instance.
 */
void wps_connection_enable_ack(wps_connection_t *connection);

/** @brief Disable acknowledgment for connection's packet.
 *
 *  @param[in] connection  Connection instance.
 */
void wps_connection_disable_ack(wps_connection_t *connection);

/** @brief Enable Stop and Wait (SaW) and Automatic Repeat Request (ARQ) for connection's packet.
 *
 *  @param[in] connection           Connection instance.
 *  @param[in] local_address        Current node address.
 *  @param[in] retry                Maximum number of retry.
 *  @param[in] deadline_quarter_ms  Deadline in 1/4 ms. Packet is dropped when deadline is reached.
 */
void wps_connection_enable_stop_and_wait_arq(wps_connection_t *connection,
                                             uint16_t local_address,
                                             uint32_t retry,
                                             uint32_t deadline_quarter_ms);

/** @brief Disable Stop and Wait (SaW) and Automatic Repeat Request (ARQ) for connection's packet.
 *
 *  Retransmission are disabled when disabling the SaW ARQ.
 *  Incomplete transmission will be notified to user every
 *  time a packet is NACK.
 *
 *  @param[in] connection  Connection instance.
 */
void wps_connection_disable_stop_and_wait_arq(wps_connection_t *connection);

/** @brief Enable auto-sync mode.
 *
 * If this mode is enabled, when the cross-layer queue is empty,
 * a small frame containing the L2 header will be sent over the air for the current time slot.
 * This can be useful to keep synchronization when no application data is being sent.
 *
 * Warning: When the auto-sync is disabled, the user should provide a consistent data rate
 * output on the coordinator in order to keep synchronization in the network.
 *
 *  @param[in] connection  Connection instance.
 */
void wps_connection_enable_auto_sync(wps_connection_t *connection);

/** @brief Disable auto-sync mode.
 *
 * If this mode is disabled, when the cross-layer queue is empty,
 * no data will be sent over the air, the radio will simply wake up and
 * the WPS will prepare the next time slot.
 *
 * Warning: When the auto-sync is disabled, the user should provide a consistent data rate
 * output on the coordinator in order to keep synchronization in the network.
 *
 *  @param[in] connection  Connection instance.
 */
void wps_connection_disable_auto_sync(wps_connection_t *connection);

/** @brief Remove connection from network.
 *
 *  @param[in] connection  Connection instance.
 *  @param[in] wps         Wireless Protocol Stack instance.
 */
void wps_connection_remove_connection(wps_connection_t *connection, wps_t *wps);

/** @brief Enable connection fixed payload size mode.
 *
 * Warning: The use of auto replies to send user payload is not supported when this mode is enabled.
 *
 *  @param[in] connection          Connection instance.
 *  @param[in] fixed_payload_size  Payload size.
 */
void wps_connection_enable_fixed_payload_size(wps_connection_t *connection, uint8_t fixed_payload_size);

/** @brief Enable a connection's Clear Channel Assessment (CCA).
 *
 *  @param[in] connection     Connection instance.
 *  @param[in] threshold      CCA threshold.
 *  @param[in] retry_time_us  CCA retry time.
 *  @param[in] try_count      CCA try count.
 *  @param[in] fail_action    CCA fail action.
 */
void wps_connection_enable_cca(wps_connection_t *connection,
                               uint8_t           threshold,
                               uint8_t           retry_time_us,
                               uint8_t           try_count,
                               cca_fail_action_t fail_action);

/** @brief Disable connection Clear Channel Assessment (CCA).
 *
 *  @note To properly disable CCA, the CCA module needs to be disabled with a threshold of 0xff.
 *
 *  @param[in] connection  Connection instance.
 */
void wps_connection_disable_cca(wps_connection_t *connection);

/** @brief Connect node to network.
 *
 *  Setup the radio internal timer and reset every layer in the WPS.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_connect(wps_t *wps);

/** @brief Disconnect node from network.
 *
 *  Put radio to sleep and disable internal radio timer
 *  to disconnect the radio from the network.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_disconnect(wps_t *wps);

/** @brief Reset the WPS when a crash occurs.
 *
 *  When a crash occurs the WPS is disconnected and then reconnected.
 *
 *  @param wps Wireless Protocol Stack instance.
 */
void wps_reset(wps_t *wps);

/** @brief Halt connection to network.
 *
 *  The node stays synchronized but doesn't send / receive application payload.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_halt(wps_t *wps);

/** @brief Resume connection to network.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_resume(wps_t *wps);

/** @brief Get buffer from the wps queue to hold the payload
 *
 *  The usage of this function is optional. If you don't want to
 *  use the WPS queue to hold the tx payload, when wps_create_connection
 *  is called, set the config.frame_length value to WPS_RADIO_HEADER_SIZE.
 *
 *  @param[in] connection  Connection instance.
 *  @param[in] payload     Pointer to the empty payload memory.
 *  @param[in] max_size    Maximum payload size.
 */
bool wps_get_free_slot(wps_connection_t *connection, uint8_t **payload);

/** @brief Send payload over the air.
 *
 *  Enqueue a node in the connection Xlayer and WPS will
 *  send at next available timeslot.
 *
 *  @param[in] connection  Connection instance.
 *  @param[in] payload     Application payload to send over the air.
 *  @param[in] size        Payload size in bytes.
 *  @retval True  Payload is successfully received by the WPS.
 *  @retval False WPS FIFO is full. Payload is not received by WPS.
 */
bool wps_send(wps_connection_t *connection, uint8_t *payload, uint8_t size);

/** @brief Read last received frame.
 *
 *  @param[in] connection  Connection instance.
 */
wps_rx_frame wps_read(wps_connection_t *connection);

/** @brief Remove the frame from the receiver FIFO.
 *
 *  @param[in] connection  Connection instance.
 *  @retval True  Successfully dequeue the oldest payload.
 *  @retval False Queue is empty, nothing to dequeue.
 */
bool wps_read_done(wps_connection_t *connection);

/** @brief Return the used space of the connection Xlayer queue.
 *
 *  @param connection  Connection instance.
 *  @return Xlayer queue's used space.
 */
uint32_t wps_get_fifo_size(wps_connection_t *connection);

/** @brief Return the free space of the connection Xlayer queue.
 *
 *  @param connection  Connection instance.
 *  @return Xlayer queues's free space.
 */
uint32_t wps_get_fifo_free_space(wps_connection_t *connection);

/** @brief Process the Wireless Protocol Stack.
 *
 *  Process the WPS state machine in order to
 *  send or receive a packet inside the network.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_process(wps_t *wps);

/** @brief Process the wps callback
 *
 * This function should be called in a context with higher
 * priority than the application, but lower priority than the
 * radio IRQs. You can run it in a really high priority RTOS
 * task or something like pendsv.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_process_callback(wps_t *wps);

/** @brief Radio IRQ signal.
 *
 *  Notify the WPS of a context switch.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_radio_irq(wps_t *wps);

/** @brief SPI transfer complete.
 *
 *  Notify the WPS of a DMA transfer complete interrupt.
 *
 *  @param[in] wps  Wireless Protocol Stack instance.
 */
void wps_transfer_complete(wps_t *wps);

/** @brief Get the current WPS error.
 *
 *  @note This function should only be called from
 *        the error_callback_t that should be implemented
 *        in the application. The output error is of type
 *        wps_error_t.
 *
 *  @param[in] connection  Current connection with an error.
 *  @return Current WPS error.
 */
wps_error_t wps_get_error(wps_connection_t *connection);

#ifdef __cplusplus
}
#endif

#endif /* WPS_H */
