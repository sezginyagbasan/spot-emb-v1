/** @file  wps_process.c
 *  @brief Wireless Protocol Stack process.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "wps.h"

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static bool is_l2_to_l1(wps_l2_t *l2);
static bool is_l2_to_wps(wps_l2_t *l2);
static void set_signal_l2_to_wps(wps_l2_t *l2, wps_t *wps);
static void set_signal_l2_to_l1(wps_l2_t *l2, wps_l1_t *l1);
static bool is_l1_to_l2(wps_l1_t *l1);
static bool is_l1_to_l7(wps_l1_t *l1);
static void set_signal_l1_to_l2(wps_l1_t *l1, wps_l2_t *l2);
static bool is_l1_to_wps(wps_l1_t *l1);

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
void wps_process(wps_t *wps)
{
    wps->status = WPS_PROCESSING;

    do {
        if (wps->signal == WPS_TRANSFER_COMPLETE) {
            wps->signal = WPS_PROCESS_L1;
        } else if (wps->signal == WPS_RADIO_IRQ) {
            wps->signal          = WPS_PROCESS_L1;
            wps->l1.input_signal = L1_SIGNAL_RADIO_IRQ;
        } else if (wps->signal == WPS_CONNECT) {
            wps->signal = WPS_PROCESS_L2;
            wps->l2.input_signal.main_signal = L2_SIGNAL_SCHEDULE;
        }

        if (wps->signal == WPS_PROCESS_L1) {
            wps->signal = WPS_NONE;

            wps_l1_process(&wps->l1);

            if (is_l1_to_wps(&wps->l1)) {
                wps->signal = WPS_YIELD;
                wps->status = WPS_IDLE;
            } else if (is_l1_to_l2(&wps->l1)) {
                wps->signal = WPS_PROCESS_L2;
                set_signal_l1_to_l2(&wps->l1, &wps->l2);
            } else if (is_l1_to_l7(&wps->l1)) {
                wps->signal = WPS_PROCESS_L7;
            } else if (wps->l1.signal_main == L1_SIGNAL_ERROR) {
                wps->signal = WPS_PROCESS_L2;
                wps->l2.input_signal.main_signal = L2_SIGNAL_SCHEDULE;
                wps_l1_reset(&wps->l1);
                wps_l2_reset(&wps->l2);
                wps->l2.current_timeslot->connection_main->wps_error = WPS_L1_CRITICAL_ERROR;
                wps->l2.output_signal.main_xlayer->config.callback = wps->l2.current_timeslot->connection_main->error_callback_t;
                wps_callback_enqueue(&wps->l7.callback_queue, wps->l2.output_signal.main_xlayer, wps->l2.current_timeslot->connection_main);
            }
        }

        if (wps->signal == WPS_PROCESS_L2) {
            wps->signal = WPS_NONE;

            wps_l2_process(&wps->l2);

            if (is_l2_to_wps(&wps->l2)) {
                set_signal_l2_to_wps(&wps->l2, wps);
            } else if (is_l2_to_l1(&wps->l2)) {
                wps->signal = WPS_PROCESS_L1;
                set_signal_l2_to_l1(&wps->l2, &wps->l1);
                wps->l1.xlayer_main = wps->l2.output_signal.main_xlayer;
                wps->l1.xlayer_auto = wps->l2.output_signal.auto_xlayer;
            }
        }

        if (wps->signal == WPS_PROCESS_L7) {
            wps->signal = WPS_YIELD;
            wps->status = WPS_IDLE;
            wps->callback_context_switch();
        }

        if (wps->signal == WPS_NONE) {
            wps->signal = WPS_PROCESS_L2;
            wps->l2.input_signal.main_signal = L2_SIGNAL_SCHEDULE;
        }

    } while (wps->status == WPS_PROCESSING);

}

void wps_radio_irq(wps_t *wps)
{
    if (wps->signal != WPS_CONNECT) {
        wps->signal = WPS_RADIO_IRQ;
    }
}

void wps_transfer_complete(wps_t *wps)
{
    wps->signal          = WPS_TRANSFER_COMPLETE;
    wps->l1.input_signal = L1_SIGNAL_DMA_CMPLT;
}

void wps_process_callback(wps_t *wps)
{
    wps_callback_inst_t *callback;

    while (circular_queue_is_empty(&wps->l7.callback_queue) == false) {
        callback = circular_queue_front(&wps->l7.callback_queue);
        if (callback != NULL && callback->func != NULL) {
            callback->func(callback->connection);
        }
        circular_queue_dequeue(&wps->l7.callback_queue);
    }
}

/* PRIVATE FUNCTION DEFINITIONS ***********************************************/
/** @brief Finish a transmission.
 *
 *  @param[in] connection  Connection instance.
 *  @retval true   On success.
 *  @retval false  On error.
 */
static bool send_done(wps_connection_t *connection)
{
    return circular_queue_dequeue(&connection->xlayer_queue);
}

/** @brief Enqueue a new RX frame to the Xlayer queue.
 *
 *  @param[in] connection Connection instance.
 *  @retval true   On success.
 *  @retval false  On error.
 */
static bool enqueue_rx_frame(wps_connection_t *connection)
{
    return circular_queue_enqueue(&connection->xlayer_queue);
}

/** @brief Check if going from L2 to L1.
 *
 *  @param[in] l2  Layer 2: Link Layer.
 *  @retval false
 *  @retval true
 */
static bool is_l2_to_l1(wps_l2_t *l2)
{
    switch (l2->output_signal.main_signal) {
    case L2_SIGNAL_WPS_SEND_FRAME:
    case L2_SIGNAL_WPS_RECEIVE_FRAME:
    case L2_SIGNAL_SYNCING:
        return true;
    default:
        return false;
    }
}

/** @brief Check if going from L2 to WPS.
 *
 *  @param[in] l2  Layer 2: Link Layer.
 *  @retval false
 *  @retval true
 */
static bool is_l2_to_wps(wps_l2_t *l2)
{
    switch (l2->output_signal.main_signal) {
    case L2_SIGNAL_WPS_EMPTY:
    case L2_SIGNAL_WPS_FRAME_RX:
    case L2_SIGNAL_WPS_FRAME_MISS:
    case L2_SIGNAL_WPS_FRAME_OVERRUN:
    case L2_SIGNAL_WPS_TX_SENT_ACK:
    case L2_SIGNAL_WPS_TX_SENT_NACK:
        return true;
    default:
        return false;
    }
}

/** @brief Check if going from L1 to L2.
 *
 *  @param[in] l1  Layer 1: PHY Layer.
 *  @retval false
 *  @retval true
 */
static bool is_l1_to_l2(wps_l1_t *l1)
{
    switch (l1->signal_main) {
    case L1_SIGNAL_FRAME_RECEIVED:
    case L1_SIGNAL_FRAME_MISSED:
    case L1_SIGNAL_FRAME_SENT_ACK:
    case L1_SIGNAL_FRAME_SENT_NACK:
        return true;
    default:
        return false;
    }
}

/** @brief Check if going from L1 to WPS.
 *
 *  @param[in] l1  Layer 1: PHY Layer.
 *  @retval false
 *  @retval true
 */
static bool is_l1_to_wps(wps_l1_t *l1)
{
    switch (l1->signal_main) {
    case L1_SIGNAL_YIELD:
        return true;
    default:
        return false;
    }
}

/** @brief Check if going from L1 to L7.
 *
 *  @param[in] l1  Layer 1: PHY Layer.
 *  @retval false
 *  @retval true
 */
static bool is_l1_to_l7(wps_l1_t *l1)
{
    switch (l1->signal_main) {
        case L1_SIGNAL_PREPARE_DONE:
            return true;
        default:
            return false;
    }
}

/** @brief Set signal between L1 and L2.
 *
 *  @param[in] l1  Layer 1: PHY Layer.
 *  @param[in] l2  Layer 2: Link Layer.
 */
static void set_signal_l1_to_l2(wps_l1_t *l1, wps_l2_t *l2)
{
    switch (l1->signal_main) {
    case L1_SIGNAL_FRAME_RECEIVED:
        l2->input_signal.main_signal = L2_SIGNAL_RX_FRAME;
        break;
    case L1_SIGNAL_FRAME_MISSED:
        l2->input_signal.main_signal = L2_SIGNAL_RX_FRAME_MISS;
        break;
    case L1_SIGNAL_FRAME_SENT_ACK:
        l2->input_signal.main_signal = L2_SIGNAL_TX_SENT_ACK;
        break;
    case L1_SIGNAL_FRAME_SENT_NACK:
        l2->input_signal.main_signal = L2_SIGNAL_TX_SENT_NACK;
        break;
    case L1_SIGNAL_NONE:
        l2->input_signal.main_signal = L2_SIGNAL_EMPTY;
        break;
    default:
        break;
    }

    switch (l1->signal_auto) {
    case L1_SIGNAL_FRAME_RECEIVED:
        l2->input_signal.auto_signal = L2_SIGNAL_RX_FRAME;
        break;
    case L1_SIGNAL_FRAME_MISSED:
        l2->input_signal.auto_signal = L2_SIGNAL_RX_FRAME_MISS;
        break;
    case L1_SIGNAL_FRAME_SENT_ACK:
        l2->input_signal.auto_signal = L2_SIGNAL_TX_SENT_ACK;
        break;
    case L1_SIGNAL_FRAME_SENT_NACK:
        l2->input_signal.auto_signal = L2_SIGNAL_TX_SENT_NACK;
        break;
    case L1_SIGNAL_NONE:
        l2->input_signal.auto_signal = L2_SIGNAL_EMPTY;
        break;
    default:
        break;
    }

    l2->input_signal.main_xlayer = l1->xlayer_main;
    l2->input_signal.auto_xlayer = l1->xlayer_auto;
}

/** @brief Set signal between L2 and WPS.
 *
 *  @param[in] l2   Layer 2: Link Layer.
 *  @param[in] wps  Wireless Protocol Stack structure.
 */
static void set_signal_l2_to_wps(wps_l2_t *l2, wps_t *wps)
{
    wps->signal = WPS_NONE;

    switch (l2->output_signal.main_signal) {
    case L2_SIGNAL_WPS_EMPTY:
        break;
    case L2_SIGNAL_WPS_FRAME_RX:
        enqueue_rx_frame(l2->current_timeslot->connection_main);
        wps_callback_enqueue(&wps->l7.callback_queue, l2->output_signal.main_xlayer, l2->current_timeslot->connection_main);
        break;
    case L2_SIGNAL_WPS_FRAME_MISS:
        break;
    case L2_SIGNAL_WPS_FRAME_OVERRUN:
        l2->current_timeslot->connection_main->wps_error = WPS_RX_OVERRUN_ERROR;
        wps_callback_enqueue(&wps->l7.callback_queue, l2->output_signal.main_xlayer, l2->current_timeslot->connection_main);
        break;
    case L2_SIGNAL_WPS_TX_SENT_ACK:
    case L2_SIGNAL_WPS_TX_SENT_NACK:
        wps_callback_enqueue(&wps->l7.callback_queue, l2->output_signal.main_xlayer, l2->current_timeslot->connection_main);
        send_done(l2->current_timeslot->connection_main);
        break;
    default:
        break;
    }

    switch (l2->output_signal.auto_signal) {
        case L2_SIGNAL_WPS_EMPTY:
            break;
        case L2_SIGNAL_WPS_FRAME_RX:
            enqueue_rx_frame(l2->current_timeslot->connection_auto_reply);
            wps_callback_enqueue(&wps->l7.callback_queue, l2->output_signal.auto_xlayer, l2->current_timeslot->connection_auto_reply);
            break;
        case L2_SIGNAL_WPS_FRAME_MISS:
        case L2_SIGNAL_WPS_FRAME_OVERRUN:
            break;
        case L2_SIGNAL_WPS_TX_SENT_ACK:
        case L2_SIGNAL_WPS_TX_SENT_NACK:
            wps_callback_enqueue(&wps->l7.callback_queue, l2->output_signal.auto_xlayer, l2->current_timeslot->connection_auto_reply);
            send_done(l2->current_timeslot->connection_auto_reply);
            break;
        default:
            break;
    }
}

/** @brief Set signal between L2 and L1.
 *
 *  @param[in] l2  Layer 2: Link Layer.
 *  @param[in] l1  Layer 1: PHY Layer.
 */
static void set_signal_l2_to_l1(wps_l2_t *l2, wps_l1_t *l1)
{
    switch(l2->output_signal.main_signal) {
        case L2_SIGNAL_SYNCING:
            l1->input_signal = L1_SIGNAL_SYNCING;
            break;
        default:
            l1->input_signal = L1_SIGNAL_PREPARE_RADIO;
            break;
    }
}
