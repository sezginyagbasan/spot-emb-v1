/** @file  wps_error.h
 *  @brief Wireless Protocol Stack error codes.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef WPS_ERROR_H_
#define WPS_ERROR_H_

/* TYPES **********************************************************************/
/** @brief WPS error enum definition.
 */
typedef enum wps_err {
    WPS_NO_ERROR = 0,
    WPS_RX_OVERRUN_ERROR,     /**< The WPS have received a frame, but the RX queue was full. Packet is discard and overrun is returned. */
    WPS_L1_CRITICAL_ERROR,    /**< L1 state machine has received a signal that is not handled. */
} wps_error_t;

#endif /* WPS_ERROR_H_ */
