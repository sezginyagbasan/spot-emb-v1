/** @file ranging_algorithm_private.h
 *  @brief Allows to fetch data from ranging algorithm for analysis.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

#ifndef RANGING_ALGORITHM_PRIVATE_H_
#define RANGING_ALGORITHM_PRIVATE_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "ranging_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANT *******************************************************************/
#ifndef RANGING_DEV_MAX_SIZE
#define RANGING_DEV_MAX_SIZE 256
#endif

/* TYPES **********************************************************************/
typedef int16_t (*dev_print_func) (void *pbuf, size_t buf_len);
typedef char * (*str_cat_func) (char *dest, char *src);
typedef char * (*format_float_func) (float nbr, unsigned char digit, unsigned char decimal_point,
                                     char lead_char, unsigned  char nul, char *str);

typedef struct ranging_prv_data {
    ranging_master_slave_info_t ranging_master_slave[RANGING_DEV_MAX_SIZE];
    float prop_del_raw[RANGING_DEV_MAX_SIZE];
    float prop_del_corrected[RANGING_DEV_MAX_SIZE];
    float prop_del_average[RANGING_DEV_MAX_SIZE];
    float last_distance[RANGING_DEV_MAX_SIZE];
} ranging_prv_data_t;

typedef struct ranging_prv {
    ranging_prv_data_t ranging_prv_data;
    uint16_t           current_idx;
    dev_print_func     print;
    str_cat_func       cat;
    format_float_func  format_float;
 } ranging_prv_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize ranging private data and information.
 *
 *  @param[in] ranging_prv  Ranging private information.
 */
void ranging_algorithm_prv_init(ranging_prv_t *ranging_prv);

/** @brief Set utils functions for ranging information.
 *
 *  @param[in] ranging_prv  Ranging private instance.
 *  @param[in] print_ptr    Pointer to print to interface.
 *  @param[in] cat          Pointer to cat defintion.
 *  @param[in] format_float Pointer to float_to_string defintion.
 */
void ranging_algorithm_prv_set_utils(ranging_prv_t *ranging_prv, dev_print_func print_ptr, str_cat_func cat,
                                     format_float_func format_float);

/** @brief Set private ranging raw data.
 *
 *  @param[in] ranging_prv  Ranging private instance.
 *  @param[in] r_info       Phase information.
 *  @param[in] size         Size to be copy.
 */
void ranging_algorithm_prv_set_raw_data(ranging_prv_t *ranging_prv, ranging_master_slave_info_t *r_info, uint16_t size);

/** @brief Set private ranging raw delay propagation.
 *
 *  @param[in] ranging_prv  Ranging private instance.
 *  @param[in] raw_prop     Raw delay propgation.
 *  @param[in] size         Size to be copy.
 */
void ranging_algorithm_prv_set_raw_del_prop(ranging_prv_t *ranging_prv, float *raw_prop, uint16_t size);

/** @brief Set private ranging corrected delay propagation.
 *
 *  @param[in] ranging_prv     Ranging private instance.
 *  @param[in] prop_corrected  Value to write to private value.
 *  @param[in] size            Size to copy
 */
void ranging_algorithm_prv_set_prop_del_corrected(ranging_prv_t *ranging_prv, float *prop_corrected, uint16_t size);

/** @brief Set private ranging last distance.
 *
 *  @param[in] ranging_prv  Ranging private instance.
 *  @param[in] dist  Distance to write to private value.
 */
void ranging_algorithm_prv_set_last_distance_data(ranging_prv_t *ranging_prv, float dist);

/** @brief Set private ranging average propgation delay.
 *
 *  @param[in] ranging_prv     Ranging private instance.
 *  @param[in] prop_corrected  Value to write to private value.
 */
void ranging_algorithm_prv_set_prop_del_avg(ranging_prv_t *ranging_prv, float del_prop_avg);

/** @brief Print the column label for the ranging dev data.
 *
 *  @param[in] ranging_prv     Ranging private instance.
 */
void ranging_algorithm_prv_print_column_label(ranging_prv_t *ranging_prv);

/** @brief Print current ranging information to interface.
 *
 *  @param[in] ranging_prv     Ranging private instance.
 */
void ranging_algorithm_prv_print(ranging_prv_t *ranging_prv);

/** @brief Increment ranging private index.
 *
 *  @param[in] ranging_prv     Ranging private instance.
 *  @param[in] nb_sample       Number of sample taken.
 */
void ranging_algorithm_prv_increment_idx(ranging_prv_t *ranging_prv, uint16_t nb_sample);

/** @brief Look if the buffer is full.
 *
 *  @param[in] ranging_prv  Ranging private instance.
 *  @retval True            Buffer is full.
 *  @retval False           Buffer is not full.
 */
bool ranging_algorithm_prv_is_buf_full(ranging_prv_t *ranging_prv);

/** @brief Look if the buffer is empty.
 *
 *  @param[in] ranging_prv  Ranging private instance.
 *  @retval True            Buffer is empty.
 *  @retval False           Buffer is not empty.
 */
bool ranging_algorithm_prv_is_buf_empty(ranging_prv_t *ranging_prv);

#ifdef __cplusplus
}
#endif


#endif /*RANGING_ALGORITHM_PRIVATE_H_ */
