/** @file  ranging_base_fixed_point.c
 *  @brief Ranging module using fixed point instead of
 *         floating point unit.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "ranging_base_fixed_point.h"

/* CONSTANTS ******************************************************************/
#define ONE_EIGHTH              0.125
#define ONE_HALF                0.5
#define ONE_QUARTER             0.25

/* TYPES **********************************************************************/
typedef struct calculate_prop_delay_param {
    fp_format_t *ranging_fp_struct;          /**< Ranging fixed point instance */
    ranging_dynamic_constants_t *ranging_constants; /**< Ranging fixed point dynamic constants structure */
    int8_t phase_1;                                 /**< Window of energy 1 from sr10x0 */
    int8_t phase_2;                                 /**< Window of energy 2 from sr10x0 */
    int8_t phase_3;                                 /**< Window of energy 3 from sr10x0 */
    int8_t phase_4;                                 /**< Window of energy 4 from sr10x0 */
} _calculate_prop_delay_param_t;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static float ranging_base_get_propagation_delay_fixed_point(fp_format_t *ranging_fp_struct,
                                                            ranging_dynamic_constants_t *ranging_constants,
                                                            ranging_master_slave_info_t *ranging_phase);
static void calculate_propagation_delay_fixed_point(_calculate_prop_delay_param_t *parameters, q_num_t *phase);
static inline q_num_t get_sign_fixed_point(ranging_dynamic_constants_t *ranging_constants, q_num_t value);

/* PUBLIC FUNCTIONS ***********************************************************/
fp_format_t ranging_base_fixed_point_init(ranging_dynamic_constants_t *ranging_constants,
                                               uint8_t precision_bits, int32_t offset)
{
    fp_format_t ranging_base_fixed_point_struct;

    /* TODO : Unsigned q_num_t unsupported for the moment */
    ranging_base_fixed_point_struct = sr1000_fp_initialization(precision_bits, 31 - precision_bits);

    ranging_constants->slope_q               = sr1000_fp_float_to_q_conv(&ranging_base_fixed_point_struct, RANGING_SLOPE);
    ranging_constants->offset_q              = sr1000_fp_float_to_q_conv(&ranging_base_fixed_point_struct,
                                                                         (float)offset/(float)RANGING_SCALING);
    ranging_constants->quarter_q             = sr1000_fp_float_to_q_conv(&ranging_base_fixed_point_struct, ONE_QUARTER);
    ranging_constants->half_q                = sr1000_fp_float_to_q_conv(&ranging_base_fixed_point_struct, ONE_HALF);
    ranging_constants->eighth_q              = sr1000_fp_float_to_q_conv(&ranging_base_fixed_point_struct, ONE_EIGHTH);
    ranging_constants->precision_q           = sr1000_fp_get_precision_q(&ranging_base_fixed_point_struct);
    ranging_constants->clk_cyc_comp_q        = sr1000_fp_float_to_q_conv(&ranging_base_fixed_point_struct, CLOCK_CYCLE_COMPARE);

    return ranging_base_fixed_point_struct;

}
void ranging_base_get_propagation_delay_array_fixed_point(fp_format_t *ranging_fp_struct,
                                                          ranging_dynamic_constants_t *ranging_constants,
                                                          ranging_master_slave_info_t *ranging_data,
                                                          uint16_t size,
                                                          q_num_t *raw_prop_delay)
{
    for (uint16_t i = 0; i < size; i++) {
        raw_prop_delay[i] = ranging_base_get_propagation_delay_fixed_point(ranging_fp_struct,
                                                                           ranging_constants,
                                                                           ranging_data + i);
    }
}

q_num_t ranging_base_calculate_average_fixed_point(fp_format_t *ranging_fp_struct,
                                                 q_num_t *data,
                                                 uint16_t size)
{
    q_num_t average    = 0;
    fp_mean_struct_t ranging_mean_fp;

    ranging_mean_fp = sr1000_fp_mean_init(ranging_fp_struct, size);

    for (uint16_t i = 0; i < size; i++) {
        sr1000_fp_mean_add(&ranging_mean_fp, data[i]);
    }
    average = sr1000_fp_mean_calculate(&ranging_mean_fp, size);
    return average;
}

q_num_t ranging_base_convert_to_distance_fixed_point(fp_format_t *ranging_fp_struct,
                                                     ranging_dynamic_constants_t *ranging_const,
                                                     q_num_t prop_delay)
{
    q_num_t multiply_part = sr1000_fp_multiply(ranging_fp_struct, prop_delay, ranging_const->slope_q);
    q_num_t result = sr1000_fp_add(multiply_part, ranging_const->offset_q);

    return result;
}

/* PRIVATE FUNCTIONS ****************************************************************/
/** @brief Get propagation delay for distance calculation.
 *
 *  @param[in] ranging_fp_struct  Fixed point format structure.
 *  @param[in] ranging_constants  Ranging fixed point converted constants.
 *  @param[in] ranging_phase      Ranging information.
 *  @return Propagation delay value.
 */
static float ranging_base_get_propagation_delay_fixed_point(fp_format_t *ranging_fp_struct,
                                                            ranging_dynamic_constants_t *ranging_constants,
                                                            ranging_master_slave_info_t *ranging_phase)
{
    q_num_t master_propagation;
    q_num_t slave_propagation;
    uint16_t rx_wait_time;
    q_num_t rx_wait_time_q;
    q_num_t sum;
    _calculate_prop_delay_param_t master_parameters;
    _calculate_prop_delay_param_t slave_parameters;


    master_parameters.phase_1           = ranging_phase->master_info.phase1;
    master_parameters.phase_2           = ranging_phase->master_info.phase2;
    master_parameters.phase_3           = ranging_phase->master_info.phase3;
    master_parameters.phase_4           = ranging_phase->master_info.phase4;
    master_parameters.ranging_constants = ranging_constants;
    master_parameters.ranging_fp_struct = ranging_fp_struct;

    calculate_propagation_delay_fixed_point(&master_parameters, &master_propagation);

    slave_parameters.phase_1           = ranging_phase->slave_info.phase1;
    slave_parameters.phase_2           = ranging_phase->slave_info.phase2;
    slave_parameters.phase_3           = ranging_phase->slave_info.phase3;
    slave_parameters.phase_4           = ranging_phase->slave_info.phase4;
    slave_parameters.ranging_constants = ranging_constants;
    slave_parameters.ranging_fp_struct = ranging_fp_struct;

    calculate_propagation_delay_fixed_point(&slave_parameters, &slave_propagation);

    rx_wait_time = ranging_phase->master_info.rx_waited0 | (ranging_phase->master_info.rx_waited1 << 8);

    /* Convert RX wait time to Q representation after all manipulation */
    rx_wait_time_q = sr1000_fp_int_to_q_conv(ranging_fp_struct, (int32_t)rx_wait_time);

    sum = sr1000_fp_add(master_propagation, sr1000_fp_add(slave_propagation, rx_wait_time_q));

    return sum;
}

/** @brief Calculate the fractional part of the propagation delay in fixed point.
 *
 *  Calculate the fine portion of the propagation delay
 *  to have high resolution measurement using fixed point arithmetic.
 *
 *  @param[in]  parameters  Fixed point and phase parameters.
 *  @param[out] phase       Precise portion of propagation delay.
 *  @return True if the measurement were abnormal.
 */
static void calculate_propagation_delay_fixed_point(_calculate_prop_delay_param_t *parameters, q_num_t *phase)
{
    q_num_t window_energy[4];
    q_num_t sides_diff                          =   0;
    q_num_t base_value                          =   0;
    q_num_t phase_value                         =   0;
    fp_format_t local_fp_struct          = *(parameters->ranging_fp_struct);
    ranging_dynamic_constants_t local_constants = *(parameters->ranging_constants);
    int8_t phase_record[4]                      =  {parameters->phase_1,
                                                    parameters->phase_2,
                                                    parameters->phase_3,
                                                    parameters->phase_4};

    if ((phase_record[0] + phase_record[1]) > (phase_record[2] + phase_record[3])) {
        if ((phase_record[0]) < 0 && (phase_record[1] < 0)) {
            window_energy[0] = -phase_record[0];
            window_energy[1] = -phase_record[1];
            window_energy[2] = -phase_record[2];
            window_energy[3] = -phase_record[3];
            base_value = 0.0;
        } else if ((phase_record[0]) >= 0 && (phase_record[1] >= 0)) {
            window_energy[0] = -phase_record[2];
            window_energy[1] = -phase_record[3];
            window_energy[2] =  phase_record[0];
            window_energy[3] =  phase_record[1];
            base_value = local_constants.half_q;
        } else {
            window_energy[0] = -phase_record[1];
            window_energy[1] = -phase_record[2];
            window_energy[2] = -phase_record[3];
            window_energy[3] =  phase_record[0];
            base_value = local_constants.quarter_q;
        }
    } else {
        if ((phase_record[2]) < 0 && (phase_record[3] < 0)) {
            window_energy[0] = -phase_record[0];
            window_energy[1] = -phase_record[1];
            window_energy[2] = -phase_record[2];
            window_energy[3] = -phase_record[3];
            base_value = 0.0;
        } else if ((phase_record[2]) >= 0 && (phase_record[3] >= 0)) {
            window_energy[0] =  phase_record[2];
            window_energy[1] =  phase_record[3];
            window_energy[2] = -phase_record[0];
            window_energy[3] = -phase_record[1];
            base_value = -local_constants.half_q;
        } else {
            window_energy[0] =  phase_record[3];
            window_energy[1] = -phase_record[0];
            window_energy[2] = -phase_record[1];
            window_energy[3] = -phase_record[2];
            base_value = -local_constants.quarter_q;
        }
    }

    /* Convert windows energy to Q representation */
    window_energy[0] = sr1000_fp_int_to_q_conv(&local_fp_struct, window_energy[0]);
    window_energy[1] = sr1000_fp_int_to_q_conv(&local_fp_struct, window_energy[1]);
    window_energy[2] = sr1000_fp_int_to_q_conv(&local_fp_struct, window_energy[2]);
    window_energy[3] = sr1000_fp_int_to_q_conv(&local_fp_struct, window_energy[3]);

    /* All window are now in Q rep */
    if (window_energy[1] > window_energy[2]) {
        /* Substract using Q rep*/
        sides_diff = sr1000_fp_sub(window_energy[2], window_energy[0]);
        if (abs(sides_diff) >= abs(window_energy[1])) {
            /* Adding first using Q rep*/
            q_num_t constant_q_signed = sr1000_fp_multiply(&local_fp_struct,
                                                         local_constants.eighth_q,
                                                         get_sign_fixed_point(&local_constants, sides_diff));

            phase_value = sr1000_fp_add(base_value, constant_q_signed);
        } else {
            q_num_t division_part = sr1000_fp_division(&local_fp_struct, sides_diff, window_energy[1]);
            q_num_t multiply_part = sr1000_fp_multiply(&local_fp_struct, local_constants.eighth_q, division_part);

            phase_value = sr1000_fp_add(base_value, multiply_part);
        }
    } else {
        sides_diff = sr1000_fp_sub(window_energy[1], window_energy[3]);
        if (abs(sides_diff) >= abs(window_energy[2])) {
            q_num_t multiply_part = sr1000_fp_multiply(&local_fp_struct,
                                                     local_constants.eighth_q,
                                                     get_sign_fixed_point(&local_constants, sides_diff));
            q_num_t base_value_sub = sr1000_fp_sub(base_value, multiply_part);

            phase_value = sr1000_fp_add(base_value_sub, local_constants.quarter_q);
        } else {
            q_num_t division_part = sr1000_fp_division(&local_fp_struct, sides_diff, window_energy[2]);
            q_num_t multiply_part = sr1000_fp_multiply(&local_fp_struct, local_constants.eighth_q, division_part);
            q_num_t sub_part = sr1000_fp_sub(base_value, multiply_part);

            phase_value = sr1000_fp_add(sub_part, local_constants.quarter_q);
        }
    }
    *phase = phase_value;
}

/** @brief Determine if value is positive or negative.
 *
 *  @param[in] ranging_constants  Current ranging fixed point dynamic constants structure.
 *  @param[in] value              Value to evaluate.
 *  @return 1 or -1 in fixed point representation.
 */
static inline q_num_t get_sign_fixed_point(ranging_dynamic_constants_t *ranging_constants, q_num_t value)
{
    q_num_t sign;

    if (value >= 0) {
        sign = ranging_constants->precision_q;
    } else {
        sign = -ranging_constants->precision_q;
    }
    return sign;
}
