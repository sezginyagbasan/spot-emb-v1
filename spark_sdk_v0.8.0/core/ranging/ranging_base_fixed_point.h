/** @file  ranging_base_fixed_point.h
 *  @brief Ranging module using fixed point instead of floating point unit.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

#ifndef RANGING_BASE_FIXED_POINT_H_
#define RANGING_BASE_FIXED_POINT_H_

/* INCLUDES *******************************************************************/
#include <stdint.h>
#include "fixed_point.h"
#include "ranging_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANT *******************************************************************/
#define CLOCK_CYCLE_COMPARE     0.5

/* TYPES **********************************************************************/
/** @brief Ranging dynamic constants.
 */
typedef struct {
    q_num_t  slope_q;               /**< Ranging slope in Q number representation */
    q_num_t  offset_q;              /**< Ranging offset in Q number representation */
    q_num_t  eighth_q;              /**< 0.125 in Q number representation */
    q_num_t  quarter_q;             /**< 0.25 in Q number representation */
    q_num_t  half_q;                /**< 0.5 in Q number representation */
    q_num_t  precision_q;           /**< User precision in Q number representation */
    q_num_t  clk_cyc_comp_q;        /**< Clock cycle compare in Q number representation */
} ranging_dynamic_constants_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize the fixed point ranging calculation
 *
 *  This is used to compute the floating point constants to
 *  Q number representation to maximize efficiency during
 *  real time ranging computation.
 *
 *  @param[in] ranging_constants  Dynamic ranging constants structure.
 *  @param[in] precision_bits     Number of precision bits.
 *  @param[in] offset             Ranging offset for distance computation.
 *  @return Instance of fixed point object.
 */
fp_format_t ranging_base_fixed_point_init(ranging_dynamic_constants_t *ranging_constants,
                                                 uint8_t precision_bits,
                                                 int32_t offset);

/** @brief Get propagation delays given an array.
 *
 *  @param[in]  ranging_fp_struct  Ranging fixed point instance.
 *  @param[in]  ranging_constants  Dynamic ranging constants structure.
 *  @param[in]  ranging_data       Ranging data array.
 *  @param[in]  size               Size of arrays.
 *  @param[out] raw_prop_delay     Propagation delay array.
 */
void ranging_base_get_propagation_delay_array_fixed_point(fp_format_t *ranging_fp_struct,
                                                          ranging_dynamic_constants_t *ranging_constants,
                                                          ranging_master_slave_info_t *ranging_data,
                                                          uint16_t size,
                                                          q_num_t *raw_prop_delay);

/** @brief Get an average removing value below a threshold.
 *
 *  @param[in] ranging_fp_struct  Ranging fixed point instance.
 *  @param[in] data               Data to average.
 *  @param[in] size               Size of the data to average.
 *  @return Average of data.
 */
q_num_t ranging_base_calculate_average_fixed_point(fp_format_t *ranging_fp_struct,
                                                   q_num_t *data,
                                                   uint16_t size);

/** @brief Get propagation delay for distance calculation using fixed point.
 *
 *  @param[in] ranging_fp_struct  Ranging fixed point instance.
 *  @param[in] ranging_constants  Dynamic ranging constants structure.
 *  @param[in] prop_delay         Propagation delay value.
 *  @return Distance in meters.
 */
q_num_t ranging_base_convert_to_distance_fixed_point(fp_format_t *ranging_fp_struct,
                                                     ranging_dynamic_constants_t *ranging_const,
                                                     q_num_t prop_delay);

#ifdef __cplusplus
}
#endif


#endif /* RANGING_BASE_FIXED_POINT_H_*/
