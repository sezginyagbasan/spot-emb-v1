/** @file  ranging_base.c
 *  @brief Ranging module.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "ranging_base.h"

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static float ranging_base_get_propagation_delay(ranging_master_slave_info_t *ranging_phase);
static void calculate_propagation_delay(int8_t phase_1, int8_t phase_2, int8_t phase_3, int8_t phase_4, float *phase);
static inline int8_t get_sign(int8_t value);

/* PUBLIC FUNCTIONS ***********************************************************/
void ranging_base_get_propagation_delay_array(ranging_master_slave_info_t *ranging_data, uint16_t size, float *raw_prop_delay)
{
    for (uint16_t i = 0; i < size; i++) {
        raw_prop_delay[i] = ranging_base_get_propagation_delay(ranging_data + i);
    }
}

float ranging_base_calculate_average(float *data, uint16_t size)
{
    float sum        = 0.0;
    float average    = 0.0;

    for (uint16_t i = 0; i < size; i++) {
        sum += data[i];
    }
    average = sum / ((float)(size));

    return average;
}

float ranging_base_convert_to_distance(float prop_delay, float offset)
{
    return ((prop_delay * RANGING_SLOPE) + offset);
}

/* PRIVATE FUNCTIONS ****************************************************************/
/** @brief Get propagation delay for distance calculation.
 *
 *  @param[in] ranging_info  Ranging information.
 *  @return Propagation delay value.
 */
static float ranging_base_get_propagation_delay(ranging_master_slave_info_t *ranging_phase)
{
    float master_propagation;
    float slave_propagation;
    float sum;
    uint16_t rx_wait_time;

    calculate_propagation_delay(ranging_phase->master_info.phase1, ranging_phase->master_info.phase2,
                                ranging_phase->master_info.phase3, ranging_phase->master_info.phase4,
                                &master_propagation);

    calculate_propagation_delay(ranging_phase->slave_info.phase1, ranging_phase->slave_info.phase2,
                                ranging_phase->slave_info.phase3, ranging_phase->slave_info.phase4,
                                &slave_propagation);

    rx_wait_time = ranging_phase->master_info.rx_waited0 | (ranging_phase->master_info.rx_waited1 << 8);

    sum = master_propagation + slave_propagation + rx_wait_time;

    return sum;
}

/** @brief Calculate the fine portion of the propagation delay to have high resolution measurement.
 *
 *  @param[in]  phase_1  Window of energy 1 from sr10x0.
 *  @param[in]  phase_2  Window of energy 2 from sr10x0.
 *  @param[in]  phase_3  Window of energy 3 from sr10x0.
 *  @param[in]  phase_4  Window of energy 4 from sr10x0.
 *  @param[out] phase    Precise portion of propagation delay.
 */
static void calculate_propagation_delay(int8_t phase_1,
                                        int8_t phase_2,
                                        int8_t phase_3,
                                        int8_t phase_4,
                                        float *phase)
{
    int16_t phase_record[4]   = {phase_1, phase_2, phase_3, phase_4};
    int16_t window_energy[4];
    int16_t sides_diff;
    float   base_value;
    float   phase_value;

    if ((phase_record[0] + phase_record[1]) > (phase_record[2] + phase_record[3])) {
        if ((phase_record[0]) < 0 && (phase_record[1] < 0)) {
            window_energy[0] = -phase_record[0];
            window_energy[1] = -phase_record[1];
            window_energy[2] = -phase_record[2];
            window_energy[3] = -phase_record[3];
            base_value = 0.0;
        } else if ((phase_record[0]) >= 0 && (phase_record[1] >= 0)) {
            window_energy[0] = -phase_record[2];
            window_energy[1] = -phase_record[3];
            window_energy[2] =  phase_record[0];
            window_energy[3] =  phase_record[1];
            base_value = 0.5;
        } else {
            window_energy[0] = -phase_record[1];
            window_energy[1] = -phase_record[2];
            window_energy[2] = -phase_record[3];
            window_energy[3] =  phase_record[0];
            base_value = 0.25;
        }
    } else {
        if ((phase_record[2]) < 0 && (phase_record[3] < 0)) {
            window_energy[0] = -phase_record[0];
            window_energy[1] = -phase_record[1];
            window_energy[2] = -phase_record[2];
            window_energy[3] = -phase_record[3];
            base_value = 0.0;
        } else if ((phase_record[2]) >= 0 && (phase_record[3] >= 0)) {
            window_energy[0] =  phase_record[2];
            window_energy[1] =  phase_record[3];
            window_energy[2] = -phase_record[0];
            window_energy[3] = -phase_record[1];
            base_value = -0.5;
        } else {
            window_energy[0] =  phase_record[3];
            window_energy[1] = -phase_record[0];
            window_energy[2] = -phase_record[1];
            window_energy[3] = -phase_record[2];
            base_value = -0.25;
        }
    }
    if (window_energy[1] > window_energy[2]) {
        sides_diff = window_energy[2] - window_energy[0];
        if (abs(sides_diff) >= abs(window_energy[1])) {
            phase_value = (float)base_value + 0.125 * get_sign(sides_diff);
        } else {
            phase_value = (float)base_value + 0.125 * (float)sides_diff / (float)window_energy[1];
        }
    } else {
        sides_diff = window_energy[1] - window_energy[3];
        if (abs(sides_diff) >= abs(window_energy[2])) {
            phase_value = (float)base_value - 0.125 * get_sign(sides_diff) + 0.25;
        } else {
            phase_value = (float)base_value - 0.125 * (float)sides_diff / (float)window_energy[2] + 0.25;
        }
    }

    *phase = phase_value;
}

/** @brief Determine if value is positive or negative.
 *
 *  @param[in] value  Value to evaluate.
 *  @return 1 or -1 depending on the value.
 */
static inline int8_t get_sign(int8_t value)
{
    int8_t sign;

    if (value >= 0) {
        sign = 1;
    } else {
        sign = -1;
    }
    return sign;
}
