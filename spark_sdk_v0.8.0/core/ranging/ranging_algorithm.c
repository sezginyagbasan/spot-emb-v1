/** @file ranging_alorithm.c
 *  @brief Enhance ranging performance.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "ranging_algorithm.h"

/* CONSTANTS ******************************************************************/
#define CLOCK_CYCLE_COMPARE                   0.5
#define POWERUP_DELAY_REGISTER_GRANULARITY_US 0.1953125
#define XTAL_CLK_PERIOD_US                    30.517578125
#define POWERUP_DELAY_CYCLE                   0x9C
#define POWERUP_DELAY_SEQUENCE_LENGTH         8
#define NUMBER_OF_SAMPLES_THRESHOLD           8

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void ranging_algorithm_get_distance_raw(algorithm_param_t *ranging_raw_parameters,
                                               void *distance_float);
static void ranging_algorithm_get_distance_data_corrected(algorithm_param_t *ranging_corrected_parameters,
                                                          void *distance_float);
static void ranging_algorithm_get_distance_raw_fixed_point(algorithm_param_t *ranging_raw_fp_parameters,
                                                           void *distance_fixed_point);
static void ranging_algorithm_get_distance_data_corrected_fixed_point(algorithm_param_t *ranging_corrected_fp_parameters,
                                                                      void *distance_fixed_point);
static bool ranging_algorithm_use_fixed_point(ranging_algo_sel_t);

/* PUBLIC FUNCTIONS ***********************************************************/
void ranging_algorithm_init(ranging_instance_t *self, ranging_algo_sel_t algo_selection, uint16_t max_size,
                            int32_t offset, uint8_t fixed_point_precision)
{
    self->ranging_algorithm_sel                           = algo_selection;
    self->algorithm_parameter.avg_size                    = max_size;
    self->algorithm_parameter.current_avg_size            = 0;
    self->algorithm_parameter.offset                      = offset;
    self->algorithm_parameter.pwrup_delay_step            = 0;
    self->algorithm_parameter.pwrup_delay_sequence_length = POWERUP_DELAY_SEQUENCE_LENGTH;

    switch (algo_selection) {
    case RANGING_DATA_CORRECTED:
        self->calculate_ranging = ranging_algorithm_get_distance_data_corrected;
        break;
    case RANGING_RAW:
        self->calculate_ranging = ranging_algorithm_get_distance_raw;
        break;
    case RANGING_DATA_FIXED_POINT_CORRECTED:{
        fp_format_t tmp_fp_struct;

        tmp_fp_struct = ranging_base_fixed_point_init(&(self->algorithm_parameter.fixed_point_inst.fp_constants),
                                                      fixed_point_precision,
                                                      offset);

        self->algorithm_parameter.fixed_point_inst.fp_parameters = tmp_fp_struct;

        self->calculate_ranging = ranging_algorithm_get_distance_data_corrected_fixed_point;
        break;
    } case RANGING_DATA_FIXED_POINT_RAW:{
        fp_format_t tmp_fp_struct;

        tmp_fp_struct = ranging_base_fixed_point_init(&(self->algorithm_parameter.fixed_point_inst.fp_constants),
                                                      fixed_point_precision,
                                                      offset);

        self->algorithm_parameter.fixed_point_inst.fp_parameters = tmp_fp_struct;

        self->calculate_ranging = ranging_algorithm_get_distance_raw_fixed_point;
        break;
    } default:
        break;
    }
#if ((defined(RANGING_ALGORITHM_DEBUG)) && (RANGING_ALGORITHM_DEBUG == 1U))
    ranging_algorithm_prv_init(&self->algorithm_parameter.ranging_prv);
#endif

    if (max_size < NUMBER_OF_SAMPLES_THRESHOLD) {
        self->algorithm_parameter.pwrup_delay_sequence_length = max_size;
    }
    /* Get the delay increase step */
    self->algorithm_parameter.pwrup_delay_step = (XTAL_CLK_PERIOD_US /
                                                        self->algorithm_parameter.pwrup_delay_sequence_length) /
                                                        POWERUP_DELAY_REGISTER_GRANULARITY_US;
}

bool ranging_algorithm_append(ranging_instance_t *self, ranging_info_t *master_info, ranging_info_t *slave_info)
{
    bool ready = false;
    ranging_master_slave_info_t new_data = {{slave_info->phase1,
                                            slave_info->phase2,
                                            slave_info->phase3,
                                            slave_info->phase4,
                                            slave_info->rx_waited1,
                                            slave_info->rx_waited0},
                                            {master_info->phase1,
                                            master_info->phase2,
                                            master_info->phase3,
                                            master_info->phase4,
                                            master_info->rx_waited1,
                                            master_info->rx_waited0} };
    /* Flawfinder: Ignore */
    memcpy(&self->algorithm_parameter.ranging_data[(self->algorithm_parameter.current_avg_size++) % self->algorithm_parameter.avg_size],
           &new_data,
           sizeof(new_data));

    if (self->algorithm_parameter.avg_size <= self->algorithm_parameter.current_avg_size) {
        ready = true;
    }
    return ready;
}

float ranging_algorithm_calculate_float(ranging_instance_t *self)
{
     ranging_distance_t distance = {0};

    self->calculate_ranging(&self->algorithm_parameter, &distance.floating_point);

    if (ranging_algorithm_use_fixed_point(self->ranging_algorithm_sel)) {
        distance.floating_point = sr1000_fp_q_to_float_conv(&(self->algorithm_parameter.fixed_point_inst.fp_parameters),
                                                            distance.q_rep);
    }

    if (distance.floating_point < 0) {
        distance.floating_point = 0;
    }

    return distance.floating_point;
}

q_num_t ranging_algorithm_calculate_qnum(ranging_instance_t *self)
{
    ranging_distance_t distance = {0};

    self->calculate_ranging(&self->algorithm_parameter, &distance.q_rep);

    if (ranging_algorithm_use_fixed_point(self->ranging_algorithm_sel)) {
        if (distance.q_rep <= 0) {
            distance.q_rep = 0;
        }
        return distance.q_rep;
    } else {
        /* Fixed point isn't initialized, can't convert to Q number */
        return (q_num_t)-1;
    }
}

bool ranging_algorithm_is_buffer_full(ranging_instance_t *self)
{
    bool ready = false;

    if (self->algorithm_parameter.avg_size <= self->algorithm_parameter.current_avg_size) {
        ready = true;
    }

    return ready;
}

void ranging_algorithm_set_avg_size(ranging_instance_t *self, uint16_t avg_size)
{
    if (avg_size < RANGING_MAX_SIZE) {
        self->algorithm_parameter.avg_size = avg_size;
    } else {
        self->algorithm_parameter.avg_size = RANGING_MAX_SIZE - 1;
    }
}

void ranging_algorithm_clear(ranging_instance_t *self)
{
    self->algorithm_parameter.current_avg_size = 0;
#if ((defined(RANGING_ALGORITHM_DEBUG)) && (RANGING_ALGORITHM_DEBUG == 1U))
    ranging_algorithm_prv_init(&self->algorithm_parameter.ranging_prv);
#endif
}

uint8_t ranging_algorithm_update_powerup_delay(ranging_instance_t *self, uint8_t *packet_count)
{
    uint8_t delay = 0;
    float even_pwrup_delay = 0;
    float odd_pwrup_delay = POWERUP_DELAY_CYCLE / 2;

    /* Check if the power-up delay cycle is completed
     *  and start a new cycle if it is the case
     */
    if (*packet_count == self->algorithm_parameter.pwrup_delay_sequence_length) {
        *packet_count = 0;
    }

    if (!(*packet_count % 2)) {
        delay = round(even_pwrup_delay + (*packet_count / 2) * self->algorithm_parameter.pwrup_delay_step);
    } else {
        delay = round(odd_pwrup_delay + (*packet_count / 2) * self->algorithm_parameter.pwrup_delay_step);
    }
    return delay;
}

/* PRIVATE FUNCTIONS ****************************************************************/
/** @brief Calculate distance base on raw data.
 *
 *  @param[in]  ranging_raw_parameters  Ranging function pointer parameters structure.
 *  @param[out] distance_float          Calculated distance, in meters, using floating point.
 */
static void ranging_algorithm_get_distance_raw(algorithm_param_t *ranging_raw_parameters, void *distance_float)
{
    float prop_del_raw[RANGING_MAX_SIZE] = {0};
    float prop_del_avg                   =  0;
    float f_offset                       =  (float)ranging_raw_parameters->offset/(float)RANGING_SCALING;
    uint16_t local_avg_size              =  0;

    *((float *)distance_float) = 0;
    /* Since the user can add x data (less or more than the buffer)
     * we need to only take the relevent data. The size may vary
     * from 1 to current_avg_size.
     */
    local_avg_size = ranging_raw_parameters->current_avg_size % (ranging_raw_parameters->avg_size + 1);
    ranging_raw_parameters->current_avg_size = 0;

    ranging_base_get_propagation_delay_array(ranging_raw_parameters->ranging_data, local_avg_size, prop_del_raw);
    prop_del_avg = ranging_base_calculate_average(prop_del_raw, local_avg_size);

    if (prop_del_avg > 0) {
        *((float *)distance_float) = ranging_base_convert_to_distance(prop_del_avg, f_offset);
    }
}

/** @brief Calculate distance and correct the data.
 *
 *  @param[in]  ranging_corrected_parameters  Ranging function pointer parameters structure.
 *  @param[out] distance_float                Calculated distance, in meters, using floating point.
 */
static void ranging_algorithm_get_distance_data_corrected(algorithm_param_t *ranging_corrected_parameters, void *distance_float)
{
    float prop_del_raw[RANGING_MAX_SIZE] = {0};
    float prop_delay_avg_with_error      =  0;
    float prop_delay_avg                 =  0;
    float f_offset                       =  (float)ranging_corrected_parameters->offset/(float)RANGING_SCALING;
    uint16_t local_avg_size              =  0;

    *((float *)distance_float) = 0;
    /* Since the user can add x data (less or more than the buffer)
     * we need to only take the relevent data. The size may vary
     * from 1 to current_avg_size.
     */
    if (ranging_corrected_parameters->current_avg_size > ranging_corrected_parameters->avg_size) {
        local_avg_size = ranging_corrected_parameters->avg_size;
    } else {
        local_avg_size = ranging_corrected_parameters->current_avg_size;
    }

    ranging_corrected_parameters->current_avg_size = 0;

    ranging_base_get_propagation_delay_array(ranging_corrected_parameters->ranging_data, local_avg_size, prop_del_raw);

#if ((defined(RANGING_ALGORITHM_DEBUG)) && (RANGING_ALGORITHM_DEBUG == 1U))
    if (!ranging_algorithm_prv_is_buf_full(&ranging_corrected_parameters->ranging_prv)) {
        ranging_algorithm_prv_set_raw_data(&ranging_corrected_parameters->ranging_prv,
                                        ranging_corrected_parameters->ranging_data, local_avg_size);
        ranging_algorithm_prv_set_raw_del_prop(&ranging_corrected_parameters->ranging_prv, prop_del_raw, local_avg_size);
    }
#endif

    prop_delay_avg_with_error = ranging_base_calculate_average(prop_del_raw, local_avg_size);
    /* Applying correction to outliers. */
    for (uint16_t i = 0; i < local_avg_size; i++) {
        /* Looking for outlier and error in data */
        if (prop_del_raw[i] < (prop_delay_avg_with_error - CLOCK_CYCLE_COMPARE)) {
            /* If the data is too small and is not -1 */
            prop_del_raw[i] += 1;
        } else if (prop_del_raw[i] > (prop_delay_avg_with_error + CLOCK_CYCLE_COMPARE)) {
            /* If the data is too big */
            prop_del_raw[i] -= 1;
        }
    }
    prop_delay_avg = ranging_base_calculate_average(prop_del_raw, local_avg_size);
    *((float *)distance_float) = ranging_base_convert_to_distance(prop_delay_avg, f_offset);

#if ((defined(RANGING_ALGORITHM_DEBUG)) && (RANGING_ALGORITHM_DEBUG == 1U))
    if (!ranging_algorithm_prv_is_buf_full(&ranging_corrected_parameters->ranging_prv)) {

        ranging_algorithm_prv_set_prop_del_corrected(&ranging_corrected_parameters->ranging_prv,
                                                     prop_del_raw, local_avg_size);
        ranging_algorithm_prv_increment_idx(&ranging_corrected_parameters->ranging_prv,
                                            ranging_corrected_parameters->avg_size);
        ranging_algorithm_prv_set_last_distance_data(&ranging_corrected_parameters->ranging_prv,
                                                     *((float *)distance_float));
        ranging_algorithm_prv_set_prop_del_avg(&ranging_corrected_parameters->ranging_prv, prop_delay_avg);
    }
#endif

}

/** @brief Calculate distance base on raw data using fixed point.
 *
 *  @param[in]  ranging_raw_fp_parameters  Ranging function pointer parameters structure.
 *  @param[out] distance_fixed_point       Calculated distance, in meters, using fixed point.
 */
static void ranging_algorithm_get_distance_raw_fixed_point(algorithm_param_t *ranging_raw_fp_parameters, void *distance_fixed_point)
{
    q_num_t prop_del_raw[RANGING_MAX_SIZE]              = {0};
    q_num_t prop_del_avg                                =  0;
    uint16_t local_avg_size                             =  0;
    fp_format_t local_fp_parameters              =  ranging_raw_fp_parameters->fixed_point_inst.fp_parameters;
    ranging_dynamic_constants_t local_dynamic_constants =  ranging_raw_fp_parameters->fixed_point_inst.fp_constants;

    *((q_num_t *)distance_fixed_point) = 0;
    /* Since the user can add x data (less or more than the buffer)
     * we need to only take the relevent data. The size may vary
     * from 1 to current_avg_size.
     */
    local_avg_size = (ranging_raw_fp_parameters->current_avg_size) % (ranging_raw_fp_parameters->avg_size + 1);
    ranging_raw_fp_parameters->current_avg_size = 0;

    ranging_base_get_propagation_delay_array_fixed_point(&local_fp_parameters,
                                                         &local_dynamic_constants,
                                                         ranging_raw_fp_parameters->ranging_data,
                                                         local_avg_size,
                                                         prop_del_raw);

    prop_del_avg = ranging_base_calculate_average_fixed_point(&local_fp_parameters,
                                                              prop_del_raw,
                                                              local_avg_size);

    if (prop_del_avg > 0) {
        *((q_num_t *)distance_fixed_point) = ranging_base_convert_to_distance_fixed_point(&local_fp_parameters,
                                                                                          &local_dynamic_constants,
                                                                                          prop_del_avg);
    }
}

/** @brief Calculate distance and correct the data using only fixed point.
 *
 *  @param[in]  ranging_corrected_fp_parameters  Ranging function pointer parameters structure.
 *  @param[out] distance_fixed_point             Calculated distance, in meters, using fixed point.
 */
static void ranging_algorithm_get_distance_data_corrected_fixed_point(algorithm_param_t *ranging_corrected_fp_parameters,
                                                                      void *distance_fixed_point)
{
    q_num_t prop_del_raw[RANGING_MAX_SIZE]              = {0};
    q_num_t prop_delay_avg_with_error                   =  0;
    q_num_t prop_delay_avg                              =  0;
    uint16_t local_avg_size                             =  0;
    fp_format_t local_fp_parameters              = ranging_corrected_fp_parameters->fixed_point_inst.fp_parameters;
    ranging_dynamic_constants_t local_dynamic_constants = ranging_corrected_fp_parameters->fixed_point_inst.fp_constants;

    *((q_num_t *)distance_fixed_point) = 0;
    /* Since the user can add x data (less or more than the buffer)
     * we need to only take the relevent data. The size may vary
     * from 1 to current_avg_size.
     */
    if (ranging_corrected_fp_parameters->current_avg_size > ranging_corrected_fp_parameters->avg_size) {
        local_avg_size = ranging_corrected_fp_parameters->avg_size;
    } else {
        local_avg_size = ranging_corrected_fp_parameters->current_avg_size;
    }

    ranging_corrected_fp_parameters->current_avg_size = 0;

    ranging_base_get_propagation_delay_array_fixed_point(&local_fp_parameters,
                                                         &local_dynamic_constants,
                                                         ranging_corrected_fp_parameters->ranging_data,
                                                         local_avg_size,
                                                         prop_del_raw);
    prop_delay_avg_with_error = ranging_base_calculate_average_fixed_point(&local_fp_parameters,
                                                                           prop_del_raw,
                                                                           local_avg_size);

    /* Applying correction to outliers. */
    for (uint16_t i = 0; i < local_avg_size; i++) {
        /* Looking for outlier and error in data */
        if ((prop_del_raw[i] < (sr1000_fp_sub(prop_delay_avg_with_error, local_dynamic_constants.clk_cyc_comp_q)))
            && prop_del_raw[i] != -1) {
            /* If the data is too small and is not -1 */
            prop_del_raw[i] = sr1000_fp_add(prop_del_raw[i], sr1000_fp_int_to_q_conv(&local_fp_parameters, 1));
        } else if (prop_del_raw[i] > (sr1000_fp_add(prop_delay_avg_with_error, local_dynamic_constants.clk_cyc_comp_q))) {
            /* If the data is too big */
            prop_del_raw[i] = sr1000_fp_sub(prop_del_raw[i], sr1000_fp_int_to_q_conv(&local_fp_parameters, 1));
        }
    }
    prop_delay_avg = ranging_base_calculate_average_fixed_point(&local_fp_parameters,
                                                                prop_del_raw,
                                                                local_avg_size);
    *((q_num_t *)distance_fixed_point) = ranging_base_convert_to_distance_fixed_point(&local_fp_parameters,
                                                                                      &local_dynamic_constants,
                                                                                      prop_delay_avg);
}

/** @brief Check if the algorithm is using fixed point.
 *
 *  @param[in] current_ranging_algorithm  Current chosen algorithm.
 *  @retval True   Current algorithm is using fixed point arithmetic.
 *  @retval False  Current algorithm is not using fixed point arithmetic.
 */
static bool ranging_algorithm_use_fixed_point(ranging_algo_sel_t current_ranging_algorithm)
{
    if (current_ranging_algorithm == RANGING_DATA_FIXED_POINT_CORRECTED ||
        current_ranging_algorithm == RANGING_DATA_FIXED_POINT_RAW) {
        return 1;
    }
    return 0;
}
