/** @file ranging_algorithm_private.c
 *  @brief Allows to fetch data from ranging algorithm for analysis.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <string.h>
#include "ranging_algorithm_private.h"

/* CONSTANT *******************************************************************/
#define DEV_CHAR_PER_LINE   100
#define FILLER_CHAR         ' '
#define SEPARATOR_CHAR      ","
#define SEPARATOR_CHAR_SIZE 1
#define PHASE_NBR_DIGIT     4
#define RX_WAIT_NBR_DIGIT   6
#define FLOAT_NBR_DIGIT     4
#define FLOAT_DEC_DIGIT     4

static char CSV_COLUMN_1[] = {"MasterP1,MasterP2,MasterP3,MasterP4,MasterRxWaited,SlaveP1,SlaveP2,SlaveP3,SlaveP4,"};
static char CSV_COLUMN_2[] = {"SlaveRxWaited,Prop_del_raw,prop_del_corrected,prop_del_average,last_distance\n"};

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void ranging_algorithm_prv_reset_idx(ranging_prv_t *ranging_prv);
static void ranging_algorithm_prv_format_data(ranging_prv_t *ranging_prv, uint16_t idx, char *tab);
static uint16_t get_full_rx_wait(ranging_info_t ranging_info);

/* FUNCTIONS ******************************************************************/
void ranging_algorithm_prv_init(ranging_prv_t *ranging_prv)
{
    /* Flawfinder: Ignore */
    memset(&ranging_prv->ranging_prv_data.ranging_master_slave, 0,
    sizeof(ranging_prv->ranging_prv_data.ranging_master_slave));
    /* Flawfinder: Ignore */
    memset(&ranging_prv->ranging_prv_data.prop_del_raw, 0, sizeof(ranging_prv->ranging_prv_data.prop_del_raw));
    /* Flawfinder: Ignore */
    memset(&ranging_prv->ranging_prv_data.prop_del_corrected, 0,
    sizeof(ranging_prv->ranging_prv_data.prop_del_corrected));
    /* Flawfinder: Ignore */
    memset(&ranging_prv->ranging_prv_data.prop_del_average, 0,
    sizeof(ranging_prv->ranging_prv_data.prop_del_average));
    /* Flawfinder: Ignore */
    memset(&ranging_prv->ranging_prv_data.last_distance, 0,
    sizeof(ranging_prv->ranging_prv_data.last_distance));

    ranging_algorithm_prv_reset_idx(ranging_prv);
}

void ranging_algorithm_prv_set_utils(ranging_prv_t *ranging_prv, dev_print_func print_ptr, str_cat_func cat,
                                     format_float_func format_float)
{
    ranging_prv->print = print_ptr;
    ranging_prv->cat = cat;
    ranging_prv->format_float = format_float;
}

void ranging_algorithm_prv_set_raw_data(ranging_prv_t *ranging_prv, ranging_master_slave_info_t *r_info, uint16_t size)
{
    /* Flawfinder: Ignore */
     memcpy(&ranging_prv->ranging_prv_data.ranging_master_slave[ranging_prv->current_idx],
            r_info, sizeof(ranging_master_slave_info_t) * size);
}

void ranging_algorithm_prv_set_raw_del_prop(ranging_prv_t *ranging_prv, float *raw_prop, uint16_t size)
{
    /* Flawfinder: Ignore */
     memcpy(&ranging_prv->ranging_prv_data.prop_del_raw[ranging_prv->current_idx], raw_prop, sizeof(float) * size);
}

void ranging_algorithm_prv_set_prop_del_corrected(ranging_prv_t *ranging_prv, float *prop_corrected,  uint16_t size)
{
    /* Flawfinder: Ignore */
    memcpy(&ranging_prv->ranging_prv_data.prop_del_corrected[ranging_prv->current_idx],
           prop_corrected, sizeof(float) * size);
}

void ranging_algorithm_prv_set_last_distance_data(ranging_prv_t *ranging_prv, float dist)
{
    ranging_prv->ranging_prv_data.last_distance[ranging_prv->current_idx - 1] = dist;
}


void ranging_algorithm_prv_set_prop_del_avg(ranging_prv_t *ranging_prv, float del_prop_avg)
{
    ranging_prv->ranging_prv_data.prop_del_average[ranging_prv->current_idx - 1] = del_prop_avg;
}

void ranging_algorithm_prv_print_column_label(ranging_prv_t *ranging_prv)
{
    char table[sizeof(CSV_COLUMN_1) + sizeof(CSV_COLUMN_2)];

    if (ranging_prv->print != NULL) {

        /* Flawfinder: Ignore */
        strncpy(table, CSV_COLUMN_1, sizeof(table));
        /* Flawfinder: Ignore */
        strncat(table, CSV_COLUMN_2, sizeof(table) - strlen(table) - 1);
        ranging_prv->print(table, sizeof(CSV_COLUMN_1) + sizeof(CSV_COLUMN_2));
    }
}

void ranging_algorithm_prv_print(ranging_prv_t *ranging_prv)
{
    char tmp_tab[DEV_CHAR_PER_LINE] = {0};

    if (ranging_prv->print != NULL) {
        for  (uint16_t i = 0; i < ranging_prv->current_idx; i++) {
            memset(tmp_tab, 0, sizeof(tmp_tab));
            ranging_algorithm_prv_format_data(ranging_prv, i, tmp_tab);
            ranging_prv->print(tmp_tab, sizeof(tmp_tab));
            ranging_prv->print((char *)"\n", 1);
        }
    }
    ranging_algorithm_prv_reset_idx(ranging_prv);
}

void ranging_algorithm_prv_increment_idx(ranging_prv_t *ranging_prv, uint16_t nb_sample)
{
    if ((RANGING_DEV_MAX_SIZE - ranging_prv->current_idx) >= nb_sample) {
        ranging_prv->current_idx += nb_sample;
    }
}

bool ranging_algorithm_prv_is_buf_full(ranging_prv_t *ranging_prv)
{
    bool ret = false;

    if (ranging_prv->current_idx >= RANGING_DEV_MAX_SIZE) {
        ret = true;
    }

    return ret;
}

bool ranging_algorithm_prv_is_buf_empty(ranging_prv_t *ranging_prv)
{
    bool ret = true;

    if (ranging_prv->current_idx > 0) {
        ret = false;
    }

    return ret;
}

/* PRIVATE FUNCTIONS ****************************************************************/
/** @brief Reset the ranging private index.
 *
 *  @param[in] ranging_prv  Ranging private instance
 */
static void ranging_algorithm_prv_reset_idx(ranging_prv_t *ranging_prv)
{
    ranging_prv->current_idx = 0;
}

/** @brief Format ranging private data to be print.
 *
 *  @param[in]  ranging_prv_data  Ranging private data instance
 *  @param[in]  idx               Index of the data to be formated
 *  @param[out] tab               Array to string print to.
 */
static void ranging_algorithm_prv_format_data(ranging_prv_t *ranging_prv, uint16_t idx, char *tab)
{
    uint8_t sum_idx = 0;
    ranging_prv_data_t *ranging_prv_data = &ranging_prv->ranging_prv_data;
    str_cat_func cat = ranging_prv->cat;
    format_float_func format_float = ranging_prv->format_float;

    format_float(ranging_prv_data->ranging_master_slave[idx].master_info.phase1,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(ranging_prv_data->ranging_master_slave[idx].master_info.phase2,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(ranging_prv_data->ranging_master_slave[idx].master_info.phase3,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(ranging_prv_data->ranging_master_slave[idx].master_info.phase4,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(get_full_rx_wait(ranging_prv_data->ranging_master_slave[idx].master_info),
                  RX_WAIT_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += RX_WAIT_NBR_DIGIT + SEPARATOR_CHAR_SIZE;

    format_float(ranging_prv_data->ranging_master_slave[idx].slave_info.phase1,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(ranging_prv_data->ranging_master_slave[idx].slave_info.phase2,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(ranging_prv_data->ranging_master_slave[idx].slave_info.phase3,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(ranging_prv_data->ranging_master_slave[idx].slave_info.phase4,
                  PHASE_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += PHASE_NBR_DIGIT + SEPARATOR_CHAR_SIZE;
    format_float(get_full_rx_wait(ranging_prv_data->ranging_master_slave[idx].slave_info),
                  RX_WAIT_NBR_DIGIT, 0, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += RX_WAIT_NBR_DIGIT + SEPARATOR_CHAR_SIZE;

    format_float(ranging_prv_data->prop_del_raw[idx], FLOAT_NBR_DIGIT,
                  FLOAT_DEC_DIGIT, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += FLOAT_NBR_DIGIT + FLOAT_DEC_DIGIT + SEPARATOR_CHAR_SIZE;

    format_float(ranging_prv_data->prop_del_corrected[idx], FLOAT_NBR_DIGIT,
                  FLOAT_DEC_DIGIT, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += FLOAT_NBR_DIGIT + FLOAT_DEC_DIGIT + SEPARATOR_CHAR_SIZE;

    format_float(ranging_prv_data->prop_del_average[idx], FLOAT_NBR_DIGIT,
                  FLOAT_DEC_DIGIT, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += FLOAT_NBR_DIGIT + FLOAT_DEC_DIGIT + SEPARATOR_CHAR_SIZE;

    format_float(ranging_prv_data->last_distance[idx], FLOAT_NBR_DIGIT,
                  FLOAT_DEC_DIGIT, FILLER_CHAR, 0, tab + sum_idx);
    cat(tab, SEPARATOR_CHAR);
    sum_idx += FLOAT_NBR_DIGIT + FLOAT_DEC_DIGIT + SEPARATOR_CHAR_SIZE;



    (void)sum_idx;
}

/** @brief Get a 16 bit rxwait value.
 *
 *  @param[in] ranging_info_t  Ranging information
 *  @retval concataned value of rxwaited registers
 */
static uint16_t get_full_rx_wait(ranging_info_t ranging_info)
{
    return ranging_info.rx_waited0 | (ranging_info.rx_waited1 << 8);
}
