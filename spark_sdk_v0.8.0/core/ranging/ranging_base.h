/** @file  ranging_base.h
 *  @brief Ranging module.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

#ifndef RANGING_BASE_H_
#define RANGING_BASE_H_

/* INCLUDES *******************************************************************/
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#define RANGING_SLOPE   7.319151806640625f
#define RANGING_SCALING 1000

/* TYPES **********************************************************************/
/** @brief Ranging informations.
 */
typedef struct {
    int8_t  phase1;     /**< Phase information #1 */
    int8_t  phase2;     /**< Phase information #2 */
    int8_t  phase3;     /**< Phase information #3 */
    int8_t  phase4;     /**< Phase information #4 */
    uint8_t rx_waited1; /**< Receiver time waited (MSB) */
    uint8_t rx_waited0; /**< Receiver time waited (LSB) */
} ranging_info_t;

/** @brief Ranging information sent over the air.
 */
typedef struct {
    ranging_info_t ranging_info; /**< Ranging informations structure */
    uint8_t        count;        /**< Number of ranging informations exchange between slave and master */
} ranging_frame_t;

/** @brief Internal master and slave ranging info structure.
 */
typedef struct {
    ranging_info_t slave_info;
    ranging_info_t master_info;
} ranging_master_slave_info_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Get propagation delays given an array.
 *
 *  @param[in]  ranging_data    Ranging data array.
 *  @param[in]  size            Size of arrays.
 *  @param[out] raw_prop_delay  Propagation delay array.
 */
void ranging_base_get_propagation_delay_array(ranging_master_slave_info_t *ranging_data, uint16_t size, float *raw_prop_delay);

/** @brief Get an average removing value below a threshold.
 *
 *  @param[in] data  Data to average.
 *  @param[in] size  Size of the data to average.
 *  @return Average of data.
 */
float ranging_base_calculate_average(float *data, uint16_t size);

/** @brief Get propagation delay for distance calculation.
 *
 *  @param[in] prop_delay  Propagation delay value.
 *  @param[in] offset      Offset value.
 *  @return Distance in meters.
 */
float ranging_base_convert_to_distance(float prop_delay, float offset);

#ifdef __cplusplus
}
#endif


#endif /* RANGING_BASE_H_*/
