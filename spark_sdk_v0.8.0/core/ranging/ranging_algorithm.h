/** @file ranging_algorithm.h
 *  @brief Enhance ranging performance.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef RANGING_ALGORITHM_H_
#define RANGING_ALGORITHM_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "ranging_base.h"
#include "ranging_base_fixed_point.h"
#include "fixed_point.h"
#include "ranging_cfg.h"
#if ((defined(RANGING_ALGORITHM_DEBUG)) && (RANGING_ALGORITHM_DEBUG == 1U))
#include "ranging_algorithm_private.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#ifndef RANGING_MAX_SIZE
#define RANGING_MAX_SIZE 256
#endif

/* TYPES **********************************************************************/
/** @brief Ranging distance union.
 */
typedef union {
    float floating_point; /**< Distance, in floating point. */
    q_num_t q_rep;        /**< Distance, in Q representation. */
} ranging_distance_t;

/** @brief Ranging algorithm fixed point elements structure.
 */
typedef struct {
    fp_format_t          fp_parameters; /**< Fixed point precision and integer bits value */
    ranging_dynamic_constants_t fp_constants;  /**< Dynamic constants used for fixed point calculation */
} ranging_fp_struct_t;

/** @brief Function pointer structure parameter.
 */
typedef struct {
    ranging_master_slave_info_t ranging_data[RANGING_MAX_SIZE]; /**< Master and slave ranging info structure. */
    ranging_fp_struct_t         fixed_point_inst;               /**< Fixed point elements needed for ranging */
    float                       pwrup_delay_step;               /**< Increase step of the power-up delay */
    int32_t                     offset;                         /**< Offset to be applied */
    uint16_t                    current_avg_size;               /**< Current average size */
    uint16_t                    avg_size;                       /**< Maximum size of sample */
    uint8_t                     pwrup_delay_sequence_length;    /**< Length of the power-up delay sequence */
#if ((defined(RANGING_ALGORITHM_DEBUG)) && (RANGING_ALGORITHM_DEBUG == 1U))
    ranging_prv_t               ranging_prv;                    /**< Internal Ranging data for development */
#endif
} algorithm_param_t;

/** @brief Function pointer for internal ranging calculation.
 */
typedef void (*fptr_algorithm_t)(algorithm_param_t *ranging_instance_parameter, void *distance);

/** @brief Ranging algorithm selection enumeration
 */
typedef enum {
    RANGING_RAW                        = 0, /**< No algorithm applied, return raw data */
    RANGING_DATA_CORRECTED             = 1, /**< SPARK algorithm for correction of ranging data */
    RANGING_DATA_FIXED_POINT_RAW       = 2, /**< No algorithm applied, return raw data using fixed point only */
    RANGING_DATA_FIXED_POINT_CORRECTED = 3  /**< SPARK fixed point algorithm for correction of ranging data */
} ranging_algo_sel_t;

/** @brief Ranging algorithm parameters structure.
 */
typedef struct {
    fptr_algorithm_t   calculate_ranging;     /**< Function that will calculate distance based on ranging value */
    ranging_algo_sel_t ranging_algorithm_sel; /**< Ranging algorithm to be used */
    algorithm_param_t  algorithm_parameter;   /**< Parameter needed for ranging calculation depending on chosen algorithm */
} ranging_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Ranging initialization.
 *
 *  @param[in] self                   Ranging handler.
 *  @param[in] algo_selection         Selection of the algorithm.
 *  @param[in] max_size               Size of the array.
 *  @param[in] offset                 Data offset for accurate measurement.
 *  @param[in] fixed_point_precision  Precision bits when using fixed point based algorithm.
 *  @return Ranging instance.
 *
 *  @note max_size must be scale with RANGING_SCALING to ensure uniformity between
 *        fixed and floating point arithmetic.
 */
void ranging_algorithm_init(ranging_instance_t *self, ranging_algo_sel_t algo_selection, uint16_t max_size,
                            int32_t offset, uint8_t fixed_point_precision);

/** @brief Append data for ranging calculation.
 *
 *  @param[in] self         Ranging handler.
 *  @param[in] master_info  Master ranging information.
 *  @param[in] slave_info   Slave_ranging information.
 *  @retval True   At least avg_size new data have been added.
 *  @retval False  Less than avg_size data have been added.
 */
bool ranging_algorithm_append(ranging_instance_t *self, ranging_info_t *master_info, ranging_info_t *slave_info);

/** @brief Calculate distance in float.
 *
 *  This method will compute the distance based on multiple measurement of ranging,
 *  base on the user settings. Regardless of the chosen algorithm, the returned
 *  distance is in floating point.
 *
 *  @param[in] self  Ranging handler.
 *  @return Distance in meters.
 *
 *  @note This method can be call even if the ranging algorithm instance
 *        has been initialize using fixed point. All of the computation
 *        is still done using fixed point but the distance is converted
 *        to float at the very end.
 *
 */
float ranging_algorithm_calculate_float(ranging_instance_t *self);

/** @brief Calculate distance in fixed point.
 *
 *  This method will compute the distance based on multiple measurement of ranging,
 *  base on the user settings. The computation are all done using fixed point
 *  arithmetic.
 *
 *  @param[in] self  Ranging handler.
 *  @return Distance in meters.
 *
 *  @note This method always returned -1 if the chosen algorithm is not
 *        using fixed point arithmetic.
 */
q_num_t ranging_algorithm_calculate_qnum(ranging_instance_t *self);

/** @brief Verify if module have accumulated it's average size data.
 *
 *  @param[in] self  Ranging handler.
 *  @return bool     True if the buffer is full.
 */
bool ranging_algorithm_is_buffer_full(ranging_instance_t *self);

/** @brief Set a new ranging average size.
 *
 *  @param[in] self      Ranging handler.
 *  @param[in] avg_size  Average size to be set.
 */
void ranging_algorithm_set_avg_size(ranging_instance_t *self, uint16_t avg_size);

/** @brief Clear ranging algorithm and ranging dev buffers.
 *
 *  @param[in] self  Ranging handler.
 */
void ranging_algorithm_clear(ranging_instance_t *self);

/** @brief Update power-up delay value based on packet count and average size.
 *
 *  @param[in] self          Ranging handler.
 *  @param[in] packet_count  packet count.
 *  @return delay            power-up delay value.
 */
uint8_t ranging_algorithm_update_powerup_delay(ranging_instance_t *self, uint8_t *packet_count);

#ifdef __cplusplus
}
#endif


#endif /* RANGING_ALGORITHM_H_ */
