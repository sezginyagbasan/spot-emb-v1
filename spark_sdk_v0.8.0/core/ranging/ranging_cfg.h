/** @file  ranging_cfg.h
 *  @brief Ranging config module.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

#ifndef RANGING_DBG_CFG_H_
#define RANGING_DBG_CFG_H_

/* INCLUDES *******************************************************************/
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#ifndef RANGING_ALGORITHM_DEBUG
#define RANGING_ALGORITHM_DEBUG 0
#endif

#ifdef __cplusplus
}
#endif


#endif /* RANGING_DBG_CFG_H_*/
