/** @file  audio_protocol.c
 *  @brief This is the audio protocol code for the audio API.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "audio_protocol.h"
#include <string.h>
#include "resampling.h"

/* CONSTANTS ******************************************************************/
#define BIT_PER_BYTE        8
#define TX_QUEUE_HIGH_LEVEL 2
#define DECIMAL_FACTOR      100

/* PRIVATE GLOBALS ************************************************************/
static resampling_instance_t resampling_instance;
static uint8_t  size_of_buffer_type = 0;
static uint8_t  *avg_arr;
static uint32_t avg_sum = 0;
static uint32_t avg_val = 0;
static uint32_t count;
static uint8_t  max_queue_offset;
static uint32_t normal_queue_size;
static uint16_t queue_avg_size = 0;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void audio_cdc_update_queue_status(queue_node_t *in_node);
static uint8_t audio_cdc_compensate(queue_node_t *in_node, queue_node_t *out_node);

/* PUBLIC FUNCTIONS ***********************************************************/
void audio_protocol_queue_avg_init(audio_pipeline_t *pipeline, mem_pool_t *mem_pool, audio_error_t *err)
{
    avg_sum = 0;
    avg_val = 0;
    queue_avg_size = pipeline->cfg.queue_avg_size;

    *err = AUDIO_ERR_NONE;

    /* Allocate rolling average memory */
    avg_arr = (uint8_t *)mem_pool_malloc(mem_pool, queue_avg_size);
    if (avg_arr == NULL) {
        *err = AUDIO_ERR_NOT_ENOUGH_MEMORY;
        return;
    }
    memset(avg_arr, 0, queue_avg_size);
}

void audio_protocol_cdc_init(audio_pipeline_t *pipeline, audio_error_t *err)
{
    *err = AUDIO_ERR_NONE;

    /* Initialize resampling configuration */
    size_of_buffer_type = (pipeline->consumer->cfg.bit_depth / BIT_PER_BYTE);

    resampling_config_t resampling_config = {
        .nb_sample = (pipeline->consumer->cfg.audio_payload_size /
                      (pipeline->consumer->cfg.bit_depth / BIT_PER_BYTE)),
        .nb_channel = pipeline->consumer->cfg.channel_count,
        .resampling_length = pipeline->cfg.cdc_resampling_length
    };

    resampling_config.buffer_type = pipeline->consumer->cfg.bit_depth - 1;

    /* Initialize the resampling instance */
    if (resampling_init(&resampling_instance, &resampling_config) != RESAMPLING_NO_ERROR) {
        *err = AUDIO_ERR_CDC_INIT_FAILURE;
        return;
    }

    /* Configure threshold */
    normal_queue_size = pipeline->consumer->cfg.queue_size * DECIMAL_FACTOR;
    uint8_t sample_amount = pipeline->consumer->cfg.audio_payload_size /
                            (pipeline->consumer->cfg.channel_count * size_of_buffer_type);
    max_queue_offset = DECIMAL_FACTOR / sample_amount;
}

queue_node_t *audio_protocol_cdc_process(audio_pipeline_t *pipeline, queue_node_t *in_node, audio_error_t *err)
{
    queue_node_t *out_node;

    *err = AUDIO_ERR_NONE;

    audio_cdc_update_queue_status(in_node);

    out_node = queue_get_free_node(&(pipeline->producer->_free_queue));
    if (out_node == NULL) {
        *err = AUDIO_ERR_PRODUCER_Q_FULL;
        return in_node;
    }
    if (audio_cdc_compensate(in_node, out_node) > 0) {
        /* AUDIO_CDC is running, use output node */
        queue_free_node(in_node);
        return out_node;
    } else {
        /* No resampling happened, return input node */
        queue_free_node(out_node);
        return in_node;
    }
}

void audio_protocol_update_tx_queue_level(audio_pipeline_t *pipeline, queue_node_t *in_node)
{
    audio_header_t *in_audio_header = audio_node_get_header(in_node);

    if (queue_get_length(&pipeline->consumer->_queue) < TX_QUEUE_HIGH_LEVEL) {
        in_audio_header->tx_queue_level_high = 0;
    } else {
        in_audio_header->tx_queue_level_high = 1;
    }
}

void audio_protocol_process_queue_avg(audio_pipeline_t *pipeline)
{
    uint16_t current_queue_length = queue_get_length(&pipeline->consumer->_queue);
    static uint16_t avg_idx;

    /* Update Rolling Avg */
    avg_sum -= avg_arr[avg_idx]; /* Remove oldest value */
    avg_arr[avg_idx] = current_queue_length;
    avg_sum += avg_arr[avg_idx]; /* Add new value */
    if (++avg_idx >= queue_avg_size) {
        avg_idx = 0;
    }
    avg_val = (avg_sum*DECIMAL_FACTOR) / queue_avg_size; /* decimals are part of int value */
}

uint32_t audio_protocol_get_avg_consumer_queue_len(void)
{
    return avg_val;
}

/* PRIVATE FUNCTIONS **********************************************************/
static void audio_cdc_update_queue_status(queue_node_t *in_node)
{
    static bool wait_for_queue_full;

    if ((audio_node_get_header(in_node)->tx_queue_level_high == 1) &&
        (resample_get_state(&resampling_instance) == RESAMPLING_IDLE)) {
        wait_for_queue_full = true;
    }
    if (resample_get_state(&resampling_instance) == RESAMPLING_WAIT_QUEUE_FULL) {
        if (avg_val >= (normal_queue_size - DECIMAL_FACTOR)) {
            resampling_instance.status = RESAMPLING_IDLE;
            wait_for_queue_full = false;
        }
    } else if (resample_get_state(&resampling_instance) == RESAMPLING_IDLE) {
        /* Verify if queue is increasing or depleting */
        if (wait_for_queue_full) {
            resampling_instance.status = RESAMPLING_WAIT_QUEUE_FULL;
        } else {
            if (count > queue_avg_size) {
                if (avg_val > (normal_queue_size + max_queue_offset)) {
                    resampling_start(&resampling_instance, RESAMPLING_REMOVE_SAMPLE);
                    count = 0;
                } else if (avg_val < (normal_queue_size - max_queue_offset)) {
                    resampling_start(&resampling_instance, RESAMPLING_ADD_SAMPLE);
                    count = 0;
                }
            } else {
                /* Give time to the avg to stabilize before checking */
                count++;
            }
        }
    }
}

static uint8_t audio_cdc_compensate(queue_node_t *in_node, queue_node_t *out_node)
{
    uint8_t sample_count = 0;
    audio_header_t *in_audio_header = (audio_header_t *)audio_node_get_header(in_node);
    audio_header_t *out_audio_header = (audio_header_t *)audio_node_get_header(out_node);

    sample_count = in_audio_header->payload_size/size_of_buffer_type;

    /* Resample and save buffer_size in output node header */
    sample_count = resample(&resampling_instance,
                            (void *)audio_node_get_data(in_node),
                            (void *)audio_node_get_data(out_node),
                            sample_count);
    out_audio_header->payload_size = sample_count * size_of_buffer_type;

    return (out_audio_header->payload_size);
}
