/** @file  audio_protocol.c
 *  @brief This is the audio protocol code for the audio API.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

#ifndef AUDIO_PROTOCOL_H_
#define AUDIO_PROTOCOL_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "audio_api.h"
#include "queue.h"

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#define CONSUMER_QUEUE_CDC_EXTRA 3

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize audio clock drift compensation (CDC) module.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code
 */
void audio_protocol_cdc_init(audio_pipeline_t *pipeline, audio_error_t *err);

/** @brief Initialize the playback consumer queue rolling average.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[in]  mem_pool  Memory pool handler.
 *  @param[out] err       Error code.
 */
void audio_protocol_queue_avg_init(audio_pipeline_t *pipeline, mem_pool_t *mem_pool, audio_error_t *err);

/** @brief Process the CDC in the node.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @param[in] node      Node containing samples.
 *  @return Processed node.
 */
queue_node_t *audio_protocol_cdc_process(audio_pipeline_t *pipeline, queue_node_t *in_node, audio_error_t *err);

/** @brief Update the node header with the tx queue level.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @param[in] node      Node containing samples.
 */
void audio_protocol_update_tx_queue_level(audio_pipeline_t *pipeline, queue_node_t *in_node);

/** @brief Run rolling average with new queue length.
 *
 *  @param[in] pipeline  Pipeline instance.
 */
void audio_protocol_process_queue_avg(audio_pipeline_t *pipeline);

/** @brief Get average consumer queue length.
 *
 *  @return Averaged queue size * 100.
 */
uint32_t audio_get_avg_consumer_queue_len(void);

#ifdef __cplusplus
}
#endif

#endif /* AUDIO_PROTOCOL_H_ */
