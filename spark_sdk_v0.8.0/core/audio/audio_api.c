/** @file  audio_api.c
 *  @brief SPARK Audio Core Application Programming Interface.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "audio_api.h"
#include <string.h>
#include "audio_protocol.h"

/* CONSTANTS ******************************************************************/
#define PRODUCER_QUEUE_SIZE                3
#define FREE_QUEUE_SIZE_OFFSET             2
#define CONSUMER_DATA_SIZE_CDC_OFFSET      4
#define USER_DATA_SIZE_BYTE                1
#define PROD_QUEUE_SIZE_MIN_WHEN_ENQUEUING 0

/* PRIVATE GLOBALS ************************************************************/
static mem_pool_t mem_pool;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void init_audio_queue(audio_pipeline_t *pipeline, bool is_producer, audio_error_t *err);
static void init_audio_process(audio_pipeline_t *pipeline, audio_error_t *err);
static uint16_t consume(audio_pipeline_t *pipeline, audio_error_t *err);
static void move_data_from_producer_node_to_consumer_queue(audio_pipeline_t *pipeline, queue_node_t *node);
static queue_node_t *process_samples(audio_pipeline_t *pipeline, queue_node_t *node);
static uint16_t produce(audio_pipeline_t *pipeline, audio_error_t *err);
static void enqueue_producer_node(audio_endpoint_t *producer_ep, audio_error_t *err);

/* PUBLIC FUNCTIONS ***********************************************************/
void audio_core_init(queue_critical_cfg_t queue_critical, uint8_t *queue_memory, uint32_t queue_memory_size)
{
    queue_init(queue_critical);
    mem_pool_init(&mem_pool, queue_memory, (size_t)queue_memory_size);
}

void audio_pipeline_init(audio_pipeline_t *pipeline, const char *name, audio_endpoint_t *producer,
                         audio_protocol_cfg_t cfg, audio_endpoint_t *consumer, audio_error_t *err)
{
    *err = AUDIO_ERR_NONE;

    pipeline->name = name;
    pipeline->producer = producer;
    pipeline->consumer = consumer;
    pipeline->cfg = cfg;

    audio_pipeline_reset_stats(pipeline);

    init_audio_queue(pipeline, true, err);
    if (*err != AUDIO_ERR_NONE) {
        return;
    }

    init_audio_queue(pipeline, false, err);
    if (*err != AUDIO_ERR_NONE) {
        return;
    }

    init_audio_process(pipeline, err);
    if (*err != AUDIO_ERR_NONE) {
        return;
    }

    if (!pipeline->consumer->cfg.use_encapsulation) {
        audio_protocol_queue_avg_init(pipeline, &mem_pool, err);
        if (*err != AUDIO_ERR_NONE) {
            return;
        }
    }

    if (pipeline->cfg.cdc_enable) {
        audio_protocol_cdc_init(pipeline, err);
        if (*err != AUDIO_ERR_NONE) {
            return;
        }
    }
}

void audio_endpoint_init(audio_endpoint_t *endpoint, void *instance, const char *name,
                         audio_endpoint_interface_t iface, audio_endpoint_cfg_t cfg)
{
    endpoint->instance = instance;
    endpoint->name = name;
    endpoint->iface = iface;
    endpoint->cfg = cfg;
}

void audio_pipeline_add_processing(audio_pipeline_t *pipeline, audio_processing_t *process, audio_error_t *err)
{
    *err = AUDIO_ERR_NONE;

    if (pipeline->_audio_process_count >= pipeline->cfg.max_process_count) {
        *err = AUDIO_ERR_PROC_STAGE_LIMIT_REACHED;
        return;
    }

    pipeline->process[pipeline->_audio_process_count] = process;
    pipeline->_audio_process_count++;
}

void audio_pipeline_produce(audio_pipeline_t *pipeline, audio_error_t *err)
{
    audio_endpoint_t *producer_ep = pipeline->producer;
    uint16_t size;

    *err = AUDIO_ERR_NONE;

    if (pipeline->producer->cfg.delayed_action) {
        if (producer_ep->_current_node != NULL) {
            /* Enqueue previous node */
            enqueue_producer_node(producer_ep, err);
            if (*err != AUDIO_ERR_NONE) {
                return;
            }
        }
        /* Start production of next node */
        produce(pipeline, err);
        if (*err != AUDIO_ERR_NONE) {
            return;
        }
    } else {
        /* Start production of next node */
        size = produce(pipeline, err);
        if (*err != AUDIO_ERR_NONE) {
            return;
        }
        if (size > 0) {
            /* Endpoint produced the node, so enqueue it */
            enqueue_producer_node(producer_ep, err);
            if (*err != AUDIO_ERR_NONE) {
                return;
            }
        }
    }
}

void audio_pipeline_consume(audio_pipeline_t *pipeline, audio_error_t *err)
{
    audio_endpoint_t *consumer_ep = pipeline->consumer;
    uint8_t size;

    *err = AUDIO_ERR_NONE;

    if (!pipeline->_buffering_complete) {
        *err = AUDIO_ERR_BUFFERING_NOT_COMPLETE;
        return;
    }

    if (pipeline->consumer->cfg.delayed_action) {
        /* Free previous node */
        queue_free_node(consumer_ep->_current_node);
        /* Get new node */
        consumer_ep->_current_node = queue_dequeue_node(&consumer_ep->_queue);
        /* Start consumption of new node */
        consume(pipeline, err);
    } else {
        /* Get the next node, if available, without dequeuing */
        consumer_ep->_current_node = queue_get_node(&consumer_ep->_queue);
        /* Start consumption of the node */
        size = consume(pipeline, err);
        if (size > 0) {
            /* Consumed successfully, so dequeue and free */
            queue_free_node(queue_dequeue_node(&consumer_ep->_queue));
        }
        consumer_ep->_current_node = NULL;
    }
}

audio_statistics_t *audio_pipeline_get_stats(audio_pipeline_t *pipeline)
{
    pipeline->_statistics.producer_buffer_load = pipeline->producer->_queue.length;
    pipeline->_statistics.consumer_buffer_load = pipeline->consumer->_queue.length;

    return &pipeline->_statistics;
}

uint32_t audio_pipeline_get_producer_buffer_load(audio_pipeline_t *pipeline)
{
    return pipeline->producer->_queue.length;
}

uint32_t audio_pipeline_get_consumer_buffer_load(audio_pipeline_t *pipeline)
{
    return pipeline->consumer->_queue.length;
}

uint32_t audio_pipeline_get_consumer_buffer_overflow_count(audio_pipeline_t *pipeline)
{
    return pipeline->_statistics.consumer_buffer_overflow_count;
}

uint32_t audio_pipeline_get_consumer_buffer_underflow_count(audio_pipeline_t *pipeline)
{
    return pipeline->_statistics.consumer_buffer_underflow_count;
}

void audio_pipeline_reset_stats(audio_pipeline_t *pipeline)
{
    uint32_t consume_size;
    uint32_t produce_size;

    produce_size = pipeline->_statistics.producer_buffer_size;
    consume_size = pipeline->_statistics.consumer_buffer_size;

    memset(&pipeline->_statistics, 0, sizeof(audio_statistics_t));

    pipeline->_statistics.producer_buffer_size  = produce_size;
    pipeline->_statistics.consumer_buffer_size = consume_size;
}

void audio_pipeline_start(audio_pipeline_t *pipeline)
{
    /* Initialize processing stages */
    for (uint8_t i = 0; i < pipeline->_audio_process_count; i++) {
        pipeline->process[i]->iface.init(pipeline->process[i]->instance, &mem_pool);
    }
    /* Initialize buffering */
    pipeline->_buffering_complete = false;
    pipeline->_buffering_threshold = (pipeline->cfg.do_initial_buffering) ?
                                      pipeline->consumer->cfg.queue_size : 1;
    /* Start endpoints */
    pipeline->producer->iface.start(pipeline->producer->instance);
    if (pipeline->_buffering_complete) {
        pipeline->consumer->iface.start(pipeline->consumer->instance);
    }
}

void audio_pipeline_stop(audio_pipeline_t *pipeline)
{
    /* Stop both endpoints */
    pipeline->producer->iface.stop(pipeline->producer->instance);
    pipeline->consumer->iface.stop(pipeline->consumer->instance);
    /* Deinit all process stages */
    for (uint8_t i = 0; i < pipeline->_audio_process_count; i++) {
        pipeline->process[i]->iface.deinit(pipeline->process[i]->instance);
    }
    /* Free current node */
    queue_free_node(pipeline->producer->_current_node);
    pipeline->producer->_current_node = NULL;
}

uint32_t audio_processing_ctrl(audio_processing_t *audio_processing, uint8_t cmd, uint32_t arg)
{
    return audio_processing->iface.ctrl(audio_processing->instance, cmd, arg);
}

void audio_pipeline_process(audio_pipeline_t *pipeline, audio_error_t *err)
{
    queue_node_t *node1, *node2;

    *err = AUDIO_ERR_NONE;

   if (!pipeline->_buffering_complete) {
        if (queue_get_length(&pipeline->consumer->_queue) >= (pipeline->_buffering_threshold)) {
            /* Buffering threshold reached */
            pipeline->_buffering_complete = true;
            pipeline->consumer->iface.start(pipeline->consumer->instance);
        }
    }

    /* Get a node with audio samples that need processing from the producer queue */
    node1 = queue_dequeue_node(&pipeline->producer->_queue);
    if (node1 == NULL) {
        *err = AUDIO_ERR_NO_SAMPLES_TO_PROCESS;
        return;
    }

    if (pipeline->_audio_process_count > 0) {
        /* Apply all processing stages on audio packet */
        node2 = process_samples(pipeline, node1);
    } else {
        /* No processing to be done */
        node2 = node1;
    }

    if (pipeline->consumer->cfg.use_encapsulation) {
        /* Update tx_queue_level_high field in audio header */
        audio_protocol_update_tx_queue_level(pipeline, node2);
        node1 = node2;
    } else {
        /* Consumer takes only audio data */
        if (pipeline->cfg.cdc_enable) {
            /* Calculate average queue length */
            audio_protocol_process_queue_avg(pipeline);
            /* Apply Clock Drift Compensation */
            node1 = audio_protocol_cdc_process(pipeline, node2, err);
        } else {
            node1 = node2;
        }
    }

    move_data_from_producer_node_to_consumer_queue(pipeline, node1);
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Initialize an audio queue.
 *.
 *  @param[in]  pipeline     Pipeline instance.
 *  @param[in]  is_producer  True if initializing producer queue.
 *                           False if initializing consumer queue.
 *  @param[out] err          Error code.
 */
static void init_audio_queue(audio_pipeline_t *pipeline, bool is_producer, audio_error_t *err)
{
    audio_endpoint_t *endpoint;
    uint16_t queue_data_size;
    uint16_t queue_size;
    uint8_t *pool_ptr;

    *err = AUDIO_ERR_NONE;

    if (is_producer) {
        endpoint = pipeline->producer;
    } else {
        endpoint = pipeline->consumer;
    }

    queue_data_size = endpoint->cfg.audio_payload_size;
    queue_data_size += sizeof(audio_header_t);
    queue_data_size += USER_DATA_SIZE_BYTE;
    if (pipeline->cfg.cdc_enable) {
        queue_data_size += CONSUMER_DATA_SIZE_CDC_OFFSET;
    }
    queue_data_size += queue_data_size % sizeof(uint16_t); /* Align nodes on 16bits */

    if (is_producer) {
        queue_size = PRODUCER_QUEUE_SIZE;
    } else {
        queue_size = endpoint->cfg.queue_size;
        if (pipeline->cfg.cdc_enable) {
            queue_size += CONSUMER_QUEUE_CDC_EXTRA;
        }
    }

    pool_ptr = mem_pool_malloc(&mem_pool, QUEUE_NB_BYTES_NEEDED(queue_size, queue_data_size));
    if (pool_ptr == NULL) {
        *err = AUDIO_ERR_NOT_ENOUGH_MEMORY;
        return;
    }
    queue_init_pool(pool_ptr,
                    &endpoint->_free_queue,
                    queue_size,
                    queue_data_size,
                    "Producer Free Queue");

    queue_init_queue(&endpoint->_queue, queue_size, "Producer Queue");
    endpoint->_current_node = NULL;

    if (is_producer) {
        pipeline->_statistics.producer_buffer_size = pipeline->producer->_queue.limit;
    } else {
        pipeline->_statistics.consumer_buffer_size = pipeline->consumer->_queue.limit;
    }
}

/** @brief Initialize audio pipeline process variables.
 *.
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code.
 */
static void init_audio_process(audio_pipeline_t *pipeline, audio_error_t *err)
{
    *err = AUDIO_ERR_NONE;

    pipeline->_audio_process_count = 0;

    pipeline->process = (audio_processing_t **)mem_pool_malloc(&mem_pool, pipeline->cfg.max_process_count * sizeof(audio_processing_t *));
    if (pipeline->process == NULL) {
        *err = AUDIO_ERR_NOT_ENOUGH_MEMORY;
        return;
    }
}

/** @brief Copy data from a node of the producer queue to a node of the consumer queue.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @param[in] node1     Node from the producer queue.
 */
static void move_data_from_producer_node_to_consumer_queue(audio_pipeline_t *pipeline, queue_node_t *node1)
{
    queue_node_t *node2;

    node2 = queue_get_free_node(&pipeline->consumer->_free_queue);
    if (node2 == NULL) {
        pipeline->_statistics.consumer_buffer_overflow_count++;

        queue_free_node(queue_dequeue_node(&pipeline->consumer->_queue));
        node2 = queue_get_free_node(&pipeline->consumer->_free_queue);
    }
    memcpy(node2->data, node1->data, audio_node_get_header(node1)->payload_size + sizeof(audio_header_t) + USER_DATA_SIZE_BYTE);
    queue_enqueue_node(&pipeline->consumer->_queue, node2);
    queue_free_node(node1);
}

/** @brief Apply all processing stages to a producer queue node.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @param[in] node1     Node from the producer queue.
 *  @return Pointer to a producer queue node containing the processed data.
 */
static queue_node_t *process_samples(audio_pipeline_t *pipeline, queue_node_t *node1)
{
    uint8_t rv;
    queue_node_t *node2, *node_tmp;
    int i = 0;

    /* Get a process destination node */
    node2 = queue_get_free_node(&pipeline->producer->_free_queue);
    do {
        /* node1 is the source node */
        rv = pipeline->process[i]->iface.process(pipeline->process[i]->instance,
                                                 audio_node_get_header(node1),
                                                 audio_node_get_data(node1),
                                                 audio_node_get_header(node1)->payload_size,
                                                 audio_node_get_data(node2));
        if (rv != 0) { /* != 0 means processing happened */
            /* Copy the header from the old source */
            memcpy(audio_node_get_header(node2), audio_node_get_header(node1), sizeof(audio_header_t));
            /* Update the size */
            audio_node_get_header(node2)->payload_size = rv;
            /* Swap node1 and node2 */
            node_tmp = node1;
            node1 = node2;
            node2 = node_tmp;
        }
        i++;
    } while (i != pipeline->_audio_process_count);
    queue_free_node(node2);

    return node1;
}

/** @brief Enqueue the current producer queue node.
 *
 *  @param[in]  producer_ep  Pointer to the producer endpoint.
 *  @param[out] err          Error code.
 */
static void enqueue_producer_node(audio_endpoint_t *producer_ep, audio_error_t *err)
{
    *err = AUDIO_ERR_NONE;

    /* Before enqueuing a node, make sure that the producer queue is empty.
        If it's not, the processing hasn't started, so dequeue the node in queue
        before enqueuing the new one */
    if (queue_get_length(&producer_ep->_queue) > PROD_QUEUE_SIZE_MIN_WHEN_ENQUEUING) {
        queue_free_node(queue_dequeue_node(&producer_ep->_queue));
    }
    if (!queue_enqueue_node(&producer_ep->_queue, producer_ep->_current_node)) {
        queue_free_node(producer_ep->_current_node);
        *err = AUDIO_ERR_PRODUCER_Q_FULL;
    }
    /* The current node is no longer been used by the producer */
    producer_ep->_current_node = NULL;
}

/** @brief Get a free producer queue node and apply the producer endpoint action.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code.
 *  @return The amount of bytes produces.
 */
static uint16_t produce(audio_pipeline_t *pipeline, audio_error_t *err)
{
    audio_endpoint_t *producer_ep = pipeline->producer;
    uint8_t *payload;
    uint8_t payload_size;

    *err = AUDIO_ERR_NONE;

    producer_ep->_current_node = queue_get_free_node(&producer_ep->_free_queue);
    if (producer_ep->_current_node == NULL) {
        *err = AUDIO_ERR_PRODUCER_Q_FULL;
        return 0;
    }

    payload_size = producer_ep->cfg.audio_payload_size;
    if (producer_ep->cfg.use_encapsulation) {
        payload = (uint8_t *)audio_node_get_header(producer_ep->_current_node);
        payload_size += sizeof(audio_header_t);
        payload_size += USER_DATA_SIZE_BYTE;
    } else {
        payload = (uint8_t *)audio_node_get_data(producer_ep->_current_node);
        audio_node_get_header(producer_ep->_current_node)->payload_size = payload_size;
    }

    return producer_ep->iface.action(producer_ep->instance, payload, payload_size);
}

/** @brief Apply the consumer endpoint action on the current node.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code.
 *  @return The amount of bytes consumed.
 */
static uint16_t consume(audio_pipeline_t *pipeline, audio_error_t *err)
{
    audio_endpoint_t *consumer_ep = pipeline->consumer;
    uint8_t *payload;
    uint8_t payload_size;

    *err = AUDIO_ERR_NONE;

    if (consumer_ep->_current_node == NULL) {
        pipeline->_statistics.consumer_buffer_underflow_count++;
        pipeline->_buffering_complete = false;
        *err = AUDIO_ERR_CONSUMER_Q_EMPTY;
        return 0;
    } else {
        payload_size = audio_node_get_header(consumer_ep->_current_node)->payload_size;
        if (consumer_ep->cfg.use_encapsulation) {
            payload = (uint8_t *)audio_node_get_header(consumer_ep->_current_node);
            payload_size += sizeof(audio_header_t);
            payload_size += USER_DATA_SIZE_BYTE;
        } else {
            payload = (uint8_t *)audio_node_get_data(consumer_ep->_current_node);
        }
    }

    return consumer_ep->iface.action(consumer_ep->instance, payload, payload_size);
}
