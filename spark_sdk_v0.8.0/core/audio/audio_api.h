/** @file  audio_api.h
 *  @brief SPARK Audio Core Application Programming Interface.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef AUDIO_API_H_
#define AUDIO_API_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "audio_error.h"
#include "queue.h"
#include "mem_pool.h"

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#define AUDIO_NO_ARG 0 /*!< Definition to use when no arguments are used if audio core calls */
#define AUDIO_PACKET_HEADER_OFFSET 0 /*!< Position of the audio header in the audio packet */
#define AUDIO_PACKET_DATA_OFFSET sizeof(audio_header_t) /*!< Position of the packet data in the audio packet */

/* MACROS *********************************************************************/
#define audio_node_get_header(node) ((audio_header_t *)(queue_get_data_ptr(node, AUDIO_PACKET_HEADER_OFFSET))) /*!< Macro used to get a pointer to the audio header in the audio packet */
#define audio_node_get_data(node) ((uint8_t *)(queue_get_data_ptr(node, AUDIO_PACKET_DATA_OFFSET))) /*!< Macro used to get a pointer to the packet data in the audio packet */

/* TYPES **********************************************************************/
typedef struct audio_header {
    uint8_t tx_queue_level_high:1; /*!< For clock drift compensation. Used by the
                                        recorder to notify the player that its TX audio buffer is filling up */
    uint8_t user_data_is_valid:1; /*!< Indicates if the audio packet trailling byte is a valid user data byte */
    uint8_t reserved:6;   /*!< Reserved for future use */
    uint8_t payload_size; /*!< Size of the payload (audio samples) expressed in bytes */
} audio_header_t;

typedef struct processing_interface {
    void (*init)(void *instance, mem_pool_t *mem_pool);   /*!< Function the audio core uses to excecute any processing
                                                               stage initialization sequence */
    void (*deinit)(void *instance); /*!< Function the audio core uses to excecute any processing
                                         stage de-initialization sequence */
    uint32_t (*ctrl)(void *instance, uint8_t cmd, uint32_t args); /*!< Function the audio application uses to interact with the
                                                                       processing stage */
    uint8_t (*process)(void *instance, audio_header_t *header,
                       uint8_t *data_in, uint8_t size, uint8_t *data_out); /*!< Function the audio core uses to do
                                                                                processing on audio samples */
} processing_interface_t;

typedef struct audio_processing {
    void *instance;               /*!< Pointer to the processing stage's specific instance */
    char *name;                   /*!< Character string describing the processing stage */
    processing_interface_t iface; /*!< Interface the processing stage must comply to */
} audio_processing_t;

typedef struct endpoint_interface {
    uint8_t (*action)(void *instance, uint8_t *samples, uint8_t size); /*!< Function the audio core uses to send or receive audio
                                                                            samples depending if the endpoint produces or consumes */
    void (*start)(void *instance); /*!< Function the audio core uses to excecute any endpoint startup sequence */
    void (*stop)(void *instance);  /*!< Function the audio core uses to stop any endpoint operations */
} audio_endpoint_interface_t;

typedef struct audio_endpoint_cfg {
    bool use_encapsulation;     /*!< True if the endpoint produces or consumes audio packets (audio header + audio payload),
                                     False for only audio payloads (audio samples) */
    bool delayed_action;        /*!< True if the endpoint requires a complete cycle to produce or consume data.
                                     False if the enpoint produces or consumes instantly */
    uint8_t channel_count;      /*!< 1 if the endpoint produces or consumes mono audio payloads and 2 for interleaved stereo */
    uint8_t bit_depth;          /*!< Bit depth of samples the endpoint produces or consumes */
    uint8_t audio_payload_size; /*!< Size in bytes of the audio payload */
    uint8_t queue_size;         /*!< Size in number of audio packets the endpoint's queue can contain */
} audio_endpoint_cfg_t;

typedef struct audio_endpoint {
    void *instance;                   /*!< Pointer to endpoint's specific instance */
    const char *name;                 /*!< Character string describing the endpoint */
    audio_endpoint_interface_t iface; /*!< Interface the endpoint must comply to */
    audio_endpoint_cfg_t cfg;         /*!< Audio enpoint configuration */
    queue_t _queue;                   /*!< Internal: queue the endpoint will use to store or retrieve audio packets */
    queue_t _free_queue;              /*!< Internal: free queue the endpoint will retrieve free nodes from */
    queue_node_t *_current_node;      /*!< Internal: pointer to the queue node the endpoint is working with at the moment */
} audio_endpoint_t;

typedef struct audio_protocol_cfg {
    bool cdc_enable;                /*!< Enable the clock drift compensation on the pipeline */
    uint16_t cdc_resampling_length; /*!< Amount of samples used when resampling */
    uint16_t queue_avg_size;        /*!< Amount of measurements used when averaging the consumer queue size.
                                         This setting is needed to use CDC */
    bool do_initial_buffering;      /*!< Wait for the consumer queue (TX audio buffer) to be
                                         full before starting to consume */
    uint8_t max_process_count;      /*!< Maximum amount of process to be used in the pipeline */
} audio_protocol_cfg_t;

typedef struct audio_statistics {
    uint32_t producer_buffer_load; /*!< Number audio packets currently in the producer queue */
    uint16_t producer_buffer_size; /*!< Maximum number audio packets the producer queue can hold */
    uint32_t consumer_buffer_load; /*!< Number audio packets currently in the consumer queue */
    uint16_t consumer_buffer_size; /*!< Maximum number audio packets the consumer queue can hold */
    uint32_t consumer_buffer_overflow_count;  /*!< Number of times the consumer queue has overflowed */
    uint32_t consumer_buffer_underflow_count; /*!< Number of times the consumer queue has underflowed */
} audio_statistics_t;

typedef struct audio_pipeline {
    const char *name;
    audio_endpoint_t *producer;     /*!< Pointer to the audio endpoint that will produce audio samples
                                         to this audio pipeline */
    audio_processing_t **process;   /*!< List of processing stages that will sequentially be
                                         used on the produced samples before they are consumed */
    uint8_t _audio_process_count;   /*!< Internal: the number of processing stages added to the pipeline */
    audio_endpoint_t *consumer;     /*!< Pointer to the audio endpoint that will consume audio samples
                                         from this audio pipeline */
    audio_protocol_cfg_t cfg;       /*!< Audio protocol configuration */
    audio_statistics_t _statistics; /*!< Audio-specific statistics */
    uint8_t _buffering_threshold;   /*!< Internal: The number of audio packets to buffer before considering
                                         the initial buffering complete */
    bool _buffering_complete;       /*!< Internal: Whether or not the initial audio buffering has been completed */
} audio_pipeline_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize the audio core.
 *
 *  @param[in] queue_critical     Critical section functions.
 *  @param[in] queue_memory       Pointer to allocated queue memory.
 *  @param[in] queue_memory_size  Size of allocated queue memory.
 */
void audio_core_init(queue_critical_cfg_t queue_critical, uint8_t *queue_memory, uint32_t queue_memory_size);

/** @brief Initialize an audio pipeline.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[in]  name      Name of the pipeline.
 *  @param[in]  producer  Producer endpoint.
 *  @param[in]  cfg       Protocol configuration.
 *  @param[in]  consumer  Consumer endpoint.
 *  @param[out] err       Error code.
 *  @return False if allocated memory too small, true otherwise.
 */
void audio_pipeline_init(audio_pipeline_t *pipeline, const char *name, audio_endpoint_t *producer,
                         audio_protocol_cfg_t cfg, audio_endpoint_t *consumer, audio_error_t *err);

/** @brief Initialize an audio endpoint.
 *
 *  @param[in] endpoint  Pointer to the endpoint to initialize.
 *  @param[in] instance  Endpoint instance.
 *  @param[in] name      Name of the pipeline.
 *  @param[in] iface     Interface of the endpoint.
 *  @param[in] cfg       Endpoint configuration.
 */
void audio_endpoint_init(audio_endpoint_t *endpoint, void *instance, const char *name,
                        audio_endpoint_interface_t iface, audio_endpoint_cfg_t cfg);

/** @brief Add a processing stage to the pipeline.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[in]  process   Prointer to processing structure.
 *  @param[out] err       Error code.
 */
void audio_pipeline_add_processing(audio_pipeline_t *pipeline, audio_processing_t *process, audio_error_t *err);

/** @brief Start the audio pipeline.
 *
 *  @param[in] pipeline  Pipeline instance.
 */
void audio_pipeline_start(audio_pipeline_t *pipeline);

/** @brief Stop the audio pipeline.
 *
 *  @param[in] pipeline  Pipeline instance.
 */
void audio_pipeline_stop(audio_pipeline_t *pipeline);

/** @brief Execute process specific control.
 *
 *  @param[in] audio_processing  Audio processing structure.
 *  @param[in] cmd               Command specific to the processing stage.
 *  @param[in] arg               Argument specific to the processing stage.
 *  @return A value specific to control function,
 */
uint32_t audio_processing_ctrl(audio_processing_t *audio_processing, uint8_t cmd, uint32_t arg);

/** @brief Execute the audio processing stages.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code.
 */
void audio_pipeline_process(audio_pipeline_t *pipeline, audio_error_t *err);

/** @brief Execute the produce endpoint.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code.
 */
void audio_pipeline_produce(audio_pipeline_t *pipeline, audio_error_t *err);

/** @brief Execute the consume endpoint.
 *
 *  @param[in]  pipeline  Pipeline instance.
 *  @param[out] err       Error code.
 */
void audio_pipeline_consume(audio_pipeline_t *pipeline, audio_error_t *err);

/** @brief Get the audio stats.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @return Pointer to audio stats structure.
 */
audio_statistics_t *audio_pipeline_get_stats(audio_pipeline_t *pipeline);

/** @brief Get producer buffer load.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @return buffer producer load value
 */
uint32_t audio_pipeline_get_producer_buffer_load(audio_pipeline_t *pipeline);

/** @brief Get consumer buffer load.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @return Consumer buffer load value.
 */
uint32_t audio_pipeline_get_consumer_buffer_load(audio_pipeline_t *pipeline);

/** @brief Get consumer buffer overflow count.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @return Consumer buffer overflow counter.
 */
uint32_t audio_pipeline_get_consumer_buffer_overflow_count(audio_pipeline_t *pipeline);

/** @brief Get consumer buffer underflow count.
 *
 *  @param[in] pipeline  Pipeline instance.
 *  @return Consumer buffer underflow counter.
 */
uint32_t audio_pipeline_get_consumer_buffer_underflow_count(audio_pipeline_t *pipeline);

/** @brief Reset the audio stats.
 *
 *  @param[in] pipeline  Pipeline instance.
 */
void audio_pipeline_reset_stats(audio_pipeline_t *pipeline);

#ifdef __cplusplus
}
#endif

#endif /* AUDIO_API_H_ */
