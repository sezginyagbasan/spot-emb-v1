/** @file  audio_volume.h
 *  @brief Audio processing functions related to the software volume control.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc.
 *  @author    SPARK FW Team.
 */
#ifndef AUDIO_VOLUME_H_
#define AUDIO_VOLUME_H_

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include "audio_api.h"
#include <stdbool.h>
#include <stdint.h>

/* CONSTANTS ******************************************************************/
#define AUDIO_VOLUME_MAX  1
#define AUDIO_VOLUME_MIN  0
#define AUDIO_VOLUME_GRAD 0.0003
#define AUDIO_VOLUME_TICK 0.1

/* TYPES **********************************************************************/
typedef enum volume_cmd {
    VOLUME_INCREASE,
    VOLUME_DECREASE,
    VOLUME_MUTE,
    VOLUME_GET_FACTOR
} volume_cmd_t;

typedef struct volume {
    float   volume_factor;
    float   volume_threshold;
    uint8_t initial_volume_level;
} volume_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize volume core.
 *
 *  @param[in] volume    Volume control instance.
 *  @param[in] mem_pool  Memory pool for memory allocation.
 */
void audio_volume_init(void *volume, mem_pool_t *mem_pool);

/** @brief Deinitialize volume core.
 *
 *  @param[in] volume  Volume control instance.
 */
void audio_volume_deinit(void *volume);

/** @brief Process volume on each audio sample.
 *
 *  @param[in]  volume       Volume instance.
 *  @param[in]  header       Audio header.
 *  @param[in]  data_in      Data in to be processed.
 *  @param[in]  bytes_count  Number of bytes to process.
 *  @param[out] data_out     Processed samples out.
 *  @return Number of samples processed. Return 0 if no samples processed.
 */
uint8_t audio_volume_process(void *volume, audio_header_t *header, uint8_t *data_in,
                             uint8_t bytes_count, uint8_t *data_out);

/** @brief Volume Control function.
 *
 *  @param[in] volume  Volume instance.
 *  @param[in] cmd     Control command.
 *  @param[in] arg     Control argument.
 *  @return Value returned dependent on command.
 */
uint32_t audio_volume_ctrl(void *volume, uint8_t cmd, uint32_t arg);

#ifdef __cplusplus
}
#endif

#endif /* AUDIO_VOLUME_H_ */
