/** @file  audio_compression.h
 *  @brief Audio ADPCM compression / decompression processing stage.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef AUDIO_COMPRESSION_H_
#define AUDIO_COMPRESSION_H_

/* INCLUDES *******************************************************************/
#include "adpcm.h"
#include "audio_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
typedef enum audio_compression_cmd {
    AUDIO_COMPRESSION_ENABLE,
    AUDIO_COMPRESSION_DISABLE,
    AUDIO_COMPRESSION_GET_STATE
} audio_compression_cmd_t;

typedef enum audio_compression_mode {
    AUDIO_COMPRESSION_PACK_STEREO,
    AUDIO_COMPRESSION_UNPACK_STEREO,
    AUDIO_COMPRESSION_PACK_MONO,
    AUDIO_COMPRESSION_UNPACK_MONO
} audio_compression_mode_t;

typedef struct audio_compression_instance {
    uint8_t compression_enabled;
    adpcm_state_t adpcm_left_state;
    adpcm_state_t adpcm_right_state;
    audio_compression_mode_t compression_mode;
} audio_compression_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize compression process.
 *
 *  @param[in] instance  Compression instance.
 *  @param[in] mem_pool  Memory pool for memory allocation.
 */
void audio_compression_init(void *instance, mem_pool_t *mem_pool);

/** @brief Deinitialize compression process.
 *
 *  @param[in] instance  Compression instance.
 */
void audio_compression_deinit(void *instance);

/** @brief Audio compression control function.
 *
 *  @param[in] instance  Compression instance.
 *  @param[in] cmd       Control command.
 *  @param[in] arg       Control argument.
 *  @return Audio Compression Status.
 */
uint32_t audio_compression_ctrl(void *instance, uint8_t cmd, uint32_t arg);

/** @brief Process audio samples compression.
 *
 *  @param[in]  instance     Compression instance.
 *  @param[in]  header       Audio header.
 *  @param[in]  data_in      Data in to be processed.
 *  @param[in]  bytes_count  Number of bytes to process.
 *  @param[out] data_out     Processed data out.
 *  @return Number of bytes processed.
 */
uint8_t audio_compression_process(void *instance, audio_header_t *header,
                                uint8_t *data_in, uint8_t size, uint8_t *data_out);

#ifdef __cplusplus
}
#endif

#endif /* AUDIO_VOLUME_H_ */
