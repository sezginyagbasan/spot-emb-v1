/** @file  audio_src_cmsis.h
 *  @brief Audio processing functions related to the cmsis sampling rate conversion
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc.
 *  @author    SPARK FW Team.
 */
#ifndef AUDIO_SRC_CMSIS_H_
#define AUDIO_SRC_CMSIS_H_

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include "audio_api.h"
#include <stdbool.h>
#include <stdint.h>
#include "arm_math.h"

/* TYPES **********************************************************************/
typedef enum src_cmsis_factor {
    AUDIO_SRC_THREE = 3,
    AUDIO_SRC_SIX   = 6
} src_cmsis_factor_t;

typedef union src_cmsis_fir_instance {
    arm_fir_interpolate_instance_q15 interpolate_instance; /*!< instance for the arm_fir interpolation */
    arm_fir_decimate_instance_q15 decimate_instance;       /*!< instance for the arm_fir decimation */
} src_cmsis_fir_instance_t;

typedef struct src_cmsis_cfg {
    src_cmsis_factor_t ratio; /*!< Ratio to use for the SRC */
    bool is_divider;          /*!< True if SRC is divider, false if SRC is multiplier */
    uint8_t block_size;       /*!< number of input samples to process per call */
} src_cmsis_cfg_t;

typedef struct src_cmsis_instance {
    src_cmsis_cfg_t cfg;                   /*!< SRC CMSIS user configuration */
    src_cmsis_fir_instance_t fir_instance; /*!< instance for the arm_fir module */
} src_cmsis_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize the src cmsis core.
 *
 *  @param[in] instance  SRC CMSIS instance.
 *  @param[in] mem_pool  Memory pool for memory allocation.
 */
void audio_src_cmsis_init(void *instance, mem_pool_t *mem_pool);

/** @brief Deinitialize the src cmsis core.
 *
 *  @param[in] instance  SRC CMSIS instance.
 */
void audio_src_cmsis_deinit(void *instance);

/** @brief Process SRC on an audio packet.
 *
 *  @param[in]  instance     SRC CMSIS instance.
 *  @param[in]  header       Audio header.
 *  @param[in]  data_in      Data in to be processed.
 *  @param[in]  bytes_count  Number of bytes to process.
 *  @param[out] data_out     Processed samples out.
 *  @return Number of bytes processed. Return 0 if no samples processed.
 */
uint8_t audio_src_cmsis_process(void *instance, audio_header_t *header, uint8_t *data_in,
                                uint8_t bytes_count, uint8_t *data_out);

/** @brief SRC CMSIS Control function.
 *
 *  @param[in] instance  SRC CMSIS instance.
 *  @param[in] cmd       Control command.
 *  @param[in] arg       Control argument.
 *  @return Value returned dependent on command.
 */
uint32_t audio_src_cmsis_ctrl(void *instance, uint8_t cmd, uint32_t arg);

#ifdef __cplusplus
}
#endif

#endif /* AUDIO_VOLUME_H_ */
