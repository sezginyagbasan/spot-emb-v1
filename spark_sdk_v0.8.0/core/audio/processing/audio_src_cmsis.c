/** @file  audio_src_cmsis.c
 *  @brief Audio processing functions related to the cmsis sampling rate conversion
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "audio_src_cmsis.h"
#include <string.h>

/* CONSTANTS ******************************************************************/
#define FIR_NUMTAPS 24 /* Must be dividable by all audio_src_factor since fir phaseLength is NumTaps/ratio */

/* Third of initial sampling frequency */
static const q15_t fir_n24_c0_20_w_hamming[FIR_NUMTAPS] = {
    58, 29, -49, -223, -454, -577, -333, 495, 1933, 3725, 5388, 6391,
    6391, 5388, 3725, 1933, 495, -333, -577, -454, -223, -49, 29, 58
};
/* Sixth of initial sampling frequency */
static const q15_t fir_n24_c0_10_w_hamming[FIR_NUMTAPS] = {
    -36, -17, 28, 139, 358, 707, 1185, 1760, 2368, 2930, 3363, 3599,
    3599, 3363, 2930, 2368, 1760, 1185, 707, 358, 139, 28, -17, -36
};

/* PUBLIC FUNCTIONS ***********************************************************/
void audio_src_cmsis_init(void *instance, mem_pool_t *mem_pool)
{
    int16_t *fir_state; /* Memory space that will be allocated using audio api's mem_pool */
    const int16_t *fir_coeff; /* FIR coefficients to use */
    src_cmsis_instance_t *src_instance = (src_cmsis_instance_t *)instance;

    fir_state = mem_pool_malloc(mem_pool, sizeof(int16_t)*(FIR_NUMTAPS + src_instance->cfg.block_size));

    switch (src_instance->cfg.ratio) {
    case AUDIO_SRC_SIX:
        fir_coeff = fir_n24_c0_10_w_hamming;
        break;
    case AUDIO_SRC_THREE:
        fir_coeff = fir_n24_c0_20_w_hamming;
        break;
    default:
        /* Invalid Ratio */
        return;
    }

    if (src_instance->cfg.is_divider) {
        arm_fir_decimate_init_q15((arm_fir_decimate_instance_q15 *)(&src_instance->fir_instance),
                                  FIR_NUMTAPS, src_instance->cfg.ratio, fir_coeff, fir_state, src_instance->cfg.block_size);
    } else {
        arm_fir_interpolate_init_q15((arm_fir_interpolate_instance_q15 *)(&src_instance->fir_instance),
                                     src_instance->cfg.ratio, FIR_NUMTAPS, fir_coeff, fir_state, src_instance->cfg.block_size);
    }
}

void audio_src_cmsis_deinit(void *instance)
{
    (void)instance;
}

uint32_t audio_src_cmsis_ctrl(void *instance, uint8_t cmd, uint32_t arg)
{
    (void)instance;
    (void)cmd;
    (void)arg;

    return 0;
}

uint8_t audio_src_cmsis_process(void *instance, audio_header_t *header,
                                uint8_t *data_in, uint8_t bytes_count, uint8_t *data_out)
{
    (void)header;
    uint8_t sample_count_out           = 0;
    uint8_t sample_count_in            = bytes_count/sizeof(q15_t);
    src_cmsis_instance_t *src_instance = (src_cmsis_instance_t *)instance;

    if (src_instance->cfg.is_divider) {
        arm_fir_decimate_q15((arm_fir_decimate_instance_q15 *)(&src_instance->fir_instance),
                             (q15_t *)data_in, (q15_t *)data_out, bytes_count/sizeof(q15_t));
        sample_count_out = sample_count_in / src_instance->cfg.ratio;
    } else {
        arm_fir_interpolate_q15((arm_fir_interpolate_instance_q15 *)(&src_instance->fir_instance),
                                (q15_t *)data_in, (q15_t *)data_out, bytes_count/sizeof(q15_t));
        sample_count_out = sample_count_in * src_instance->cfg.ratio;
    }

    return sample_count_out * sizeof(q15_t);
}
