/** @file  audio_user_data.h
 *  @brief  Processing stage used to append/extract 1 byte of user data to/from an audio payload.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef AUDIO_USER_DATA_H_
#define AUDIO_USER_DATA_H_

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "audio_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TYPES **********************************************************************/
typedef enum audio_user_data_cmd {
    AUDIO_USER_DATA_SEND_BYTE
} audio_user_data_cmd_t;

typedef enum audio_user_data_mode {
    AUDIO_USER_DATA_TX,
    AUDIO_USER_DATA_RX
} audio_user_data_mode_t;

typedef struct rx_audio_user_data_instance {
    audio_user_data_mode_t mode;
    uint8_t data;
    bool data_valid;
    void (*rx_callback)(uint8_t data);
} audio_user_data_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize user data process.
 *
 *  @param[in] instance  User data instance.
 *  @param[in] mem_pool  Memory pool for memory allocation.
 */
void audio_user_data_init(void *instance, mem_pool_t *mem_pool);

/** @brief Deinitialize user data process.
 *
 *  @param[in] instance  User data instance.
 */
void audio_user_data_deinit(void *instance);

/** @brief Audio compression control function.
 *
 *  @param[in] instance  User data instance.
 *  @param[in] cmd       Control command.
 *  @param[in] arg       Control argument.
 *  @return Audio Compression Status.
 */
uint32_t audio_user_data_ctrl(void *instance, uint8_t cmd, uint32_t arg);

/** @brief Process audio samples compression.
 *
 *  @param[in]  instance     User data instance.
 *  @param[in]  header       Audio header.
 *  @param[in]  data_in      Data in to be processed.
 *  @param[in]  bytes_count  Number of bytes to process.
 *  @param[out] data_out     Processed data out.
 *  @return Number of bytes processed.
 */
uint8_t audio_user_data_process(void *instance, audio_header_t *header,
                                uint8_t *data_in, uint8_t size, uint8_t *data_out);

#ifdef __cplusplus
}
#endif

#endif /* AUDIO_USER_DATA_H_ */
