/** @file  audio_volume.c
 *  @brief Audio processing functions related to the software volume control.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "audio_volume.h"
#include <string.h>

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void audio_volume_increase(volume_t *volume_ctrl);
static void audio_volume_decrease(volume_t *volume_ctrl);
static void audio_volume_mute(volume_t *volume_ctrl);
static float audio_volume_get_level(volume_t *volume_ctrl);
static void adjust_samples_volume(volume_t *volume, int16_t *audio_samples_in,
                                  uint8_t samples_count, int16_t *audio_samples_out);

/* PUBLIC FUNCTIONS ***********************************************************/
void audio_volume_init(void *volume, mem_pool_t *mem_pool)
{
    (void)mem_pool;
    volume_t *vol_inst = (volume_t *) volume;

    vol_inst->volume_factor = (vol_inst->initial_volume_level / 100.0);
    vol_inst->volume_threshold = (vol_inst->initial_volume_level / 100.0);
}

void audio_volume_deinit(void *volume)
{
    volume_t *vol_inst = (volume_t *) volume;

    audio_volume_mute(vol_inst);
}

uint32_t audio_volume_ctrl(void *volume, uint8_t cmd, uint32_t arg)
{
    (void)arg;
    uint32_t ret = 0;
    volume_t *vol_inst = (volume_t *) volume;

    switch ((volume_cmd_t)cmd) {
    case VOLUME_INCREASE:
        audio_volume_increase(vol_inst);
        break;
    case VOLUME_DECREASE:
        audio_volume_decrease(vol_inst);
        break;
    case VOLUME_MUTE:
        audio_volume_mute(vol_inst);
        break;
    case VOLUME_GET_FACTOR:
        ret = (uint32_t)(audio_volume_get_level(vol_inst) * 10000.0);
        break;
    }
    return ret;
}

uint8_t audio_volume_process(void *volume, audio_header_t *header,
                             uint8_t *data_in, uint8_t size, uint8_t *data_out)
{
    (void)header;
    volume_t *vol_inst = (volume_t *) volume;

    if ((vol_inst->volume_threshold != AUDIO_VOLUME_MAX) || (vol_inst->volume_factor != AUDIO_VOLUME_MAX)) {
        adjust_samples_volume(vol_inst, (int16_t *)data_in, size / sizeof(int16_t), (int16_t *)data_out);
        return size;
    } else {
        return 0;
    }
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Increase the audio volume.
 *
 *  @param[in] volume  Volume instance.
 */
static void audio_volume_increase(volume_t *volume)
{
    volume->volume_threshold += AUDIO_VOLUME_TICK;
    if (volume->volume_threshold >= AUDIO_VOLUME_MAX) {
        volume->volume_threshold = AUDIO_VOLUME_MAX;
    }
}

/** @brief Decrease the audio volume.
 *
 *  @param[in] volume  Volume instance.
 */
static void audio_volume_decrease(volume_t *volume)
{
    volume->volume_threshold -= AUDIO_VOLUME_TICK;
    if (volume->volume_threshold <= AUDIO_VOLUME_MIN) {
        volume->volume_threshold = AUDIO_VOLUME_MIN;
    }
}

/** @brief Mute the audio.
 *
 *  @param[in] volume  Volume instance.
 */
static void audio_volume_mute(volume_t *volume)
{
    volume->volume_factor = 0;
    volume->volume_threshold = 0;
}

/** @brief Get the audio volume level.
 *
 *  @param[in] volume  Volume instance.
 *  @return Volume level.
 */
static float audio_volume_get_level(volume_t *volume)
{
    return volume->volume_factor;
}

/** @brief Process a volume factor on each sample.
 *
 *  @param[in]  volume             Volume instance.
 *  @param[in]  audio_samples_in   Sample pointer of data in.
 *  @param[in]  samples_count      Number of samples to process.
 *  @param[out] audio_samples_out  Sample pointer of data out.
 */
static void adjust_samples_volume(volume_t *volume, int16_t *audio_samples_in,
                                  uint8_t samples_count, int16_t *audio_samples_out)
{
    /* Test if factor increases or decreases and reaches the desired value */
    /* The value is correct if overflow */
    if (volume->volume_factor < volume->volume_threshold) {
        volume->volume_factor += AUDIO_VOLUME_GRAD;
        if (volume->volume_factor >= volume->volume_threshold) {
            volume->volume_factor = volume->volume_threshold;
        }
    } else if (volume->volume_factor > volume->volume_threshold) {
        volume->volume_factor -= AUDIO_VOLUME_GRAD;
        if (volume->volume_factor <= volume->volume_threshold) {
            volume->volume_factor = volume->volume_threshold;
        }
    }

    /* Apply factor to each sample */
    for (uint8_t count = 0; count < samples_count; count++) {
        audio_samples_out[count] = audio_samples_in[count] * volume->volume_factor;
    }
}
