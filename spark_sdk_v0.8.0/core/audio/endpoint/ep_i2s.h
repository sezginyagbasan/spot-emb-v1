/** @file  ep_i2s.h
 *  @brief Endpoint for the I2S interface.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef EP_I2S_H_
#define EP_I2S_H_

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include <stdint.h>

/* TYPES **********************************************************************/
typedef struct ep_i2s_instance {
    void (*i2s_action)(uint8_t *data, uint8_t size); /*!< i2s produce or consume action from
                                                          the board bsp */
    void (*i2s_start)(void);                         /*!< i2s start action from the board bsp */
    void (*i2s_stop)(void);                          /*!< i2s stop action from the board bsp */
    uint8_t bytes_per_sample;
} ep_i2s_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Launch the i2s endpoint action.
 *
 *  @param[in] instance  Endpoint instance.
 *  @param[in] samples   Pointer to data.
 *  @param[in] instance  Number of bytes to read.
 *  @return 0
 */
uint8_t ep_i2s_action(void *instance, uint8_t *samples, uint8_t size);

/** @brief Start the i2s endpoint.
 *
 *  @param[in] instance  Endpoint instance.
 */
void ep_i2s_start(void *instance);

/** @brief Stop the i2s endpoint.
 *
 *  @param[in] instance  Endpoint instance.
 */
void ep_i2s_stop(void *instance);

#ifdef __cplusplus
}
#endif

#endif /* EP_I2S_H_ */
