/** @file  ep_wps.c
 *  @brief Endpoint for the SPARK Wireless Protocol Stack (WPS).
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef EP_WPS_H_
#define EP_WPS_H_

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include <stdint.h>
#include "wps.h"

/* TYPES **********************************************************************/
typedef struct ep_wps_args {
    wps_connection_t *connection;
} ep_wps_instance_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Read audio data from WPS.
 *
 *  @param[in] instance  Endpoint instance.
 *  @param[in] samples   Pointer to data.
 *  @param[in] instance  Number of bytes to read.
 *  @return Number of bytes read.
 */
uint8_t ep_wps_produce(void *instance, uint8_t *samples, uint8_t size);

/** @brief Write audio data to WPS.
 *
 *  @param[in] instance  Endpoint instance.
 *  @param[in] samples   Pointer to data.
 *  @param[in] instance  Number of bytes to write.
 *  @return Number of bytes written.
 */
uint8_t ep_wps_consume(void *instance, uint8_t *samples, uint8_t size);

/** @brief Start WPS.
 *
 *  @param[in] instance  Endpoint instance.
 */
void ep_wps_start(void *instance);

/** @brief Stop WPS.
 *
 *  @param[in] instance  Endpoint instance.
 */
void ep_wps_stop(void *instance);

#ifdef __cplusplus
}
#endif

#endif /* EP_WPS_H_ */
