/** @file  ep_wps.c
 *  @brief Endpoint for the SPARK Wireless Protocol Stack (WPS).
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "ep_wps.h"

/* PUBLIC FUNCTIONS ***********************************************************/
uint8_t ep_wps_produce(void *instance, uint8_t *samples, uint8_t size)
{
    (void)size;

    ep_wps_instance_t *inst = (ep_wps_instance_t *)instance;

    wps_rx_frame frame_out = wps_read(inst->connection);

    memcpy(samples, frame_out.payload, frame_out.size);
    wps_read_done(inst->connection);

    return frame_out.size;
}

uint8_t ep_wps_consume(void *instance, uint8_t *samples, uint8_t size)
{
    ep_wps_instance_t *inst = (ep_wps_instance_t *)instance;
    uint8_t *buf;

    if (wps_get_fifo_free_space(inst->connection) > 0) {
        /* Get buffer from queue to hold data */
        wps_get_free_slot(inst->connection, &buf);
        /* Format the new payload */
        memcpy(buf, samples, size);
        /* Send the payload through the WPS */
        wps_send(inst->connection, buf, size);
        return size;
    } else {
        return 0;
    }
}

void ep_wps_start(void *instance)
{
    (void)instance;
}

void ep_wps_stop(void *instance)
{
    (void)instance;
}
