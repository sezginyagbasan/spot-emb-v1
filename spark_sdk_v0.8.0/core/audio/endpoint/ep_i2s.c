/** @file  ep_i2s.c
 *  @brief Endpoint for the I2S interface.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "ep_i2s.h"

/* PUBLIC FUNCTIONS ***********************************************************/
uint8_t ep_i2s_action(void *instance, uint8_t *samples, uint8_t size)
{
    ((ep_i2s_instance_t *)instance)->i2s_action(samples, (size / ((ep_i2s_instance_t *)instance)->bytes_per_sample));

    return 0;
}

void ep_i2s_start(void *instance)
{
    ((ep_i2s_instance_t *)instance)->i2s_start();
}

void ep_i2s_stop(void *instance)
{
    ((ep_i2s_instance_t *)instance)->i2s_stop();
}
