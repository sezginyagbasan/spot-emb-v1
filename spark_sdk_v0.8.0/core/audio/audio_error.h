/** @file  audio_error.h
 *  @brief SPARK Audio Core error codes.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef AUDIO_ERROR_H_
#define AUDIO_ERROR_H_

/* TYPES **********************************************************************/
/** @brief Audio error enum definition.
 */
typedef enum audio_error {
    AUDIO_ERR_NONE = 0, /**< No error occurred */
    AUDIO_ERR_NOT_ENOUGH_MEMORY, /**< Not enough memory is allocated by the application
                                      for a full audio core initialization */
    AUDIO_ERR_PROC_STAGE_LIMIT_REACHED, /**< Maximum number of processing stages for a given audio pipeline
                                             is already reached when trying to add another one */
    AUDIO_ERR_PRODUCER_Q_FULL, /**< Producer's queue is full when trying to produce */
    AUDIO_ERR_CONSUMER_Q_EMPTY, /**< Consumer's queue is empty when trying to consume */
    AUDIO_ERR_BUFFERING_NOT_COMPLETE, /**< Initial buffering is not completed when trying to consume */
    AUDIO_ERR_NO_SAMPLES_TO_PROCESS, /**< Producer's queue is empty when trying to process */
    AUDIO_ERR_CDC_INIT_FAILURE /**< An error occurred during the clock drift compensation module initialization */
} audio_error_t;


#endif /* AUDIO_ERROR_H_ */
