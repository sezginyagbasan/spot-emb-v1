/** @file  bsp_led.h
 *  @brief This module controls LED features of SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_LED_H_
#define BSP_LED_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Turn on LED.
 *
 *  @param[in] led  LED id number
 */
void bsp_led_on(bsp_led_t led);

/** @brief Turn off LED.
 *
 *  @param[in] led  LED id number
 */
void bsp_led_off(bsp_led_t led);

/** @brief Toggle LED.
 *
 *  @param[in] led  LED id number
 */
void bsp_led_toggle(bsp_led_t led);

#ifdef __cplusplus
}
#endif


#endif /* BSP_LED_H_ */