/** @file  bsp_power.h
 *  @brief This module controls the power features of the SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_POWER_H_
#define BSP_POWER_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Select board voltage between 1.8V and 3.3V.
 *
 *  @param[in] bsp_vdd_t  vdd
 *      @li VDD_1V8
 *      @li VDD_3V3
 */
void bsp_set_board_voltage(bsp_vdd_t vdd);

#ifdef __cplusplus
}
#endif


#endif /* BSP_POWER_H_ */
