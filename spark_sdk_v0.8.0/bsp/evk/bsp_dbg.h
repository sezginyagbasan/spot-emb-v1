/** @file  bsp_dbg.h
 *  @brief This module controls debug io of SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_DBG_H_
#define BSP_DBG_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Toggle io.
 *
 *  @param[in] io  io id number
 */
void bsp_dbg_on(bsp_dbg_t io);

/** @brief Toggle io.
 *
 *  @param[in] io  io id number
 */
void bsp_dbg_off(bsp_dbg_t io);

/** @brief Toggle io.
 *
 *  @param[in] io  io id number
 */
void bsp_dbg_toggle(bsp_dbg_t io);

#ifdef __cplusplus
}
#endif


#endif /* BSP_DBG_H_ */
