/** @file  bsp.h
 *  @brief Board Support Package for SPARK EVK board.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_H_
#define BSP_H_

/* INCLUDES *******************************************************************/
#include "bsp_button.h"
#include "bsp_clock.h"
#include "bsp_dbg.h"
#include "bsp_flash.h"
#include "bsp_it.h"
#include "bsp_led.h"
#include "bsp_power.h"
#include "bsp_radio.h"
#include "bsp_timer.h"
#include "bsp_uart.h"
#include "bsp_usb.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize the board's peripherals.
 */
void bsp_init(void);

/** @brief Reset the MCU.
 */
void bsp_system_reset(void);


#endif /* BSP_H_ */
