/** @file  bsp_power.c
 *  @brief TThis module controls the power features of the SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "bsp_power.h"

/* PUBLIC FUNCTIONS ***********************************************************/
void bsp_set_board_voltage(bsp_vdd_t vdd)
{
    if (vdd == VDD_1V8) {
        HAL_GPIO_WritePin(VOLTAGE_SEL_PORT, VOLTAGE_SEL_PIN, GPIO_PIN_RESET);
    } else if (vdd == VDD_3V3) {
        HAL_GPIO_WritePin(VOLTAGE_SEL_PORT, VOLTAGE_SEL_PIN, GPIO_PIN_SET);
    }
}
