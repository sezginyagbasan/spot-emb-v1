/** @file  bsp_clock.h
 *  @brief This module control clock related features.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_CLOCK_H_
#define BSP_CLOCK_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Set the system clock frequency.
 *
 *  @param bsp_clk_freq  Clock frequency selection.
 */
void bsp_set_system_clock(bsp_clk_freq_t bsp_clk_freq);

/** @brief Get the sytem clock frequency value.
 *
 *  @return System clock frequency.
 */
uint32_t bsp_get_system_clock_freq(void);

/** @brief De-initialize the peripheral USB clock.
 */
void bsp_usb_clock_deinit(void);

/** @brief Initialize the external PLL oscillator clock.
 *
 *  This external PLL can be optionally used to drive the radio's PLL.
 */
void bsp_init_ext_osc_clk(void);

/** @brief De-initialize the external PLL oscillator clock.
 */
void bsp_deinit_ext_osc_clk(void);

/** @brief Set the PLL select pin to enable the external oscillator clock.
 *
 *  This function turns on the 20.48MHz oscillator.
 *  The oscillator provides an optional external PLL for the radio.
 *  When the oscillator is on, the radio should deactivate its internal PLL.
 */
void bsp_enable_ext_osc_clk(void);

/** @brief Reset the PLL select pin to disable the external oscillator clock.
 *
 *  This function turns off the 20.48MHz oscillator.
 *  When the oscillator is off, the radio should activate its internal PLL.
 */
void bsp_disable_ext_osc_clk(void);

/** @brief Initialize the external XTAL clock.
 *
 *  This external XTAL clock can be optionally used to drive the radio's XTAL clock.
 */
void bsp_init_xtal_clk(void);

/** @brief De-initialize the external XTAL clock.
 */
void bsp_deinit_xtal_clk(void);

/** @brief Enable the XTAL clock.
 *
 *  This signal is a PWM with a 50% duty cycle and a 32.768kHz frequency.
 *  It provides an optional external XTAL clock for the radio.
 *  When the XTAL clock is on, the radio should deactivate its internal XTAL clock.
 */
void bsp_enable_xtal_clk(void);

/** @brief Disable the XTAL clock.
 *
 *  This function turns off the XTAL clock PWM signal.
 *  When the XTAL clock is off, the radio should activate its internal XTAL clock.
 */
void bsp_disable_xtal_clk(void);

/** @brief Initialize the peripheral USB clock.
 */
void bsp_init_usb_clock(void);


#ifdef __cplusplus
}
#endif

#endif /* BSP_CLOCK_H_ */