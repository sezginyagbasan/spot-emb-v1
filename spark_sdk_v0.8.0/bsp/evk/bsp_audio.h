/** @file  bsp_audio.h
 *  @brief Initialise relate perihperal for audio.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_AUDIO_H_
#define BSP_AUDIO_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp.h"

typedef void (*audio_callback)(void);
typedef void (*audio_error_callback)(DMA_HandleTypeDef * hsai);

/* PUBLIC FUNCTIONS ***********************************************************/
/** @brief Configure I2C, SAI, DMA and CODEC.
 *
 *  @param[in] codec_config   Codec configuration.
 *
 */
void bsp_audio_init();

/** @brief deinitialise sai I2C peripheral and stop dma.
 *
 */
void bsp_audio_deinit(void);

/** @brief Start audio playback.
 *
 */
void bsp_audio_playback_start(void);

/** @brief Start audio record.
 *
 */
void bsp_audio_record_start(void);

/** @brief Stop audio playback.
 *
 */
void bsp_audio_playback_stop(void);

/** @brief Stop audio record.
 *
 */
void bsp_audio_record_stop(void);

/** @brief Write 1 byte data on the I2C1 peripheral.
 *
 *  @param[in] dev_address  Audio sampling rate.
 *  @param[in] mem_addr     Memory device adress to write.
 *  @param[in] data         Data to write on the memory.
 *
 */
 void bsp_audio_i2c_write(uint8_t dev_address, uint8_t mem_addr, uint8_t data);

/** @brief Read 1 byte data on the I2C1 peripheral.
 *
 *  @param[in]  dev_address  Audio sampling rate.
 *  @param[in]  mem_addr     Memory device adress to write.
 *  @param[out] data         Data to read from the memory.
 */
void bsp_audio_i2c_read(uint8_t dev_address, uint8_t mem_addr, uint8_t * data);

/** @brief Write sample on the SAI with DMA.
 *
 *  @param[in] data           Audio sample buffer.
 *  @param[in] samples_count  Number of sample in the data pointer.
 */
void bsp_audio_sai_write(uint8_t * data, uint8_t samples_count);

/** @brief Read the data from the SAI with DMA.
 *
 *  @param[out] data           Audio sample buffer.
 *  @param[in]  samples_count  Number of sample in the data pointer.
 */
void bsp_audio_sai_read(uint8_t * data, uint8_t samples_count);

/** @brief Set the channel number for the playback audio stream.
 *
 *  @param[in] channel  channel number of the data stream.
 */
void bsp_audio_set_playback_channel(bsp_audio_num_channel_t channel);

/** @brief Set the channel number for the record audio stream.
 *
 *  @param[in] channel  channel number of the data stream.
 */
void bsp_audio_set_record_channel(bsp_audio_num_channel_t channel);

/** @brief Set the sampling rate of the data stream.
 *
 *  @param[in] sampling_rate  Sampling rate frequency of the data stream in Hz.
 */
void bsp_audio_set_sampling_rate(bsp_audio_sampling_rate_t audio_frequency);

/** @brief Set the reception sai callback.
 *
 *  @param[in] callback  Callback function pointer for sai reception.
 */
void bsp_audio_set_rx_callback(audio_callback callback);

/** @brief Set the transmission sai callback.
 *
 *  @param[in] callback  Callback function pointer of sai transmission.
 */
void bsp_audio_set_tx_callback(audio_callback callback);

/** @brief Set the error sai peripheral callback.
 *
 *  @param[in] callback  Callback function pointer of sai transmission.
 */
void bsp_audio_set_error_callback(audio_error_callback error_callback);

void bsp_audio_config1(void);

void bsp_audio_config2(void);

void bsp_audio_config3(void);

#ifdef __cplusplus
}
#endif

#endif /* BSP_AUDIO_H_ */
