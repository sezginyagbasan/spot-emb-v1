/** @file  bsp_usb.c
 *  @brief This module controls USB features of the SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "bsp_clock.h"
#include "bsp_usb.h"
#include "usbd_cdc_if.h"

/* EXTERNS ********************************************************************/
extern USBD_HandleTypeDef hUsbDeviceFS;
extern USBD_DescriptorsTypeDef CDC_Desc;

/* PUBLIC FUNCTIONS ***********************************************************/
void bsp_usb_cdc_init_rx_cb(int8_t (* usb_cdc_receive_cb)(uint8_t *buf, uint32_t *len))
{
    usbd_cdc_init_rx_cb(usb_cdc_receive_cb);
}

void bsp_usb_cdc_init_tx_cb(int8_t (*usb_cdc_transmit_cb)(uint8_t *buf, uint32_t *len))
{
    usbd_cdc_init_tx_cb(usb_cdc_transmit_cb);
}

void bsp_usb_cdc_init_init_cb(int8_t (*usb_cdc_init_cb)(void))
{
    usbd_cdc_init_init_cb(usb_cdc_init_cb);
}

void bsp_usb_cdc_init(uwb_err *err)
{
    bsp_init_usb_clock();

    *err = USBD_Init(&hUsbDeviceFS, &CDC_Desc, DEVICE_FS);
    if (*err != USBD_OK) {
        return;
    }
    *err = USBD_RegisterClass(&hUsbDeviceFS, &USBD_CDC);
    if (*err != USBD_OK) {
        return;
    }
    *err = USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
    if (*err != USBD_OK) {
        return;
    }
    *err = USBD_Start(&hUsbDeviceFS);
    if (*err != USBD_OK) {
        return;
    }
}

void bsp_cdc_usb_deinit(uwb_err *err)
{
    *err = USBD_DeInit(&hUsbDeviceFS);
    if (*err != USBD_OK) {
        return;
    }
    bsp_usb_clock_deinit();
}

bool bsp_cdc_usb_connect(uwb_err *err)
{
    *err = UWB_ERR_NONE;

    if (bsp_is_usb_detected()) {
        bsp_usb_cdc_init(err);
        if (*err == USBD_OK) {
            return true;
        }
    }
    return false;
}

bool bsp_cdc_usb_disconnect(uwb_err *err)
{
    *err = UWB_ERR_NONE;

    if (!bsp_is_usb_detected()) {
        bsp_cdc_usb_deinit(err);
        if (*err != USBD_OK) {
            return true;
        }
    }
    return false;
}

bool bsp_is_usb_detected(void)
{
    if (HAL_GPIO_ReadPin(USB_DETECT_PORT, USB_DETECT_PIN) == GPIO_PIN_RESET) {
        return true;
    } else {
        return false;
    }
}

uwb_err bsp_usb_cdc_putc(uint8_t c)
{
    return CDC_Transmit_FS(&c, 1);
}

uwb_err bsp_usb_cdc_send_buf(uint8_t *buf, uint16_t buf_len)
{
    return CDC_Transmit_FS(buf, buf_len);
}

void bsp_usb_enter_critical(void)
{
    NVIC_DisableIRQ(USB_LP_IRQn);
}

void bsp_usb_exit_critical(void)
{
    NVIC_EnableIRQ(USB_LP_IRQn);
}
