/** @file  bsp_timer.c
 *  @brief This module controls timer features of the SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "bsp_it.h"
#include "bsp_timer.h"

/* PRIVATE GLOBALS ************************************************************/
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim20;

/* CONSTANT *******************************************************************/
#define MAX_VALUE_UINT16_T  0xffff
#define APP_TIMER_PRESCALER 3

/* PUBLIC FUNCTIONS ***********************************************************/
uint32_t bsp_get_tick(void)
{
    return HAL_GetTick();
}

void bsp_delay_ms(uint32_t ms)
{
    HAL_Delay(ms);
}

uint32_t bsp_get_tick_us(void)
{
    return __HAL_TIM_GET_COUNTER(&htim3);
}

void bsp_delay_us(uint32_t delay)
{
    __HAL_TIM_SET_COUNTER(&htim3, 0);

    while(__HAL_TIM_GET_COUNTER(&htim3) < delay);
}

uint64_t bsp_get_free_running_timer_tick_ms(void)
{
    static uint64_t count;
    static uint32_t last_time;
    uint64_t ret_count;

    bsp_enter_critical();
    uint32_t current_time = __HAL_TIM_GET_COUNTER(&htim6) / 4;
    if(last_time > current_time)
    {
        count += (((MAX_VALUE_UINT16_T) / 4) - last_time) + current_time ;
    } else {
        count += current_time - last_time;
    }
    last_time = current_time;
    ret_count = count;
    bsp_exit_critical();
    return ret_count;
}

void bsp_app_timer_init(uint32_t period_us)
{
    uint32_t uwTimclock = 0;
    uint32_t frequency  = 1000000 / period_us;

    /* Enable TIM20 clock */
    __HAL_RCC_TIM20_CLK_ENABLE();

    uwTimclock = HAL_RCC_GetPCLK1Freq() / APP_TIMER_PRESCALER;

    htim20.Instance         = TIM20;
    htim20.Init.Prescaler   = APP_TIMER_PRESCALER - 1;
    htim20.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim20.Init.Period      = (uwTimclock / frequency) - 1; /* 1/4000 s*/
    htim20.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim20) != HAL_OK) {
        Error_Handler();
    }

    HAL_NVIC_SetPriority(TIM20_UP_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(TIM20_UP_IRQn);
}

void bsp_app_timer_start(void)
{
    if (HAL_TIM_Base_Start_IT(&htim20) != HAL_OK) {
        Error_Handler();
    }
}

void bsp_app_timer_stop(void)
{
    if (HAL_TIM_Base_Stop_IT(&htim20) != HAL_OK) {
        Error_Handler();
    }
}
