/** @file  bsp_button.h
 *  @brief This module controls button feature of the SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_BUTTON_H_
#define BSP_BUTTON_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Read the input from the user button.
 *
 *  @param[in] btn  Button number.
 *  @retval true   Button is pressed.
 *  @retval false  Button is not pressed.
 */
bool bsp_read_btn_state(bsp_btn_t btn);

#ifdef __cplusplus
}
#endif

#endif /* BSP_BUTTON_H_ */