/** @file  bsp_it.h
 *  @brief This module controls interrupt related features.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_IT_H_
#define BSP_IT_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"

/* TYPES **********************************************************************/
typedef void (*irq_callback)(void);

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief This function sets the function callback for USB detection interrupt.
 */
void bsp_set_usb_detect_callback(irq_callback callback);

/** @brief Set the application user timer callback.
 *
 *  @note This timer is used to mock application priority
 *        for WPS timing and behavior caracterization.
 *
 *  @param[in] callback  Timer user callback.
 */
void bsp_set_app_timer_callback(irq_callback callback);

/** @brief This function set the function callback for the radio pin interrupt.
 *
 *  @param[in] callback  External interrupt callback function pointer.
 */
void bsp_set_radio_irq_callback(irq_callback callback);

/** @brief This function sets the function callback for the DMA_RX ISR.
 *
 *  @param[in] callback  External interrupt callback function pointer.
 */
void bsp_set_radio_dma_rx_callback(irq_callback callback);

/** @brief Set the application user timer callback.
 *
 *  @note This timer is used to mock application priority
 *        for WPS timing and behavior caracterization.
 *
 *  @param[in] callback  Timer user callback.
 */
void bsp_set_app_timer_callback(irq_callback callback);

/** @brief This function sets the function callback for the pendsv.
 *
 *  @param[in] callback  External interrupt callback function pointer.
 */
void bsp_set_pendsv_callback(irq_callback callback);

/** @brief Disable IRQ Interrupts
 */
void bsp_enter_critical(void);

/** @brief Enable IRQ Interrupts
 */
void bsp_exit_critical(void);

/** @brief Error handle used by STM32 HAL.
 */
void Error_Handler(void);

#ifdef __cplusplus
}
#endif

#endif /* BSP_IT_H_ */
