/** @file  bsp_audio.c
 *  @brief Initialise relate perihperal for audio.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "bsp_audio.h"

/* PRIVATE GLOBALS ************************************************************/
I2C_HandleTypeDef hi2c1;
SAI_HandleTypeDef hsai_block_a1;
SAI_HandleTypeDef hsai_block_b1;
DMA_HandleTypeDef hdma_sai1_b;
DMA_HandleTypeDef hdma_sai1_a;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void audio_sai_transmit(SAI_HandleTypeDef *hsai, uint8_t *pData, uint16_t size);
static void audio_sai_receive(SAI_HandleTypeDef *hsai, uint8_t *pData, uint16_t size);
static void dma_receive(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength);
static void dma_transmit(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength);
static void init_dma(void);
static void init_i2c(void);
static void init_sai(void);
static void init_sai_block_a(void);
static void init_sai_block_b(void);
static void deinit_i2c(void);
static void deinit_sai(void);
static void sai_transmit_dma_complete_callback(DMA_HandleTypeDef *hdma);
static void sai_receive_dma_complete_callback(DMA_HandleTypeDef *hdma);

static audio_callback audio_tx_callback = NULL;
static audio_callback audio_rx_callback = NULL;

/* PUBLIC FUNCTIONS ***********************************************************/
 void bsp_audio_init()
{
    init_i2c();
    init_sai();
    init_dma();
}

void bsp_audio_playback_start(void)
{
    static uint8_t data[4];
    bsp_audio_sai_write(data, AUDIO_CHANNELS_STEREO);
}

void bsp_audio_record_start(void)
{
    static uint8_t data[4];
    bsp_audio_sai_read(data, AUDIO_CHANNELS_STEREO);
}

void bsp_audio_playback_stop(void)
{
    DMA_HandleTypeDef *dma_b = hsai_block_b1.hdmatx;

    /* Test DMA interrupt flag SAI Section B.*/
    if (hsai_block_b1.hdmatx->Instance->CCR & DMA_IT_TC) {
        __HAL_DMA_DISABLE_IT(dma_b, DMA_IT_TC);
    }

    if (hsai_block_b1.hdmatx->Instance->CCR & DMA_IT_TE) {
        __HAL_DMA_DISABLE_IT(dma_b, DMA_IT_TE);
    }
}

void bsp_audio_record_stop(void)
{
    DMA_HandleTypeDef *dma_a = hsai_block_a1.hdmarx;

    /* Test DMA interrupt flag SAI Section A.*/
    if (hsai_block_a1.hdmarx->Instance->CCR & DMA_IT_TC) {
        __HAL_DMA_DISABLE_IT(dma_a, DMA_IT_TC);
    }

    if (hsai_block_a1.hdmarx->Instance->CCR & DMA_IT_TE) {
        __HAL_DMA_DISABLE_IT(dma_a, DMA_IT_TE);
    }
}

void bsp_audio_deinit(void)
{
    deinit_i2c();
    deinit_sai();
}

void bsp_audio_i2c_write(uint8_t dev_address, uint8_t mem_addr, uint8_t data)
{
    uint8_t i2c_transmit[2];

    i2c_transmit[0] = mem_addr;
    i2c_transmit[1] = data;

    HAL_I2C_Master_Transmit(&hi2c1, dev_address, (uint8_t*)i2c_transmit, sizeof(i2c_transmit), HAL_MAX_DELAY);
}

void bsp_audio_i2c_read(uint8_t dev_address, uint8_t mem_addr, uint8_t* data)
{
    HAL_I2C_Mem_Read(&hi2c1, dev_address, mem_addr, I2C_MEMADD_SIZE_8BIT, data, 1, HAL_MAX_DELAY);
}

void bsp_audio_sai_write(uint8_t *data, uint8_t samples_count)
{
    audio_sai_transmit(&hsai_block_b1, data, samples_count);
}

void bsp_audio_sai_read(uint8_t *data, uint8_t samples_count)
{
    audio_sai_receive(&hsai_block_a1, data, samples_count);
}

void bsp_audio_set_sampling_rate(bsp_audio_sampling_rate_t audio_frequency)
{
    switch (audio_frequency) {
    case AUDIO_48KHZ:
        hsai_block_b1.Init.MckOverSampling = SAI_MCK_OVERSAMPLING_DISABLE;
        break;
    case AUDIO_32KHZ:
        hsai_block_b1.Init.MckOverSampling = SAI_MCK_OVERSAMPLING_ENABLE;
        break;
    case AUDIO_24KHZ:
        hsai_block_b1.Init.MckOverSampling = SAI_MCK_OVERSAMPLING_ENABLE;
        break;
    case AUDIO_16KHZ:
        hsai_block_b1.Init.MckOverSampling = SAI_MCK_OVERSAMPLING_DISABLE;
        break;
    default:
        /* Invalid */
        return;
    }

    hsai_block_b1.Init.AudioFrequency = audio_frequency;
}

void bsp_audio_set_playback_channel(bsp_audio_num_channel_t channel)
{
    /* Bloc B1 channel */
    switch(channel) {
        case AUDIO_CHANNELS_STEREO:
            hsai_block_b1.Init.MonoStereoMode = SAI_STEREOMODE;
            break;
        case AUDIO_CHANNELS_MONO:
            hsai_block_b1.Init.MonoStereoMode = SAI_MONOMODE;
            break;
    }
}

void bsp_audio_set_record_channel(bsp_audio_num_channel_t channel)
{
    /* Bloc A1 channel */
    switch(channel) {
        case AUDIO_CHANNELS_STEREO:
            hsai_block_a1.Init.MonoStereoMode = SAI_STEREOMODE;
            break;
        case AUDIO_CHANNELS_MONO:
            hsai_block_a1.Init.MonoStereoMode = SAI_MONOMODE;
            break;
    }
}

/** @brief Initialize the I2C clock.
 *
 *  @param[in] i2cHandle  I2C handler pointer.
 */
void HAL_I2C_MspInit(I2C_HandleTypeDef *i2cHandle)
{
    UNUSED(i2cHandle);

    /* I2C1 clock enable */
     __HAL_RCC_I2C1_CLK_ENABLE();
}

/** @brief deInitialize the I2C clock.
 *
 *  @param[in] i2cHandle  I2C handler pointer.
 */
void HAL_I2C_MspDeInit(I2C_HandleTypeDef* i2cHandle)
{
    UNUSED(i2cHandle);

    /* I2C1 clock disable */
    __HAL_RCC_I2C1_CLK_DISABLE();
}

/** @brief Initialise Sai peripheral callback.
 *
 *  @param[in] hsai  SAI peripheral to initialise.
 */
void HAL_SAI_MspInit(SAI_HandleTypeDef *hsai)
{
    /* SAI1 clock enable */
    __HAL_RCC_SAI1_CLK_ENABLE();

    /* SAI1 */
    if (hsai->Instance == SAI1_Block_A) {
        /** Several peripheral DMA handle pointers point to the same DMA handle.
         *  Be aware that there is only one channel to perform all the requested DMAs.
         */
        __HAL_LINKDMA(hsai, hdmarx, hdma_sai1_a);
        __HAL_LINKDMA(hsai, hdmatx, hdma_sai1_a);
    }
    if (hsai->Instance == SAI1_Block_B) {
        /** Several peripheral DMA handle pointers point to the same DMA handle.
         *  Be aware that there is only one channel to perform all the requested DMAs.
         */
        __HAL_LINKDMA(hsai, hdmarx, hdma_sai1_b);
        __HAL_LINKDMA(hsai, hdmatx, hdma_sai1_b);
    }
}

/** @brief Deinitialise SAI Peripheral callback.
 *
 *  @param[in]  hsai  Sai peripheral to deinitialise.
 */
void HAL_SAI_MspDeInit(SAI_HandleTypeDef *hsai)
{
    /* Deinitialise SAI bloc A */
    if (hsai->Instance == SAI1_Block_A) {
        HAL_DMA_DeInit(hsai->hdmarx);
        HAL_DMA_DeInit(hsai->hdmatx);
    }

    /* Deinitialise SAI block B */
    if (hsai->Instance == SAI1_Block_B) {
        HAL_DMA_DeInit(hsai->hdmarx);
        HAL_DMA_DeInit(hsai->hdmatx);
    }
}

void bsp_audio_set_tx_callback(audio_callback callback)
{
    audio_tx_callback = callback;
}

void bsp_audio_set_rx_callback(audio_callback callback)
{
    audio_rx_callback = callback;
}

void bsp_audio_set_error_callback(audio_error_callback error_callback)
{
    hsai_block_a1.hdmarx->XferCpltCallback = error_callback;
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Init I2C Peripheral.
 *
 */
static void init_i2c(void)
{
    hi2c1.Instance = I2C1;
    hi2c1.Init.Timing = 0x30A0A7FB;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK) {
        Error_Handler();
    }
    /** Configure Analogue filter
     */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK) {
        Error_Handler();
    }
    /** Configure Digital filter
     */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK) {
        Error_Handler();
    }
}

/** @brief deInit I2C Peripheral.
 *
 */
static void deinit_i2c(void)
{
    HAL_I2C_DeInit(&hi2c1);
}

/** @brief Initialise sai peripheral.
 *
 * @param[in] channel       Number of channel the audio stream suport.
 * @param[in] sampling_rate Sampling rate of the audio streaming.
 *
 */
static void init_sai(void)
{
    init_sai_block_a();

    if (HAL_SAI_InitProtocol(&hsai_block_a1, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_16BIT, 2) != HAL_OK) {
        Error_Handler();
    }

    init_sai_block_b();

    if (HAL_SAI_InitProtocol(&hsai_block_b1, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_16BIT, 2) != HAL_OK) {
        Error_Handler();
    }
}

/** @brief deInit SAI Peripheral.
 *
 */
static void deinit_sai(void)
{
    HAL_SAI_DMAStop(&hsai_block_b1);
    HAL_SAI_DMAStop(&hsai_block_a1);
}

/** @brief Initialise sai peripheral bloc a.
 *
 */
static void init_sai_block_a(void)
{
    hsai_block_a1.Instance = SAI1_Block_A;
    hsai_block_a1.Init.AudioMode = SAI_MODESLAVE_RX;
    hsai_block_a1.Init.Synchro = SAI_SYNCHRONOUS;
    hsai_block_a1.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
    hsai_block_a1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
    hsai_block_a1.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
    hsai_block_a1.Init.CompandingMode = SAI_NOCOMPANDING;
    hsai_block_a1.Init.TriState = SAI_OUTPUT_NOTRELEASED;
}

/** @brief Initialise sai peripheral bloc b.
 *
 * @param[in] channel       Number of channel the audio stream suport.
 * @param[in] sampling_rate Sampling rate of the audio streaming.
 */
static void init_sai_block_b(void)
{
    hsai_block_b1.Instance = SAI1_Block_B;
    hsai_block_b1.Init.AudioMode = SAI_MODEMASTER_TX;
    hsai_block_b1.Init.Synchro = SAI_ASYNCHRONOUS;
    hsai_block_b1.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
    hsai_block_b1.Init.NoDivider = SAI_MASTERDIVIDER_ENABLE;
    hsai_block_b1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
    hsai_block_b1.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
    hsai_block_b1.Init.CompandingMode = SAI_NOCOMPANDING;
    hsai_block_b1.Init.TriState = SAI_OUTPUT_NOTRELEASED;
}

static void init_dma(void)
{
    DMA_HandleTypeDef *dma_b = hsai_block_b1.hdmatx;

    if (audio_rx_callback != NULL) {
        /* Set the SAI RxDMA transfer complete callback */
        hsai_block_a1.hdmarx->XferCpltCallback = sai_receive_dma_complete_callback;
    }

    if (audio_tx_callback != NULL) {
        /* Set the SAI TxDMA transfer complete callback */
        hsai_block_b1.hdmatx->XferCpltCallback = sai_transmit_dma_complete_callback;
    }

    /* kickstart SAI */
    dma_transmit(hsai_block_b1.hdmatx, (uint32_t)NULL, (uint32_t)&hsai_block_b1.Instance->DR, AUDIO_CHANNELS_STEREO);

    /* Test DMA interrupt flag SAI Section B.*/
    if (hsai_block_b1.hdmarx->Instance->CCR & DMA_IT_TC) {
        __HAL_DMA_DISABLE_IT(dma_b, DMA_IT_TC);
    }

    if (hsai_block_b1.hdmarx->Instance->CCR & DMA_IT_TE) {
         __HAL_DMA_DISABLE_IT(dma_b, DMA_IT_TE);
    }

    /* Enable SAI peripheral */
    __HAL_SAI_ENABLE(&hsai_block_b1);

    /* Enable SAI DMA Request */
    hsai_block_b1.Instance->CR1 |= SAI_xCR1_DMAEN;
}

/** @brief Transmit data throught dma.
 *
 * @param[in] hdma        Dma peripheral pointer adress.
 * @param[in] src_address Source adress of the memory/peripheral.
 * @param[in] dst_address Destination adress of the memory/peripheral.
 * @param[in] data_length Length of the data to transmit.
 */
static void dma_transmit(DMA_HandleTypeDef *hdma, uint32_t src_address, uint32_t dst_address, uint32_t data_length)
{
    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);

    /* Clear all flags */
    hdma->DmaBaseAddress->IFCR = (DMA_ISR_GIF1 << (hdma->ChannelIndex & 0x1FU));

    /* Configure DMA Channel data length */
    hdma->Instance->CNDTR = data_length;

    /* Configure DMA Channel destination address */
    hdma->Instance->CPAR = dst_address;

    /* Configure DMA Channel source address */
    hdma->Instance->CMAR = src_address;

    /* Enable the Peripheral */
    __HAL_DMA_ENABLE(hdma);
}

/** @brief Receive data throught dma.
 *
 * @param[in] hdma        Dma peripheral pointer adress.
 * @param[in] src_address Source adress of the memory/peripheral.
 * @param[in] dst_address Destination adress of the memory/peripheral.
 * @param[in] data_length Length of the data to transmit.
 */
static void dma_receive(DMA_HandleTypeDef *hdma, uint32_t src_address, uint32_t dst_address, uint32_t data_length)
{
    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);

    /* Clear all flags */
    hdma->DmaBaseAddress->IFCR = (DMA_ISR_GIF1 << (hdma->ChannelIndex & 0x1FU));

    /* Configure DMA Channel data length */
    hdma->Instance->CNDTR = data_length;

    /* Configure DMA Channel destination address */
    hdma->Instance->CPAR = src_address;

    /* Configure DMA Channel source address */
    hdma->Instance->CMAR = dst_address;

    /* Enable the Peripheral */
    __HAL_DMA_ENABLE(hdma);
}

/** @brief Callback when all data on dma peripheral are transmit.
 *
 * @param[in] hdma  Dma peripheral pointer adress.
 */
static void sai_transmit_dma_complete_callback(DMA_HandleTypeDef *hdma)
{
    /* Disable SAI Tx DMA Request */
    hdma->Instance->CCR &= (uint32_t)(~SAI_xCR1_DMAEN);

    audio_tx_callback();
}

/** @brief Callback when all data on dma peripheral are receive.
 *
 * @param[in] hdma  Dma peripheral pointer adress.
 */
static void sai_receive_dma_complete_callback(DMA_HandleTypeDef *hdma)
{
    /* Disable SAI Tx DMA Request */
    hdma->Instance->CCR &= (uint32_t)(~SAI_xCR1_DMAEN);

    audio_rx_callback();
}

/** @brief Transmit data to the dma and set the callback.
 *
 *  @param[in]  hsai    Sai peripheral to deinitialise.
 *  @param[in]  p_Data  Data pointer to transmit.
 *  @param[in]  size    Size of data to transmit.
 */
static void audio_sai_transmit(SAI_HandleTypeDef *hsai, uint8_t *p_data, uint16_t size)
{
    if (hsai_block_b1.hdmatx->XferCpltCallback != NULL) {
        dma_transmit(hsai->hdmatx, (uint32_t)p_data, (uint32_t)&hsai->Instance->DR, size);

        /* Enable transfert complete and Transfert error */
        __HAL_DMA_ENABLE_IT(hsai->hdmatx, DMA_IT_TC);

        /* Enable SAI DMA Request */
        hsai->Instance->CR1 |= SAI_xCR1_DMAEN;

        /* Enable SAI peripheral */
        __HAL_SAI_ENABLE(hsai);
    }
}

/** @brief Receive data from dma and set the callback.
 *
 *  @param[in]  hsai    Sai peripheral to deinitialise.
 *  @param[in]  p_data  Data pointer to transmit.
 *  @param[in]  size    Size of data to transmit.
 */
static void audio_sai_receive(SAI_HandleTypeDef *hsai, uint8_t *p_data, uint16_t size)
{
    if (hsai_block_a1.hdmarx->XferCpltCallback != NULL) {
        dma_receive(hsai->hdmarx, (uint32_t)&hsai->Instance->DR, (uint32_t)p_data, size);

        /* Enable transfert complete and Transfert error */
        __HAL_DMA_ENABLE_IT(hsai->hdmarx, DMA_IT_TC);

        /* Enable SAI DMA Request */
        hsai->Instance->CR1 |= SAI_xCR1_DMAEN;

        /* Enable SAI peripheral */
        __HAL_SAI_ENABLE(hsai);
    }
}