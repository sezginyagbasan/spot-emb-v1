/** @file  bsp_def.h
 *  @brief This defines MCU pinout and other control definitions.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_DEF_H_
#define BSP_DEF_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "stm32g4xx_hal.h"
#include "stm32g4xx_ll_bus.h"
#include "stm32g4xx_ll_dma.h"
#include "stm32g4xx_ll_gpio.h"
#include "stm32g4xx_ll_pwr.h"
#include "stm32g4xx_ll_spi.h"
#include "uwb_error.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

/* CONSTANTS ******************************************************************/
/* Buttons */
#define BTN0_PORT            GPIOB
#define BTN0_PIN             GPIO_PIN_8

#define BTN1_PORT            GPIOC
#define BTN1_PIN             GPIO_PIN_3

/* LEDs */
#define LED0_PORT            GPIOC
#define LED0_PIN             GPIO_PIN_5

#define LED1_PORT            GPIOB
#define LED1_PIN             GPIO_PIN_10

#define LED2_PORT            GPIOC
#define LED2_PIN             GPIO_PIN_6

/* External Clock Select */
#define PLL_SEL_PORT         GPIOA
#define PLL_SEL_PIN          GPIO_PIN_9
#define XTAL_CLK_PORT        GPIOA
#define XTAL_CLK_PIN         GPIO_PIN_2

#define XTAL_CLK_FREQ        32768

/* USB */
#define USB_DETECT_PORT      GPIOA
#define USB_DETECT_PIN       GPIO_PIN_0
#define USB_IRQ_NVIC         EXTI0_IRQn
#define USB_IRQ_NVIC_PRIO    14

/* Power Management */
#define VOLTAGE_SEL_PORT     GPIOD
#define VOLTAGE_SEL_PIN      GPIO_PIN_2

/* Radio */
#define RADIO_SHUTDOWN_PORT  GPIOB
#define RADIO_SHUTDOWN_PIN   GPIO_PIN_1

#define RADIO_RESET_PORT     GPIOB
#define RADIO_RESET_PIN      GPIO_PIN_0

#define RADIO_IRQ_PORT       GPIOB
#define RADIO_IRQ_PIN        GPIO_PIN_2
#define RADIO_IRQ_NVIC       EXTI2_IRQn
#define RADIO_IRQ_NVIC_PRIO  2

#define RADIO_SCK_PORT       GPIOB
#define RADIO_SCK_PIN        GPIO_PIN_13
#define RADIO_SCK_AF         GPIO_AF5_SPI2

#define RADIO_MOSI_PORT      GPIOB
#define RADIO_MOSI_PIN       GPIO_PIN_15
#define RADIO_MOSI_AF        GPIO_AF5_SPI2

#define RADIO_MISO_PORT      GPIOB
#define RADIO_MISO_PIN       GPIO_PIN_14
#define RADIO_MISO_AF        GPIO_AF5_SPI2

#define RADIO_CS_PORT        GPIOB
#define RADIO_CS_PIN         GPIO_PIN_12

#define RADIO_DEBUG_EN_PORT  GPIOB
#define RADIO_DEBUG_EN_PIN   GPIO_PIN_9

#define EXP_LPUART_IRQ_NVIC_PRIO 14

#define RADIO_SPI_TIMEOUT_MS 10
#define LOG_UART_TIMEOUT_MS  10
#define TIMER_IT_PRIORITY    2

/* Expansion IO */
#define EXP_SCK_PORT     GPIOA
#define EXP_SCK_PIN      GPIO_PIN_5
#define EXP_SCK_AF       GPIO_AF5_SPI1

#define EXP_MOSI_PORT    GPIOA
#define EXP_MOSI_PIN     GPIO_PIN_7
#define EXP_MOSI_AF      GPIO_AF5_SPI1

#define EXP_MISO_PORT    GPIOA
#define EXP_MISO_PIN     GPIO_PIN_6
#define EXP_MISO_AF      GPIO_AF5_SPI1

#define EXP_CS_PORT      GPIOA
#define EXP_CS_PIN       GPIO_PIN_4

#define EXP_PC7_PORT     GPIOC
#define EXP_PC7_PIN      GPIO_PIN_7

#define EXP_PC8_PORT     GPIOC
#define EXP_PC8_PIN      GPIO_PIN_8

#define EXP_PC10_PORT    GPIOC
#define EXP_PC10_PIN     GPIO_PIN_10

#define EXP_PC11_PORT    GPIOC
#define EXP_PC11_PIN     GPIO_PIN_11

#define EXP_PA3_PORT     GPIOA
#define EXP_PA3_PIN      GPIO_PIN_3

#define EXP_UART_RX_PORT GPIOC
#define EXP_UART_RX_PIN  GPIO_PIN_0

#define EXP_UART_TX_PORT GPIOC
#define EXP_UART_tX_PIN  GPIO_PIN_1

#define EXP_I2C_SCL_PORT GPIOA
#define EXP_I2C_SCL_PIN  GPIO_PIN_15

#define EXP_I2C_SDA_PORT GPIOB
#define EXP_I2C_SDA_PIN  GPIO_PIN_7

#define SAI_IN_CLK_PORT GPIOC
#define SAI_IN_CLK      GPIO_PIN_9

#define MCO_CLK_PORT    GPIOA
#define MCO_CLK         GPIO_PIN_8

#define I2C_SDA_PORT    GPIOB
#define I2C_SDA         GPIO_PIN_7

#define I2C_SCL_PORT    GPIOA
#define I2C_SCL         GPIO_PIN_15

#define SAI1_SD_A_PORT  GPIOA
#define SAI1_SD_A       GPIO_PIN_10

#define SAI1_SCK_PORT   GPIOB
#define SAI1_SCK        GPIO_PIN_3

#define SAI1_MCLK_PORT  GPIOB
#define SAI1_MCLK       GPIO_PIN_4

#define SAI1_SD_B_PORT  GPIOB
#define SAI1_SD_B       GPIO_PIN_5

#define SAI1_FS_PORT    GPIOB
#define SAI1_FS         GPIO_PIN_6

#define EXP_SPI_TIMEOUT_MS 10

/* Priority */
#define RADIO_DMA_IRQ_NVIC_PRIO   RADIO_IRQ_NVIC_PRIO + 1
#define EXP_SPI_IRQ_NVIC_PRIO     3
#define EXP_SPI_DMA_IRQ_NVIC_PRIO 3
#define EXP_SPI_CS_IRQ_NVIC_PRIO  0
#define PENDSV_NVIC_PRIO          15

/* Wait latency clock frequency limit*/
#define RANGE1_0WS_FREQ 20000000
#define RANGE1_1WS_FREQ 40000000
#define RANGE1_2WS_FREQ 60000000
#define RANGE1_3WS_FREQ 80000000
#define RANGE1_4WS_FREQ 100000000
#define RANGE1_5WS_FREQ 120000000
#define RANGE1_6WS_FREQ 140000000
#define RANGE1_7WS_FREQ 160000000
#define RANGE1_8WS_FREQ 170000000

#define RANGE2_0WS_FREQ 80000000
#define RANGE2_1WS_FREQ 16000000
#define RANGE2_2WS_FREQ 26000000

/* Voltage scale clock frequency limit */
#define RANGE2_LOWPOW_LIMIT 26000000
#define RANGE1_BOOST1_LIMIT 150000000
#define RANGE1_BOOST0_LIMIT 170000000

/* TYPES **********************************************************************/
/** @brief Board's LED enumeration.
 */
typedef enum bsp_led {
    LED0,
    LED1,
    LED2
} bsp_led_t;

/** @brief Board's debug io enumeration.
 */
typedef enum bsp_dbg {
    DBG0,
    DBG1,
    DBG2,
    DBG3
} bsp_dbg_t;

/** @brief Board's debug pins enumeration.
 */
typedef enum bsp_debug_pin {
    DEBUG_PC7,
    DEBUG_PC8,
    DEBUG_PC10,
    DEBUG_PC11
} bsp_debug_pin_t;

/** @brief Board's User button enumeration.
 */
typedef enum bsp_btn {
    BTN1,
    BTN2,
} bsp_btn_t;

/** @brief Board's VDD enumeration.
 */
typedef enum bsp_vdd {
    VDD_1V8,
    VDD_3V3
} bsp_vdd_t;

/** @brief Board's supported clock frequency.
 */
typedef enum bsp_clk_freq {
    CLK_163_84MHZ = 163840000,
    CLK_81_92MHZ  = 81920000,
    CLK_40_96MHZ  = 40960000,
    CLK_25_20MHZ  = 25200000,
    CLK_20_48MHZ  = 20480000
} bsp_clk_freq_t;

typedef enum spi_prescaler {
    SPI_PRESCALER_2   = SPI_BAUDRATEPRESCALER_2,
    SPI_PRESCALER_4   = SPI_BAUDRATEPRESCALER_4,
    SPI_PRESCALER_8   = SPI_BAUDRATEPRESCALER_8,
    SPI_PRESCALER_16  = SPI_BAUDRATEPRESCALER_16,
    SPI_PRESCALER_32  = SPI_BAUDRATEPRESCALER_32,
    SPI_PRESCALER_64  = SPI_BAUDRATEPRESCALER_64,
    SPI_PRESCALER_128 = SPI_BAUDRATEPRESCALER_128,
    SPI_PRESCALER_256 = SPI_BAUDRATEPRESCALER_256
} spi_prescaler_t;

/** @brief Sampling rate for the MAX98091
 */
typedef enum bsp_audio_sampling_rate {
    AUDIO_48KHZ = SAI_AUDIO_FREQUENCY_48K,
    AUDIO_32KHZ = SAI_AUDIO_FREQUENCY_32K,
    AUDIO_24KHZ = SAI_AUDIO_FREQUENCY_48K/2,
    AUDIO_16KHZ = SAI_AUDIO_FREQUENCY_16K,
    AUDIO_12KHZ = SAI_AUDIO_FREQUENCY_48K/4,
    AUDIO_8KHZ  = SAI_AUDIO_FREQUENCY_8K,
} bsp_audio_sampling_rate_t;

/** @brief Number of channel for audio application.
 */
typedef enum bsp_audio_num_channel {
    AUDIO_CHANNELS_MONO = 1,
    AUDIO_CHANNELS_STEREO = 2
} bsp_audio_num_channel_t;

#ifdef __cplusplus
}
#endif

#endif /* BSP_DEF_H_ */
