/** @file  bsp_usb.h
 *  @brief This module controls USB features of the SPARK EVK board.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef BSP_USB_H_
#define BSP_USB_H_

#ifdef __cplusplus
extern
 {
#endif

/* INCLUDES *******************************************************************/
#include "bsp_def.h"
#include "bsp_usb.h"
#include "usbd_cdc_if.h"

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Set the callback function for data received via communication device class.
 *
 *  @param[in] usb_cdc_receive_cb  Pointer to the callback function.
 */
void bsp_usb_cdc_init_rx_cb(int8_t (* usb_cdc_receive_cb)(uint8_t *buf, uint32_t *len));

/** @brief Set the callback function for data transmitted via communication device class.
 *
 *  @param[in] usb_cdc_receive_cb  Pointer to the callback function.
 */
void bsp_usb_cdc_init_tx_cb(int8_t (* usb_cdc_transmit_cb)(uint8_t *buf, uint32_t *len));

/** @brief Set the callback function for usb initialization complete.
 *
 *  @param[in]  usb_cdc_init_cb  Pointer to the callback function.
 */
void bsp_usb_cdc_init_init_cb(int8_t (*usb_cdc_init_cb)(void));

/** @brief Initialize the USB in communication device class mode.
 *
 *  @param[out] err  Pointer that receives an error code.
 */
void bsp_usb_cdc_init(uwb_err *err);

/** @brief De-initialize the USB from communication device class mode.
 *
 *  @param[out] err  Pointer that receives an error code.
 */
void bsp_cdc_usb_deinit(uwb_err *err);

/** @brief Init the peripheral but only if there is a USB detected
 *
 *  @retval True  USB is connected.
 */
bool bsp_cdc_usb_connect(uwb_err *err);

/** @brief De-init the peripheral but only if there is no USB detected
 *
 *  @retval True  USB is disconnected.
 */
bool bsp_cdc_usb_disconnect(uwb_err *err);

/** @brief Check if a powered USB cable is connected to the board.
 *
 *  @retval True   USB is connected.
 *  @retval False  USB is not connected.
 */
bool bsp_is_usb_detected(void);

/** @brief Send character on USB.
 *
 *  @param[out] c    Character to write.
 *  @param[out] err  Pointer that receives an error code.
 */
uwb_err bsp_usb_cdc_putc(uint8_t c);

/** @brief Send buffer on USB.
 *
 *  @param[out] buf      Buffer to write.
 *  @param[out] buf_len  Buffer length.
 */
uwb_err bsp_usb_cdc_send_buf(uint8_t *buf, uint16_t buf_len);

/** @brief Disable USB interrupt.
 */
void bsp_usb_enter_critical(void);

/** @brief Enable USB interrupt.
 */
void bsp_usb_exit_critical(void);

#ifdef __cplusplus
}
#endif


#endif /* BSP_USB_H_ */
