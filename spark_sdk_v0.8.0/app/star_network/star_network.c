/** @file  star_network.c
 *  @brief This is a basic example on how to use a SPARK star network.
 *         This example uses the SPARK Wireless Protocol Stack.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <stdint.h>
#include "bsp.h"
#include "wps.h"
#include "wps_cfg.h"

/* CONSTANTS ******************************************************************/
#define PRINTF_BUF_SIZE 64
#define BUTTON_PRESSED  0xff

/* MACROS *********************************************************************/
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))

/* TYPES **********************************************************************/
typedef enum role {
    COORDINATOR = 0,
    NODE1,
    NODE2
} role_t;

/* PRIVATE GLOBALS ************************************************************/
static wps_t wps = {0};
static wps_connection_t coordinator_to_node1_conn = {0};
static wps_connection_t coordinator_to_node2_conn = {0};
static wps_connection_t node1_to_coordinator_conn = {0};
static wps_connection_t node2_to_coordinator_conn = {0};

static uint8_t spi_rx_buffer[SPI_BUFFER_SIZE] = {0};
static uint8_t spi_tx_buffer[SPI_BUFFER_SIZE] = {0};

static uint32_t timeslot_us[] = SCHEDULE_TIMING;

static xlayer_t local1_fifo[XLAYER_SIZE]                        = {0};
static uint8_t  local1_frame[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE]  = {0};

static xlayer_t local2_fifo[XLAYER_SIZE]                        = {0};
static uint8_t  local2_frame[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE]  = {0};

static xlayer_t remote1_fifo[XLAYER_SIZE]                       = {0};
static uint8_t  remote1_frame[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE] = {0};

static xlayer_t remote2_fifo[XLAYER_SIZE]                       = {0};
static uint8_t  remote2_frame[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE] = {0};

static int32_t coordinator1_timeslot[]  = COORDINATOR_TO_NODE1_TIMESLOTS;
static int32_t coordinator2_timeslot[]  = COORDINATOR_TO_NODE2_TIMESLOTS;
static int32_t node1_timeslot[]         = NODE1_TO_COORDINATOR_TIMESLOTS;
static int32_t node2_timeslot[]         = NODE2_TO_COORDINATOR_TIMESLOTS;

static uint32_t channel_sequence[] = CHANNEL_SEQUENCE;

static timeslot_t timeslot[ARRAY_SIZE(timeslot_us)] = {0};
static wps_callback_inst_t callback[XLAYER_SIZE];

#if ((defined(STAR_ROLE)) && (STAR_ROLE == 0U))
    static role_t star_role = COORDINATOR;
#elif ((defined(STAR_ROLE)) && (STAR_ROLE == 1U))
    static role_t star_role = NODE1;
#elif ((defined(STAR_ROLE)) && (STAR_ROLE == 2U))
    static role_t star_role = NODE2;
#else
    #error Please select a role with compile options "STAR_ROLE". 0:Coordinator, 1:Node1, 2:Node2.
#endif

/* PRIVATE FUNCTION PROTOTYPE *************************************************/
static void coordinator_send(void);
static void node_send(wps_connection_t * conn);
static void conn_tx_ack_callback(wps_connection_t *conn);
static void conn_tx_nack_callback(wps_connection_t *conn);
static void conn_rx_callback(wps_connection_t *conn);
static void radio_irq_cb(void);
static void radio_dma_spi_cb(void);
static void radio_callback_cb(void);
static void usb_gpio_callback(void);
static void usb_printf(const char *fmt, ...);
static void app_error_handler(uwb_err err, uint8_t *file, uint16_t line);

/* PUBLIC FUNCTIONS ***********************************************************/
int main(void)
{
    wps_connection_t *local_conn;

    /* Initialize HAL, GPIO, SPI & USB peripheral */
    bsp_init();
    bsp_set_usb_detect_callback(usb_gpio_callback);
    uwb_error_init(app_error_handler);

    /* Handle USB connection on init */
    if (bsp_is_usb_detected()) {
        uwb_err err;
        bsp_cdc_usb_connect(&err);
        bsp_delay_ms(1000);
        ERROR_CHECK(err);
    }

    /* Set radio related callbacks to WPS provided callbacks */
    bsp_set_radio_irq_callback(radio_irq_cb);
    bsp_set_radio_dma_rx_callback(radio_dma_spi_cb);
    bsp_set_pendsv_callback(radio_callback_cb);

    /* Disable radio IRQ when initializing */
    bsp_radio_disable_irq_it();

    wps_critical_cfg_t critical_cfg = {
        bsp_enter_critical,
        bsp_exit_critical
    };
    wps_init_critical_section(&critical_cfg);

    /* Radio instance parameters */
    wps_radio_t radio_instance = {
        /* Radio hardware abstraction layer parameters */
        .radio_hal = (radio_hal_t) {
            .set_shutdown_pin   = bsp_radio_set_shutdown_pin,
            .reset_shutdown_pin = bsp_radio_reset_shutdown_pin,
            .set_reset_pin      = bsp_radio_set_reset_pin,
            .reset_reset_pin    = bsp_radio_reset_reset_pin,
            .read_irq_pin       = bsp_radio_read_irq_pin,
            .set_cs             = bsp_radio_spi_set_cs,
            .reset_cs           = bsp_radio_spi_reset_cs,
            .write              = bsp_radio_spi_write_blocking,
            .read               = bsp_radio_spi_read_blocking,
            .delay_ms           = bsp_delay_ms
        },
        /* Radio advance hardware abstraction layer parameters */
        .radio_hal_adv = (radio_hal_adv_t) {
            .transfer_full_duplex_blocking     = bsp_radio_spi_transfer_full_duplex_blocking,
            .transfer_full_duplex_non_blocking = bsp_radio_spi_transfer_full_duplex_non_blocking,
            .is_spi_busy                       = bsp_radio_is_spi_busy,
            .context_switch                    = bsp_radio_context_switch,
            .disable_radio_irq                 = bsp_radio_disable_irq_it,
            .enable_radio_irq                  = bsp_radio_enable_irq_it,
            .disable_radio_dma_irq             = bsp_radio_disable_irq_it,
            .enable_radio_dma_irq              = bsp_radio_enable_irq_it
        },
         /* SPI parameters */
        .spi_buffer_size = SPI_BUFFER_SIZE,
        .spi_rx_buffer   = spi_rx_buffer,
        .spi_tx_buffer   = spi_tx_buffer,
        .irq_polarity    = IRQ_ACTIVE_HIGH,
    };

    wps_error_t error = WPS_NO_ERROR;

    /* Initialize the radio instance and apply link configuration */
    wps_radio_init(&radio_instance, &error);
    wps_radio_calibrate(&radio_instance, &error);
    ERROR_CHECK(error);

    /* Initialize schedule timing */
    wps_config_network_schedule(&wps, timeslot_us, timeslot, ARRAY_SIZE(timeslot_us));

    uint16_t local_address = (star_role == COORDINATOR) ? WPS_DEFAULT_COORD_ADDR :
                             (star_role == NODE1) ? WPS_DEFAULT_NODE1_ADDR : WPS_DEFAULT_NODE2_ADDR;

    /* Initialize the node/xlayer for coordinator/node */
    wps_node_cfg_t node_cfg = {
        .role           = (star_role == COORDINATOR) ? NETWORK_COORDINATOR: NETWORK_NODE,
        .preamble_len   = WPS_DEFAULT_PREAMBLE_LEN,
        .sleep_lvl      = WPS_DEFAULT_SLEEP_LEVEL,
        .crc_polynomial = WPS_DEFAULT_CRC,
        .local_address  = local_address,
        .syncword       = sync_word_table[WPS_DEFAULT_SYNC_TABLE_IDX],
    };

    wps_node_t node;

    wps_config_node(&node, &radio_instance, &node_cfg);

    /* Initialize the wps callback queue for the application */
    wps_init_callback_queue(&wps, callback, ARRAY_SIZE(callback), bsp_radio_callback_context_switch);

    /* Initialize SPARK channel and power settings */
     tx_power_settings_t tx_power_settings = {
            .pulse_count = WPS_DEFAULT_PULSE_COUNT,
            .pulse_width = WPS_DEFAULT_PULSE_WIDTH,
            .tx_gain     = WPS_DEFAULT_TX_GAIN,
    };

    channel_cfg_t channel_cfg = {
            .power              = &tx_power_settings,
            .freq_shift_enable  = WPS_DEFAULT_FREQ_SHIFT,
            .pulse_spacing      = WPS_DEFAULT_PULSE_SPACING,
            .pulse_start_pos    = WPS_DEFAULT_PULSE_START_POS,
            .rdn_phase_enable   = WPS_DEFAULT_RND_PHASE,
            .frequency          = WPS_DEFAULT_TS0_FREQ,
            .integgain          = 0,
    };

    /* Initialize multiple connections depending on the star network role */
    if ((star_role == COORDINATOR) || (star_role == NODE1)) {
        /* Initialize connections needed between coordinator and node 1 */
        wps_connection_cfg_t coordinator_to_node1_cfg = {
            .source_address         = WPS_DEFAULT_COORD_ADDR,
            .destination_address    = WPS_DEFAULT_NODE1_ADDR,
            .fifo_buffer_size       = XLAYER_SIZE,
            .header_length          = WPS_RADIO_HEADER_SIZE,
            .frame_length           = WPS_DEFAULT_FRAME_SIZE,
            .send_ack_callback_t    = conn_tx_ack_callback,
            .send_nack_callback_t   = conn_tx_nack_callback,
            .receive_callback_t     = conn_rx_callback,
            .get_tick_quarter_ms    = bsp_get_free_running_timer_tick_ms,
        };
        wps_connection_cfg_t node1_to_coordinator_cfg = {
            .source_address         = WPS_DEFAULT_NODE1_ADDR,
            .destination_address    = WPS_DEFAULT_COORD_ADDR,
            .fifo_buffer_size       = XLAYER_SIZE,
            .header_length          = WPS_RADIO_HEADER_SIZE,
            .frame_length           = WPS_DEFAULT_FRAME_SIZE,
            .send_ack_callback_t    = conn_tx_ack_callback,
            .send_nack_callback_t   = conn_tx_nack_callback,
            .receive_callback_t     = conn_rx_callback,
            .get_tick_quarter_ms    = bsp_get_free_running_timer_tick_ms,
        };

        if (star_role == COORDINATOR) {
            /* Set local node configuration */
            coordinator_to_node1_cfg.fifo_buffer  = local1_fifo;
            coordinator_to_node1_cfg.frame_buffer = &(local1_frame[0][0]);

            /* Set remote node configuration */
            node1_to_coordinator_cfg.fifo_buffer  = remote1_fifo;
            node1_to_coordinator_cfg.frame_buffer = &(remote1_frame[0][0]);

        } else {
            local_conn = &node1_to_coordinator_conn;

            /* Set local node configuration */
            node1_to_coordinator_cfg.fifo_buffer  = local1_fifo;
            node1_to_coordinator_cfg.frame_buffer = &(local1_frame[0][0]);

            /* Set remote node configuration */
            coordinator_to_node1_cfg.fifo_buffer  = remote1_fifo;
            coordinator_to_node1_cfg.frame_buffer = &(remote1_frame[0][0]);
        }

        wps_create_connection(&coordinator_to_node1_conn, &node, &coordinator_to_node1_cfg);
        wps_create_connection(&node1_to_coordinator_conn, &node, &node1_to_coordinator_cfg);

        /* Enable stop and wait */
        wps_connection_enable_stop_and_wait_arq(&coordinator_to_node1_conn, local_address, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);
        wps_connection_enable_stop_and_wait_arq(&node1_to_coordinator_conn, local_address, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);

        /* Enable acknowledge */
        wps_connection_enable_ack(&coordinator_to_node1_conn);
        wps_connection_enable_ack(&node1_to_coordinator_conn);

        /* Enable auto synchronization */
        wps_connection_enable_auto_sync(&coordinator_to_node1_conn);
        wps_connection_enable_auto_sync(&node1_to_coordinator_conn);

        /* Disable clear channel access */
        wps_connection_disable_cca(&coordinator_to_node1_conn);
        wps_connection_disable_cca(&node1_to_coordinator_conn);

         /* Setup paramameters in connection */
        wps_connection_config_frame(&coordinator_to_node1_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);
        wps_connection_config_frame(&node1_to_coordinator_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);

        /* Timeslot allocation (Main/Auto-reply connection slot) */
        wps_connection_set_timeslot(&coordinator_to_node1_conn, &wps, coordinator1_timeslot, ARRAY_SIZE(coordinator1_timeslot));
        wps_connection_set_timeslot(&node1_to_coordinator_conn, &wps, node1_timeslot, ARRAY_SIZE(node1_timeslot));

        /* Use the frequency 6758.4 Mhz for channel 0 */
        channel_cfg.frequency = WPS_DEFAULT_TS0_FREQ;
        wps_connection_config_channel(&coordinator_to_node1_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);
        wps_connection_config_channel(&node1_to_coordinator_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);

        /* Use the frequency 7577.6 Mhz for channel 1 */
        channel_cfg.frequency = WPS_DEFAULT_TS1_FREQ;
        wps_connection_config_channel(&coordinator_to_node1_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
        wps_connection_config_channel(&node1_to_coordinator_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
    }

    if ((star_role == COORDINATOR) || (star_role == NODE2)) {
        /* Initialize connections needed between coordinator and node 2 */
        wps_connection_cfg_t coordinator_to_node2_cfg = {
            .source_address         = WPS_DEFAULT_COORD_ADDR,
            .destination_address    = WPS_DEFAULT_NODE2_ADDR,
            .fifo_buffer_size       = XLAYER_SIZE,
            .header_length          = WPS_RADIO_HEADER_SIZE,
            .frame_length           = WPS_DEFAULT_FRAME_SIZE,
            .send_ack_callback_t    = conn_tx_ack_callback,
            .send_nack_callback_t   = conn_tx_nack_callback,
            .receive_callback_t     = conn_rx_callback,
            .get_tick_quarter_ms    = bsp_get_free_running_timer_tick_ms,
        };

        wps_connection_cfg_t node2_to_coordinator_cfg = {
            .source_address         = WPS_DEFAULT_NODE2_ADDR,
            .destination_address    = WPS_DEFAULT_COORD_ADDR,
            .fifo_buffer_size       = XLAYER_SIZE,
            .header_length          = WPS_RADIO_HEADER_SIZE,
            .frame_length           = WPS_DEFAULT_FRAME_SIZE,
            .send_ack_callback_t    = conn_tx_ack_callback,
            .send_nack_callback_t   = conn_tx_nack_callback,
            .receive_callback_t     = conn_rx_callback,
            .get_tick_quarter_ms    = bsp_get_free_running_timer_tick_ms,
        };

        if (star_role == COORDINATOR) {
            /* Set local node configuration */
            coordinator_to_node2_cfg.fifo_buffer  = local2_fifo;
            coordinator_to_node2_cfg.frame_buffer = &(local2_frame[0][0]);

            /* Set remote node configuration */
            node2_to_coordinator_cfg.fifo_buffer  = remote2_fifo;
            node2_to_coordinator_cfg.frame_buffer = &(remote2_frame[0][0]);
        } else {
            local_conn = &node2_to_coordinator_conn;

            /* Set local node configuration */
            node2_to_coordinator_cfg.fifo_buffer  = local2_fifo;
            node2_to_coordinator_cfg.frame_buffer = &(local2_frame[0][0]);

            /* Set remote node configuration */
            coordinator_to_node2_cfg.fifo_buffer  = remote2_fifo;
            coordinator_to_node2_cfg.frame_buffer = &(remote2_frame[0][0]);
        }
        /* Set local node configuration */
        coordinator_to_node2_cfg.fifo_buffer  = local2_fifo;
        coordinator_to_node2_cfg.frame_buffer = &(local2_frame[0][0]);

        /* Set remote node configuration */
        node2_to_coordinator_cfg.fifo_buffer  = remote2_fifo;
        node2_to_coordinator_cfg.frame_buffer = &(remote2_frame[0][0]);
        wps_create_connection(&coordinator_to_node2_conn, &node, &coordinator_to_node2_cfg);
        wps_create_connection(&node2_to_coordinator_conn, &node, &node2_to_coordinator_cfg);

        /* Enable stop and wait */
        wps_connection_enable_stop_and_wait_arq(&coordinator_to_node2_conn, local_address, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);
        wps_connection_enable_stop_and_wait_arq(&node2_to_coordinator_conn, local_address, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);

        /* Enable acknowledge */
        wps_connection_enable_ack(&coordinator_to_node2_conn);
        wps_connection_enable_ack(&node2_to_coordinator_conn);

        /* Enable auto synchronization */
        wps_connection_enable_auto_sync(&coordinator_to_node2_conn);
        wps_connection_enable_auto_sync(&node2_to_coordinator_conn);

        /* Disable clear channel access */
        wps_connection_disable_cca(&coordinator_to_node2_conn);
        wps_connection_disable_cca(&node2_to_coordinator_conn);

        /* Setup paramameters in connection */
        wps_connection_config_frame(&coordinator_to_node2_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);
        wps_connection_config_frame(&node2_to_coordinator_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);

         /* Timeslot allocation (Main/Auto-reply connection slot) */
        wps_connection_set_timeslot(&coordinator_to_node2_conn, &wps, coordinator2_timeslot, ARRAY_SIZE(coordinator2_timeslot));
        wps_connection_set_timeslot(&node2_to_coordinator_conn, &wps, node2_timeslot, ARRAY_SIZE(node2_timeslot));

        /* Use the frequency 6758.4 Mhz for channel 0 */
        channel_cfg.frequency = WPS_DEFAULT_TS0_FREQ;
        wps_connection_config_channel(&coordinator_to_node2_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);
        wps_connection_config_channel(&node2_to_coordinator_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);

        /* Use the frequency 7577.6 Mhz for channel 1 */
        channel_cfg.frequency = WPS_DEFAULT_TS1_FREQ;
        wps_connection_config_channel(&coordinator_to_node2_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
        wps_connection_config_channel(&node2_to_coordinator_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
    }

    /* Init the WPS */
    wps_init(&wps, &node);
    wps_set_coordinator_address(&wps, WPS_DEFAULT_COORD_ADDR);
    wps_config_network_channel_sequence(&wps, channel_sequence, ARRAY_SIZE(channel_sequence));
    wps_disable_concurrency(&wps);
    wps_disable_fast_sync(&wps);

    /* Init the WPS PHY */
    wps_phy_t wps_phy = {
        .lna_imped = true,
        .manuphase = MANUPHASE_PHASE_TRACKING_ENABLE,
    };

    wps_phy_init(&radio_instance, &wps_phy);

    /* Flush RX/TX FIFO */
    uwb_flush_rx_buffer(&radio_instance.radio);
    uwb_flush_tx_buffer(&radio_instance.radio);

    /* Start the SPARK Wireless Protocol Stack */
    wps_connect(&wps);

    while (1) {
        bsp_delay_ms(50);

        if (star_role == COORDINATOR) {
            coordinator_send();
        } else {
            node_send(local_conn);
        }
    }
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Coordinator sends count value to nodes.
 */
static void coordinator_send(void)
{
    static unsigned long inc_node1 = 0;
    static unsigned long inc_node2 = 0;
    uint8_t *hello_world_buf;
    uint8_t payload_size;

    if (wps_get_fifo_free_space(&coordinator_to_node1_conn) > 0) {
        wps_get_free_slot(&coordinator_to_node1_conn, &hello_world_buf);

        if (!bsp_read_btn_state(BTN1)) {
            /* Format the new payload */
            payload_size = snprintf((char *)hello_world_buf, WPS_MAX_PAYLOAD_SIZE, "Hello World %lu \n\r", inc_node1++);

            /* Send the payload through the WPS */
            wps_send(&coordinator_to_node1_conn, hello_world_buf, payload_size);
        } else {
            hello_world_buf[0] = BUTTON_PRESSED;
            wps_send(&coordinator_to_node1_conn, hello_world_buf, 1);
        }
    }

    if (wps_get_fifo_free_space(&coordinator_to_node2_conn) > 0) {
        wps_get_free_slot(&coordinator_to_node2_conn, &hello_world_buf);

        if (!bsp_read_btn_state(BTN2)) {
            payload_size = snprintf((char *)hello_world_buf, WPS_MAX_PAYLOAD_SIZE, "Hello World %lu\n\r", inc_node2++);

            /* Send the payload through the WPS */
            wps_send(&coordinator_to_node2_conn, hello_world_buf, payload_size);
        } else {
            hello_world_buf[0] = BUTTON_PRESSED;

            /* Send the payload through the WPS */
            wps_send(&coordinator_to_node2_conn, hello_world_buf, 1);
        }
    }
}

/** @brief Node sends count value to coordinator
 *
 *  @param[in] conn  Pointer to connection to sends.
 */
static void node_send(wps_connection_t *conn)
{
    static unsigned long i = 0;
    uint8_t *hello_world_buf;
    uint8_t payload_size;

    if (wps_get_fifo_free_space(conn) > 0)  {
        wps_get_free_slot(conn, &hello_world_buf);

        if (!bsp_read_btn_state(BTN1)) {

            /* Format the new payload */
            payload_size = snprintf((char *)hello_world_buf, WPS_MAX_PAYLOAD_SIZE, "Hello World %lu \n\r", i++);

            /* Send the payload through the WPS */
            wps_send(conn, hello_world_buf, payload_size);
        } else {
            hello_world_buf[0] = BUTTON_PRESSED;

            /* Send the payload through the WPS */
            wps_send(conn, hello_world_buf, 1);
        }
    }
}

/** @brief Callback for the coordinator TX acknowledge.
 *
 *  @param[in] conn  Pointer to connection to sends.
 */
static void conn_tx_ack_callback(wps_connection_t *conn)
{
    (void)conn;
}

/** @brief Callback for coordinator TX NACK.
 *
 *  This callback will be called when you expected an
 *  ACK and didn't receive it, or in the case of a connection
 *  without ACK enabled, each time a new frame is sent on
 *  the connection.
 *
 *  @param[in] conn  Pointer to connection to sends.
 */
static void conn_tx_nack_callback(wps_connection_t *conn)
{
    (void)conn;
}

/** @brief Callback for coordinator new frame received.
 *
 *  @param[in] conn  Pointer to connection to sends.
 */
static void conn_rx_callback(wps_connection_t *conn)
{
    wps_rx_frame frame_out;

    /* Get new payload */
    frame_out = wps_read(conn);

    /* Frame received from node 1 */
    if (conn == &node1_to_coordinator_conn) {
        if(frame_out.payload[0] == BUTTON_PRESSED) {
            bsp_led_on(LED0);
        } else {
            bsp_led_off(LED0);
            usb_printf("Received from node1 : %s", frame_out.payload);
        }

    /* Frame received from node 2 */
    } else if (conn == &node2_to_coordinator_conn) {
        if(frame_out.payload[0] == BUTTON_PRESSED) {
            bsp_led_on(LED1);
        } else {
            bsp_led_off(LED1);
            usb_printf("Received from node2 : %s", frame_out.payload);
        }

    /* Frame received from coordinator */
    } else {
        if(frame_out.payload[0] == BUTTON_PRESSED) {
            bsp_led_on(LED0);
        } else {
            bsp_led_off(LED0);
            usb_printf("Received from coordinator : %s", frame_out.payload);
        }
    }

    /* Notify the WPS that the new payload has been read */
    wps_read_done(conn);
}

/** @brief Callback for the radio IRQ.
 *
 *  These functions must absolutely be called here if
 *  the user intends to use the wireless protocol stack.
 */
static void radio_irq_cb(void)
{
    wps_radio_irq(&wps);
    wps_process(&wps);
}

/** @brief Callback for the radio DMA SPI.
 *
 *  These functions must absolutely be called here if
 *  the user intends to use the Wireless Protocol Stack.
 */
static void radio_dma_spi_cb(void)
{
    wps_transfer_complete(&wps);
    wps_process(&wps);
}

/** @brief Software interrupt callback to empty the WPS queue.
 */
static void radio_callback_cb(void)
{
    wps_process_callback(&wps);
}

/** @brief USB line detection callback.
 */
static void usb_gpio_callback(void)
{
    uwb_err err;

    if (bsp_is_usb_detected()) {
        bsp_cdc_usb_connect(&err);
    } else {
        bsp_cdc_usb_disconnect(&err);
    }

    ERROR_CHECK(err);
}

/** @brief Print characters through USB.
 *
 *  @param[in] fmt  Pointer to the character to be printed.
 *  @param[in] ...  Variable argument list.
 */
static void usb_printf(const char *fmt, ...)
{
    char std_buf[PRINTF_BUF_SIZE];
    va_list va;

    va_start(va, fmt);

    vsprintf(std_buf, fmt, va);

    /*
     * Does not handle strings that are not \0-terminated; if given one it may
     * perform an over-read (it could cause a crash if unprotected) (CWE-126).
     */
    bsp_usb_cdc_send_buf((uint8_t *)std_buf, strlen(std_buf));

    va_end(va);
}

/** @brief This function is called if an error occurred.
 *
 *  It should be passed as pointer by uwb_error_init for initialization.
 *
 *  @param[in] err   Error code.
 *  @param[in] file  string of the file name where the error occurred.
 *  @param[in] line  Line in the code where the error occurred.
 */
static void app_error_handler(uwb_err err, uint8_t *file, uint16_t line)
{
    (void)err;
    (void)file;
    (void)line;

    while(1)
    {

    }
}
