################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/SY/STM32CubeIDE/workspace_1.7.0/SPOT-V1/spot-emb-v1/spark_sdk_v0.8.0/core/audio/processing/audio_compression.c \
C:/Users/SY/STM32CubeIDE/workspace_1.7.0/SPOT-V1/spot-emb-v1/spark_sdk_v0.8.0/core/audio/processing/audio_user_data.c \
C:/Users/SY/STM32CubeIDE/workspace_1.7.0/SPOT-V1/spot-emb-v1/spark_sdk_v0.8.0/core/audio/processing/audio_volume.c 

OBJS += \
./core/audio/processing/audio_compression.o \
./core/audio/processing/audio_user_data.o \
./core/audio/processing/audio_volume.o 

C_DEPS += \
./core/audio/processing/audio_compression.d \
./core/audio/processing/audio_user_data.d \
./core/audio/processing/audio_volume.d 


# Each subdirectory must supply rules for building sources it contributes
core/audio/processing/audio_compression.o: C:/Users/SY/STM32CubeIDE/workspace_1.7.0/SPOT-V1/spot-emb-v1/spark_sdk_v0.8.0/core/audio/processing/audio_compression.c core/audio/processing/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DWPS_IS_COORD -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/audio -I../../../../lib/spark/memory -I../../../../lib/spark/resampling -I../../../../core/audio/protocol -I../../../../core/audio/endpoint -I../../../../core/audio/processing -I../../../../driver/spark/max98091 -I../../../../core/ranging -I../../../../lib/spark/adpcm -I../../../../app -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/audio/processing/audio_user_data.o: C:/Users/SY/STM32CubeIDE/workspace_1.7.0/SPOT-V1/spot-emb-v1/spark_sdk_v0.8.0/core/audio/processing/audio_user_data.c core/audio/processing/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DWPS_IS_COORD -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/audio -I../../../../lib/spark/memory -I../../../../lib/spark/resampling -I../../../../core/audio/protocol -I../../../../core/audio/endpoint -I../../../../core/audio/processing -I../../../../driver/spark/max98091 -I../../../../core/ranging -I../../../../lib/spark/adpcm -I../../../../app -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/audio/processing/audio_volume.o: C:/Users/SY/STM32CubeIDE/workspace_1.7.0/SPOT-V1/spot-emb-v1/spark_sdk_v0.8.0/core/audio/processing/audio_volume.c core/audio/processing/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DWPS_IS_COORD -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/audio -I../../../../lib/spark/memory -I../../../../lib/spark/resampling -I../../../../core/audio/protocol -I../../../../core/audio/endpoint -I../../../../core/audio/processing -I../../../../driver/spark/max98091 -I../../../../core/ranging -I../../../../lib/spark/adpcm -I../../../../app -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

