/** @file  ranging_initiator.c
 *  @brief Ranging application example for initiator board.
 *
 *  This file describes an example on how to setup and run a ranging application
 *  on an initiator board. It will setup the radio to talk to a responder board of address
 *  0x1337 and compute the distance in centimeters between the two radios.
 *  This example uses the SR1000 device drivers.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <stdarg.h>
#include "bsp.h"
#include "link_gain_loop.h"
#include "ranging_algorithm.h"
#include "ranging_cfg.h"
#include "sr1000_api.h"

/* CONSTANT *******************************************************************/
#define DESTINATION_ADDRESS            0xC001
#define LOCAL_ADDRESS                  0x1001
#define ANC2_ADDRESS                   0xA005



#define CCA_DEFAULT_RETRY_TIME         10
#define CCA_ENABLE                     1
#define CCA_INIT_THRESHOLD_UPDATE_TIME 30

#define PRINTF_BUF_SIZE                64
#define PLL_POWERUP_WAITING_DURATION   0x93

/* PRIVATE GLOBALS ************************************************************/
static radio_t *radio_handle;
static ranging_instance_t *ranging_handle;
volatile bool flag_irq;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static bool uwb_init_ranging_config(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address);
static void uwb_cca_initialization(radio_t *radio_inst, uint8_t retry_time, uint8_t nb_update, bool cca_enable);
static radio_events_t receiver_handle(ranging_frame_t *responder_ranging_frame);
static bool is_frame_good(radio_events_t events);
static bool timeout(uint32_t start, uint32_t timeout_ms);
static void set_ranging_data(phase_info_t *phases, ranging_info_t *ranging_info);
static void set_flag_irq(void);
static void usb_gpio_callback(void);
static void usb_printf(const char *fmt, ...);
static void app_error_handler(uwb_err err, uint8_t *file, uint16_t line);


/*************************************/
#define ANCcount 4
uint16_t ANC_array[4]={0xA001,0xA002,0xA003,0xA004};
uint16_t ANC_array_Val[4]={0,0,0,0};

uint8_t rcvFrame[50];
static radio_events_t rcv_frame_handle(void);


radio_t radio_instance;
gain_loop_t gain_loop = {0};
frame_quality_t frame_quality = {0};
uint32_t timeout_start;
radio_t radio_instance;
ranging_info_t initiator_ranging_info;
phase_info_t phases_info;
ranging_frame_t responder_ranging_frame;
ranging_instance_t ranging_instance;

/* Application's variables */
radio_events_t events;
float distance = 0;
uint32_t timeout_start;
uint32_t tick_now;
uint32_t delta_ms;
uint32_t ranging_sampling_period_ms = RANGING_SAMPLING_PERIOD_MS;
uint8_t  initiator_ranging_info_count  = 0;
uint8_t  powerup_delay_sample_count = 0;
uint8_t  delay                      = 0;
bool     apply_powerup_delay;
int coll_process(void);
static void transmitter_handle(void)
{
    ranging_frame_t current_ranging_frame;
    phase_info_t phase_info;
    uint8_t size;

    do {
        /* Read all packets received and keep the last one. */
    	 size = uwb_read(radio_handle, (uint8_t *)rcvFrame, sizeof(rcvFrame));
       // size = uwb_read(radio_handle, (uint8_t *)&current_ranging_frame.count, sizeof(current_ranging_frame.count));
    } while (uwb_get_rx_payload_size(radio_handle) > 0);

    /* ASSERT that RXFIFO is empty. */
    uwb_flush_rx_buffer(radio_handle);

    /* Get responder ranging info */
    phase_info =  uwb_get_phases_info(radio_handle);
    set_ranging_data(&phase_info, &current_ranging_frame.ranging_info);


    /* Fill TX FIFO with ranging frame for next auto-reply */
    uwb_fill_tx_fifo(radio_handle, (uint8_t *)&current_ranging_frame, sizeof(current_ranging_frame));

    UNUSED(size);
}
typedef struct {
	    	uint16_t caddr;
	    	uint16_t ancaddr1;
	    	uint16_t ancaddr2;
	    	uint16_t ancaddr3;
	    	uint16_t ancaddr4;
	    	uint16_t ancaddr5;
	    	uint16_t ancaddr6;
	    } CTmessaging;
	    CTmessaging CTmsj;
uint32_t msjid=0;
uint32_t searchForMsj(uint8_t * rcv)
{
	uint32_t j=0;
	  for(j=0;j<sizeof(rcvFrame);j++)
	           		   {
	           			if(*(rcv+j)==115 && *(rcv+j+1)==101 && *(rcv+j+2)==110)
	           			{
	           				return j;
	           			}
	           		   }
	  return 0;
}
/* Initialize the hardware abstraction layer structure */
radio_hal_t radio_hal = {
    bsp_radio_set_shutdown_pin,
    bsp_radio_reset_shutdown_pin,
    bsp_radio_set_reset_pin,
    bsp_radio_reset_reset_pin,
    bsp_radio_read_irq_pin,
    bsp_radio_spi_set_cs,
    bsp_radio_spi_reset_cs,
    bsp_radio_spi_write_blocking,
    bsp_radio_spi_read_blocking,
    bsp_delay_ms
};
uint8_t initRang=0;
static bool uwb_init_ranging_config_en(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address);

float GetDistance (uint16_t ANC_ADDRESS)
{

	 while (true) {

	     /* Update power-up delay value */
	     if (apply_powerup_delay){
	         delay = ranging_algorithm_update_powerup_delay(ranging_handle, (uint8_t *)(&powerup_delay_sample_count));
	     }

	     /*
	      * Transmit a "count" to responder address (0x1337).
	      * The value is used as a trigger for an auto-reply from the responder.
	      * Ranging measurements are time sensitive. The data from the initiator
	      * and responder must correspond to the same packet exchange otherwise
	      * ranging measurements will be erroneous. The count allows time
	      * sensitive ranging data to be synchronized.
	      */
	     uwb_send(radio_handle, ANC_ADDRESS, (uint8_t *)(&initiator_ranging_info_count), sizeof(initiator_ranging_info_count), delay);
	     powerup_delay_sample_count++;

	     /* Wait for auto-reply from the responder */
	     timeout_start = bsp_get_tick();
	     while ((timeout(timeout_start, ranging_sampling_period_ms) == false) && (flag_irq == false)) {
	         /* Wait */
	     }

	     /*
	      * Frame received, compute the distance between responder and initiator
	      * if received packet is valid.
	      */
	     if (flag_irq) {
	         flag_irq = false;

	         /*
	          * Assert that the received packet is valid and
	          * return responder ranging frame and events.
	          */
	         events = receiver_handle(&responder_ranging_frame);

	         if ((uint8_t)(initiator_ranging_info_count - 1 == responder_ranging_frame.count)) {
	             if (ranging_algorithm_append(ranging_handle, &initiator_ranging_info, &responder_ranging_frame.ranging_info)) {
	                 /*
	                  * Compute the distance when the ranging handle internal buffer is full.
	                  * See @RANGING_AVERAGING_SIZE_SAMPLE .
	                  */
	                 distance = ranging_algorithm_calculate_float(ranging_handle);
	                 usb_printf("Distance = %.2f m\n\r", (double)distance);
	                 return distance;

	             }
	         }
	         initiator_ranging_info_count++;
	         /*
	          * After processing the received info, update the initiator
	          * ones to get the k-1 sample to match the responder.
	          * A bad CRC will produce bad ranging measurement.
	          */
	         if (is_frame_good(events)) {
	             phases_info = uwb_get_phases_info(radio_handle);
	             set_ranging_data(&phases_info, &initiator_ranging_info);

	             /* Update gain loop */
	             frame_quality = uwb_get_frame_quality(radio_handle);
	             link_gain_loop_update(FRAME_RECEIVED, frame_quality.rssi, &gain_loop);
	         }

	         /* Sleep until next transfer */
	         uwb_sleep(radio_handle);
	     } else {
	         /* Missed the auto-reply */
	         frame_quality = uwb_get_frame_quality(radio_handle);
	         link_gain_loop_update(FRAME_LOST, frame_quality.rssi, &gain_loop);
	     }

	     /* Update receiver gain */
	     uwb_set_receiver_gain(radio_handle, link_gain_loop_get_gain_value(&gain_loop));

	     /* Wait the rest of the sampling period */
	     tick_now = bsp_get_tick();
	     delta_ms = tick_now - timeout_start;

	     if (delta_ms > ranging_sampling_period_ms) {
	         /* Nothing to do */
	     } else {
	         bsp_delay_ms(ranging_sampling_period_ms - delta_ms);
	     }

	 }

}

void ranging_process(void){

	 /* Enable auto-reply */
uint32_t j;
	if(initRang==0)
	{
		initRang=1;

	apply_powerup_delay=  uwb_init_ranging_config_en(&radio_instance, &radio_hal, IRQ_ACTIVE_HIGH, LOCAL_ADDRESS);


/* Initialize the ranging algorithm instance with user configuration */
 ranging_algorithm_init(&ranging_instance, RANGING_ALGORITHM_TYPE, RANGING_AVERAGING_SIZE_SAMPLE,
                         USER_DEFINED_RANGING_OFFSET, RANGING_FIXED_POINT_PRECISION);

 /* Initialize the radio handle that will be used for all further radio operations */
 radio_handle = &radio_instance;
 ranging_handle = &ranging_instance;

 /* CCA Initialization */
 uwb_cca_initialization(radio_handle, CCA_DEFAULT_RETRY_TIME, CCA_INIT_THRESHOLD_UPDATE_TIME, CCA_ENABLE);

 /*
  * Get part of the ranging information to compute distance.
  * The responder auto-reply will send its ranging information.
  * The responder always updates its ranging information with k-1
  * sample so the initiator needs to compute with its k-1 sample.
  */
 phases_info = uwb_get_phases_info(radio_handle);
 set_ranging_data(&phases_info, &initiator_ranging_info);
	}else {
		 uwb_enable_auto_reply(&radio_instance);

	}



	for(j=0;j<ANCcount;j++)
	{
		ANC_array_Val[j]=(uint16_t)(GetDistance(ANC_array[j])*100);
	}

	  /* Enable auto-reply */
	                    uwb_disable_auto_reply(&radio_instance);
	                 return;



}
/* PUBLIC FUNCTIONS ***********************************************************/
int main(void)
{
    /* Initialize HAL, GPIO & SPI peripheral */
    bsp_init();
    bsp_set_radio_irq_callback(set_flag_irq);
    bsp_set_board_voltage(VDD_3V3);
    /* Handle USB connection on initialization */
    if (bsp_is_usb_detected()) {
        uwb_err err;
        bsp_cdc_usb_connect(&err);
        bsp_delay_ms(1000);
        ERROR_CHECK(err);
    }

    //ranging_process();

    /* Initialize the radio instance and apply default configuration */
    uwb_init_ranging_config(&radio_instance, &radio_hal, IRQ_ACTIVE_HIGH, LOCAL_ADDRESS);

    /* Initialize the radio handle that will be used for all further radio operations */
    radio_handle = &radio_instance;

    /* Initialize flag for ISR */
    flag_irq = false;

    /*
     * Set destination address for auto-reply, Since we want to send a packet
     * the fastest possible way for the ranging's information, we use
     * the auto-reply. The destination address must be set before the packet
     * is received in order to send the auto-reply to the right device.
     */
    uwb_set_destination_address(radio_handle, DESTINATION_ADDRESS);

    /* Enable the receiver to listen for incoming packet from the initiator */
    uwb_receiver_on(radio_handle, NO_TIMEOUT);

    /* Start timeout. If IDLE for too long, increase receiver gain */
    timeout_start = bsp_get_tick();

    uint32_t ledms,ti,j=0;
    while (true) {

    	  tick_now = bsp_get_tick();
    	    			 if((tick_now-ledms)>500)
    	    			 {
    	    			 ledms=tick_now;
    	    			 HAL_GPIO_TogglePin(LED0_PORT, LED0_PIN);
    	    			 }

        /* Frame transmit, update the TX FIFO for the next auto-reply packet */
        if (flag_irq) {
            flag_irq = false;

            /* Clear IRQ by reading status */
            uwb_get_events(radio_handle);

            /* Update gain loop */
            frame_quality = uwb_get_frame_quality(radio_handle);
            link_gain_loop_update(FRAME_RECEIVED, frame_quality.rssi, &gain_loop);

            /* Update receiver gain */
            uwb_set_receiver_gain(radio_handle, link_gain_loop_get_gain_value(&gain_loop));

            /* Get current ranging info and prepare TX FIFO */
           transmitter_handle();

           	   if(rcvFrame[0]!=0)
       			{
       				//memcpy((uint8_t *)&CTmsj, (uint8_t *)&rcvFrame[0],sizeof(CTmessaging));
           		   ti=searchForMsj(rcvFrame);
           		 if(ti>0)
           		 {
           			msjid=atoll((char*)&rcvFrame[ti]);
           			usb_printf("\nrcv-->ti=%li  msjid=%li %s\n",ti, msjid, &rcvFrame[ti]);
           		   	memset(rcvFrame,0,sizeof(rcvFrame));

           		   			//transmitter_handle();
           		 HAL_GPIO_TogglePin(LED1_PORT, LED1_PIN);
           		 ranging_process();

           		 HAL_Delay(1);
           		/* Clear IRQ by reading status */
           		            uwb_get_events(radio_handle);
           		         /* Get current ranging info and prepare TX FIFO */
           		                   transmitter_handle();
           					coll_process();




           							//	usb_printf("rang Stop\n");

           		 }else
           		 {
           			 /*
           			  usb_printf("\nrcv--> but no MSJID %s\n",rcvFrame);
           			   ti=searchForMsj(rcvFrame);
           			   msjid=atoll((char*)&rcvFrame[ti]);
           			   usb_printf("\nrcv-->ti=%li  msjid=%li %s\n",ti, msjid, &rcvFrame[ti]);
           			 */
           		 }



       			}


		    /* Enable interrupt on RX complete */
		   // uwb_enable_irq(radio_handle, NEW_PACKET_IT);
            /* Enable receiver to listen for initiator request */
            uwb_receiver_on(radio_handle, NO_TIMEOUT);

            /* Update timeout */
            timeout_start = bsp_get_tick();
        }


    }
    return 0;
}


uint32_t senti=0;
int coll_process(void)
{


	    typedef struct {
	    	uint8_t  dummy;
	    	uint8_t  dummy1;
	    	uint8_t  dummy2;
	    	uint8_t  dummy3;
	    	uint16_t taddr;
	    	uint16_t anc1val;
	    	uint16_t anc2val;
	    	uint16_t anc3val;
	    	uint16_t anc4val;
	    } Tmessaging;
	    Tmessaging Tmsj;

	    Tmsj.dummy=0x55;
	    Tmsj.dummy1=115;
	    Tmsj.dummy2=101;
	    Tmsj.dummy3=110;
	    Tmsj.taddr=LOCAL_ADDRESS;
	    Tmsj.anc1val=ANC_array_Val[0];
	    Tmsj.anc2val=ANC_array_Val[1];
	    Tmsj.anc3val=ANC_array_Val[2];
	    Tmsj.anc4val=ANC_array_Val[3];

	 /*   uint8_t sentStr[100];
	    memset(sentStr,0,sizeof(sentStr));

	    memcpy(sentStr, (uint8_t *)&CTmsj,sizeof(CTmessaging));

	    char str[20];
	    	    snprintf(str,sizeof(str),"senCnt:%li dis:%.2f",senti++,distance);
	    	    distance=0;
*/


	    uwb_send(radio_handle, DESTINATION_ADDRESS, (uint8_t *)(&Tmsj), sizeof(Tmessaging), delay);
	 //   uwb_flush_tx_buffer(radio_handle);

	  //  usb_printf("CTmsj Sent \n");

    return 0;
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Radio and link initializations of initiator board for ranging application.
 *
 *  SPARK SR1020 initiator ranging initialization :
 *   - Initialize the low level part of the radio.
 *   - Calibrate the radio.
 *   - Setup the synchronization word for the link between the radios.
 *   - Configure the CRC polynomial for packet validity.
 *   - Configure the link frame (FEC level, modulation, preamble).
 *   - Configure channel for the application.
 *   - Set local address.
 *   - Configure sleep level.
 *   - Flush RX/TX buffer for clean start.
 *   - Configure interrupt for external GPIO signal(NEW_PACKET_IT for initiator packet reception).
 *   - Setup auto-reply.
 *   - Configure address filtering.
 *   - Select configured channel.
 *   - Read event status to reset IRQ pin.
 *
 *  @param[out] radio          Radio's instance.
 *  @param[in]  hal            Radio GPIO, SPI and delay related function structure.
 *  @param[in]  irq_pol        Polarity of the IRQ pin when it is asserted.
 *  @param[in]  local_address  Address of the current board.
 */
static bool uwb_init_ranging_config(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address)
{
    uwb_error_t  error;
    bool apply_powerup_delay = true;
    uint8_t sleep_level = SLEEP_SHALLOW;

    uwb_init(radio, hal, irq_pol, &error);
    ERROR_CHECK(error);

    /* Calibrate radio */
    uwb_calibrate(radio);

    /* Configure syncword */
    syncword_cfg_t sync_cfg;

    sync_cfg.syncword = sync_word_table[0];
    sync_cfg.syncword_bit_cost = 2;
    sync_cfg.syncword_tolerance = 0xC;
    sync_cfg.syncword_length = SYNCWORD_LENGTH_32;

    uwb_config_syncword(radio, sync_cfg);

    /* Configure CRC */
    uwb_set_crc(radio, 0xBAAD);

    /* Configure frame */
    frame_cfg_t frame_cfg;

    frame_cfg.fec = FEC_LVL_2;
    frame_cfg.modulation = MODULATION_2BITPPM;
    frame_cfg.preamble_length = 94;

    uwb_config_frame(radio, frame_cfg);

    /* Configure channels */
    uwb_config_channel(radio, CHANNEL_0, LOW_OUTPUT_POWER_RANGING);

    /* Set local address */
    uwb_set_local_address(radio, local_address, ADDRESS_LENGTH_16);

    /* Set sleep level */
    uwb_set_sleep_level(radio, sleep_level, SLEEP_NO_EVENT);

    /* Flush RX/TX FIFO */
    uwb_flush_rx_buffer(radio);
    uwb_flush_tx_buffer(radio);

    /* Enable interrupt on RX complete */
    uwb_enable_irq(radio, NEW_PACKET_IT);

    /* Enable auto-reply */
   // uwb_enable_auto_reply(radio);
    uwb_disable_auto_reply(radio);

    /* Disable address filtering for initiator auto-reply message */
    uwb_disable_address_filter(radio);

    /* Select channel */
    uwb_select_channel(radio, CHANNEL_0);

    /* Read status to clear IRQ pin */
    uwb_get_events(radio);

    /* Set PLL waiting time */
    uwb_set_pll_wait_time(radio, PLL_POWERUP_WAITING_DURATION);

    if (sleep_level == SLEEP_IDLE) {
        apply_powerup_delay = false;
    }

    return apply_powerup_delay;
}
static bool uwb_init_ranging_config_en(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address)
{
    uwb_error_t  error;
    bool apply_powerup_delay = true;
    uint8_t sleep_level = SLEEP_SHALLOW;

    uwb_init(radio, hal, irq_pol, &error);
    ERROR_CHECK(error);

    /* Calibrate radio */
    uwb_calibrate(radio);

    /* Configure syncword */
    syncword_cfg_t sync_cfg;

    sync_cfg.syncword = sync_word_table[0];
    sync_cfg.syncword_bit_cost = 2;
    sync_cfg.syncword_tolerance = 0xC;
    sync_cfg.syncword_length = SYNCWORD_LENGTH_32;

    uwb_config_syncword(radio, sync_cfg);

    /* Configure CRC */
    uwb_set_crc(radio, 0xBAAD);

    /* Configure frame */
    frame_cfg_t frame_cfg;

    frame_cfg.fec = FEC_LVL_2;
    frame_cfg.modulation = MODULATION_2BITPPM;
    frame_cfg.preamble_length = 94;

    uwb_config_frame(radio, frame_cfg);

    /* Configure channels */
    uwb_config_channel(radio, CHANNEL_0, LOW_OUTPUT_POWER_RANGING);

    /* Set local address */
    uwb_set_local_address(radio, local_address, ADDRESS_LENGTH_16);

    /* Set sleep level */
    uwb_set_sleep_level(radio, sleep_level, SLEEP_NO_EVENT);

    /* Flush RX/TX FIFO */
    uwb_flush_rx_buffer(radio);
    uwb_flush_tx_buffer(radio);

    /* Enable interrupt on RX complete */
    uwb_enable_irq(radio, NEW_PACKET_IT);

    /* Enable auto-reply */
    uwb_enable_auto_reply(radio);

    /* Disable address filtering for initiator auto-reply message */
    uwb_disable_address_filter(radio);

    /* Select channel */
    uwb_select_channel(radio, CHANNEL_0);

    /* Read status to clear IRQ pin */
    uwb_get_events(radio);

    /* Set PLL waiting time */
    uwb_set_pll_wait_time(radio, PLL_POWERUP_WAITING_DURATION);

    if (sleep_level == SLEEP_IDLE) {
        apply_powerup_delay = false;
    }

    return apply_powerup_delay;
}

/** @brief Initialization of the CCA functionality.
 *
 *  Initialize the clear channel assessment functionality for the application.
 *
 *  @param[in] radio_instance  Application radio instance.
 *  @param[in] retry_time      Time between transmission when air is occupied.
 *  @param[in] nb_update       Threshold update time.
 *  @param[in] cca_enable      Whether the CCA functionality is enabled.
 */
static void uwb_cca_initialization(radio_t *radio_instance, uint8_t retry_time, uint8_t nb_update, bool cca_enable)
{
    if (cca_enable) {
        /*
         * A value higher than that will result in an 8-bit
         * register overflow because of the formula used to
         * convert time to register data.
         */
        if (retry_time > CCA_MAX_RETRY_TIME_US) {
            retry_time = CCA_MAX_RETRY_TIME_US;
        }

        /* Initialize algorithm coefficients */
        uwb_cca_init(radio_instance, retry_time);

        /* Update offset for CCA threshold calculation */
        uwb_cca_update_rnsi_offset(radio_instance);

        /* Enable receiver to properly update CCA threshold */
        uwb_receiver_on(radio_instance, NO_TIMEOUT);

        /* Update CCA threshold */
        for (uint8_t i = 0; i < nb_update; i++) {
            uwb_cca_update_threshold(radio_instance);
        }

        /* Enable the CCA for the application */
        uwb_cca_enable(radio_instance);
    }
}

/** @brief Handle reception of packet from a responder board.
 *
 *  This function asserts whether or not the packet is valid with the packet
 *  address and the CRC. This function then returns the responder_ranging_frame as
 *  well as the current event. The received info are the one from the responder last
 *  TX_END_IT, this means that this is the k-1 sample.
 *
 *  @param[out] responder_ranging_frame  Ranging info of the responder board.
 */
static radio_events_t receiver_handle(ranging_frame_t *responder_ranging_frame)
{
    radio_events_t events = 0;
    uint8_t size;

    if (radio_handle) {
        events = uwb_get_events(radio_handle);
        /*
         * Assert that the received packet is valid. This is useful if there are
         * more than one board and also to verify the integrity of the packet.
         */
        if (is_frame_good(events)) {
            do {
                /* Read all packets receive and keep the last one */
                size = uwb_read(radio_handle, (uint8_t *)responder_ranging_frame, sizeof(ranging_frame_t));
            } while (uwb_get_rx_payload_size(radio_handle) > 0);

        } else {
            /*
             * Invalid packet received (Invalid CRC or Address).
             * Flush the RX buffer of the packet.
             */
            uwb_flush_rx_buffer(radio_handle);
        }
    }
    UNUSED(size);

    return events;
}


static radio_events_t rcv_frame_handle(void)
{
    radio_events_t events = 0;
    uint8_t size;

    if (radio_handle) {
        events = uwb_get_events(radio_handle);
        /*
         * Assert that the received packet is valid. This is useful if there are
         * more than one board and also to verify the integrity of the packet.
         */
        if (is_frame_good(events)) {
            do {
                /* Read all packets receive and keep the last one */
                size = uwb_read(radio_handle, (uint8_t *)rcvFrame, sizeof(rcvFrame));
            } while (uwb_get_rx_payload_size(radio_handle) > 0);

        } else {
            /*
             * Invalid packet received (Invalid CRC or Address).
             * Flush the RX buffer of the packet.
             */
            uwb_flush_rx_buffer(radio_handle);
        }
    }
    UNUSED(size);

    return events;
}
/** @brief Check if the frame received is valid.
 *
 *  A frame is valid based on the address matching and CRC.
 *
 *  @param[in] events  Radio event enumeration.
 *  @retval true   Frame is good.
 *  @retval false  Frame is invalid (Invalid Address or CRC).
 */
static bool is_frame_good(radio_events_t events)
{
    if ((events & ADDR_MATCH_IT) && (events & CRC_PASS_IT)) {
        return true;
    } else {
        return false;
    }
}

/** @brief Function that check if timeout is expired.
 *
 *  @param[in] start       Start time for timeout.
 *  @param[in] timeout_ms  timeout value in ms.
 *  @retval true   Timeout is expired.
 *  @retval false  Timeout is not expired.
 */
static bool timeout(uint32_t start, uint32_t timeout_ms)
{
    if (bsp_get_tick() > (start + timeout_ms)) {
        return true;
    } else {
        return false;
    }
}

/** @brief Function that checks if timeout is expired.
 *
 *  @param[in]  phases       Phases structure from radio.
 *  @param[out] ranging_info Ranging information structure.
 */
static void set_ranging_data(phase_info_t *phases, ranging_info_t *ranging_info)
{
    ranging_info->phase1 = phases->phase1;
    ranging_info->phase2 = phases->phase2;
    ranging_info->phase3 = phases->phase3;
    ranging_info->phase4 = phases->phase4;
    ranging_info->rx_waited0 = phases->rx_waited0;
    ranging_info->rx_waited1 = phases->rx_waited1;
}

/** @brief Radio callback to set the Radio's IRQ flag.
 *
 *  This callback is initialized using the bsp_set_radio_irq_callback function.
 */
static void set_flag_irq(void)
{
    flag_irq = true;
}

/** @brief USB line detection callback.
 */
static void usb_gpio_callback(void)
{
    uwb_err err;
    if (bsp_is_usb_detected()) {
        bsp_cdc_usb_connect(&err);
    } else {
        bsp_cdc_usb_disconnect(&err);
    }
    ERROR_CHECK(err);
}

/** @brief Print characters through USB.
 *
 *  @param[in] fmt  Pointer to the character to be printed.
 *  @param[in] ...  Variable argument list.
 */
static void usb_printf(const char *fmt, ...)
{
    char std_buf[PRINTF_BUF_SIZE];
    va_list va;

    va_start(va, fmt);

    vsprintf(std_buf, fmt, va);

    /*
     * Does not handle strings that are not \0-terminated; if given one it may
     * perform an over-read (it could cause a crash if unprotected) (CWE-126).
     */
    bsp_usb_cdc_send_buf((uint8_t *)std_buf, strlen(std_buf));

    va_end(va);
}

/** @brief This function is called if an error occurred.
 *
 *  It should be passed as pointer by uwb_error_init for initialization.
 *
 *  @param[in] err   Error code.
 *  @param[in] file  String of the file name where the error occurred.
 *  @param[in] line  Line in the code where the error occurred.
 */
static void app_error_handler(uwb_err err, uint8_t *file, uint16_t line)
{
    (void)err;
    (void)file;
    (void)line;

    while(1)
    {

    }
}
