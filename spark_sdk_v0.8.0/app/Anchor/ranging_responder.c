/** @file  ranging_responder.c
 *  @brief Ranging application example for responder board.
 *
 *  This file describes an example on how to setup and run a ranging application
 *  on a responder board. It will setup the radio to automaticaly respond the intiator
 *  request of address 0xABCD with his ranging information for distance calculation
 *  between the two boards. This example uses the SR1000 device drivers.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "bsp.h"
#include "link_gain_loop.h"
#include "ranging_algorithm.h"
#include "ranging_cfg.h"
#include "sr1000_api.h"

#define ANC1				  0XA001
#define ANC2				  0XA002
#define ANC3				  0XA003
#define ANC4				  0XA004
#define ANC5				  0XA005
/* CONSTANT *******************************************************************/
#ifndef DESTINATION_ADDRESS
#define DESTINATION_ADDRESS   0x1001
#define LOCAL_ADDRESS         ANC5
#endif

#define GAIN_LOOP_IDLE_MAX_MS 100

/* PRIVATE GLOBALS ************************************************************/
static radio_t *radio_handle;
volatile bool flag_irq;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void uwb_init_ranging_config(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address);
static bool timeout(uint32_t start, uint32_t timeout_ms);
static void transmitter_handle(void);
static void set_ranging_data(phase_info_t *phases, ranging_info_t *ranging_info);
static void set_flag_irq(void);

/* PUBLIC FUNCTIONS ***********************************************************/
int main(void)
{
    radio_t radio_instance;
    gain_loop_t gain_loop = {0};
    frame_quality_t frame_quality = {0};
    uint32_t timeout_start;

    /* Initialize the hardware abstraction layer structure */
    radio_hal_t radio_hal = {
        bsp_radio_set_shutdown_pin,
        bsp_radio_reset_shutdown_pin,
        bsp_radio_set_reset_pin,
        bsp_radio_reset_reset_pin,
        bsp_radio_read_irq_pin,
        bsp_radio_spi_set_cs,
        bsp_radio_spi_reset_cs,
        bsp_radio_spi_write_blocking,
        bsp_radio_spi_read_blocking,
        bsp_delay_ms
    };

    /* Initialize HAL, GPIO & SPI peripheral */
    bsp_init();
    bsp_set_radio_irq_callback(set_flag_irq);
    bsp_set_board_voltage(VDD_3V3);

    /* Initialize the radio instance and apply default configuration */
    uwb_init_ranging_config(&radio_instance, &radio_hal, IRQ_ACTIVE_HIGH, LOCAL_ADDRESS);

    /* Initialize the radio handle that will be used for all further radio operations */
    radio_handle = &radio_instance;

    /* Initialize flag for ISR */
    flag_irq = false;

    /*
     * Set destination address for auto-reply, Since we want to send a packet
     * the fastest possible way for the ranging's information, we use
     * the auto-reply. The destination address must be set before the packet
     * is received in order to send the auto-reply to the right device.
     */
    uwb_set_destination_address(radio_handle, DESTINATION_ADDRESS);

    /* Enable the receiver to listen for incoming packet from the initiator */
    uwb_receiver_on(radio_handle, NO_TIMEOUT);

    /* Start timeout. If IDLE for too long, increase receiver gain */
    timeout_start = bsp_get_tick();
    uint32_t tick_now,ledms=0;
    while (true) {

    	  tick_now = bsp_get_tick();
    	    			 if((tick_now-ledms)>500)
    	    			 {
    	    			 ledms=tick_now;
    	    			 HAL_GPIO_TogglePin(LED0_PORT, LED0_PIN);
    	    			 }
        /* Frame transmit, update the TX FIFO for the next auto-reply packet */
        if (flag_irq) {
            flag_irq = false;

            /* Clear IRQ by reading status */
            uwb_get_events(radio_handle);

            /* Update gain loop */
            frame_quality = uwb_get_frame_quality(radio_handle);
            link_gain_loop_update(FRAME_RECEIVED, frame_quality.rssi, &gain_loop);

            /* Update receiver gain */
            uwb_set_receiver_gain(radio_handle, link_gain_loop_get_gain_value(&gain_loop));

            /* Get current ranging info and prepare TX FIFO */
            transmitter_handle();

            /* Enable receiver to listen for initiator request */
            uwb_receiver_on(radio_handle, NO_TIMEOUT);

            /* Update timeout */
            timeout_start = bsp_get_tick();
        }

    }
    return 0;
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Radio and link initialization of responder board for ranging application.
 *
 *  SPARK SR1020 responder ranging initialization :
 *   - Initialize the low level part of the radio.
 *   - Calibrate the radio.
 *   - Setup the synchronization word for the link between the radios.
 *   - Configure the CRC polynomial for packet validity.
 *   - Configure the link frame (FEC level, modulation, preamble).
 *   - Configure channel for the application.
 *   - Set local address.
 *   - Configure sleep level.
 *   - Flush RX/TX buffer for clean start.
 *   - Configure interrupt for external GPIO signal (TX_END_IT for auto-reply refill).
 *   - Setup auto-reply to get the most accurate measurement.
 *   - Configure address filtering.
 *   - Select configured channel.
 *   - Read event status to reset IRQ pin.
 *
 *  @param[out] radio          Radio's instance.
 *  @param[in]  hal            Radio GPIO, SPI and delay related function structure.
 *  @param[in]  irq_pol        Polarity of the IRQ pin when it is asserted.
 *  @param[in]  local_address  Address of the current board.
 */
static void uwb_init_ranging_config(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address)
{
    uwb_error_t  error;

    /* Populate radio object */
    uwb_init(radio, hal, irq_pol, &error);

    /* Calibrate radio */
    uwb_calibrate(radio);

    /* Configure syncword */
    syncword_cfg_t sync_cfg;

    sync_cfg.syncword = sync_word_table[0];
    sync_cfg.syncword_bit_cost = 2;
    sync_cfg.syncword_tolerance = 0xC;
    sync_cfg.syncword_length = SYNCWORD_LENGTH_32;

    uwb_config_syncword(radio, sync_cfg);

    /* Configure CRC */
    uwb_set_crc(radio, 0xBAAD);

    /* Configure frame */
    frame_cfg_t frame_cfg;

    frame_cfg.fec = FEC_LVL_2;
    frame_cfg.modulation = MODULATION_2BITPPM;
    frame_cfg.preamble_length = 94;

    uwb_config_frame(radio, frame_cfg);

    /* Configure channels */
    uwb_config_channel(radio, CHANNEL_0, LOW_OUTPUT_POWER_RANGING);

    /* Set local address */
    uwb_set_local_address(radio, local_address, ADDRESS_LENGTH_16);

    /* Set sleep level */
    uwb_set_sleep_level(radio, SLEEP_IDLE, SLEEP_NO_EVENT);

    /* Flush RX/TX FIFO */
    uwb_flush_rx_buffer(radio);
    uwb_flush_tx_buffer(radio);

    /* Enable interrupt on TX complete */
    uwb_enable_irq(radio, TX_END_IT);

    /* Enable auto-reply */
    uwb_enable_auto_reply(radio);

    /* Select channel */
    uwb_select_channel(radio, CHANNEL_0);

    /* Read status to clear IRQ pin */
    uwb_get_events(radio);
}

/** @brief Function that checks if timeout is expired.
 *
 *  @param[in] start       Start time for timeout.
 *  @param[in] timeout_ms  Timeout value in ms.
 *  @retval true   Timeout is expired.
 *  @retval false  Timeout is not expired.
 */
static bool timeout(uint32_t start, uint32_t timeout_ms)
{
    if (bsp_get_tick() > (start + timeout_ms)) {
        return true;
    } else {
        return false;
    }
}

/** @brief Fill up TX FIFO for next auto-reply.
 *
 *  This handle should be called right after the responder transmit an auto-reply.
 *  This will ensure that the TX FIFO is filled for the next auto-reply that
 *  will happen the next time the responder receives a packet from the initiator.
 */
static void transmitter_handle(void)
{
    ranging_frame_t current_ranging_frame;
    phase_info_t phase_info;
    uint8_t size;

    do {
        /* Read all packets received and keep the last one. */
        size = uwb_read(radio_handle, (uint8_t *)&current_ranging_frame.count, sizeof(current_ranging_frame.count));
    } while (uwb_get_rx_payload_size(radio_handle) > 0);

    /* ASSERT that RXFIFO is empty. */
    uwb_flush_rx_buffer(radio_handle);

    /* Get responder ranging info */
    phase_info =  uwb_get_phases_info(radio_handle);
    set_ranging_data(&phase_info, &current_ranging_frame.ranging_info);


    /* Fill TX FIFO with ranging frame for next auto-reply */
    uwb_fill_tx_fifo(radio_handle, (uint8_t *)&current_ranging_frame, sizeof(current_ranging_frame));

    UNUSED(size);
}

/** @brief Function that checks if timeout is expired.
 *
 *  @param[in]  phases        Phases structure from radio.
 *  @param[out] ranging_info  Ranging information structure.
 */
static void set_ranging_data(phase_info_t *phases, ranging_info_t *ranging_info)
{
    ranging_info->phase1 = phases->phase1;
    ranging_info->phase2 = phases->phase2;
    ranging_info->phase3 = phases->phase3;
    ranging_info->phase4 = phases->phase4;
    ranging_info->rx_waited0 = phases->rx_waited0;
    ranging_info->rx_waited1 = phases->rx_waited1;
}

/** @brief Radio callback to set the Radio's IRQ flag.
 *
 *  This callback is initialized using the bsp_set_radio_irq_callback function.
 */
static void set_flag_irq(void)
{
    flag_irq = true;
}
