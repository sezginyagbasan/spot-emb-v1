################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/lib/spark/queue/circular_queue.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/lib/spark/queue/queue.c 

OBJS += \
./lib/spark/queue/circular_queue.o \
./lib/spark/queue/queue.o 

C_DEPS += \
./lib/spark/queue/circular_queue.d \
./lib/spark/queue/queue.d 


# Each subdirectory must supply rules for building sources it contributes
lib/spark/queue/circular_queue.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/lib/spark/queue/circular_queue.c lib/spark/queue/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
lib/spark/queue/queue.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/lib/spark/queue/queue.c lib/spark/queue/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-lib-2f-spark-2f-queue

clean-lib-2f-spark-2f-queue:
	-$(RM) ./lib/spark/queue/circular_queue.d ./lib/spark/queue/circular_queue.o ./lib/spark/queue/queue.d ./lib/spark/queue/queue.o

.PHONY: clean-lib-2f-spark-2f-queue

