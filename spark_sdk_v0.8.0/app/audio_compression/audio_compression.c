/** @file  audio_compression.c
 *  @brief This application creates a unidirectional compressed audio stream
 *         from the MAX98091 audio codec Line-In of the coordinator to the
 *         Headphone-Out of the node.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES ******************************************************************/
#include "audio_api.h"
#include "audio_compression.h"
#include "audio_volume.h"
#include "bsp.h"
#include "bsp_audio.h"
#include "ep_i2s.h"
#include "ep_wps.h"
#include "max98091.h"
#include "wps.h"
#include "wps_cfg.h"

/* CONSTANTS ******************************************************************/
#define AUDIO_MEM_POOL_SIZE_BYTE  7000

/* MACROS *********************************************************************/
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))

/* PRIVATE GLOBALS ************************************************************/
static wps_t wps = {0};

static wps_node_t node;

static wps_radio_t radio_instance = {0};

static wps_connection_t tx_wps_conn = {0};
static wps_connection_t rx_wps_conn = {0};

static wps_connection_cfg_t tx_wps_cfg;
static wps_connection_cfg_t rx_wps_cfg;

static uint8_t frame_buffer_tx[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE]  = {0};
static uint8_t frame_buffer_rx[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE]  = {0};

static uint32_t timeslot_us[]                       = SCHEDULE_TIMING;
static timeslot_t timeslot[ARRAY_SIZE(timeslot_us)] = {0};

static uint32_t channel_sequence[] = CHANNEL_SEQUENCE;

static xlayer_t wps_fifo_tx[XLAYER_SIZE]  = {0};
static xlayer_t wps_fifo_rx[XLAYER_SIZE]  = {0};

static channel_cfg_t channel_cfg;
static tx_power_settings_t tx_power_settings;

static uint8_t spi_rx_buffer[SPI_BUFFER_SIZE] = {0};
static uint8_t spi_tx_buffer[SPI_BUFFER_SIZE] = {0};

static wps_callback_inst_t callback_tx[XLAYER_SIZE];
static wps_callback_inst_t callback_rx[XLAYER_SIZE];

static uint8_t audio_memory_pool[AUDIO_MEM_POOL_SIZE_BYTE];
static audio_pipeline_t primary_audio_pipeline;

static max98091_i2c_hal_t codec_hal = {
    .read = bsp_audio_i2c_read,
    .write = bsp_audio_i2c_write
};

static volume_t volume_control = {
    .initial_volume_level = 100
};

static audio_processing_t volume_processing = {
    .instance = &volume_control,
    .name = "Digital Volume Control",
    .iface = {
        .init = audio_volume_init,
        .deinit = audio_volume_deinit,
        .ctrl = audio_volume_ctrl,
        .process = audio_volume_process
    }
};

static audio_compression_instance_t compression_instance = {
    .compression_mode = AUDIO_COMPRESSION_PACK_STEREO
};

static audio_compression_instance_t decompression_instance = {
    .compression_mode = AUDIO_COMPRESSION_UNPACK_STEREO
};

static audio_processing_t compression_processing = {
    .instance = &compression_instance,
    .name = "Audio Compression",
    .iface.init = audio_compression_init,
    .iface.deinit = audio_compression_deinit,
    .iface.ctrl = audio_compression_ctrl,
    .iface.process = audio_compression_process
};

static audio_processing_t decompression_processing = {
    .instance = &decompression_instance,
    .name = "Audio Decompression",
    .iface.init = audio_compression_init,
    .iface.deinit = audio_compression_deinit,
    .iface.ctrl = audio_compression_ctrl,
    .iface.process = audio_compression_process
};

static ep_i2s_instance_t max98091_producer_instance = {
    .i2s_action = bsp_audio_sai_read,
    .i2s_start = bsp_audio_record_start,
    .i2s_stop = bsp_audio_record_stop,
    .bytes_per_sample = 2
};
static audio_endpoint_t max98091_producer;

static ep_i2s_instance_t max98091_consumer_instance = {
    .i2s_action = bsp_audio_sai_write,
    .i2s_start = bsp_audio_playback_start,
    .i2s_stop = bsp_audio_playback_stop,
    .bytes_per_sample = 2
};

static audio_endpoint_t max98091_consumer;

static ep_wps_instance_t wps_producer_instance = {
    .connection = &rx_wps_conn
};

static audio_endpoint_t wps_producer;

static ep_wps_instance_t wps_consumer_instance = {
    .connection = &tx_wps_conn
};

static audio_endpoint_t wps_consumer;

/* PRIVATE FUNCTION PROTOTYPE *************************************************/
static void app_wps_init(void);

static void conn_tx_ack_callback(wps_connection_t *conn);
static void conn_tx_nack_callback(wps_connection_t *conn);
static void conn_rx_callback(wps_connection_t *conn);

static void radio_irq_cb(void);
static void radio_dma_spi_cb(void);
static void radio_callback_cb(void);

static void app_audio_core_init(audio_error_t *audio_err);
static void audio_i2s_tx_complete_cb(void);
static void audio_i2s_rx_complete_cb(void);

static void volume_handling(void);

/* PUBLIC FUNCTIONS ***********************************************************/
int main(void)
{
    audio_error_t audio_err;

    bsp_init();
    bsp_radio_set_debug_en();
    bsp_set_radio_irq_callback(radio_irq_cb);
    bsp_set_radio_dma_rx_callback(radio_dma_spi_cb);
    bsp_set_pendsv_callback(radio_callback_cb);

    app_wps_init();

    app_audio_core_init(&audio_err);
    if (audio_err != AUDIO_ERR_NONE) {
        while (1);
    }

    /* BSP audio initialization */
    if (IS_COORD) {
        bsp_audio_set_record_channel(AUDIO_CHANNELS_STEREO);
    } else {
        bsp_audio_set_playback_channel(AUDIO_CHANNELS_STEREO);
    }
    bsp_audio_set_sampling_rate(AUDIO_48KHZ);
    bsp_audio_init();

    /* MAX98091 CODEC initialization */
    uwb_err err;
    max98091_codec_cfg_t codec_config;
    if (IS_COORD) {
        codec_config.sampling_rate = MAX98091_AUDIO_48KHZ;
        codec_config.record_enabled = true;
        codec_config.playback_enabled = false;
        codec_config.record_filter_enabled = false;
        codec_config.playback_filter_enabled = false;
    } else {
        codec_config.sampling_rate = MAX98091_AUDIO_48KHZ;
        codec_config.record_enabled = false;
        codec_config.playback_enabled = true;
        codec_config.record_filter_enabled = false;
        codec_config.playback_filter_enabled = false;
    }
    max98091_init(&codec_hal, &codec_config, &err);

    /* Start audio pipelines */
    audio_pipeline_start(&primary_audio_pipeline);

    wps_connect(&wps);

    while (1) {
        audio_pipeline_process(&primary_audio_pipeline, &audio_err);
        if (IS_COORD) {
            if (queue_get_length(&primary_audio_pipeline.consumer->_queue) > 0) {
                audio_pipeline_consume(&primary_audio_pipeline, &audio_err);
            }
        } else {
            volume_handling();
        }
    }

    return 0;
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Initialize the wps.
 */
static void app_wps_init(void)
{
    /* Radio hardware abstraction layer parameters */
    radio_hal_t radio_hal = {
        bsp_radio_set_shutdown_pin,
        bsp_radio_reset_shutdown_pin,
        bsp_radio_set_reset_pin,
        bsp_radio_reset_reset_pin,
        bsp_radio_read_irq_pin,
        bsp_radio_spi_set_cs,
        bsp_radio_spi_reset_cs,
        bsp_radio_spi_write_blocking,
        bsp_radio_spi_read_blocking,
        bsp_delay_ms
    };

    /* Radio advance hardware abstraction layer parameters */
    radio_hal_adv_t radio_hal_adv = {
        bsp_radio_spi_transfer_full_duplex_blocking,
        bsp_radio_spi_transfer_full_duplex_non_blocking,
        bsp_radio_is_spi_busy,
        bsp_radio_context_switch,
        bsp_radio_disable_irq_it,
        bsp_radio_enable_irq_it,
        bsp_radio_disable_irq_it,
        bsp_radio_enable_irq_it
    };

    /* Critical section hardware abstraction layer parameters */
    wps_critical_cfg_t critical_cfg = {
        bsp_enter_critical, bsp_exit_critical
    };

    /* Disable radio IRQ when initializing */
    bsp_radio_disable_irq_it();

    /* Initialize the radio instance and apply link configuration */
    wps_error_t error;

    /* Radio instance parameters */
    radio_instance.radio_hal       = radio_hal;
    radio_instance.radio_hal_adv   = radio_hal_adv;
    radio_instance.spi_buffer_size = SPI_BUFFER_SIZE;
    radio_instance.spi_rx_buffer   = spi_rx_buffer;
    radio_instance.spi_tx_buffer   = spi_tx_buffer;
    radio_instance.irq_polarity    = IRQ_ACTIVE_HIGH;

    wps_init_critical_section(&critical_cfg);
    wps_radio_init(&radio_instance, &error);
    wps_radio_calibrate(&radio_instance, &error);

    ERROR_CHECK(error);

    /* Initialize schedule timing */
    wps_config_network_schedule(&wps, timeslot_us, timeslot, ARRAY_SIZE(timeslot_us));
    wps_config_network_channel_sequence(&wps, channel_sequence, ARRAY_SIZE(channel_sequence));
    wps_set_coordinator_address(&wps, WPS_DEFAULT_COORD_ADDR);

    /* Initialize the node/xlayer for coordinator/node */
    wps_node_cfg_t node_cfg;

    node_cfg.role           = ROLE;
    node_cfg.preamble_len   = WPS_DEFAULT_PREAMBLE_LEN;
    node_cfg.sleep_lvl      = WPS_DEFAULT_SLEEP_LEVEL;
    node_cfg.crc_polynomial = WPS_DEFAULT_CRC;
    node_cfg.local_address  = LOCAL_ADDRESS;
    node_cfg.syncword       = sync_word_table[WPS_DEFAULT_SYNC_TABLE_IDX];

    wps_config_node(&node, &radio_instance, &node_cfg);

    wps_init_callback_queue(&wps, callback_tx, ARRAY_SIZE(callback_tx), bsp_radio_callback_context_switch);
    wps_init_callback_queue(&wps, callback_rx, ARRAY_SIZE(callback_rx), bsp_radio_callback_context_switch);

    /* Configure radio channels */
    tx_power_settings.pulse_count = WPS_DEFAULT_PULSE_COUNT;
    tx_power_settings.pulse_width = WPS_DEFAULT_PULSE_WIDTH;
    tx_power_settings.tx_gain     = WPS_DEFAULT_TX_GAIN;
    channel_cfg.power             = &tx_power_settings;
    channel_cfg.freq_shift_enable = WPS_DEFAULT_FREQ_SHIFT;
    channel_cfg.pulse_spacing     = WPS_DEFAULT_PULSE_SPACING;
    channel_cfg.pulse_start_pos   = WPS_DEFAULT_PULSE_START_POS;
    channel_cfg.rdn_phase_enable  = WPS_DEFAULT_RND_PHASE;
    channel_cfg.integgain         = WPS_DEFAULT_INTEGGAIN;

    /* WPS Connections configuration */
    if (IS_COORD) {
        tx_wps_cfg.source_address       = LOCAL_ADDRESS;
        tx_wps_cfg.destination_address  = REMOTE_ADDRESS;
        tx_wps_cfg.header_length        = WPS_RADIO_HEADER_SIZE;
        tx_wps_cfg.frame_length         = WPS_DEFAULT_FRAME_SIZE;
        tx_wps_cfg.get_tick_quarter_ms  = bsp_get_free_running_timer_tick_ms;
        tx_wps_cfg.fifo_buffer          = wps_fifo_tx;
        tx_wps_cfg.fifo_buffer_size     = XLAYER_SIZE;
        tx_wps_cfg.frame_buffer         = &(frame_buffer_tx[0][0]);
        tx_wps_cfg.send_ack_callback_t  = conn_tx_ack_callback;
        tx_wps_cfg.send_nack_callback_t = conn_tx_nack_callback;
        tx_wps_cfg.receive_callback_t   = NULL;

        wps_create_connection(&tx_wps_conn, &node, &tx_wps_cfg);
        wps_connection_enable_stop_and_wait_arq(&tx_wps_conn, LOCAL_ADDRESS, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);
        wps_connection_enable_ack(&tx_wps_conn);
        wps_connection_enable_auto_sync(&tx_wps_conn);
        wps_connection_disable_cca(&tx_wps_conn);


        int32_t tx_timeslots[] = COORDINATOR_TIMESLOTS;
        wps_connection_set_timeslot(&tx_wps_conn, &wps, tx_timeslots, ARRAY_SIZE(tx_timeslots));
        wps_connection_config_frame(&tx_wps_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);
        channel_cfg.frequency = WPS_DEFAULT_TS0_FREQ;
        wps_connection_config_channel(&tx_wps_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);
        channel_cfg.frequency = WPS_DEFAULT_TS1_FREQ;
        wps_connection_config_channel(&tx_wps_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
        channel_cfg.frequency = WPS_DEFAULT_TS2_FREQ;
        wps_connection_config_channel(&tx_wps_conn, &node, WPS_DEFAULT_CHAN2, &channel_cfg);
        channel_cfg.frequency = WPS_DEFAULT_TS3_FREQ;
        wps_connection_config_channel(&tx_wps_conn, &node, WPS_DEFAULT_CHAN3, &channel_cfg);
    } else {
        rx_wps_cfg.source_address       = REMOTE_ADDRESS;
        rx_wps_cfg.destination_address  = LOCAL_ADDRESS;
        rx_wps_cfg.header_length        = WPS_RADIO_HEADER_SIZE;
        rx_wps_cfg.frame_length         = WPS_DEFAULT_FRAME_SIZE;
        rx_wps_cfg.get_tick_quarter_ms  = bsp_get_free_running_timer_tick_ms;
        rx_wps_cfg.fifo_buffer          = wps_fifo_rx;
        rx_wps_cfg.fifo_buffer_size     = XLAYER_SIZE;
        rx_wps_cfg.frame_buffer         = &(frame_buffer_rx[0][0]);
        rx_wps_cfg.send_ack_callback_t  = NULL;
        rx_wps_cfg.send_nack_callback_t = NULL;
        rx_wps_cfg.receive_callback_t   = conn_rx_callback;

        wps_create_connection(&rx_wps_conn, &node, &rx_wps_cfg);
        wps_connection_enable_stop_and_wait_arq(&rx_wps_conn, LOCAL_ADDRESS, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);
        wps_connection_enable_ack(&rx_wps_conn);
        wps_connection_enable_auto_sync(&rx_wps_conn);
        wps_connection_disable_cca(&rx_wps_conn);

        int32_t rx_timeslots[] = COORDINATOR_TIMESLOTS;
        wps_connection_set_timeslot(&rx_wps_conn, &wps, rx_timeslots, ARRAY_SIZE(rx_timeslots));
        wps_connection_config_frame(&rx_wps_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);
        channel_cfg.frequency = WPS_DEFAULT_TS0_FREQ;
        wps_connection_config_channel(&rx_wps_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);
        channel_cfg.frequency = WPS_DEFAULT_TS1_FREQ;
        wps_connection_config_channel(&rx_wps_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
        channel_cfg.frequency = WPS_DEFAULT_TS2_FREQ;
        wps_connection_config_channel(&rx_wps_conn, &node, WPS_DEFAULT_CHAN2, &channel_cfg);
        channel_cfg.frequency = WPS_DEFAULT_TS3_FREQ;
        wps_connection_config_channel(&rx_wps_conn, &node, WPS_DEFAULT_CHAN3, &channel_cfg);
    }

    /* Init the WPS */
    wps_init(&wps, &node);

    /* Init the WPS PHY */
    wps_phy_t wps_phy;

    wps_phy.lna_imped = true;
    wps_phy.manuphase = MANUPHASE_PHASE_TRACKING_ENABLE;
    wps_phy_init(&radio_instance, &wps_phy);

    /* Enable radio IRQ after initialization */
    bsp_radio_enable_irq_it();

    /* Start wireless link */
    uwb_flush_rx_buffer(&radio_instance.radio);
    uwb_flush_tx_buffer(&radio_instance.radio);
}

/** @brief Callback for the coordinator TX acknowledge.
 */
static void conn_tx_ack_callback(wps_connection_t *conn)
{
    (void)conn;
    bsp_led_toggle(LED0);
}

/** @brief Callback for coordinator the TX NACK.
 */
static void conn_tx_nack_callback(wps_connection_t *conn)
{
    (void)conn;
}

/** @brief Callback for coordinator new frame received.
 */
static void conn_rx_callback(wps_connection_t *conn)
{
    audio_error_t audio_err;
    (void)conn;
    audio_pipeline_produce(&primary_audio_pipeline, &audio_err);
}

/** @brief Callback for the radio IRQ.
 *
 *  These two functions must absolutely be called here
 *  if you intend to use the wireless protocol stack.
 */
static void radio_irq_cb(void)
{
    wps_radio_irq(&wps);
    wps_process(&wps);
}

/** @brief Callback for the radio DMA SPI.
 *
 *  This function must absolutely be called here if
 *  you intend to use the wireless protocol stack.
 */
static void radio_dma_spi_cb(void)
{
    wps_transfer_complete(&wps);
    wps_process(&wps);
}

/** @brief Software interrupt callback to empty the WPS queue.
 *
 */
static void radio_callback_cb(void)
{
    wps_process_callback(&wps);
}

/** @brief Initialize the audio core.
 *
 *  @return False if allocated memory too small, true otherwise.
 */
static void app_audio_core_init(audio_error_t *audio_err)
{
    audio_endpoint_interface_t ep_wps_iface = {
        .start = ep_wps_start,
        .stop = ep_wps_stop
    };
    audio_endpoint_interface_t ep_i2s_iface = {
        .action = ep_i2s_action,
        .start = ep_i2s_start,
        .stop = ep_i2s_stop
    };
    audio_protocol_cfg_t protocol_cfg = {
        .cdc_resampling_length = 5760,
        .queue_avg_size = 1000,
        .max_process_count = 2
    };
    audio_endpoint_cfg_t primary_endpoint_cfg = {
        .channel_count      = 2,
        .bit_depth          = 16,
        .audio_payload_size = 120,
        .queue_size         = 14
    };
    queue_critical_cfg_t queue_critical = {
        .enter_critical = bsp_enter_critical,
        .exit_critical = bsp_exit_critical
    };

    audio_core_init(queue_critical, audio_memory_pool, AUDIO_MEM_POOL_SIZE_BYTE);
    bsp_audio_set_tx_callback(audio_i2s_tx_complete_cb);
    bsp_audio_set_rx_callback(audio_i2s_rx_complete_cb);

    if (IS_COORD) {
        /* INIT PRIMARY PIPELINE */
        primary_endpoint_cfg.use_encapsulation  = false;
        primary_endpoint_cfg.delayed_action     = true;
        audio_endpoint_init(&max98091_producer, (void *)&max98091_producer_instance, "MAX98091 EP (Producer)",
                            ep_i2s_iface, primary_endpoint_cfg);

        ep_wps_iface.action = ep_wps_consume;
        primary_endpoint_cfg.use_encapsulation  = true;
        primary_endpoint_cfg.delayed_action     = false;
        audio_endpoint_init(&wps_consumer, (void *)&wps_consumer_instance, "WPS EP (Consumer)",
                            ep_wps_iface, primary_endpoint_cfg);

        protocol_cfg.cdc_enable = false, /* Consumer is WPS */
        protocol_cfg.do_initial_buffering = true;
        audio_pipeline_init(&primary_audio_pipeline, "Codec -> WPS", &max98091_producer, protocol_cfg, &wps_consumer, audio_err);
        audio_pipeline_add_processing(&primary_audio_pipeline, &compression_processing, audio_err);
    } else {
        /* INIT PRIMARY PIPELINE */
        ep_wps_iface.action = ep_wps_produce;
        primary_endpoint_cfg.use_encapsulation  = true;
        primary_endpoint_cfg.delayed_action     = false;
        audio_endpoint_init(&wps_producer, (void *)&wps_producer_instance, "WPS EP (Producer)",
                            ep_wps_iface, primary_endpoint_cfg);

        primary_endpoint_cfg.use_encapsulation  = false;
        primary_endpoint_cfg.delayed_action     = true;
        audio_endpoint_init(&max98091_consumer, (void *)&max98091_consumer_instance, "MAX98091 EP (Consumer)",
                            ep_i2s_iface, primary_endpoint_cfg);

        protocol_cfg.cdc_enable = true, /* Consumer is CODEC */
        protocol_cfg.do_initial_buffering = false;
        audio_pipeline_init(&primary_audio_pipeline, "WPS -> Codec", &wps_producer, protocol_cfg, &max98091_consumer, audio_err);
        audio_pipeline_add_processing(&primary_audio_pipeline, &decompression_processing, audio_err);
        audio_pipeline_add_processing(&primary_audio_pipeline, &volume_processing, audio_err);
    }
}

static void volume_handling(void)
{
    static bool btn1_active = false;
    static bool btn2_active = false;

    if (btn1_active) {
        if (!bsp_read_btn_state(BTN1)) {
            btn1_active = false;
        }
    }
    if (btn2_active) {
        if (!bsp_read_btn_state(BTN2)) {
            btn2_active = false;
        }
    }
    if (!btn1_active && !btn2_active) {
        if (bsp_read_btn_state(BTN1)) {
            audio_processing_ctrl(&volume_processing, VOLUME_INCREASE, AUDIO_NO_ARG);
            btn1_active = true;
        } else if (bsp_read_btn_state(BTN2)) {
            audio_processing_ctrl(&volume_processing, VOLUME_DECREASE, AUDIO_NO_ARG);
            btn2_active = true;
        }
    }
}

/** @brief SAI DMA TX complete callback.
 *
 *  This feeds the CODEC with audio samples from audio packets
 *  taken from the playback queue.
 */
static void audio_i2s_tx_complete_cb(void)
{
    audio_error_t audio_err;

    if (IS_COORD == false) {
        audio_pipeline_consume(&primary_audio_pipeline, &audio_err);
    }
}

/** @brief SAI DMA RX complete callback.
 *
 *  This receives audio samples from the CODEC and packages them as
 *  audio packets in the record queue.
 */
static void audio_i2s_rx_complete_cb(void)
{
    audio_error_t audio_err;

    if (IS_COORD) {
        audio_pipeline_produce(&primary_audio_pipeline, &audio_err);
    }
}
