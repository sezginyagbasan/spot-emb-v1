################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 
OBJCOPY_BIN := 

# Every subdirectory with source files must be described here
SUBDIRS := \
app \
bsp/evk/GCC \
bsp/evk \
bsp/evk/usb_device/app \
bsp/evk/usb_device/target \
core/audio \
core/audio/endpoint \
core/audio/processing \
core/audio/protocol \
core/ranging \
core/wireless/link \
core/wireless/phy \
core/wireless/protocol_stack \
core/wireless/transceiver \
driver/spark/max98091 \
lib/spark/adpcm \
lib/spark/buffer \
lib/spark/error \
lib/spark/fixed_point \
lib/spark/memory \
lib/spark/queue \
lib/spark/resampling \
lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Src \
lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Src \
lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Src \

