################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/bsp/evk/usb_device/app/usb_device.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/bsp/evk/usb_device/app/usbd_cdc_if.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/bsp/evk/usb_device/app/usbd_desc.c 

OBJS += \
./bsp/evk/usb_device/app/usb_device.o \
./bsp/evk/usb_device/app/usbd_cdc_if.o \
./bsp/evk/usb_device/app/usbd_desc.o 

C_DEPS += \
./bsp/evk/usb_device/app/usb_device.d \
./bsp/evk/usb_device/app/usbd_cdc_if.d \
./bsp/evk/usb_device/app/usbd_desc.d 


# Each subdirectory must supply rules for building sources it contributes
bsp/evk/usb_device/app/usb_device.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/bsp/evk/usb_device/app/usb_device.c bsp/evk/usb_device/app/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
bsp/evk/usb_device/app/usbd_cdc_if.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/bsp/evk/usb_device/app/usbd_cdc_if.c bsp/evk/usb_device/app/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
bsp/evk/usb_device/app/usbd_desc.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/bsp/evk/usb_device/app/usbd_desc.c bsp/evk/usb_device/app/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-bsp-2f-evk-2f-usb_device-2f-app

clean-bsp-2f-evk-2f-usb_device-2f-app:
	-$(RM) ./bsp/evk/usb_device/app/usb_device.d ./bsp/evk/usb_device/app/usb_device.o ./bsp/evk/usb_device/app/usbd_cdc_if.d ./bsp/evk/usb_device/app/usbd_cdc_if.o ./bsp/evk/usb_device/app/usbd_desc.d ./bsp/evk/usb_device/app/usbd_desc.o

.PHONY: clean-bsp-2f-evk-2f-usb_device-2f-app

