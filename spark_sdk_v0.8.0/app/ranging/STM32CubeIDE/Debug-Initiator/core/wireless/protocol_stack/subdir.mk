################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_callback.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_l1.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_l2.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_process.c \
C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_stats.c 

OBJS += \
./core/wireless/protocol_stack/wps.o \
./core/wireless/protocol_stack/wps_callback.o \
./core/wireless/protocol_stack/wps_l1.o \
./core/wireless/protocol_stack/wps_l2.o \
./core/wireless/protocol_stack/wps_process.o \
./core/wireless/protocol_stack/wps_stats.o 

C_DEPS += \
./core/wireless/protocol_stack/wps.d \
./core/wireless/protocol_stack/wps_callback.d \
./core/wireless/protocol_stack/wps_l1.d \
./core/wireless/protocol_stack/wps_l2.d \
./core/wireless/protocol_stack/wps_process.d \
./core/wireless/protocol_stack/wps_stats.d 


# Each subdirectory must supply rules for building sources it contributes
core/wireless/protocol_stack/wps.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps.c core/wireless/protocol_stack/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/wireless/protocol_stack/wps_callback.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_callback.c core/wireless/protocol_stack/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/wireless/protocol_stack/wps_l1.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_l1.c core/wireless/protocol_stack/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/wireless/protocol_stack/wps_l2.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_l2.c core/wireless/protocol_stack/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/wireless/protocol_stack/wps_process.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_process.c core/wireless/protocol_stack/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
core/wireless/protocol_stack/wps_stats.o: C:/Users/admin/STM32CubeIDE/workspace_1.8.0/spot-emb-v1/spark_sdk_v0.8.0/core/wireless/protocol_stack/wps_stats.c core/wireless/protocol_stack/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G473xx -DLOCAL_ADDRESS=0xABCD -DDESTINATION_ADDRESS=0X1337 -c -I../../../../lib/third-party/stmicroelectronics/stm32g4xx_hal_driver/Inc -I../../../../core/wireless/transceiver -I../../../../lib/third-party/stmicroelectronics/cmsis_device_g4/Include -I../../../../bsp/evk/CMSIS/Core/Include -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Core/Inc -I../../../../lib/third-party/stmicroelectronics/stm32_mw_usb_device/Class/CDC/Inc -I../../../../bsp/evk/usb_device/app -I../../../../bsp/evk/usb_device/target -I../../../../core/wireless/phy -I../../../../core/wireless/link -I../../../../lib/spark/error -I../../../../lib/spark/fixed_point -I../../../../lib/spark/queue -I../../../../core/wireless/xlayer -I../../../../core/wireless/protocol_stack -I../../../../bsp/evk -I../../../../core/ranging -O2 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-core-2f-wireless-2f-protocol_stack

clean-core-2f-wireless-2f-protocol_stack:
	-$(RM) ./core/wireless/protocol_stack/wps.d ./core/wireless/protocol_stack/wps.o ./core/wireless/protocol_stack/wps_callback.d ./core/wireless/protocol_stack/wps_callback.o ./core/wireless/protocol_stack/wps_l1.d ./core/wireless/protocol_stack/wps_l1.o ./core/wireless/protocol_stack/wps_l2.d ./core/wireless/protocol_stack/wps_l2.o ./core/wireless/protocol_stack/wps_process.d ./core/wireless/protocol_stack/wps_process.o ./core/wireless/protocol_stack/wps_stats.d ./core/wireless/protocol_stack/wps_stats.o

.PHONY: clean-core-2f-wireless-2f-protocol_stack

