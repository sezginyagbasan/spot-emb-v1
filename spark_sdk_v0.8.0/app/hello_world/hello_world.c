/** @file  hello_world.c
 *  @brief This is a basic example of how to use the SPARK wireless protocol stack.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <stdint.h>
#include "bsp.h"
#include "wps.h"
#include "wps_cfg.h"

/* CONSTANTS ******************************************************************/
#define PRINTF_BUF_SIZE 64

/* MACROS *********************************************************************/
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))

/* PRIVATE GLOBALS ************************************************************/
static wps_t wps = {0};

static uint8_t spi_rx_buffer[SPI_BUFFER_SIZE] = {0};
static uint8_t spi_tx_buffer[SPI_BUFFER_SIZE] = {0};
static uint32_t timeslot_us[] = SCHEDULE_TIMING;

static wps_connection_t coordinator_conn = {0};
static wps_connection_t node_conn        = {0};

static xlayer_t local_fifo[XLAYER_SIZE]                        = {0};
static uint8_t local_frame[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE]   = {0};

static xlayer_t remote_fifo[XLAYER_SIZE]                       = {0};
static uint8_t remote_frame[XLAYER_SIZE][WPS_RADIO_FIFO_SIZE]  = {0};

static int32_t coordinator_timeslot[]  = COORDINATOR_TIMESLOTS;
static int32_t node_timeslot[]         = NODE_TIMESLOTS;

static uint32_t channel_sequence[] = CHANNEL_SEQUENCE;

static timeslot_t timeslot[ARRAY_SIZE(timeslot_us)] = {0};
static wps_callback_inst_t callback[XLAYER_SIZE];

/* PRIVATE FUNCTION PROTOTYPE *************************************************/
static void conn_tx_ack_callback(wps_connection_t *conn);
static void conn_tx_nack_callback(wps_connection_t *conn);
static void conn_rx_callback(wps_connection_t *conn);
static void radio_irq_cb(void);
static void radio_dma_spi_cb(void);
static void radio_callback_cb(void);
static void usb_gpio_callback(void);
static void usb_printf(const char *fmt, ...);
static void app_error_handler(uwb_err err, uint8_t *file, uint16_t line);

/* PUBLIC FUNCTIONS ***********************************************************/
int main(void)
{
    /* Initialize HAL, GPIO, SPI & USB peripheral */
    bsp_init();
    bsp_set_usb_detect_callback(usb_gpio_callback);
    uwb_error_init(app_error_handler);

    /* Handle USB connection on init */
    if (bsp_is_usb_detected()) {
        uwb_err err;
        bsp_cdc_usb_connect(&err);
        bsp_delay_ms(1000);
        ERROR_CHECK(err);
    }

    /* Set radio related callbacks to WPS provided callbacks */
    bsp_set_radio_irq_callback(radio_irq_cb);
    bsp_set_radio_dma_rx_callback(radio_dma_spi_cb);
    bsp_set_pendsv_callback(radio_callback_cb);

    /* Disable radio IRQ when initializing */
    bsp_radio_disable_irq_it();

    wps_critical_cfg_t critical_cfg = {
        bsp_enter_critical,
        bsp_exit_critical
    };
    wps_init_critical_section(&critical_cfg);

    /* Radio instance parameters */
    wps_radio_t radio_instance = {
        /* Radio hardware abstraction layer parameters */
        .radio_hal = (radio_hal_t) {
            .set_shutdown_pin   = bsp_radio_set_shutdown_pin,
            .reset_shutdown_pin = bsp_radio_reset_shutdown_pin,
            .set_reset_pin      = bsp_radio_set_reset_pin,
            .reset_reset_pin    = bsp_radio_reset_reset_pin,
            .read_irq_pin       = bsp_radio_read_irq_pin,
            .set_cs             = bsp_radio_spi_set_cs,
            .reset_cs           = bsp_radio_spi_reset_cs,
            .write              = bsp_radio_spi_write_blocking,
            .read               = bsp_radio_spi_read_blocking,
            .delay_ms           = bsp_delay_ms
        },
        /* Radio advance hardware abstraction layer parameters */
        .radio_hal_adv = (radio_hal_adv_t) {
            .transfer_full_duplex_blocking     = bsp_radio_spi_transfer_full_duplex_blocking,
            .transfer_full_duplex_non_blocking = bsp_radio_spi_transfer_full_duplex_non_blocking,
            .is_spi_busy                       = bsp_radio_is_spi_busy,
            .context_switch                    = bsp_radio_context_switch,
            .disable_radio_irq                 = bsp_radio_disable_irq_it,
            .enable_radio_irq                  = bsp_radio_enable_irq_it,
            .disable_radio_dma_irq             = bsp_radio_disable_irq_it,
            .enable_radio_dma_irq              = bsp_radio_enable_irq_it
        },
        /* SPI parameters */
        .spi_buffer_size = SPI_BUFFER_SIZE,
        .spi_rx_buffer   = spi_rx_buffer,
        .spi_tx_buffer   = spi_tx_buffer,
        .irq_polarity    = IRQ_ACTIVE_HIGH,
    };

    wps_error_t error = WPS_NO_ERROR;

    /* Initialize the radio instance and apply link configuration */
    wps_radio_init(&radio_instance, &error);
    wps_radio_calibrate(&radio_instance, &error);
    ERROR_CHECK(error);

    /* Initialize schedule timing */
    wps_config_network_schedule(&wps, timeslot_us, timeslot, ARRAY_SIZE(timeslot_us));

    /* Initialize the node/xlayer for coordinator/node */
    wps_node_cfg_t node_cfg = {
        .role           = ROLE,
        .preamble_len   = WPS_DEFAULT_PREAMBLE_LEN,
        .sleep_lvl      = WPS_DEFAULT_SLEEP_LEVEL,
        .crc_polynomial = WPS_DEFAULT_CRC,
        .local_address  = LOCAL_ADDRESS,
        .syncword       = sync_word_table[WPS_DEFAULT_SYNC_TABLE_IDX],
    };

    wps_node_t node;

    wps_config_node(&node, &radio_instance, &node_cfg);

    wps_init_callback_queue(&wps, callback, ARRAY_SIZE(callback), bsp_radio_callback_context_switch);

    wps_connection_cfg_t coordinator_conn_cfg = {
        /* Set nodes address */
        .source_address         = WPS_DEFAULT_COORD_ADDR,
        .destination_address    = WPS_DEFAULT_NODE_ADDR,
        .fifo_buffer_size       = XLAYER_SIZE,
        .header_length          = WPS_RADIO_HEADER_SIZE,
        .frame_length           = WPS_DEFAULT_FRAME_SIZE,
        .send_ack_callback_t    = conn_tx_ack_callback,
        .send_nack_callback_t   = conn_tx_nack_callback,
        .receive_callback_t     = conn_rx_callback,
        .get_tick_quarter_ms    = bsp_get_free_running_timer_tick_ms,
    };
    wps_connection_cfg_t node_conn_cfg = {
        /* Set nodes address */
        .source_address         = WPS_DEFAULT_NODE_ADDR,
        .destination_address    = WPS_DEFAULT_COORD_ADDR,
        .fifo_buffer_size       = XLAYER_SIZE,
        .header_length          = WPS_RADIO_HEADER_SIZE,
        .frame_length           = WPS_DEFAULT_FRAME_SIZE,
        .send_ack_callback_t    = conn_tx_ack_callback,
        .send_nack_callback_t   = conn_tx_nack_callback,
        .receive_callback_t     = conn_rx_callback,
        .get_tick_quarter_ms    = bsp_get_free_running_timer_tick_ms,
    };

#ifdef WPS_IS_COORD
    wps_connection_t *local_conn  = &coordinator_conn;
    /* Set local node configuration */
    coordinator_conn_cfg.fifo_buffer   = local_fifo;
    coordinator_conn_cfg.frame_buffer  = &(local_frame[0][0]);
    /* Set remote node configuration */
    node_conn_cfg.fifo_buffer          = remote_fifo;
    node_conn_cfg.frame_buffer         = &(remote_frame[0][0]);
#else
    wps_connection_t *local_conn  = &node_conn;
    /* Set local node configuration */
    node_conn_cfg.fifo_buffer          = local_fifo;
    node_conn_cfg.frame_buffer         = &(local_frame[0][0]);
    /* Set remote node configuration */
    coordinator_conn_cfg.fifo_buffer   = remote_fifo;
    coordinator_conn_cfg.frame_buffer  = &(remote_frame[0][0]);
#endif

    /* Initialize connections between coordinator and node */
    wps_create_connection(&coordinator_conn, &node, &coordinator_conn_cfg);
    wps_create_connection(&node_conn, &node, &node_conn_cfg);

    /* Enable stop and wait */
    wps_connection_enable_stop_and_wait_arq(&coordinator_conn, LOCAL_ADDRESS, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);
    wps_connection_enable_stop_and_wait_arq(&node_conn, LOCAL_ADDRESS, WPS_DEFAULT_RETRY_COUNT, WPS_DEFAULT_DEADLINE);

    /* Enable acknowledge */
    wps_connection_enable_ack(&coordinator_conn);
    wps_connection_enable_ack(&node_conn);

    /* Enable auto synchronization */
    wps_connection_enable_auto_sync(&coordinator_conn);
    wps_connection_enable_auto_sync(&node_conn);

    /* Disable clear channel access */
    wps_connection_disable_cca(&coordinator_conn);
    wps_connection_disable_cca(&node_conn);

     /* Timeslot allocation (Main/Auto-reply connection slot) */
    wps_connection_set_timeslot(&coordinator_conn, &wps, coordinator_timeslot, ARRAY_SIZE(coordinator_timeslot));
    wps_connection_set_timeslot(&node_conn, &wps, node_timeslot, ARRAY_SIZE(node_timeslot));

    /* Configure connection modulation and FEC level */
    wps_connection_config_frame(&coordinator_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);
    wps_connection_config_frame(&node_conn, WPS_DEFAULT_MODULATION, WPS_DEFAULT_FEC);

    /* Initialize SPARK channel and power settings */
    tx_power_settings_t tx_power_settings = {
            .pulse_count = WPS_DEFAULT_PULSE_COUNT,
            .pulse_width = WPS_DEFAULT_PULSE_WIDTH,
            .tx_gain     = WPS_DEFAULT_TX_GAIN,
    };

    channel_cfg_t channel_cfg = {
            .power              = &tx_power_settings,
            .freq_shift_enable  = WPS_DEFAULT_FREQ_SHIFT,
            .pulse_spacing      = WPS_DEFAULT_PULSE_SPACING,
            .pulse_start_pos    = WPS_DEFAULT_PULSE_START_POS,
            .rdn_phase_enable   = WPS_DEFAULT_RND_PHASE,
            .frequency          = WPS_DEFAULT_TS0_FREQ,
            .integgain          = 0,
    };

    /* Use the frequency 6758.4 Mhz for channel 0 */
    channel_cfg.frequency = WPS_DEFAULT_TS0_FREQ;
    wps_connection_config_channel(&coordinator_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);
    wps_connection_config_channel(&node_conn, &node, WPS_DEFAULT_CHAN0, &channel_cfg);

    /* Use the frequency 7577.6 Mhz for channel 1 */
    channel_cfg.frequency = WPS_DEFAULT_TS1_FREQ;
    wps_connection_config_channel(&coordinator_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);
    wps_connection_config_channel(&node_conn, &node, WPS_DEFAULT_CHAN1, &channel_cfg);

    /* Init the WPS */
    wps_init(&wps, &node);

    wps_set_coordinator_address(&wps, WPS_DEFAULT_COORD_ADDR);
    wps_config_network_channel_sequence(&wps, channel_sequence, ARRAY_SIZE(channel_sequence));
    wps_disable_concurrency(&wps);
    wps_disable_fast_sync(&wps);

    /* Init the WPS PHY */
    wps_phy_t wps_phy = {
        .lna_imped = true,
        .manuphase = MANUPHASE_PHASE_TRACKING_ENABLE,
    };
    wps_phy_init(&radio_instance, &wps_phy);
    /* Flush RX/TX FIFO */
    uwb_flush_rx_buffer(&radio_instance.radio);
    uwb_flush_tx_buffer(&radio_instance.radio);

    /* Start the SPARK wireless protocol stack */
    wps_connect(&wps);

    unsigned long i = 0;
    uint8_t *hello_world_buf;
    uint8_t payload_size;

    while (1) {
        bsp_delay_ms(50);
        if (wps_get_fifo_free_space(local_conn) > 0) {
            wps_get_free_slot(local_conn, &hello_world_buf);
            /* Format the new payload */
            payload_size = snprintf((char *)hello_world_buf, WPS_MAX_PAYLOAD_SIZE, "Hello World %lu\n\r", i++);
            /* Send the payload through the WPS */
            wps_send(local_conn, hello_world_buf, payload_size);
        }
    }
}

/* PRIVATE FUNCTIONS **********************************************************/
/** @brief Callback for the coordinator TX acknowledge.
 *
 *  @param[in] conn  Pointer to connection to send.
 */
static void conn_tx_ack_callback(wps_connection_t *conn)
{
    (void)conn;
    bsp_led_toggle(LED0);
}

/** @brief Callback for coordinator TX NACK.
 *
 *  This callback will be called when you expected an
 *  ACK and didn't receive it, or in the case of a connection
 *  without ACK enabled, each time a new frame is sent on
 *  the connection.
 *
 *  @param[in] conn  Pointer to connection to send.
 */
static void conn_tx_nack_callback(wps_connection_t *conn)
{
    (void)conn;
}

/** @brief Callback for coordinator new frame received.
 *
 *  @param[in] conn  Pointer to connection to send.
 */
static void conn_rx_callback(wps_connection_t *conn)
{
    wps_rx_frame frame_out;

    /* Get new payload */
    frame_out = wps_read(conn);
    /* Print payload to USB */
    usb_printf("%s", frame_out.payload);

    /* Notify the WPS that the new payload has been read */
    wps_read_done(conn);
    bsp_led_toggle(LED1);
}

/** @brief Callback for the radio IRQ.
 *
 *  These functions must absolutely be called here if
 *  the user intends to use the Wireless Protocol Stack.
 */
static void radio_irq_cb(void)
{
    wps_radio_irq(&wps);
    wps_process(&wps);
}

/** @brief Callback for the radio DMA SPI.
 *
 *  These functions must absolutely be called here if
 *  the user intends to use the Wireless Protocol Stack.
 */
static void radio_dma_spi_cb(void)
{
    wps_transfer_complete(&wps);
    wps_process(&wps);
}

/** @brief Software interrupt callback to empty the WPS queue.
 */
static void radio_callback_cb(void)
{
    wps_process_callback(&wps);
}

/** @brief USB line detection callback.
 */
static void usb_gpio_callback(void)
{
    uwb_err err;

    if (bsp_is_usb_detected()) {
        bsp_cdc_usb_connect(&err);
    } else {
        bsp_cdc_usb_disconnect(&err);
    }
    ERROR_CHECK(err);
}

/** @brief Print characters through USB.
 *
 *  @param[in] fmt  Pointer to the character to be printed.
 *  @param[in] ...  Variable argument list.
 */
static void usb_printf(const char *fmt, ...)
{
    char std_buf[PRINTF_BUF_SIZE];
    va_list va;

    va_start(va, fmt);

    vsprintf(std_buf, fmt, va);

    /*
     * Does not handle strings that are not \0-terminated; if given one it may
     * perform an over-read (it could cause a crash if unprotected) (CWE-126).
     */
    bsp_usb_cdc_send_buf((uint8_t *)std_buf, strlen(std_buf));

    va_end(va);
}

/** @brief This function is called if an error occurred.
 *
 *  It should be passed as pointer by uwb_error_init for initialization.
 *
 *  @param[in] err   Error code.
 *  @param[in] file  string of the file name where the error occurred.
 *  @param[in] line  Line in the code where the error occurred.
 */
static void app_error_handler(uwb_err err, uint8_t *file, uint16_t line)
{
    (void)err;
    (void)file;
    (void)line;

    while(1)
    {

    }
}
