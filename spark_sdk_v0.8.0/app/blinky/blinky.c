/** @file  blinky.c
 *  @brief Blinky application example using the SR1000 device drivers.
 *
 *  This file describes an example of how to setup and run a blinky application using the SR1000 device drivers.
 *  It will setup the radio to send a packet every 500ms. After the packet is sent,
 *  the board waits for the other board's packet. When the packet is received,
 *  it checks if the packet is good and toggles the on-board LED.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "bsp.h"
#include "link_gain_loop.h"
#include "sr1000_api.h"

/* CONSTANTS ******************************************************************/
#ifndef DESTINATION_ADDRESS
#define DESTINATION_ADDRESS            0x1337
#define LOCAL_ADDRESS                  0xABCD
#endif

#define CCA_DEFAULT_RETRY_TIME         10
#define CCA_ENABLE                     1
#define CCA_INIT_THRESHOLD_UPDATE_TIME 30

/* PRIVATE GLOBALS ************************************************************/
static   radio_t *radio_handle;
static   uint8_t rx_buffer[MAX_FRAMESIZE];
static   bool    blink_received;
volatile bool    flag_irq;

/* PRIVATE FUNCTION PROTOTYPES ************************************************/
static void uwb_init_blink_config(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address);
static void uwb_cca_initialization(radio_t *radio_instance, uint8_t retry_time, uint8_t nb_update, bool cca_enable);
static void receiver_handle(void);
static bool is_transmission_complete(radio_events_t events);
static bool is_frame_good(radio_events_t events);
static bool timeout(uint32_t start, uint32_t timeout_ms);
static void set_flag_irq(void);

/* PUBLIC FUNCTION ************************************************************/
int main(void)
{
    /* Application variables */
    radio_t radio_instance;
    radio_events_t status;
    uint32_t timeout_start;
    uint32_t tick_now;
    uint32_t delta_ms;
    uint32_t blink_ms = 500;
    uint32_t count = 0;

    /* Initialize the hardware abstraction layer structures */
    radio_hal_t radio_hal = {
        bsp_radio_set_shutdown_pin,
        bsp_radio_reset_shutdown_pin,
        bsp_radio_set_reset_pin,
        bsp_radio_reset_reset_pin,
        bsp_radio_read_irq_pin,
        bsp_radio_spi_set_cs,
        bsp_radio_spi_reset_cs,
        bsp_radio_spi_write_blocking,
        bsp_radio_spi_read_blocking,
        bsp_delay_ms
    };

    /* Initialize HAL, GPIO & SPI peripheral */
    bsp_init();
    bsp_set_radio_irq_callback(set_flag_irq);
    bsp_set_board_voltage(VDD_3V3);

    /* Initialize the radio instance and apply link configuration */
    uwb_init_blink_config(&radio_instance, &radio_hal, IRQ_ACTIVE_HIGH, LOCAL_ADDRESS);

    /* Initialize the radio handle that will be used for all further radio operations */
    radio_handle = &radio_instance;

    /* CCA Initialization */
    uwb_cca_initialization(radio_handle, CCA_DEFAULT_RETRY_TIME, CCA_INIT_THRESHOLD_UPDATE_TIME, CCA_ENABLE);

    /* Gain loop variables */
    gain_loop_t gain_loop = {0};
    frame_quality_t frame_quality = {0};
    UNUSED(frame_quality.rnsi);

    /* Blinky application main loop */
    while (true) {

        uwb_sleep(radio_handle);

        /* Transmit "Blink" to [address] */
        uwb_send(radio_handle, DESTINATION_ADDRESS, (uint8_t *)(&count), sizeof(count), NO_DELAY);
        count++;

        /* Wait for transmission complete */
        timeout_start = bsp_get_tick();
        do {
            status = uwb_get_events(radio_handle);
        } while ((is_transmission_complete(status) == false) && (timeout(timeout_start, 1) == false));

        /* Listen & wait for new packet */
        uwb_receiver_on(radio_handle, NO_TIMEOUT);

        /* Wait for packet */
        timeout_start = bsp_get_tick();
        while ((timeout(timeout_start, blink_ms) == false) && (!flag_irq)) {
            /* Wait */
        }

        /* Frame received, toggle LED if frame is good */
        if (flag_irq) {
            flag_irq = false;

            receiver_handle();

            if (blink_received) {
                bsp_led_toggle(LED0);
                blink_received = false;

                /* Update gain loop */
                frame_quality = uwb_get_frame_quality(radio_handle);
                link_gain_loop_update(FRAME_RECEIVED, frame_quality.rssi, &gain_loop);
            } else {
                /* A frame has been received but it is invalid (CRC failure or address mismatch) */

                /* Update gain loop */
                frame_quality = uwb_get_frame_quality(radio_handle);
                link_gain_loop_update(FRAME_REJECTED, frame_quality.rssi, &gain_loop);
            }

            /* Update receiver gain */
            uwb_set_receiver_gain(radio_handle, link_gain_loop_get_gain_value(&gain_loop));

            /* Wait for the rest of the period */
            tick_now = bsp_get_tick();
            delta_ms = blink_ms - (uint32_t)(tick_now - timeout_start);

            if (delta_ms > blink_ms) {
                delta_ms = 0;
            }

            /* Send radio to sleep for the rest of the period */
            uwb_sleep(radio_handle);
            bsp_delay_ms(delta_ms);
        } else {
            /* Update gain loop */
            frame_quality = uwb_get_frame_quality(radio_handle);
            link_gain_loop_update(FRAME_LOST, frame_quality.rssi, &gain_loop);

            /* Update receiver gain */
            uwb_set_receiver_gain(radio_handle, link_gain_loop_get_gain_value(&gain_loop));
        }
    }

    return 0;
}

/* PRIVATE FUNCTION ***********************************************************/
/** @brief Radio initialization & link configuration.
 *
 * SPARK Radio application initialization :
 *  - Initialize the low level part of the radio.
 *  - Calibrate the radio.
 *  - Setup the synchronization word for the link between the radios.
 *  - Configure the CRC polynomial for packet validity.
 *  - Configure the link frame (FEC level, modulation, preamble).
 *  - Configure channel for the application.
 *  - Set local address.
 *  - Configure sleep level.
 *  - Flush RX/TX buffer for a clean start.
 *  - Configure interrupt for external GPIO signal.
 *  - Disable auto-reply (not needed for general application).
 *  - Select configured channel.
 *  - Read event status to reset IRQ pin.
 *
 *  @param[out] radio          Radio's instance.
 *  @param[in]  hal            Radio GPIO, SPI and delay related function structure.
 *  @param[in]  irq_pol        Polarity of the IRQ pin when it is asserted.
 *  @param[in]  local_address  Address of the current board.
 */
static void uwb_init_blink_config(radio_t *radio, radio_hal_t *hal, irq_polarity_t irq_pol, uint16_t local_address)
{
    uwb_error_t  error;

    uwb_init(radio, hal, irq_pol, &error);

    /* Calibrate radio */
    uwb_calibrate(radio);

    /* Configure syncword */
    syncword_cfg_t sync_cfg;

    sync_cfg.syncword = sync_word_table[0];
    sync_cfg.syncword_bit_cost = 2;
    sync_cfg.syncword_tolerance = 0xC;
    sync_cfg.syncword_length = SYNCWORD_LENGTH_32;

    uwb_config_syncword(radio, sync_cfg);

    /* Configure CRC */
    uwb_set_crc(radio, 0xBAAD);

    /* Configure frame */
    frame_cfg_t frame_cfg;

    frame_cfg.fec = FEC_LVL_2;
    frame_cfg.modulation = MODULATION_2BITPPM;
    frame_cfg.preamble_length = 94;

    uwb_config_frame(radio, frame_cfg);

    /* Configure channels */
    uwb_config_channel(radio, CHANNEL_0, MINUS_3_6_DBFS);

    /* Set local address */
    uwb_set_local_address(radio, local_address, ADDRESS_LENGTH_16);

    /* Set sleep level */
    uwb_set_sleep_level(radio, SLEEP_IDLE, SLEEP_NO_EVENT);

    /* Flush RX/TX FIFO */
    uwb_flush_rx_buffer(radio);
    uwb_flush_tx_buffer(radio);

    /* Enable interrupt on address match */
    uwb_enable_irq(radio, ADDR_MATCH_IT);

    /* No auto-reply */
    uwb_disable_auto_reply(radio);

    /* Select channel */
    uwb_select_channel(radio, CHANNEL_0);

    /* Read status to clear IRQ pin */
    uwb_get_events(radio);
}

/** @brief Initialization of the CCA functionality.
 *
 *  Initialize the clear channel assessment functionality for the application.
 *
 *  @param[in] radio_instance  Application radio instance.
 *  @param[in] retry_time      Time between transmission when air is occupied.
 *  @param[in] nb_update       Threshold update time.
 *  @param[in] cca_enable      Whether the CCA functionality is enabled.
 */
static void uwb_cca_initialization(radio_t *radio, uint8_t retry_time, uint8_t nb_update, bool cca_enable)
{
    if (cca_enable) {
        /*
         * A value higher than that will result in an 8-bit register
         * overflow because of the formula used to convert time to register
         * data.
         */
        if (retry_time > CCA_MAX_RETRY_TIME_US) {
            retry_time = CCA_MAX_RETRY_TIME_US;
        }

        /* Initialize algorithm coefficients */
        uwb_cca_init(radio, retry_time);

        /* Update offset for CCA threshold calculation */
        uwb_cca_update_rnsi_offset(radio);

        /* Enable receiver to properly update CCA threshold */
        uwb_receiver_on(radio, NO_TIMEOUT);

        /* Update CCA threshold */
        for (uint8_t i = 0; i < nb_update; i++) {
            uwb_cca_update_threshold(radio);
        }

        /* Enable the CCA for the application */
        uwb_cca_enable(radio);
    }
}

/** @brief Receiver handle to lighten interrupt loop.
 */
static void receiver_handle(void)
{
    radio_events_t events;
    uint8_t size;

    /* Check if handle is valid */
    if (radio_handle) {

        /* Get radio events status */
        events = uwb_get_events(radio_handle);

        /* Check if the frame is meant for us & check that the CRC pass */
        if (is_frame_good(events)) {
            do {
                /* Read payload */
                size = uwb_read(radio_handle, rx_buffer, MAX_FRAMESIZE);

                /* Loop until the RX FIFO is empty */
            } while (uwb_get_rx_payload_size(radio_handle) > 0);

            /* Set flag for application */
            blink_received = true;
        }
    }

    UNUSED(size);
}

/** @brief Function that check if the transmission is complete.
 *
 *  @param[in] events  Current radio events status.
 *  @retval true   Transmission complete.
 *  @retval false  Transmission not complete.
 */
static bool is_transmission_complete(radio_events_t events)
{
    return (events & TX_END_IT);
}

/** @brief Function that check if the received frame is good.
 *
 *  @param[in] events  Current radio events status.
 *  @retval true   Frame is good.
 *  @retval false  Frame is not good.
 */
static bool is_frame_good(radio_events_t events)
{
    return ((events & ADDR_MATCH_IT) && (events & CRC_PASS_IT));
}

/** @brief Function that checks if timeout is expired.
 *
 *  @param[in] start       Tick value at the beginning. Tick is assumed to be 1ms.
 *  @param[in] timeout_ms  Timeout value in milliseconds.
 *  @retval true   Timeout is expired.
 *  @retval false  Timeout is not expired.
 */
static bool timeout(uint32_t start, uint32_t timeout_ms)
{
    if (bsp_get_tick() > (start + timeout_ms)) {
        return true;
    } else {
        return false;
    }
}

/** @brief Radio callback to set the Radio's IRQ flag.
 *
 *  This callback is initialized using the bsp_set_radio_irq_callback function.
 */
static void set_flag_irq(void)
{
    flag_irq = true;
}
