/** @file  wps_cfg.h
 *  @brief Application specific configuration constants for the Wireless Protocol Stack.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef WPS_CFG_H_
#define WPS_CFG_H_

/* CONSTANTS ******************************************************************/
#define SPI_BUFFER_SIZE 200
#define XLAYER_SIZE     2

#define WPS_DEFAULT_PAYLOAD_SIZE WPS_MAX_PAYLOAD_SIZE
#define WPS_DEFAULT_FRAME_SIZE   WPS_MAX_PAYLOAD_SIZE + WPS_RADIO_HEADER_SIZE + 1

#define WPS_DEFAULT_PREAMBLE_LEN   94
#define WPS_DEFAULT_SLEEP_LEVEL    SLEEP_IDLE
#define WPS_DEFAULT_CRC            0xBAAD
#define WPS_DEFAULT_SYNC_TABLE_IDX 11

#define WPS_DEFAULT_COORD_ADDR      0x1337
#define WPS_DEFAULT_NODE_ADDR       0xABCD
#define WPS_DEFAULT_PULSE_COUNT     2
#define WPS_DEFAULT_PULSE_WIDTH     4
#define WPS_DEFAULT_TX_GAIN         0
#define WPS_DEFAULT_PULSE_SPACING   1
#define WPS_DEFAULT_MODULATION      MODULATION_IOOK
#define WPS_DEFAULT_FEC             FEC_LVL_2
#define WPS_DEFAULT_RETRY_COUNT     0
#define WPS_DEFAULT_DEADLINE        0
#define WPS_DEFAULT_FREQ_SHIFT      true
#define WPS_DEFAULT_PULSE_START_POS 2
#define WPS_DEFAULT_RND_PHASE       RND_PHASE_ENABLE
#define WPS_DEFAULT_INTEGGAIN       0

#define WPS_DEFAULT_CHAN_NUM 4
#define WPS_DEFAULT_CHAN0    0
#define WPS_DEFAULT_CHAN1    1
#define WPS_DEFAULT_CHAN2    2
#define WPS_DEFAULT_CHAN3    3
#define WPS_DEFAULT_TS_NUM   4
#define WPS_DEFAULT_TS0_FREQ 165
#define WPS_DEFAULT_TS1_FREQ 172
#define WPS_DEFAULT_TS2_FREQ 179
#define WPS_DEFAULT_TS3_FREQ 186

#define COORDINATOR_TIMESLOTS {                                            \
    MAIN_TIMESLOT(0), MAIN_TIMESLOT(1), MAIN_TIMESLOT(3), MAIN_TIMESLOT(4) \
}

#define NODE_TIMESLOTS {               \
    MAIN_TIMESLOT(2), MAIN_TIMESLOT(5) \
}

#ifndef WPS_IS_COORD
    #define IS_COORD       false
    #define ROLE           NETWORK_NODE
    #define LOCAL_ADDRESS  WPS_DEFAULT_NODE_ADDR
    #define REMOTE_ADDRESS WPS_DEFAULT_COORD_ADDR
    #define TX_TIMESLOTS   NODE_TIMESLOTS
    #define RX_TIMESLOTS   COORDINATOR_TIMESLOTS
#else
    #define IS_COORD       true
    #define ROLE           NETWORK_COORDINATOR
    #define LOCAL_ADDRESS  WPS_DEFAULT_COORD_ADDR
    #define REMOTE_ADDRESS WPS_DEFAULT_NODE_ADDR
    #define TX_TIMESLOTS   COORDINATOR_TIMESLOTS
    #define RX_TIMESLOTS   NODE_TIMESLOTS
#endif

#define SCHEDULE_TIMING {        \
    666, 666, 666, 666, 666, 666 \
}

#define CHANNEL_SEQUENCE { \
    0, 1, 2, 3             \
}


#endif /* WPS_CFG_H_ */
