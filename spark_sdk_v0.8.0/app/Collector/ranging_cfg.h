/** @file  ranging_cfg.h
 *  @brief Ranging configuration flags.
 *
 *  @copyright Copyright (C) 2018 SPARK Microsystems International Inc. All rights reserved.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author SPARK FW Team.
 */
#ifndef RANGING_CFG_H_
#define RANGING_CFG_H_

/* CONSTANTS ******************************************************************/
/* Only 1, 2, 4, and multiples of 8 should be used for RANGING_AVERAGING_SIZE_SAMPLE */
#define RANGING_AVERAGING_SIZE_SAMPLE 24
#define RANGING_FIXED_POINT_PRECISION 16
#define RANGING_SAMPLING_PERIOD_MS    10
#define RANGING_ALGORITHM_TYPE        RANGING_DATA_CORRECTED
#define USER_DEFINED_RANGING_OFFSET   (int32_t)(-1073.35 * RANGING_SCALING)


#endif /* RANGING_CFG_H_ */
