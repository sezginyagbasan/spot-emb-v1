/** @file  uwb_circular_buffer.c
 *  @brief Circular buffer.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <string.h>
#include "uwb_circular_buffer.h"

/* PUBLIC FUNCTIONS ***********************************************************/

/** @brief Initialize circular buffer.
 *
 *  User can provide an empty buf struct to the function. It will be initialized
 *  here.
 *
 *  @param[in] buf      Struct that keep track of the buffer state.
 *  @param[in] buf_ptr  Pointer to start of the buffer.
 *  @param[in] capacity Maximum number of element in the buffer.
 *  @param[in] size     Size of one element in the buffer.
 */
void uwb_circ_buff_init(circ_buffer_t *buf, void *buf_ptr, uint32_t capacity, uint8_t size)
{
    buf->buf_full     = false;
    buf->buf_empty    = true;
    buf->buf_capacity = capacity;
    buf->item_size    = size;
    buf->buffer       = buf_ptr;
    buf->num_data     = 0;
    buf->free_space   = capacity;
    buf->in_idx       = buf->buffer;
    buf->out_idx      = buf->buffer;
    buf->buffer_end   = (char*)buf->buffer + capacity * size;
}

/** @brief Push data to the circular buffer.
 *
 *  If the buffer is full, new data is discarded and an error is returned.
 *
 *  @param[in]  buf  Struct that keep track of the buffer state.
 *  @param[in]  data Pointer to the data to push.
 *  @param[in]  size Number of element to push
 *  @param[out] err  Pointer that receive an error code.
 *      @lo UWB_ERR_CIRC_BUF_NONE,
 *      @lo UWB_ERR_CIRC_BUF_FULL,
 */
void uwb_circ_buff_in(circ_buffer_t *buf, void *data, uint8_t size, uwb_err *err)
{
    uint16_t copied_len = 0;
    uint16_t cpy_size   = (buf->item_size * size);

    *err = UWB_ERR_CIRC_BUF_NONE;

    if (size > buf->free_space) {
        *err = UWB_ERR_CIRC_BUF_FULL;
        return;
    }

    buf->buf_empty = false;

    if ((buf->buffer_end - buf->in_idx) < cpy_size) {
        uint8_t remaining_size = (buf->buffer_end - buf->in_idx)/buf->item_size;
        copied_len = (buf->buffer_end - buf->in_idx);
        cpy_size -= (buf->buffer_end - buf->in_idx);
        size -= remaining_size;
        uwb_circ_buff_in(buf, data, remaining_size, err);
        if (*err != UWB_ERR_CIRC_BUF_NONE) {
            return;
        }
    }

    memcpy(buf->in_idx, data + copied_len, cpy_size);
    buf->in_idx = (char*)buf->in_idx + cpy_size;
    if (buf->in_idx == buf->buffer_end) {
        buf->in_idx = buf->buffer;
    }
    if (buf->buf_full) {
        buf->out_idx = buf->in_idx;
    } else {
        buf->free_space -= size;
        buf->num_data += size;
    }

    if (buf->in_idx == buf->out_idx) {
        buf->buf_full = true;
    }
}

/** @brief Pull data from the circular buffer.
 *
 *  @param[in]  buf  Struct that keep track of the buffer state.
 *  @param[out] data Pointer to write the pulled data.
 *  @param[in]  size Number of element to pull
 *  @param[out] err  Pointer that receive an error code.
 *      @lo UWB_ERR_CIRC_BUF_NONE,
 *      @lo UWB_ERR_CIRC_BUF_EMPTY,
 */
void uwb_circ_buff_out(circ_buffer_t *buf, void *data, uint8_t size, uwb_err *err)
{
    uint16_t copied_len = 0;
    uint16_t cpy_size   = (buf->item_size * size);

    *err = UWB_ERR_CIRC_BUF_NONE;

    if (buf->buf_empty) {
        *err = UWB_ERR_CIRC_BUF_EMPTY;
        return;
    }

    buf->buf_full = false;

    if ((buf->buffer_end - buf->out_idx) < cpy_size) {
        uint8_t remaining_size = (buf->buffer_end - buf->out_idx)/buf->item_size;
        copied_len = (buf->buffer_end - buf->out_idx);
        cpy_size -= (buf->buffer_end - buf->out_idx);
        size -= remaining_size;
        uwb_circ_buff_out(buf, data, remaining_size, err);
        if (*err != UWB_ERR_CIRC_BUF_NONE) {
            return;
        }
    }

    memcpy(data + copied_len, buf->out_idx, cpy_size);
    buf->out_idx = (char*)buf->out_idx + cpy_size;
    if (buf->out_idx == buf->buffer_end)
        buf->out_idx = buf->buffer;

    buf->num_data -= size;
    buf->free_space += size;

    if (buf->out_idx == buf->in_idx) {
        buf->buf_empty = true;
    }
}

/** @brief Return true or false if the buffer is empty or not.
 *
 *  @param[in]  buf  Struct that keep track of the buffer state.
 *  @return True if the buffer is empty. False otherwise.
 */
bool uwb_circ_buff_is_empty(circ_buffer_t *buf)
{
    return buf->buf_empty;
}

/** @brief Return true or false if the buffer is full or not.
 *
 *  @param[in]  buf  Struct that keep track of the buffer state.
 *  @return True if the buffer is full. False otherwise.
 */
bool uwb_buff_is_full(circ_buffer_t *buf)
{
    return buf->buf_full;
}

/** @brief Return the number of elements in the buffer
 *
 *  @param[in]  buf  Struct that keep track of the buffer state.
 *  @return Number of elements.
 */
bool uwb_buff_num_elements(circ_buffer_t *buf)
{
    return buf->num_data;
}

/** @brief Return the number of element that can be added to the buffer
 *         before it is full.
 *
 *  @param[in]  buf  Struct that keep track of the buffer state.
 *  @return Number of free elements.
 */
bool uwb_buff_free_space(circ_buffer_t *buf)
{
    return buf->free_space;
}
