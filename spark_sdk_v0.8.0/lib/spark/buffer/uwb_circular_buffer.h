/** @file  sr1000_circular_buffer.h
 *  @brief Circular buffer.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef UWB_CIRCULAR_BUFFER_H_
#define UWB_CIRCULAR_BUFFER_H_

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "uwb_error.h"

/* TYPES **********************************************************************/
#ifndef ERR_BASE_CIRC_BUF
#error Set the base error index in your project symbols.
#endif

#ifndef UWB_ERR_TYPE
#define UWB_ERR_TYPE
typedef uint32_t uwb_err;
#endif

#define UWB_ERR_CIRC_BUF_NONE  0x0000u
#define UWB_ERR_CIRC_BUF_EMPTY ((ERR_BASE_CIRC_BUF << ERR_BASE_POS) + 0x0001u)
#define UWB_ERR_CIRC_BUF_FULL  ((ERR_BASE_CIRC_BUF << ERR_BASE_POS) + 0x0002u)

typedef struct {
    void    *in_idx;
    void    *out_idx;
    bool     buf_full;
    bool     buf_empty;
    uint32_t buf_capacity;
    uint8_t  item_size;
    void    *buffer;
    void    *buffer_end;
    uint32_t num_data;
    uint32_t free_space;
} circ_buffer_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
void uwb_circ_buff_init(circ_buffer_t *buf, void *buf_ptr, uint32_t capacity, uint8_t size);
void uwb_circ_buff_in(circ_buffer_t *buf, void *data, uint8_t size, uwb_err *err);
void uwb_circ_buff_out(circ_buffer_t *buf, void *data, uint8_t size, uwb_err *err);
bool uwb_circ_buff_is_empty(circ_buffer_t *buf);
bool uwb_buff_is_full(circ_buffer_t *buf);
bool uwb_buff_num_elements(circ_buffer_t *buf);
bool uwb_buff_free_space(circ_buffer_t *buf);

#ifdef __cplusplus
}
#endif
#endif /* UWB_CIRCULAR_BUFFER_H_ */
