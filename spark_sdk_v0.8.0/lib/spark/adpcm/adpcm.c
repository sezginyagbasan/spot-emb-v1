/** @file  adpcm.c
 *  @brief Definitions for ADPCM compression related function.
 *
 *  Implementation of the ADPCM encoder and decoder. Based on example from
 *  STMicroelectronics ; STSW-STM32022 and from Microchip's AN643.
 *
 *  This implementation used a state type to hold the encoder and
 *  decoder state information, thus allowing multiple instances of
 *  each to coexist.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include "adpcm.h"

/* CONSTANTS ******************************************************************/
/* Quantizer step size lookup table */
const uint16_t adpcm_step_size_table[89] = {
                                    7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
                                    19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
                                    50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
                                    130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
                                    337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
                                    876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
                                    2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
                                    5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
                                    15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767};
/* Table of index changes */
const int8_t adpcm_index_table[16] = {-1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8};

/* PUBLIC FUNCTIONS ***********************************************************/
void adpcm_init_state(adpcm_state_t *state)
{
    state->state.index      = 0;
    state->state.predsample = 0;
}

uint8_t adpcm_encode(int32_t sample, adpcm_state_t *state)
{
    int32_t  diff    = 0;
    int32_t  diffq   = 0;
    uint16_t step    = 0;
    uint16_t tmpstep = 0;
    uint8_t  code    = 0;

    int32_t predsample = (int32_t)state->state.predsample;
    int16_t index      = (int16_t)state->state.index;

    step = adpcm_step_size_table[index];

    /* Compute diff and record sign and absolute value. */
    diff = sample-predsample;
    if (diff < 0) {
        code = 8;
        diff = -diff;
    }

    /* Quantize the diff into ADPCM code and inverse quantize the code into a predicted diff. */
    tmpstep = step;
    diffq   = (step >> 3);

    if (diff >= tmpstep) {
        code  |= 0x04;
        diff  -= tmpstep;
        diffq += step;
    }

    tmpstep = tmpstep >> 1;

    if (diff >= tmpstep) {
        code  |= 0x02;
        diff  -= tmpstep;
        diffq += (step >> 1);
    }

    tmpstep = tmpstep >> 1;

    if (diff >= tmpstep) {
        code  |= 0x01;
        diffq += (step >> 2);
    }

    /* Fixed predictor to get new predicted sample. */
    if (code & 8) {
        predsample -= diffq;
    } else {
        predsample += diffq;
    }

    /* Check for overflow and underflow. Clipping if needed. */
    if (predsample > 32767) {
        predsample = 32767;
    } else if (predsample < -32768) {
        predsample = -32768;
    }

    /* Find new stepsize index. */
    index += adpcm_index_table[code];
    /* Check for overflow and underflow. Clipping if needed. */
    if (index < 0) {
        index = 0;
    } else if (index > 88) {
        index = 88;
    }

    /* Save state. */
    state->state.index      = (int8_t)index;
    state->state.predsample = (int16_t)predsample;

    /* Return compressed sample. */
    return (code & 0x0f);
}

int16_t adpcm_decode(uint8_t code, adpcm_state_t *state)
{
    int32_t  diffq = 0;
    uint16_t step  = 0;

    int32_t predsample = (int32_t)state->state.predsample;
    int16_t index      = (int16_t)state->state.index;

    step = adpcm_step_size_table[index];

    /* Inverse code into diff. */
    diffq = step >> 3;
    if (code & 4) {
        diffq += step;
    }

    if (code & 2) {
        diffq += step >> 1;
    }

    if (code & 1) {
        diffq += step >> 2;
    }

    /* Add diff to predicted sample. */
    if (code & 8) {
        predsample -= diffq;
    } else {
        predsample += diffq;
    }

    /* Check for overflow and underflow. Clipping if needed. */
    if (predsample > 32767) {
        predsample = 32767;
    } else if (predsample < -32768) {
        predsample = -32768;
    }

    /* Find new quantizer step size. */
    index += adpcm_index_table[code];
    /* Check for overflow and underflow. Clipping if needed. */
    if (index < 0) {
        index = 0;
    }
    if (index > 88) {
        index = 88;
    }

    /* Save state. */
    state->state.index      = (int8_t)index;
    state->state.predsample = (int16_t)predsample;

    /* Return PCM sample. */
    return (int16_t)predsample;
}
