/** @file  adpcm.h
 *  @brief Types definitions and functions prototypes for ADPCM compression.
 *
 *  Implementation of the ADPCM encoder and decoder. Based on example from
 *  STMicroelectronics ; STSW-STM32022 and from Microchip's AN643.
 *
 *  This implementation used a state type to hold the encoder and
 *  decoder state information, thus allowing multiple instances of
 *  each to coexist.
 *
 *  @copyright Copyright (C) 2021 SPARK Microsystems International Inc.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef ADPCM_H_
#define ADPCM_H_

#ifdef __cplusplus
extern "C" {
#endif

/* INCLUDES *******************************************************************/
#include <stdint.h>

/* TYPES **********************************************************************/
typedef struct state_variable {
        int16_t predsample;
        int8_t index;
} __attribute__((packed)) state_variable_t;

typedef union adpcm_state {
	state_variable_t state;
    int8_t byte_array[sizeof(state_variable_t)];
} __attribute__((packed)) adpcm_state_t;

/* PUBLIC FUNCTION PROTOTYPES *************************************************/
/** @brief Initialize ADPCM state.
 *
 *  @param[out]  state ADPCM state.
 */
void adpcm_init_state(adpcm_state_t *state);

/** @brief Encode 16-bit PCM sample using ADPCM compression.
 *
 *  @param[in]  sample  16-bit PCM sample.
 *  @param[out] state   Internal ADPCM encoder state.
 *  @retval 4-bit ADPCM sample.
 */
uint8_t adpcm_encode(int32_t sample, adpcm_state_t *state);

/** @brief Decode 4-bit ADPCM sample into 16-bit PCM sample.
 *
 *  @param[in]  code   4-bit ADPCM sample.
 *  @param[out] state  Internal ADPCM decoder state.
 *  @retval 16-bit PCM sample.
 */
int16_t adpcm_decode(uint8_t code, adpcm_state_t *state);

#ifdef __cplusplus
}
#endif


#endif /* ADPCM_H_ */
