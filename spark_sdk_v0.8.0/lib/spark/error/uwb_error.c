/** @file  uwb_error.c
 *  @brief Error system for the SDK.
 *
 *  @copyright Copyright (C) 2020 SPARK Microsystems International Inc.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */

/* INCLUDES *******************************************************************/
#include <stdio.h>
#include "uwb_error.h"

/* PRIVATE GLOBALS ************************************************************/
void (*error_handler)(uwb_err err, uint8_t *file, uint16_t line) = NULL;

/* PUBLIC FUNCTIONS ***********************************************************/

/** @brief Initialization of the error module
 *
 *  @param[in] Function pointer to the application error handler
 */
void uwb_error_init (void (*app_error_handler)(uwb_err err, uint8_t *file, uint16_t line))
{
    error_handler = app_error_handler;
}

/** @brief This function call the error_handler if an error occur.
 *
 *  This function should be called trough the macro ERROR_CHECK(err)
 *
 *  @param[in] err Error code
 *  @param[in] file  string of the file name where the error occurred.
 *  @param[in] line  Line in the code where the error occured.
 */
void uwb_error_check (uwb_err err, uint8_t *file, uint16_t line) {
    if ((err & ERR_BASE_MASK) != UWB_ERR_NONE) {
        error_handler(err, file, line);
    }
}
