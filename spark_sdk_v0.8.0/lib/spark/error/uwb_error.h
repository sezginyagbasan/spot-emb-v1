/** @file  uwb_error.h
 *  @brief Error system for the SDK.
 *
 *  @copyright Copyright (C) 2020-2021 SPARK Microsystems International Inc.
 *  @license   This source code is proprietary and subject to the SPARK Microsystems
 *             Software EULA found in this package in file EULA.txt.
 *  @author    SPARK FW Team.
 */
#ifndef UWB_ERROR_H_
#define UWB_ERROR_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* CONSTANTS ******************************************************************/
#define UWB_ERR_NONE      0x00000u
#define ERR_BASE_MASK     0x0FFFFu

#ifndef ERR_BASE_POS
#define ERR_BASE_POS         0x10u
#endif

#define ERR_BASE_APP           1
#define ERR_BASE_CIRC_BUF      2
#define ERR_BASE_LOG           3
#define ERR_BASE_ACCESS_ADV    4
#define ERR_BASE_API_ADV       5
#define ERR_BASE_LAUNCHER      6
#define ERR_BASE_SPI_PIPE      7
#define ERR_BASE_M2M           8
#define ERR_BASE_LINK_PROTOCOL 9

/* MACROS *********************************************************************/
#define ERROR_CHECK(ERR_CODE)                                       \
    do                                                              \
    {                                                               \
        uwb_error_check((ERR_CODE), (uint8_t*) __FILE__, __LINE__); \
    } while (0)

/* TYPES **********************************************************************/
#ifndef UWB_ERR_TYPE
#define UWB_ERR_TYPE
typedef uint32_t uwb_err;
#endif

/* PUBLIC FUNCTIONS ***********************************************************/
void uwb_error_init (void (*app_error_handler)(uwb_err err, uint8_t *file, uint16_t line));
void uwb_error_check (uwb_err err, uint8_t *file, uint16_t line);

#ifdef __cplusplus
}
#endif

#endif /* UWB_ERROR_H_ */
